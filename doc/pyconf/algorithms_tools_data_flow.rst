Algorithms, tools, and data flow
================================

Members of application
----------------------
.. automodule:: PyConf.application
   :members:
   :exclude-members: ApplicationOptions

   .. we explicitly specify the members to not pull in __slots__ see https://github.com/sphinx-doc/sphinx/pull/8546

   .. autoclass:: PyConf.application.ApplicationOptions
      :members: lockOption, finalize, set_conds_from_testfiledb, set_input_from_testfiledb, set_input_and_conds_from_testfiledb, applyConf, getProperties


