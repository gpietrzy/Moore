Application
===========

The ``Moore.options`` object holds an instance of `PyConf.application.ApplicationOptions`.

The other members of the ``Moore`` module are used for high-level application
configuration. Most 'main' options files will call `Moore.run_moore`.

.. automodule:: Moore

  .. autofunction:: Moore.run_moore
  .. autofunction:: Moore.moore_control_flow
