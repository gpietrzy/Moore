###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD
from RecoConf.standalone import standalone_hlt2_light_reco_without_UT
from RecoConf.hlt1_tracking import (make_reco_pvs, make_PatPV3DFuture_pvs,
                                    make_VeloClusterTrackingSIMD,
                                    make_RetinaClusters)
from RecoConf.hlt2_tracking import (
    make_PrKalmanFilter_noUT_tracks, make_PrKalmanFilter_Seed_tracks,
    make_PrKalmanFilter_Velo_tracks, make_TrackBestTrackCreator_tracks,
    get_UpgradeGhostId_tool_no_UT)

with make_VeloClusterTrackingSIMD.bind(algorithm=VeloRetinaClusterTrackingSIMD, SkipForward=4),\
     make_PatPV3DFuture_pvs.bind(velo_open=True),\
     make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
     make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=4.2),\
     make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=4.2),\
     make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=4.2),\
     make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=4.2),\
        get_UpgradeGhostId_tool_no_UT.bind(velo_hits=make_RetinaClusters):
    run_reconstruction(options, standalone_hlt2_light_reco_without_UT)
