###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt1_reco
from RecoConf.decoders import default_ft_decoding_version
from RecoConf.hlt1_tracking import make_VeloClusterTrackingSIMD, make_velo_full_clusters
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD, VPRetinaFullClusterDecoder

# The suffix is needed to remove a race condition between tests
suffix = '_v6' if default_ft_decoding_version() == 6 else ''
options.histo_file = "MCMatching_decode_retina_MiniBias{}.root".format(suffix)

with standalone_hlt1_reco.bind(do_mc_checking=True),\
     make_VeloClusterTrackingSIMD.bind(algorithm=VeloRetinaClusterTrackingSIMD),\
     make_velo_full_clusters.bind(make_full_cluster=VPRetinaFullClusterDecoder),\
     default_ft_decoding_version.bind(value=6):
    run_reconstruction(options, standalone_hlt1_reco)

options.histo_file = "MCMatching_decode_retina_MiniBias.root"
