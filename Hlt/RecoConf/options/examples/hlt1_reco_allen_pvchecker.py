###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_allen_reconstruction
from Moore.config import Reconstruction
from RecoConf.event_filters import require_gec
from RecoConf.mc_checking import get_pv_checkers
from Configurables import ApplicationMgr
from Configurables import NTupleSvc
from RecoConf.hlt1_allen import make_allen_tracks, make_allen_pvs


def hlt1_reco_pvchecker():

    allen_tracks = make_allen_tracks()
    pvs = make_allen_pvs()

    data = [pvs]
    data += get_pv_checkers(pvs, allen_tracks["Velo"], produce_ntuple=True)

    return Reconstruction('PVperformance', data, [require_gec()])


run_allen_reconstruction(options, hlt1_reco_pvchecker)

NTupleSvc().Output += [
    "FILE1 DATAFILE='Hlt1_PVperformance_Allen.root' TYPE='ROOT' OPT='NEW'"
]
ApplicationMgr().ExtSvc += [NTupleSvc()]
ApplicationMgr().HistogramPersistency = "ROOT"
