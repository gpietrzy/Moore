###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from Moore.config import Reconstruction

from PyConf.application import make_odin

from RecoConf.hlt1_tracking import make_FTRawBankDecoder_clusters

from RecoConf.hlt2_tracking import (make_hlt2_tracks_without_UT,
                                    get_UpgradeGhostId_tool_no_UT)
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD
#from RecoConf.hlt1_tracking import make_VeloClusterTrackingSIMD, make_RetinaDecoder_raw_event
from RecoConf.hlt1_tracking import make_VeloClusterTrackingSIMD, make_RetinaClusters
from RecoConf.hlt1_muonid import make_muon_hits

from PyConf.Algorithms import (
    FTHitEfficiencyMonitor,
    PrStoreSciFiHits,
    PrKalmanFilter_noUT,
    PrKalmanFilter_Velo,
    PrKalmanFilter_Seed,
)
# save output file with histograms
options.histo_file = (options.getProp("histo_file")
                      or 'bias_hlt2_FT_hit_efficiency.root')


def hit_efficiency_sequence():
    odin = make_odin()
    data = [odin]
    with PrKalmanFilter_noUT.bind(FillFitResult=True), \
        PrKalmanFilter_Seed.bind(FillFitResult=True), \
        PrKalmanFilter_Velo.bind(FillFitResult=True):
        hlt2_tracks = make_hlt2_tracks_without_UT(
            light_reco=True, fast_reco=False, use_pr_kf=True)

        all_FT_pr_hits = PrStoreSciFiHits(
            HitsLocation=make_FTRawBankDecoder_clusters()).Output

        for layer_under_study in range(12):  # histograms for the first station
            my_ft_efficiency_alg = FTHitEfficiencyMonitor(
                name="FTHitEfficiencyLayer{}".format(layer_under_study),
                TrackLocation=hlt2_tracks["BestLong"]["v1"],
                ExpertMode=True,
                PrFTHitsLocation=all_FT_pr_hits,
                LayerUnderStudy=layer_under_study,
                MaxDoca=2.0,
                MinTrackP=3000,
                MinTrackPT=300)

            data += [my_ft_efficiency_alg]

    return Reconstruction('hlt2_hit_eff_reco', data, [])


with make_VeloClusterTrackingSIMD.bind(algorithm=VeloRetinaClusterTrackingSIMD),\
get_UpgradeGhostId_tool_no_UT.bind(velo_hits=make_RetinaClusters),\
make_muon_hits.bind(geometry_version=3):
    run_reconstruction(options, hit_efficiency_sequence)
