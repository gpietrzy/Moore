###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from PyConf.Algorithms import bankKiller, Gaudi__Hive__FetchLeavesFromFile, BackwardsCompatibleMergeViewIntoRawEvent, VPRetinaClusterCreator, VPRetinaSPmixer
from PyConf.application import default_raw_banks, root_copy_input_writer
from PyConf.components import force_location


def combiner_digi():

    # mix VELO SPs
    spmix = VPRetinaSPmixer(RawBanks=default_raw_banks("VP"))
    # create VELO retina clusters
    vpclus = VPRetinaClusterCreator(RawBanks=spmix.MixedRawBanks)
    # remove VELO SP bank
    spkiller = bankKiller(BankTypes=["VP"])
    # put mixed SPs and clusters together with rest of event
    raw_event_combiner = BackwardsCompatibleMergeViewIntoRawEvent(
        RawBankViews=[spmix.MixedRawBanks, vpclus.RetinaRawBanks],
        RawEvent=force_location('/Event/DAQ/RawEvent'))

    input_leaves = Gaudi__Hive__FetchLeavesFromFile()
    # spkiller can not opt-in to automated dataflow (as it modifies the TES contents)
    # so need to set up a control flow node instead to explicitly decide when to run it...
    data = [
        spkiller, raw_event_combiner,
        root_copy_input_writer(options.output_file, input_leaves,
                               ['/Event/DAQ/RawEvent'])
    ]

    return Reconstruction('write_digi', data)


run_reconstruction(options, combiner_digi)
