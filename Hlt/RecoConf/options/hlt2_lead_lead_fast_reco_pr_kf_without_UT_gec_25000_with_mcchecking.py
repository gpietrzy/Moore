###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options, run_reconstruction
from RecoConf.hlt1_tracking import (make_VeloClusterTrackingSIMD,
                                    make_reco_pvs, make_PatPV3DFuture_pvs)
from RecoConf.hlt2_tracking import make_TrackBestTrackCreator_tracks, get_UpgradeGhostId_tool_no_UT

from RecoConf.event_filters import require_gec
from RecoConf.mc_checking import check_track_resolution
from RecoConf.hlt1_muonid import make_muon_hits

from PyConf.Algorithms import PrHybridSeeding
from PyConf.packing import persistreco_writing_version

options.histo_file = 'hlt2_lead_lead_track_monitoring_with_mc_histos.root'
options.ntuple_file = 'hlt2_lead_lead_track_monitoring_with_mc_ntuples.root'

from RecoConf.standalone import standalone_hlt2_light_reco_without_UT


with standalone_hlt2_light_reco_without_UT.bind(do_mc_checking=True, do_data_monitoring=False, use_pr_kf=True, fast_reco=True),\
    require_gec.bind(cut=25000,skipUT=True), \
    PrHybridSeeding.bind(RemoveBeamHole=True, RemoveClones_forLead=True), \
    make_VeloClusterTrackingSIMD.bind(SkipForward=4),\
    make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
    check_track_resolution.bind(per_hit_resolutions=True, split_per_type=True), \
    make_TrackBestTrackCreator_tracks.bind(max_ghost_prob=0.8),\
    get_UpgradeGhostId_tool_no_UT.bind(for_PbPb=True),\
    make_muon_hits.bind(geometry_version=3),\
    persistreco_writing_version.bind(version=1.1):
    config = run_reconstruction(options, standalone_hlt2_light_reco_without_UT)
