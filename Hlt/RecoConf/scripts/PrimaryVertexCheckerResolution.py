###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#!/usr/bin/python

# The script for plotting PV efficinecy as the function
# of various distributions: nTracks, z, r.
# As input the NTuple created by hlt1_reco_pvchecker.py
# is needed.
#
# The efficency is calculated usig TGraphAsymmErrors
# and Bayesian error bars
#
# author: Agnieszka Dziurda (agnieszka.dziurda@cern.ch)
# date:   02/2020
#
# Example of usage:
# ../../../run python PrimaryVertexCheckerResolution.py
# --file file1.root file2.root --label name1 name2
#

import os, sys
import argparse

from ROOT import gROOT, TLegend

parser = argparse.ArgumentParser()
parser.add_argument(
    '--file', dest='fileName', default="", nargs='+', help='filename to plot')
parser.add_argument(
    '--label', dest='label', default="", nargs='+', help='labels for files')
parser.add_argument(
    '--tree',
    dest='treeName',
    default="",
    nargs='+',
    help='tree name to plot',
)
parser.add_argument(
    '--smog',
    dest='smog',
    default=False,
    action='store_true',
    help='set true for SMOG')
parser.add_argument(
    '--multi',
    dest='multi',
    default=False,
    action='store_true',
    help='add multiplicity plots')
parser.add_argument(
    '--isol',
    dest='isol',
    default=False,
    action='store_true',
    help='add isolated/closed plots')
parser.add_argument(
    '--dist',
    dest='dist',
    default=False,
    action='store_true',
    help='plot distributions in the canvas')
parser.add_argument(
    '--prefix',
    dest='prefix',
    default="pv_resol",
    help='prefix for the plot name',
)
parser.add_argument(
    '--dir',
    dest='directory',
    default=os.getcwd(),
    help='tree name to plot',
)

parser.add_argument(
    '--offset',
    dest='offset',
    default=0,
    help='offset for plot colors',
)


def get_labels(number_of_files):
    label = []
    for i in range(0, number_of_files):
        label.append("PVChecker{number}".format(number=str(i + 1)))
    return label


if __name__ == '__main__':
    args = parser.parse_args()
    path = args.directory + "/utils/"
    sys.path.append(os.path.abspath(path))
    offset = int(args.offset)

    gROOT.SetBatch()

    from pvutils import get_files, get_trees
    from pvutils import get_global, plot_comparison
    from pvutils import get_dependence
    from pvutils import plot

    from pvconfig import get_variable_ranges
    from pvconfig import set_legend, get_style, get_categories

    ranges = get_variable_ranges(args.smog)
    style = get_style()

    cat = get_categories(args.multi, args.isol, args.smog)
    label = args.label
    if args.label == "":
        label = get_labels(len(args.fileName))

    tr = {}
    tf = {}
    tf = get_files(tf, label, args.fileName)
    tr = get_trees(tf, tr, label, args.treeName, True)

    hist_x = {}
    hist_y = {}
    hist_z = {}
    norm = True

    hist_x = get_global(hist_x, tr, "dx", "#Delta x [mm]",
                        "Candidates Normalized", style, ranges, cat, label,
                        offset)
    hist_y = get_global(hist_y, tr, "dy", "#Delta y [mm]",
                        "Candidates Normalized", style, ranges, cat, label,
                        offset)
    hist_z = get_global(hist_z, tr, "dz", "#Delta z [mm]",
                        "Candidates Normalized", style, ranges, cat, label,
                        offset)

    plot_comparison(hist_x, args.prefix, "dx", cat, label, style, norm, offset)
    plot_comparison(hist_y, args.prefix, "dy", cat, label, style, norm, offset)
    plot_comparison(hist_z, args.prefix, "dz", cat, label, style, norm, offset)

    from ROOT import gEnv
    gEnv.SetValue("Hist.Binning.1D.x", "100")

    graph = {}
    graph["tracks"] = {}
    graph["tracks"]["dx"] = {}
    graph["tracks"]["dy"] = {}
    graph["tracks"]["dz"] = {}
    graph["tracks"]["dx"] = get_dependence(graph["tracks"]["dx"], tr, "dx",
                                           "nrectrmc", ranges, style, cat,
                                           label, offset)
    graph["tracks"]["dy"] = get_dependence(graph["tracks"]["dy"], tr, "dy",
                                           "nrectrmc", ranges, style, cat,
                                           label, offset)
    graph["tracks"]["dz"] = get_dependence(graph["tracks"]["dz"], tr, "dz",
                                           "nrectrmc", ranges, style, cat,
                                           label, offset)

    legend = TLegend(0.15, 0.86, 0.88, 0.98)
    legend = set_legend(legend, label, graph["tracks"]["dz"], "sigma")

    lhcbtextpos = (0.9, 0.75)
    plot(
        graph["tracks"]["dx"],
        "mean",
        args.prefix + "_ntracks_mean",
        "dx",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextpos)
    plot(
        graph["tracks"]["dx"],
        "sigma",
        args.prefix + "_ntracks_sigma",
        "dx",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextpos)

    plot(
        graph["tracks"]["dy"],
        "mean",
        args.prefix + "_ntracks_mean",
        "dy",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextpos)
    plot(
        graph["tracks"]["dy"],
        "sigma",
        args.prefix + "_ntracks_sigma",
        "dy",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextpos)

    plot(
        graph["tracks"]["dz"],
        "mean",
        args.prefix + "_ntracks_mean",
        "dz",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextpos)
    plot(
        graph["tracks"]["dz"],
        "sigma",
        args.prefix + "_ntracks_sigma",
        "dz",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextpos)

    graph["z"] = {}
    graph["z"]["dx"] = {}
    graph["z"]["dy"] = {}
    graph["z"]["dz"] = {}
    graph["z"]["dx"] = get_dependence(graph["z"]["dx"], tr, "dx", "zMC",
                                      ranges, style, cat, label, offset)
    graph["z"]["dy"] = get_dependence(graph["z"]["dy"], tr, "dy", "zMC",
                                      ranges, style, cat, label, offset)
    graph["z"]["dz"] = get_dependence(graph["z"]["dz"], tr, "dz", "zMC",
                                      ranges, style, cat, label, offset)

    plot(
        graph["z"]["dx"],
        "mean",
        args.prefix + "_z_mean",
        "dx",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextpos)
    plot(
        graph["z"]["dx"],
        "sigma",
        args.prefix + "_z_sigma",
        "dx",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextpos)

    plot(
        graph["z"]["dy"],
        "mean",
        args.prefix + "_z_mean",
        "dy",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextpos)
    plot(
        graph["z"]["dy"],
        "sigma",
        args.prefix + "_z_sigma",
        "dy",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextpos)

    plot(
        graph["z"]["dz"],
        "mean",
        args.prefix + "_z_mean",
        "dz",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextpos)
    plot(
        graph["z"]["dz"],
        "sigma",
        args.prefix + "_z_sigma",
        "dz",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextpos)
