###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

gaudi_install(PYTHON)

gaudi_add_tests(QMTest)

if(BINARY_TAG MATCHES "san$")
    # When running in sanitizer platforms, the tests take longer and
    # the test job gets killed at the 10h mark. Here we disable some
    # test to make the runtime more reasonable.

    # Disable mcchecking and resolution, tests and tests of examples, as
    # they are less interesting for production.
    get_property(recoconf_tests DIRECTORY PROPERTY TESTS)
    foreach(test IN ITEMS ${recoconf_tests})
        if(test MATCHES "mcchecking|efficiency|plots|examples")
            set_property(TEST ${test} PROPERTY DISABLED TRUE)
        endif()
    endforeach()
endif()

if (BINARY_TAG MATCHES ".*cuda.*")
    # When compiling for GPU, Gaudi-Allen algorithm wrappers call the device algorithms
    # since these are incompatible with calling from Moore / Gaudi, disable Gaudi-Allen tests
    get_property(recoconf_tests DIRECTORY PROPERTY TESTS)
    foreach(test IN ITEMS ${recoconf_tests})
        if(test MATCHES "allen")
	    #message(STATUS "Disabling ${test}")
            set_property(TEST ${test} PROPERTY DISABLED TRUE)
        endif()
    endforeach()
endif()


if(BUILD_TESTING AND USE_DD4HEP)
    # Disable some tests that are not yet dd4hep ready
    set_property(
        TEST
           # These need muon geometry v2
	   RecoConf.decoding.compare_hlt1_hlt2_muon_decoding_v2geometry
           RecoConf.examples.hlt2_reco_test_ldst_input
           RecoConf.hlt1_reco_baseline_with_mcchecking
           RecoConf.performance.hlt1_reco_trackingeff_plots
           RecoConf.hlt1_reco_decode_retina_with_mcchecking_FTv6
           RecoConf.hlt2_lead_lead_fast_reco_pr_kf_without_UT_gec_25000_with_mcchecking_velo_open_2023
           RecoConf.hlt2_light_reco_pr_kf_velo_open_without_UT_with_mcchecking
           RecoConf.hlt2_pr_kf_longmuon
           RecoConf.hlt2_protoparticles_baseline
           RecoConf.hlt2_protoparticles_fastest
           RecoConf.performance.hlt1_reco_muonIDeff
           RecoConf.performance.hlt1_reco_muonIDeff_plots
           # needs sim-20180530-vc-md100 as condition version
           RecoConf.hlt2_reco_calo_cluster_shapes
           RecoConf.hlt2_reco_calo_efficiency
           RecoConf.hlt2_reco_calo_resolution_gamma
           RecoConf.hlt2_plot_calo_resolution_gamma
           RecoConf.hlt1_pvs_PatPV3DFuture
           RecoConf.hlt1_reco_allen_calo_efficiency
           RecoConf.hlt1_reco_allen_calo_resolution
           RecoConf.hlt2_light_reco_calo_efficiency
           RecoConf.mc_hit_resolution_checker
           RecoConf.mc_run_unpackers
           RecoConf.decoding.compare_hlt1_hlt2_calo_decoding_v2bank_v3geometry
           RecoConf.examples.hlt2_reco_tracking
           RecoConf.hlt2_light_reco_pr_kf_without_UT_on_data_with_monitoring_2022
           RecoConf.hlt2_particles_baseline
           RecoConf.hlt2_reco_baseline
           RecoConf.hlt2_reco_baseline_with_parametrised_scatters
           RecoConf.VP_cluster_monitoring
           RecoConf.VP_cluster_monitoring_baseline
           RecoConf.performance.VP_cluster_monitoring_baseline_plots
           RecoConf.VP_cluster_monitoring_retina
           RecoConf.performance.VP_cluster_monitoring_retina_plots
           RecoConf.performance.VP_cluster_monitoring_comparison_baseline_retina_plots
           RecoConf.hlt1_reco_baseline_with_mcchecking_FTv6
           RecoConf.performance.hlt1_reco_trackresolution
           RecoConf.performance.hlt1_reco_trackresolution_plots
           RecoConf.VP_tracking_monitors
           RecoConf.performance.hlt1_reco_IPresolution
           RecoConf.performance.hlt1_reco_IPresolution_plots
           # needs sim-20171127-vc-md100 as condition version
           RecoConf.hlt2_reco_calo_resolution_pi0
           RecoConf.hlt2_plot_calo_resolution_pi0
           # Should probably be reenabled, at least input is from 2022 and thus recent enough
           RecoConf.decoding.compare_hlt1_hlt2_ut_decoding_with_mcchecking
           RecoConf.decoding.compare_hlt1_hlt2_ut_decoding_with_mcchecking_v4r2
           RecoConf.allen_gaudi_velo_ut_with_mcchecking
           RecoConf.hlt2_fast_reco_with_mcchecking
           RecoConf.hlt2_fastest_reco_with_mcchecking
           RecoConf.hlt2_light_reco_pr_kf_with_mcchecking
           RecoConf.hlt2_light_reco_with_mcchecking
           RecoConf.hlt2_protoparticles_ttrack_baseline
           RecoConf.hlt2_reco_baseline_with_mcchecking
           RecoConf.hlt2_reco_baseline_with_mcchecking_mpp_with_bfield
           RecoConf.hlt2_reco_upstream_particles_low_momentum
           RecoConf.hlt2_ut_filtered_forward_track_reco_with_mcchecking
           # Disabled due to UT issues.
           # Note a number of tests above likely also fall into this category as well
           # See e.g. https://gitlab.cern.ch/lhcb/Moore/-/issues/633 for a related discussion.
           # Also note a number of these tests might have more issues than just UT, but until
           # UT is commissioned and known to be working OK with DD4HEP it is hard to tell.
           RecoConf.hlt2_pr_kf_tool_example
           RecoConf.pr_kf_special_only
           RecoConf.hlt2_pattern_reco_hit_masking
           RecoConf.hlt2_reco_VP2DMeasurements
           RecoConf.hlt2_reco_full_geometry
           RecoConf.hlt2_ttracks_mva_filter
        PROPERTY
            DISABLED TRUE
    )
endif()

if(BUILD_TESTING AND NOT USE_DD4HEP)
    # Disable some tests that are not detdesc compatible
    set_property(
        TEST
	   RecoConf.phoenix_event_data_dump
       RecoConf.hlt2_reco_data_2023
        PROPERTY
            DISABLED TRUE
    )
endif()
