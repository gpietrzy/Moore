###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.reading import postprocess_unpacked_data
from .data_from_file import reco_unpackers


def upfront_reconstruction():
    """Return a list DataHandles that define the upfront reconstruction output.

    This differs from `reconstruction` as it should not be used as inputs to
    other algorithms, but only to define the control flow, i.e. the return
    value of this function should be run before all HLT2 lines.

    """
    return list(reco_unpackers().values())


def reconstruction():
    """Return a {name: DataHandle} dict that define the reconstruction output."""

    ### Temporary: as long as we persist v1, we need to insert a converter for the new PVs
    data = {k: v.OutputName for k, v in reco_unpackers().items()}

    # FIXME 'packable' set to true while 'SharedObjectsContainer`s are not persistable
    postprocess_unpacked_data(data, packable=True)

    return data


def make_charged_protoparticles(track_type='Charged'):
    return reconstruction()[f'{track_type}Protos']


def make_neutral_protoparticles():
    return reconstruction()['NeutralProtos']


def get_IsPhoton_table():
    """
    Return a DataHandle to the Relation table for IsPhoton.
    """

    return reconstruction()["IsPhoton"]


def get_IsNotH_table():
    """
    Return a DataHandle to the Relation table for IsNotH.
    """

    return reconstruction()["IsNotH"]


def get_ClusterMass_table():
    """
    Return a DataHandle to the Relation table for ClusterMass.
    """

    return reconstruction()["ClusterMass"]


def get_CaloClusterCode_table():
    """
    Return a DataHandle to the Relation table for CaloClusterCode.
    """

    return reconstruction()["CaloClusterCode"]


def make_pvs():
    return reconstruction()['PVs']


def make_tracks(track_type='Tracks'):
    return reconstruction()[track_type]
