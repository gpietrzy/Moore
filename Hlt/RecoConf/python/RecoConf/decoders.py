###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
TODO add more decoders from hlt1_tracking and hlt2_tracking here.
"""
import logging
from PyConf import configurable

log = logging.getLogger(__name__)


@configurable
def default_ft_decoding_version(value=4):
    """Sets the FTDecoding version.

       Args:
           value (int): FTDecoding version.

       Returns:
           int: value
    """
    return value
