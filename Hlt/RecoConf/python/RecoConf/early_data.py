###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
'''Example usage
from PyConf.Algorithms import (PrForwardTrackingVelo, PrMatchNN)
from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt2_light_reco_without_UT
from RecoConf.hlt2_tracking import (
    make_PrKalmanFilter_noUT_tracks,
    make_TrackBestTrackCreator_tracks,
)
from RecoConf.early_data import (
    get_loose_PrForwardTrackingVelo_params,
    get_loose_PrMatchNN_params,
    get_loose_PrKalmanFilter_params,
    get_loose_TrackBestTrackCreator_params,
)
with PrForwardTrackingVelo.bind(**get_loose_PrForwardTrackingVelo_params()), \
     PrMatchNN.bind(**get_loose_PrMatchNN_params()), \
     make_PrKalmanFilter_noUT_tracks.bind(**get_loose_PrKalmanFilter_params()), \
     make_TrackBestTrackCreator_tracks.bind(**get_loose_TrackBestTrackCreator_params()):
    run_reconstruction(options, standalone_hlt2_light_reco_without_UT)

'''


@configurable
def get_loose_PrForwardTrackingVelo_params(
        min_quality: float = 0,
        delta_quality: float = 0,
        min_total_hits: int = 9,
        max_chi2_dof: float = 50,
        max_chi2_xprojection: float = 60,
        max_chi2_dof_final: float = 28,
        max_chi2_stereo: float = 16,
        max_chi2_stereo_add: float = 16) -> dict:
    """Get loose parameters for the PrForwardTrackingVelo.

    Args:
        min_quality (float, optional): Minimum NN classifier response for track. Defaults to 0.0.
        delta_quality (float, optional): Difference to min_quality for track acceptance. Defaults to 0.0.
        decision_lda (float, optional): LDA boundary for x candidate rejection. Defaults to -99..
        min_total_hits (int, optional): Minimum number of hits on track. Defaults to 9.
        max_chi2_dof (float, optional): Max chi2 per dof of x candidate. Defaults to 50.
        max_chi2_xprojection (float, optional): Max single hit chi2 of x candidate. Defaults to 60.
        max_chi2_dof_final (float, optional): Max chi2 per dof of track. Defaults to 28.
        max_chi2_stereo (float, optional): Max single hit chi2 of stereo candidate. Defaults to 16.
        max_chi2_stereo_add (float, optional): Max chi2 for adding stereo hit. Defaults to 16.

    Note:
        This configuration aims at finding Long tracks in a misaligned detector and should not be used for regular
        data taking. Typical use case is the first Alignment step.


    Returns:
        dict: Pairs of Gaudi property name and value.
    """
    return dict(
        MinQuality=min_quality,
        DeltaQuality=delta_quality,
        MinTotalHits=min_total_hits,
        MaxChi2PerDoF=max_chi2_dof,
        MaxChi2XProjection=max_chi2_xprojection,
        MaxChi2PerDoFFinal=max_chi2_dof_final,
        MaxChi2Stereo=max_chi2_stereo,
        MaxChi2StereoAdd=max_chi2_stereo_add,
    )


@configurable
def get_loose_PrMatchNN_params(
        max_match_chi2: float = 20,
        min_match_nn: float = 0,
        max_d_dist: float = 0.3,
        dy_tol: float = 12,
        dy_tol_slope: float = 600,
) -> dict:
    """Get loose parameters for the PrMatchNN tracking algorithm.

    Args:
        max_match_chi2 (float, optional): Max chi2 for match between velo and Seed track. Defaults to 20..
        min_match_nn (float, optional): Minimum NN response for match. Defaults to 0..
        max_d_dist (float, optional): Max difference of NN response for track acceptance. Defaults to 0.3.
        dy_tol (float, optional): Intercept of matching distance in y. Defaults to 12..
        dy_tol_slope (float, optional): Slope of matching distance in y. Defaults to 600..

    Note:
        This configuration aims at finding Long tracks in a misaligned detector and should not be used for regular
        data taking. Typical use case is the first Alignment step.

    Returns:
        dict: Pairs of Gaudi property name and value.
    """
    return dict(
        MaxMatchChi2=max_match_chi2,
        MinMatchNN=min_match_nn,
        MaxdDist=max_d_dist,
        dyTol=dy_tol,
        dyTolSlope=dy_tol_slope,
    )


@configurable
def get_loose_PrKalmanFilter_params(
        max_chi2ndof: float = 20,
        max_chi2ndof_pre_outlier_removal: float = 999,
        min_chi2_outlier: float = 25,
) -> dict:
    """Get loose parameters for the PrKalmanFilter algorithm.

    Args:
        max_chi2ndof (float, optional): Max chi2/ndf a track may have without being killed. Defaults to 20.
        max_chi2ndof_pre_outlier_removal (float, optional): Max chi2/ndf a track may have before outliers are removed. Defaults to 999.
        min_chi2_outlier (float, optional): Min chi2 of a fit node for it to be considered as outlier. Defaults to 25.

    Note:
        This configuration aims at finding Long tracks in a misaligned detector and should not be used for regular
        data taking. Typical use case is the first Alignment step.

    Returns:
        dict: Pairs of config function keyword arg name and value.
    """
    return dict(
        max_chi2ndof=max_chi2ndof,
        max_chi2ndof_pre_outlier_removal=max_chi2ndof_pre_outlier_removal,
        min_chi2_outlier=min_chi2_outlier,
    )


@configurable
def get_loose_TrackBestTrackCreator_params(max_chi2ndof: float = 20, ) -> dict:
    """Get loose parameters for the TrackBestTrackCreator.

    Args:
        max_chi2ndof (float, optional): Max chi2/ndf a track may have without being killed. Defaults to 20.

    Note:
        Should be consistent with `get_loose_PrKalmanFilter_params`. This configuration aims at finding Long tracks in a misaligned detector and should not be used for regular
        data taking. Typical use case is the first Alignment step.

    Returns:
        dict: Pairs of config function keyword arg name and value.
    """
    return dict(max_chi2ndof=max_chi2ndof)
