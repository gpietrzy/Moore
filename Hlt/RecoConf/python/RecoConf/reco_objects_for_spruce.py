###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
from GaudiConf import reading
from PyConf.packing import reco_locations, pp2mcp_locations
from PyConf.reading import upfront_decoder, dstdata_filter, postprocess_unpacked_data
from PyConf.components import force_location


def _make_locations(manifest, stream):
    """Return a location list to unpack and decode
    Args:
        stream (str): TES location prefix
        manifest: dict from json file

    """
    locations_to_decode = []
    for (l, t) in manifest['PackedLocations']:
        if l.startswith(stream) and not l.startswith(stream + "/MC"):
            locations_to_decode.append(force_location(l))
    return locations_to_decode


@configurable
def upfront_reconstruction(simulation=False):
    """Return a list DataHandles that define the upfront reconstruction output.

    This differs from `reconstruction` as it should not be used as inputs to
    other algorithms, but only to define the control flow, i.e. the return
    value of this function should be ran before all HLT2 lines.

    """

    stream = '/Event/HLT2'

    inv_map = {v: k for k, v in reading.type_map().items()}

    reco_loc = reco_locations(stream)
    dl = {v[0]: inv_map[v[1]] for v in reco_loc.values()}

    mc_algs = []
    if simulation:
        mc_algs = reading.mc_unpackers(
            input_process='Hlt2', configurables=False)

        pp2mcp_loc = pp2mcp_locations(stream)
        dl.update((v[0], inv_map[v[1]]) for v in pp2mcp_loc.values())

    # As reco locations have predefined types, there is no need for a manifest file
    # To use same functionality as in reding.py, make a manifest from known locations/types
    m = {'PackedLocations': [(k, v) for k, v in dl.items()]}

    # TODO: should not have to use/create manifest _at all_ here
    unpackers = reading.unpackers(
        _make_locations(m, stream),
        m,
        upfront_decoder(source="Hlt2"),
        configurables=False)

    ### TODO:FIXME take advantage of the fact that the above have datahandles...
    # i.e. should _not_ have to return decoder here, and should just return the _output handles_ and not the algorithms
    # i.e. `upfront_reconstruction` should be a drop-in replacement for `reconstruction()`, with the same return type
    return [
        dstdata_filter(source='Hlt2'),
        upfront_decoder(source="Hlt2").producer
    ] + mc_algs + unpackers


@configurable
def reconstruction(simulation=False):
    """Return a {name: DataHandle} dict that define the reconstruction output."""

    stream = '/Event/HLT2'
    data = {}
    unpackers = upfront_reconstruction(simulation)
    reco_loc = reco_locations(stream)

    if simulation:
        reco_loc |= pp2mcp_locations(stream)

    for key, value in reco_loc.items():
        for v in unpackers:
            if "OutputName" in v.outputs.keys(
            ) and v.OutputName.location == value[0]:
                data[key] = v.OutputName

    # FIXME 'packable' set to true while 'SharedObjectsContainer`s are not persistable
    postprocess_unpacked_data(data, packable=True)

    return data
