###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## Options used for Sprucing 2022 data first time around (note this only happened locally)

from Moore import Options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from Hlt2Conf.lines import sprucing_lines
from Moore.lines import PassLine

from Hlt2Conf.lines.b_to_charmonia import sprucing_lines as spruce_b2cc_lines
from Hlt2Conf.lines.b_to_open_charm import sprucing_lines as spruce_b2oc_lines
from Hlt2Conf.lines.bandq import sprucing_lines as spruce_bandq_lines
from Hlt2Conf.lines.semileptonic import sprucing_lines as spruce_sl_lines
from Hlt2Conf.lines.bnoc import sprucing_lines as spruce_bnoc_lines
from Hlt2Conf.lines.rd import sprucing_lines as spruce_rd_lines
from Hlt2Conf.lines.qee import sprucing_lines as spruce_qee_lines

from Moore.streams_hlt2 import DETECTOR_RAW_BANK_TYPES
from Moore import streams_spruce

public_tools = [stateProvider_with_simplified_geom()]

#######################################################
##Should only need to change the following dictionaries.

# Note that wg_lines are themselves dict types here
fulllinedict = {
    "b2oc": spruce_b2oc_lines,
    "bandq": spruce_bandq_lines,
    "b2cc": spruce_b2cc_lines,
    "rd": spruce_rd_lines,
    "sl": spruce_sl_lines,
    "qee": spruce_qee_lines,
    "bnoc": spruce_bnoc_lines,
}

turbolinedict = {
    "b2oc": ["Hlt2B2OC.*Decision"],
    "bandq": [
        "Hlt2BandQ.*Decision", 'Hlt2_JpsiToMuMuDecision',
        'Hlt2_Psi2SToMuMuDecision', 'Hlt2_DiMuonPsi2STightDecision',
        'Hlt2_DiMuonJPsiTightDecision'
    ],
    "b2cc": ["Hlt2B2CC.*Decision"],
    "sl": ["Hlt2SLB.*Decision"],
    "charm": ["Hlt2Charm.*Decision"],
    "qee": ["Hlt2QEE.*Decision", "Hlt2.*DiMuonNoIP.*Decision"],
    "rd": ["Hlt2RD.*Decision"],
    "bnoc": ["Hlt2BnoC.*Decision"],
}

# For Turcal make 2 sets of line dict which will create 6 streams
# The first 3 streams will contain all events but no det rawbanks
# The second 3 streams will have a 10% prescale but have all det rawbanks
turcallinedict = {
    "pid": ["Hlt2PID.*Decision"],
    "trackeff": ["Hlt2TrackEff.*Decision", "Hlt2HadInt.*Decision"],
    "monitoring": ["Hlt2Monitoring.*Decision"],
    "pid_raw": ["Hlt2PID.*Decision"],
    "trackeff_raw": ["Hlt2TrackEff.*Decision", "Hlt2HadInt.*Decision"],
    "monitoring_raw": ["Hlt2Monitoring.*Decision"],
}

#######################################################


def lines_running(linedict):
    """ Return the full list of lines to be run"""

    return [
        item for sublist in [list(linedict[wg].keys()) for wg in linedict]
        for item in sublist
    ]


def lines_not_running(all_lines, lines_to_run):
    """Return the list of lines that are declared in the framework but that are not set to run"""
    return [item for item in list(all_lines) if item not in lines_to_run]


def excl_spruce_production(options: Options):

    streams_spruce.streams = list(fulllinedict.keys())
    streams_spruce.stream_banks = {key: [] for key in streams_spruce.streams}

    spruce_b2oc_lines.pop('SpruceB2OC_BdToDsmK_DsmToHHH_FEST')

    lines_to_run = lines_running(fulllinedict)
    missing_lines = lines_not_running(sprucing_lines, lines_to_run)

    def make_streams():
        streamdict = {
            wg: [builder() for builder in fulllinedict[wg].values()]
            for wg in fulllinedict
        }
        return streamdict

    print(f"Running {len(lines_to_run)} lines")
    for wg in fulllinedict.keys():
        lines = list(fulllinedict[wg].keys())
        print(f"Stream {wg} will contain the lines: {lines} \n")

    print(
        f"The following lines exist but are not appended to any stream : {missing_lines} \n end of missing lines."
    )

    config = run_moore(options, make_streams, public_tools=[])
    return config


def pass_spruce_production(options: Options):

    streams_spruce.streams = list(turbolinedict.keys())
    streams_spruce.stream_banks = {
        key: DETECTOR_RAW_BANK_TYPES
        for key in streams_spruce.streams
    }

    def make_streams():
        streamdict = {
            wg:
            [PassLine(
                name="Pass" + wg,
                hlt2_filter_code=turbolinedict[wg],
            )]
            for wg in turbolinedict
        }

        return streamdict

    for wg in turbolinedict.keys():
        print(
            f"Stream {wg} will contain the lines matching the regex : {turbolinedict[wg]} \n"
        )

    config = run_moore(options, make_streams, public_tools=[])
    return config


def turcal_spruce_production(options: Options):

    streams_spruce.streams = list(turcallinedict.keys())
    streams_spruce.stream_banks = {
        key: []
        for key in streams_spruce.streams if "raw" not in key
    }

    # For 10% of events we want to keep detector rawbanks (see the prescale)
    streams_spruce.stream_banks.update({
        key: DETECTOR_RAW_BANK_TYPES
        for key in streams_spruce.streams if "raw" in key
    })
    print("Banks for each stream : ", streams_spruce.stream_banks)

    def make_streams():
        streamdict = {
            wg: [
                PassLine(
                    name="Pass" + wg,
                    hlt2_filter_code=turcallinedict[wg],
                )
            ]
            for wg in turcallinedict if "raw" not in wg
        }

        streamdict.update({
            wg: [
                PassLine(
                    name="Pass" + wg,
                    hlt2_filter_code=turcallinedict[wg],
                    prescale=0.1)
            ]
            for wg in turcallinedict if "raw" in wg
        })
        return streamdict

    for wg in turcallinedict.keys():
        print(
            f"Stream {wg} will contain the lines matching the regex : {turcallinedict[wg]} \n"
        )

    config = run_moore(options, make_streams, public_tools=[])
    return config
