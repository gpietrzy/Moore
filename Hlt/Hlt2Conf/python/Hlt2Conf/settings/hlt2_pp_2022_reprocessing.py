###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for testing the first data with HLT2.
"""
from Moore.lines import Hlt2Line, Hlt2LuminosityLine
from Hlt2Conf.lines.topological_b import all_lines as topological_b_lines
from Hlt2Conf.lines.inclusive_detached_dilepton import all_lines as inclusive_detached_dilepton_lines
from Hlt2Conf.lines.pid import all_lines as pid_lines
from Hlt2Conf.lines.trackeff import all_lines as trackeff_lines
from Hlt2Conf.lines.monitoring import all_lines as monitoring_lines
from Hlt2Conf.lines.b_to_open_charm import all_lines as b_to_open_charm_lines
from Hlt2Conf.lines.rd import full_lines as rd_full_lines
from Hlt2Conf.lines.rd import turbo_lines as rd_turbo_lines
from Hlt2Conf.lines.bandq import all_lines as bandq_lines
from Hlt2Conf.lines.qee import hlt2_full_lines as qee_full_lines
from Hlt2Conf.lines.qee import hlt2_turbo_lines as qee_turbo_lines
from Hlt2Conf.lines.charm import all_lines as charm_lines
from Hlt2Conf.lines.b_to_charmonia import all_lines as b_to_charmonia_lines
from Hlt2Conf.lines.semileptonic import all_lines as semileptonic_lines
from Hlt2Conf.lines.charmonium_to_dimuon import all_lines as charmonium_to_dimuon_lines
from Hlt2Conf.lines.charmonium_to_dimuon_detached import all_lines as charmonium_to_dimuon_detached_lines
from Hlt2Conf.lines.bnoc import all_lines as bnoc_lines
from Hlt2Conf.lines.ift import ift_full_lines
from Hlt2Conf.lines.ift import ift_turbo_lines
import re


def _hlt2_lumi_line():
    return Hlt2LuminosityLine(algs=[], hlt1_filter_code="Hlt1ODINLumiDecision")


def _hlt2_lumi_default_raw_banks_line(name='Hlt2LumiDefaultRawBanks',
                                      prescale=1):
    """Propagate raw banks for (some) lumi events.

    Hlt2LuminosityLine only requests the minimal set of banks. In order to propagate the banks
    defined in streams_hlt2.DETECTOR_RAW_BANK_TYPES_PER_STREAM for the LUMI stream, we need
    another line that passes these events.

    """
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code="Hlt1ODINLumiDecision",
        prescale=prescale,
    )


def _hlt2_lumi_calibration_line(name='Hlt2LumiCalibration', prescale=1):
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code="Hlt1ODIN1kHzLumiDecision",
        prescale=prescale)


def _remove_lines(lines_dict, pattern_to_remove):
    filtered = {
        name: line
        for name, line in lines_dict.items()
        if re.search(pattern_to_remove, name) is None
    }
    return filtered


## for removing lines that are incompatible with DD4HEP
to_remove = [
    "Hlt2TrackEff_DiMuon_Downstream", "Hlt2TrackEff_DiMuon_MuonUT",
    "Hlt2QEE_TrackEff_ZToMuMu_Downstream", "Hlt2QEE_TrackEff_ZToMuMu_UTMuon"
]


def _make_lines(*lines_dicts):
    all_lines = []
    for lines in lines_dicts:
        trunc_lines = lines
        for remove in to_remove:
            trunc_lines = _remove_lines(trunc_lines, remove)
        if len(lines) != len(trunc_lines):
            print("Manually removed lines due to DD4HEP: ",
                  lines.keys() - trunc_lines.keys())
        all_lines += [builder() for builder in trunc_lines.values()]
    return all_lines


def _make_streams():
    full_lines = _make_lines(
        topological_b_lines,
        inclusive_detached_dilepton_lines,
        charmonium_to_dimuon_detached_lines,
        rd_full_lines,
        qee_full_lines,
        semileptonic_lines,
        ift_full_lines,
    ) + [_hlt2_lumi_line()]
    turbo_lines = _make_lines(
        b_to_open_charm_lines,
        rd_turbo_lines,
        bandq_lines,
        qee_turbo_lines,
        charm_lines,
        b_to_charmonia_lines,
        charmonium_to_dimuon_lines,
        bnoc_lines,
        ift_turbo_lines,
    ) + [_hlt2_lumi_line()]
    turcal_lines = _make_lines(
        pid_lines,
        trackeff_lines,
        monitoring_lines,
    ) + [_hlt2_lumi_line()]
    lumi_lines = [
        _hlt2_lumi_line(),
        _hlt2_lumi_default_raw_banks_line(),
        _hlt2_lumi_calibration_line(),
    ]

    # make sure persistreco is true for full and turcal lines
    problems = []
    for l in full_lines:
        if isinstance(l, Hlt2Line) and not l.persistreco:
            problems.append(f"Line {l.name} is in the FULL stream" +
                            "and must have persistreco=True")
    for l in turcal_lines:
        if isinstance(l, Hlt2Line) and not l.persistreco:
            problems.append(f"Line {l.name} is in the TURCAL stream" +
                            "and must have persistreco=True")
    if problems:
        raise RuntimeError("Misconfigured lines found:\n" +
                           "\n".join(problems))

    return dict(
        full=full_lines,
        turboraw=turbo_lines,
        turcal=turcal_lines,
        lumi=lumi_lines)


def make_streams(real_make_streams=_make_streams):

    from RecoConf.hlt1_tracking import (
        make_VeloClusterTrackingSIMD, make_RetinaClusters, make_reco_pvs,
        make_PatPV3DFuture_pvs, get_global_measurement_provider)
    from RecoConf.hlt1_muonid import make_muon_hits
    from RecoConf.hlt2_tracking import (
        make_PrKalmanFilter_noUT_tracks, make_PrKalmanFilter_Seed_tracks,
        make_PrKalmanFilter_Velo_tracks, make_TrackBestTrackCreator_tracks,
        get_UpgradeGhostId_tool_no_UT)
    from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD
    # Workaround to enable running of Tracking efficiency lines using special muon reconstruction
    from PyConf.Tools import TrackMasterFitter

    make_muon_hits.global_bind(geometry_version=3)

    with make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=4.),\
        make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=6.),\
        make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=8.),\
        make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=5.),\
        make_VeloClusterTrackingSIMD.bind(algorithm=VeloRetinaClusterTrackingSIMD, SkipForward=4),\
        get_UpgradeGhostId_tool_no_UT.bind(velo_hits=make_RetinaClusters),\
        make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
        get_global_measurement_provider.bind(velo_hits=make_RetinaClusters),\
        TrackMasterFitter.bind(FastMaterialApproximation=True):

        return real_make_streams()
