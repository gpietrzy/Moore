###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for the van der Meer scan in Nov. 2022
"""
from Moore.lines import Hlt2Line

import Moore.streams_hlt2
from PyConf.Algorithms import OdinTypesFilter
from PyConf.application import make_odin


# has to be split in beam beam, eb, be, and ee
def _hlt2_lumi_bb_line(name='Hlt2LumiBeamBeam', prescale=1):
    odinFilter = OdinTypesFilter(ODIN=make_odin(), BXTypes=['BeamCrossing'])
    return Hlt2Line(
        name=name,
        algs=[odinFilter],
        hlt1_filter_code="Hlt1ODINLumiDecision",
        prescale=prescale)


def _hlt2_lumi_be_line(name='Hlt2LumiBeamEmpty', prescale=0.1):
    odinFilter = OdinTypesFilter(ODIN=make_odin(), BXTypes=['Beam1'])
    return Hlt2Line(
        name=name,
        algs=[odinFilter],
        hlt1_filter_code="Hlt1ODINLumiDecision",
        prescale=prescale)


def _hlt2_lumi_eb_line(name='Hlt2LumiEmptyBeam', prescale=0.1):
    odinFilter = OdinTypesFilter(ODIN=make_odin(), BXTypes=['Beam2'])
    return Hlt2Line(
        name=name,
        algs=[odinFilter],
        hlt1_filter_code="Hlt1ODINLumiDecision",
        prescale=prescale)


def _hlt2_lumi_ee_line(name='Hlt2LumiEmptyEmpty', prescale=1):
    odinFilter = OdinTypesFilter(ODIN=make_odin(), BXTypes=['NoBeam'])
    return Hlt2Line(
        name=name,
        algs=[odinFilter],
        hlt1_filter_code="Hlt1ODINLumiDecision",
        prescale=prescale)


def _hlt2_beamgas_line(name="Hlt2BeamGas", prescale=1):
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code="Hlt1BGI.*Decision",
        prescale=prescale)


def _hlt2_hlt1passthrough_line(name="Hlt2Hlt1PassThrough", prescale=1):
    '''
      Anything else than the beamgas or lumi lines
    '''
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code=r"^Hlt1(?!ODINLumi|BGI).*Decision",
        prescale=prescale)


def make_streams():
    return dict(
        beamgas_stream=[
            _hlt2_beamgas_line(),
            _hlt2_lumi_bb_line(name='Hlt2LumiBeamBeam_ForBGI', prescale=0.1),
            _hlt2_lumi_be_line(name='Hlt2LumiBeamEmpty_ForBGI', prescale=0.01),
            _hlt2_lumi_eb_line(name='Hlt2LumiEmptyBeam_ForBGI', prescale=0.01),
            _hlt2_lumi_ee_line(name='Hlt2LumiEmptyEmpty_ForBGI', prescale=0.1)
        ],
        full_stream=[_hlt2_hlt1passthrough_line()],
        lumi_stream=[
            _hlt2_lumi_bb_line(),
            _hlt2_lumi_be_line(),
            _hlt2_lumi_eb_line(),
            _hlt2_lumi_ee_line()
        ],
    )


# Modify stream dictionaries for testing purposes
# Sets the output banks. Should be empty for Turbo at some point in time.
Moore.streams_hlt2.DETECTOR_RAW_BANK_TYPES_PER_STREAM = {
    # mstahl: checked with pcie40_mdfreader on Hlt1 file from run with all subdetectors included.
    # Dec-, Sel-reports, Lumi summary and routing bits are alwyas added
    "beamgas_stream":
    ['ODIN', 'VPRetinaCluster', 'Rich', 'FTCluster', 'Calo', 'Muon', 'Plume'],
    "full_stream":
    ['ODIN', 'VPRetinaCluster', 'Rich', 'FTCluster', 'Calo', 'Muon', 'Plume'],
    "lumi_stream": ['ODIN', 'Plume']
}
Moore.streams_hlt2.stream_bits = dict(
    beamgas_stream=86, full_stream=87, lumi_stream=93)
