###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.standard_particles import (
    make_has_rich_long_pions, make_has_rich_up_pions, make_has_rich_long_kaons,
    make_has_rich_up_kaons, make_has_rich_long_protons,
    make_has_rich_up_protons, make_long_electrons_with_brem,
    make_ismuon_long_muon, make_up_electrons_no_brem, make_up_pions,
    make_up_muons)
from Hlt2Conf.algorithms_thor import ParticleFilter
import Functors as F

from PyConf.Algorithms import AdvancedCloneKiller
""" cuts from BTaggingTool applied to every particles type:
  Gaudi::Property<double> m_cutTagPart_MinTheta{this, "CutTagPart_MinTheta", 0.012,
                                                "Tagging particle requirement: Minimum theta angle"};
    change into eta cause eta functor exists
    eta = - ln ( tan (theta/2 ) ) ==> maxEta = 5.12

  Gaudi::Property<double> m_cutTagPart_MaxGhostProb{this, "CutTagPart_MaxGhostProb", 0.5,
                                                    "Tagging particle requirement: Maximum ghost probability"};

  Gaudi::Property<double> m_cutTagPart_MinP{this, "CutTagPart_MinP", 2,
                                            "Tagging particle requirement: Minimum P (in GeV)"};

  Gaudi::Property<double> m_cutTagPart_MaxP{this, "CutTagPart_MaxP", 200,
                                            "Tagging particle requirement: Maximum P (in GeV)"};

  Gaudi::Property<double> m_cutTagPart_MaxPT{this, "CutTagPart_MaxPT", 10,
                                             "Tagging particle requirement: Maximum PT (in GeV)"};

  doesn't currently exist
  Gaudi::Property<double> m_cutTagPart_MinCloneDist{this, "CutTagPart_MinCloneDist", 5000,
                                                    "Tagging particle requirement: Minimum Track::CloneDist"};
"""


def _make_particles_with_selection(make_particles, selection_code):
    return ParticleFilter(Input=make_particles(), Cut=F.FILTER(selection_code))


def _make_pions_with_selection(make_particles, selection_code):
    return ParticleFilter(Input=make_particles(), Cut=F.FILTER(selection_code))


def _make_protons_with_selection(make_particles, selection_code):
    return ParticleFilter(Input=make_particles(), Cut=F.FILTER(selection_code))


def _make_kaons_with_selection(make_particles, selection_code):
    return ParticleFilter(Input=make_particles(), Cut=F.FILTER(selection_code))


def _make_electrons_with_selection(make_particles, selection_code):
    return ParticleFilter(Input=make_particles(), Cut=F.FILTER(selection_code))


def _make_muons_with_selection(make_particles, selection_code):
    return ParticleFilter(Input=make_particles(), Cut=F.FILTER(selection_code))


def make_sameside_tagging_pions(pvs=make_pvs,
                                p_min=2 * GeV,
                                p_max=200 * GeV,
                                pt_min=0.4 * GeV,
                                pt_max=10 * GeV,
                                eta_max=5.12,
                                pidp_max=5,
                                pidk_max=5,
                                ghostprob_max=0.5):
    selection_code = F.require_all(
        F.P > p_min, F.P < p_max, F.PT > pt_min, F.PT < pt_max,
        F.PID_P < pidp_max, F.PID_K < pidk_max, F.GHOSTPROB < ghostprob_max,
        F.ETA < eta_max)

    long_pions = _make_pions_with_selection(make_has_rich_long_pions,
                                            selection_code)
    up_pions = _make_pions_with_selection(make_has_rich_up_pions,
                                          selection_code)

    return AdvancedCloneKiller(InputParticles=[long_pions, up_pions])


def make_sameside_tagging_kaons(pvs=make_pvs,
                                p_min=2 * GeV,
                                p_max=200 * GeV,
                                pt_max=10 * GeV,
                                eta_max=5.12,
                                ghostprob_max=0.5):
    selection_code = F.require_all(F.P > p_min, F.P < p_max, F.PT < pt_max,
                                   F.GHOSTPROB < ghostprob_max,
                                   F.ETA < eta_max)
    long_kaons = _make_kaons_with_selection(make_has_rich_long_kaons,
                                            selection_code)
    up_kaons = _make_kaons_with_selection(make_has_rich_up_kaons,
                                          selection_code)

    return AdvancedCloneKiller(InputParticles=[long_kaons, up_kaons])


def make_sameside_tagging_protons(pvs=make_pvs,
                                  p_min=2 * GeV,
                                  p_max=200 * GeV,
                                  pt_min=0.4 * GeV,
                                  pt_max=10 * GeV,
                                  eta_max=5.12,
                                  pidp_min=5,
                                  ghostprob_max=0.5):
    selection_code = F.require_all(
        F.P > p_min, F.P < p_max, F.PT > pt_min, F.PT < pt_max,
        F.PID_P > pidp_min, F.GHOSTPROB < ghostprob_max, F.ETA < eta_max)
    long_protons = _make_protons_with_selection(make_has_rich_long_protons,
                                                selection_code)
    up_protons = _make_protons_with_selection(make_has_rich_up_protons,
                                              selection_code)

    return AdvancedCloneKiller(InputParticles=[long_protons, up_protons])


def make_oppositeside_tagging_kaons(pvs=make_pvs,
                                    p_min=5 * GeV,
                                    p_max=200 * GeV,
                                    pt_max=10 * GeV,
                                    eta_max=5.12,
                                    ghostprob_max=0.52,
                                    trchi2dof_max=3.0):
    selection_code = F.require_all(F.P > p_min, F.P < p_max, F.PT < pt_max,
                                   F.GHOSTPROB < ghostprob_max,
                                   F.ETA < eta_max, F.CHI2DOF < trchi2dof_max)
    long_kaons = _make_kaons_with_selection(make_has_rich_long_kaons,
                                            selection_code)

    return AdvancedCloneKiller(InputParticles=[long_kaons])


def make_oppositeside_tagging_electrons(pvs=make_pvs,
                                        p_min=5.035169513449979 * GeV,
                                        p_max=200 * GeV,
                                        pt_min=1.4033298418685027 * GeV,
                                        pt_max=10 * GeV,
                                        eta_max=5.12,
                                        ghostprob_max=0.5,
                                        trchi2dof_max=3.0,
                                        PIDe_min=4.33257736223062):
    selection_code = F.require_all(F.P > p_min, F.P < p_max, F.PT > pt_min,
                                   F.PT < pt_max, F.GHOSTPROB < ghostprob_max,
                                   F.ETA < eta_max, F.CHI2DOF < trchi2dof_max,
                                   F.PID_E > PIDe_min)
    long_electrons = _make_electrons_with_selection(
        make_long_electrons_with_brem, selection_code)

    return AdvancedCloneKiller(InputParticles=[long_electrons])


def make_oppositeside_tagging_muons(pvs=make_pvs,
                                    p_min=2.54 * GeV,
                                    p_max=200 * GeV,
                                    pt_min=0.951 * GeV,
                                    pt_max=10 * GeV,
                                    eta_max=5.12,
                                    ghostprob_max=0.5,
                                    trchi2dof_max=3.0):
    selection_code = F.require_all(F.P > p_min, F.P < p_max, F.PT > pt_min,
                                   F.PT < pt_max, F.GHOSTPROB < ghostprob_max,
                                   F.ETA < eta_max, F.CHI2DOF < trchi2dof_max)
    long_muons = _make_muons_with_selection(make_ismuon_long_muon,
                                            selection_code)

    return AdvancedCloneKiller(InputParticles=[long_muons])


def make_oppositeside_vertex_charge_tagging_particles(pvs=make_pvs,
                                                      p_min=2 * GeV,
                                                      p_max=200 * GeV,
                                                      pt_max=10 * GeV,
                                                      eta_max=5.12,
                                                      ghostprob_max=0.5):
    selection_code = F.require_all(F.P > p_min, F.P < p_max, F.PT < pt_max,
                                   F.GHOSTPROB < ghostprob_max,
                                   F.ETA < eta_max)
    long_pions = _make_pions_with_selection(make_has_rich_long_pions,
                                            selection_code)
    long_muons = _make_muons_with_selection(make_ismuon_long_muon,
                                            selection_code)
    long_electrons = _make_electrons_with_selection(
        make_long_electrons_with_brem, selection_code)

    upstream_pions = _make_pions_with_selection(make_up_pions, selection_code)
    upstream_muons = _make_muons_with_selection(make_up_muons, selection_code)
    upstream_electrons = _make_electrons_with_selection(
        make_up_electrons_no_brem, selection_code)

    return AdvancedCloneKiller(InputParticles=[
        long_pions, long_muons, long_electrons, upstream_pions, upstream_muons,
        upstream_electrons
    ])
