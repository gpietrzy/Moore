###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of control modes for B -> h e mu lines.

Currently includes (Jpsi to ee and mumu):

B+ -> K+ Jpsi
B+ -> pi+ Jpsi
Bs -> phi(-> K+ K-) Jpsi
Bd -> K*(892)0(-> K+ pi-) Jpsi
Lb -> p K Jpsi
"""

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import make_pvs
from PyConf import configurable
from GaudiKernel.SystemOfUnits import MeV
import Functors as F

from .builders import b_to_hemu_builders
from .builders import rdbuilder_thor

from .builders.rd_prefilters import rd_prefilter, _RD_MONITORING_VARIABLES

all_lines = {}


@register_line_builder(all_lines)
@configurable
def butokjpsi_mumu_line(name="Hlt2RD_BuToKpJpsi_JpsiToMuMu", prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(mipchi2dvprimary_min=9)
    dileptons = rdbuilder_thor.make_rd_detached_dimuon(am_max=5500 * MeV)
    bu = b_to_hemu_builders.make_btohemu(kaons, dileptons, pvs,
                                         "[B+ -> J/psi(1S) K+]cc")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dileptons, bu],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def butopijpsi_mumu_line(name="Hlt2RD_BuToPipJpsi_JpsiToMuMu", prescale=1):
    pvs = make_pvs()
    pions = rdbuilder_thor.make_rd_detached_pions(mipchi2dvprimary_min=9)
    dileptons = rdbuilder_thor.make_rd_detached_dimuon(am_max=5500 * MeV)
    bu = b_to_hemu_builders.make_btohemu(pions, dileptons, pvs,
                                         "[B+ -> J/psi(1S) pi+]cc")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dileptons, bu],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def bstophijpsi_mumu_line(name="Hlt2RD_BsToPhiJpsi_PhiToKK_JpsiToMuMu",
                          prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons()
    dileptons = rdbuilder_thor.make_rd_detached_dimuon(am_max=5500 * MeV)
    dikaons = b_to_hemu_builders.make_hh(
        kaons,
        kaons,
        pvs,
        "phi(1020) -> K+ K-",
        pt_min=400 * MeV,
        m_min=0 * MeV,
        m_max=1100 * MeV,
    )
    bs = b_to_hemu_builders.make_btohemu(dikaons, dileptons, pvs,
                                         "[B+ -> J/psi(1S) phi(1020)]cc")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dikaons, dileptons, bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def bdtokstjpsi_mumu_line(name="Hlt2RD_BdToKstJpsi_KstToKpPim_JpsiToMuMu",
                          prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons()
    pions = rdbuilder_thor.make_rd_detached_pions()
    dileptons = rdbuilder_thor.make_rd_detached_dimuon(am_max=5500 * MeV)
    kstars = b_to_hemu_builders.make_hh(kaons, pions, pvs,
                                        "[K*(892)0 -> K+ pi-]cc")
    bd = b_to_hemu_builders.make_btohemu(kstars, dileptons, pvs,
                                         "[B+ -> J/psi(1S) K*(892)0]cc")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kstars, dileptons, bd],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def lbtopkjpsi_mumu_line(name="Hlt2RD_LbToPKJpsi_JpsiToMuMu", prescale=1):
    pvs = make_pvs()
    dileptons = rdbuilder_thor.make_rd_detached_dimuon(am_max=5500 * MeV)
    kaons = rdbuilder_thor.make_rd_detached_kaons(pid=(F.PID_K > 0))
    protons = rdbuilder_thor.make_rd_detached_protons(
        pid=F.require_all(F.PID_P > 4., F.PID_P - F.PID_K > 0.))
    dihadron = b_to_hemu_builders.make_hh(
        protons,
        kaons,
        pvs,
        "[Lambda(1520)0 -> p+ K-]cc",
        pt_min=400 * MeV,
        m_min=0 * MeV,
        m_max=2600 * MeV)
    lb = b_to_hemu_builders.make_btohemu(
        dihadron, dileptons, pvs, "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dihadron, dileptons, lb],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def butokjpsi_ee_line(name="Hlt2RD_BuToKpJpsi_JpsiToEE", prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(mipchi2dvprimary_min=9)
    dileptons = rdbuilder_thor.make_rd_detached_dielectron(
        pid_e_min=2, am_max=5500 * MeV)
    bu = b_to_hemu_builders.make_btohemu(kaons, dileptons, pvs,
                                         "[B+ -> J/psi(1S) K+]cc")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dileptons, bu],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def butopijpsi_ee_line(name="Hlt2RD_BuToPipJpsi_JpsiToEE", prescale=1):
    pvs = make_pvs()
    pions = rdbuilder_thor.make_rd_detached_pions(mipchi2dvprimary_min=9)
    dileptons = rdbuilder_thor.make_rd_detached_dielectron(
        pid_e_min=2, am_max=5500 * MeV)
    bu = b_to_hemu_builders.make_btohemu(pions, dileptons, pvs,
                                         "[B+ -> J/psi(1S) pi+]cc")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dileptons, bu],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def bstophijpsi_ee_line(name="Hlt2RD_BsToPhiJpsi_PhiToKK_JpsiToEE",
                        prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons()
    dileptons = rdbuilder_thor.make_rd_detached_dielectron(
        pid_e_min=2, am_max=5500 * MeV)
    dikaons = b_to_hemu_builders.make_hh(
        kaons,
        kaons,
        pvs,
        "phi(1020) -> K+ K-",
        pt_min=400 * MeV,
        m_min=0 * MeV,
        m_max=1100 * MeV,
    )
    bs = b_to_hemu_builders.make_btohemu(dikaons, dileptons, pvs,
                                         "[B+ -> J/psi(1S) phi(1020)]cc")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dikaons, dileptons, bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def bdtokstjpsi_ee_line(name="Hlt2RD_BdToKstJpsi_KstToKpPim_JpsiToEE",
                        prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons()
    pions = rdbuilder_thor.make_rd_detached_pions()
    dileptons = rdbuilder_thor.make_rd_detached_dielectron(
        pid_e_min=2, am_max=5500 * MeV)
    kstars = b_to_hemu_builders.make_hh(kaons, pions, pvs,
                                        "[K*(892)0 -> K+ pi-]cc")
    bd = b_to_hemu_builders.make_btohemu(kstars, dileptons, pvs,
                                         "[B+ -> J/psi(1S) K*(892)0]cc")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kstars, dileptons, bd],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def lbtopkjpsi_ee_line(name="Hlt2RD_LbToPKJpsi_JpsiToEE", prescale=1):
    pvs = make_pvs()
    dileptons = rdbuilder_thor.make_rd_detached_dielectron(
        pid_e_min=2, am_max=5500 * MeV)
    kaons = rdbuilder_thor.make_rd_detached_kaons(pid=(F.PID_K > 0))
    protons = rdbuilder_thor.make_rd_detached_protons(
        pid=F.require_all(F.PID_P > 4., F.PID_P - F.PID_K > 0.))
    dihadron = b_to_hemu_builders.make_hh(
        protons,
        kaons,
        pvs,
        "[Lambda(1520)0 -> p+ K-]cc",
        pt_min=400 * MeV,
        m_min=0 * MeV,
        m_max=2600 * MeV)
    lb = b_to_hemu_builders.make_btohemu(
        dihadron, dileptons, pvs, "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dihadron, dileptons, lb],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)
