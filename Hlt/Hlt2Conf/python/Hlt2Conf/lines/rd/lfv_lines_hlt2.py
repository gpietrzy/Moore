###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''

Definition of some Lepton Flavour Violation lines:
    1) Bs -> pi mu     Hlt2RD_BToPipMu
    2) Bs -> K mu      Hlt2RD_BToKpMu
    3) Lz -> p mu      Hlt2RD_LambdaToPMu
    4) Lb -> p mu      Hlt2RD_LbToPMu

Notes:
    * For all channels two lines are defined, one with the right charge, one with wrong charge (SS)

    1), 2) This is the HLT2 porting of the B2PMu Stripping line:
              https://gitlab.cern.ch/lhcb/Stripping/-/blob/2018-patches/Phys/StrippingSelections/python/StrippingSelections/StrippingRD/StrippingLFVLines.py#L2415
    3) HLT2 porting of Lz2KMu Stripping line:
          https://gitlab.cern.ch/lhcb/Stripping/-/blob/2018-patches/Phys/StrippingSelections/python/StrippingSelections/StrippingRD/StrippingBLVLines.py#L234
    4) HLT2 porting of Lb2KMu Stripping line:
          https://gitlab.cern.ch/lhcb/Stripping/-/blob/2018-patches/Phys/StrippingSelections/python/StrippingSelections/StrippingRD/StrippingBLVLines.py#L250


Author: Fabio De Vellis
Contact: fabio.de.vellis@cern.ch

'''

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_detached_muons, make_rd_detached_pions, make_rd_detached_kaons, make_rd_detached_protons
from Hlt2Conf.lines.rd.builders.bnv_builders import make_BToHMu, make_LzToHMu, make_LbToHMu

from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter, _VRD_MONITORING_VARIABLES

from RecoConf.reconstruction_objects import make_pvs

import Functors as F

from GaudiKernel.SystemOfUnits import MeV

all_lines = {}


# 1) Bs -> pi mu
@register_line_builder(all_lines)
def BToPiMu_line(name="Hlt2RD_BToPipMu", prescale=1):

    descriptor = "[B_s0 -> pi+ mu-]cc"

    pvs = make_pvs()
    pion = make_rd_detached_pions(pt_min=500. * MeV, pid=(F.PID_K < 0.))
    muon = make_rd_detached_muons(
        pt_min=500. * MeV, pid=F.require_all(F.ISMUON, F.PID_MU > 3.))

    B = make_BToHMu(pvs, pion, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [B],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def BToPiMu_SS_line(name="Hlt2RD_BToPipMu_SS", prescale=1):

    descriptor = "[B_s0 -> pi+ mu+]cc"

    pvs = make_pvs()
    pion = make_rd_detached_pions(pt_min=500. * MeV, pid=(F.PID_K < 0.))
    muon = make_rd_detached_muons(
        pt_min=500. * MeV, pid=F.require_all(F.ISMUON, F.PID_MU > 3.))

    B = make_BToHMu(pvs, pion, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [B],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


# 2) Bs -> K mu
@register_line_builder(all_lines)
def BToKMu_line(name="Hlt2RD_BToKpMu", prescale=1 / 2.):

    descriptor = "[B_s0 -> K+ mu-]cc"

    pvs = make_pvs()
    kaon = make_rd_detached_kaons(pt_min=500. * MeV, pid=(F.PID_K > 3.))
    muon = make_rd_detached_muons(
        pt_min=500. * MeV, pid=F.require_all(F.ISMUON, F.PID_MU > 3.))

    B = make_BToHMu(pvs, kaon, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [B],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def BToKMu_SS_line(name="Hlt2RD_BToKpMu_SS", prescale=1):

    descriptor = "[B_s0 -> K+ mu+]cc"

    pvs = make_pvs()
    kaon = make_rd_detached_kaons(pt_min=500. * MeV, pid=(F.PID_K > 3.))
    muon = make_rd_detached_muons(
        pt_min=500. * MeV, pid=F.require_all(F.ISMUON, F.PID_MU > 3.))

    B = make_BToHMu(pvs, kaon, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [B],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


# 3) Lz -> p mu
@register_line_builder(all_lines)
def LzToPMu_line(name="Hlt2RD_LambdaToPpMu", prescale=1):

    descriptor = "[Lambda0 -> p+ mu-]cc"

    pvs = make_pvs()
    proton = make_rd_detached_protons(pt_min=500. * MeV, pid=(F.PID_P > 3.))
    muon = make_rd_detached_muons(
        pt_min=500. * MeV, pid=F.require_all(F.ISMUON, F.PID_MU > 3.))

    Lz = make_LzToHMu(pvs, proton, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [Lz],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def LzToPMu_SS_line(name="Hlt2RD_LambdaToPpMu_SS", prescale=1 / 3.):

    descriptor = "[Lambda0 -> p+ mu+]cc"

    pvs = make_pvs()
    proton = make_rd_detached_protons(pt_min=500. * MeV, pid=(F.PID_P > 3.))
    muon = make_rd_detached_muons(
        pt_min=500. * MeV, pid=F.require_all(F.ISMUON, F.PID_MU > 3.))

    Lz = make_LzToHMu(pvs, proton, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [Lz],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


# 4) Lb -> p mu
@register_line_builder(all_lines)
def LbToPMu_line(name="Hlt2RD_LbToPpMu", prescale=1):

    descriptor = "[Lambda_b0 -> p+ mu-]cc"

    pvs = make_pvs()
    proton = make_rd_detached_protons(pt_min=500. * MeV, pid=(F.PID_P > 3.))
    muon = make_rd_detached_muons(
        pt_min=500. * MeV, pid=F.require_all(F.ISMUON, F.PID_MU > 3.))

    Lb = make_LbToHMu(pvs, proton, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [Lb],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def LbToPMu_SS_line(name="Hlt2RD_LbToPpMu_SS", prescale=1):

    descriptor = "[Lambda_b0 -> p+ mu+]cc"

    pvs = make_pvs()
    proton = make_rd_detached_protons(pt_min=500. * MeV, pid=(F.PID_P > 3.))
    muon = make_rd_detached_muons(
        pt_min=500. * MeV, pid=F.require_all(F.ISMUON, F.PID_MU > 3.))

    Lb = make_LbToHMu(pvs, proton, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [Lb],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES)
