###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''                                                                                               
Author: Olaf Massen                                                                          
Contact: olaf.massen@cern.ch                                                                 
Date: 17/01/2022                                                                                   
'''

######################################################################################
####                                                                              ####
#### Builders for exclusive LFV lines Yb -> l l'                                  ####
####                                                                              ####
######################################################################################

from PyConf import configurable
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner

import Functors as F
from Functors.math import in_range

from Hlt2Conf.lines.rd.builders.rdbuilder_thor import (
    make_rd_detached_muons, make_rd_tauons_hadronic_decay,
    make_rd_detached_electrons)


@configurable
def make_taus(pi_pt_min=250 * MeV,
              best_pi_pt_min=750 * MeV,
              pi_pid=F.PID_K < 0.,
              tau_am_min=600 * MeV,
              tau_am_max=2100 * MeV,
              tau_m_min=1000 * MeV,
              tau_m_max=2000 * MeV,
              tau_bpvipchi2_min=12,
              tau_bpvvdrho_min=0.05 * mm,
              tau_bpvvdrho_max=8 * mm,
              tau_bpvvd_min=2 * mm):

    pvs = make_pvs()

    code = F.require_all(
        in_range(tau_m_min, F.MASS, tau_m_max),
        in_range(tau_bpvvdrho_min, F.BPVVDRHO(pvs), tau_bpvvdrho_max),
        F.BPVIPCHI2(pvs) > tau_bpvipchi2_min,
        F.BPVVDZ(pvs) > tau_bpvvd_min)

    taus = ParticleFilter(
        make_rd_tauons_hadronic_decay(
            pi_pt_min=pi_pt_min,
            best_pi_pt_min=best_pi_pt_min,
            pi_pid=pi_pid,
            am_min=tau_am_min,
            am_max=tau_am_max),
        Cut=F.FILTER(code))

    return taus


@configurable
def make_muons(mu_pt_min=500 * MeV,
               mu_p_min=2 * GeV,
               mu_pid=F.require_all(F.ISMUON, F.PID_MU > 2.),
               mu_mipchi2dvprimary_min=5.):

    muons = make_rd_detached_muons(
        pt_min=mu_pt_min,
        p_min=mu_p_min,
        mipchi2dvprimary_min=mu_mipchi2dvprimary_min,
        pid=mu_pid)

    return muons


@configurable
def make_electrons(
        e_PID=(F.PID_E > 3.),
        e_p_min=2 * GeV,
        e_pt_min=500 * MeV,
        e_mipchi2dvprimary_min=5.):

    electrons = make_rd_detached_electrons(
        pt_min=e_pt_min,
        p_min=e_p_min,
        pid=e_PID,
        mipchi2dvprimary_min=e_mipchi2dvprimary_min)

    return electrons


@configurable
def make_B_from_taumu(tau,
                      muon,
                      name="B_from_taumu",
                      parent_id='B0',
                      comb_m_min=2500 * MeV,
                      comb_m_max=6000 * MeV,
                      vchi2pdof_max=120,
                      ipchi2_min=3.,
                      ipchi2_max=35.,
                      fdchi2_min=80,
                      dira_min=0.99):
    """
    Make tau-mu combination. 
    The tau is filtered from rdbuilder_thor with requirements on IPCHI2, VDRHO, VDZ and M
    """
    pvs = make_pvs()

    combination_code = (in_range(comb_m_min, F.MASS, comb_m_max))

    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        in_range(ipchi2_min, F.BPVIPCHI2(pvs), ipchi2_max),
        F.BPVFDCHI2(pvs) > fdchi2_min,
        F.BPVDIRA(pvs) > dira_min)

    decay_descriptor = f'[{parent_id} -> tau- mu+]cc'

    return ParticleCombiner(
        Inputs=[tau, muon],
        name=name,
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_B_from_taue(tau,
                     electron,
                     name2="B_from_taue",
                     parent_id='B0',
                     comb_m_min=2500 * MeV,
                     comb_m_max=6000 * MeV,
                     vchi2pdof_max=120,
                     ipchi2_min=3.,
                     ipchi2_max=35.,
                     fdchi2_min=80,
                     dira_min=0.99):
    """
    Make tau-e combination. 
    The tau is filtered from rdbuilder_thor with requirements on IPCHI2, VDRHO, VDZ and M
    """
    pvs = make_pvs()

    combination_code = (in_range(comb_m_min, F.MASS, comb_m_max))

    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        in_range(ipchi2_min, F.BPVIPCHI2(pvs), ipchi2_max),
        F.BPVFDCHI2(pvs) > fdchi2_min,
        F.BPVDIRA(pvs) > dira_min)

    decay_descriptor = f'[{parent_id} -> tau- e+]cc'

    return ParticleCombiner(
        Inputs=[tau, electron],
        name=name2,
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)
