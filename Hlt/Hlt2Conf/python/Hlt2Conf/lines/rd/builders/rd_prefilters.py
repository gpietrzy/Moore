###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Common prefilters used by RD HLT2 lines.
Global event cut is not generally required in HLT2 as lines will be filtered based on HLT1 selection. But possibility of passing explicit GEC is kept for now.
- rd_prefilter: basic RD prefilter requiring reconstruction and PV for each event
- rd_prefilter_topo: modificaiton of the rd_prefilter, only events selected by topo as passed for a further selection
- rd_prefilter_noPV: special prefilter without PV requirement and possibility of passing GEC
"""
from RecoConf.event_filters import require_pvs, require_gec
from RecoConf.reconstruction_objects import (make_pvs, upfront_reconstruction)
from Hlt2Conf.lines.topological_b import require_topo_candidate

####################################
# RD prefilter                     #
####################################


def rd_prefilter(require_GEC=False):
    """
    Args:
        require_GEC (bool, optional): require the General Event Cut.
    """
    gec = [require_gec()] if require_GEC else []
    return upfront_reconstruction() + gec + [require_pvs(make_pvs())]


def rd_prefilter_topo(require_GEC=False):
    """
    Args:
        require_GEC (bool, optional): require the General Event Cut.
    """
    gec = [require_gec()] if require_GEC else []
    return upfront_reconstruction() + gec + [
        require_pvs(make_pvs()),
        require_topo_candidate()
    ]


def rd_prefilter_noPV(require_GEC=False):
    """
    Args:
        require_GEC (bool, optional): require the General Event Cut.
    """
    gec = [require_gec()] if require_GEC else []
    return upfront_reconstruction() + gec


_RD_MONITORING_VARIABLES = ("m", "pt", "eta", "vchi2", "ipchi2",
                            "n_candidates")
_VRD_MONITORING_VARIABLES = ("pt", "eta", "vchi2", "ipchi2", "n_candidates")
_VRD_NOPV_MONITORING_VARIABLES = ("pt", "eta", "vchi2", "n_candidates")

#################
## END OF FILE ##
#################
