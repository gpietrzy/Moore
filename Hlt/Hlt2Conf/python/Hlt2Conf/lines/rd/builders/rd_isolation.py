###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

#RD isolation algorithm
from PyConf import configurable
from PyConf.Algorithms import ThOrParticleSelection
from Hlt2Conf.standard_particles import make_long_pions, make_up_pions, make_down_pions, make_ttrack_pions, make_photons, make_merged_pi0s
from Hlt2Conf.isolation import extra_outputs_for_isolation

from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_tauons_hadronic_decay

import Functors as F


@configurable
def find_in_decay(input=make_rd_tauons_hadronic_decay, id="pi+"):
    """
    Retrieve the selection of particles matching 'id' argument from 'input' decay tree
    Args:
        input: Composite particle
        id: String representation of the ParticleProperty to get
    """
    code = (F.FILTER(F.IS_ABS_ID(id)) @ F.GET_ALL_DESCENDANTS())

    return ThOrParticleSelection(
        InputParticles=input, Functor=code).OutputSelection


@configurable
def select_parts_for_isolation(names=[],
                               candidates=[],
                               cut=(F.DR2 < 1.),
                               LongTrackIso=True,
                               TTrackIso=False,
                               DownstreamTrackIso=False,
                               UpstreamTrackIso=False,
                               NeutralIso=True,
                               PiZerosIso=False):
    """
    Add to the extra_outputs different kind of isolations by properly setting the given flag 
    Args:
        names: List of strings with prefix of the TES location for extra selection 
        candidates: List of containers of reference particles to relate extra particles
        cut: Predicate to select extra information to persist. By default: cone geometry with max dr2=1
        LongTrackIso: Boolean flag to make isolation with long tracks. By default True.
        TTrackIso: Boolean flag to make isolation with tt tracks. By default False.
        DownstreamTrackIso: Boolean flag to make isolation with downstream tracks. By default False.
        UpstreamTrackIso: Boolean flag to make isolation with upstream tracks. By default False.
        NeutralIso: Boolean flag to make isolation with neutral particles. By default True.
    """
    extra_outputs = []

    for name, cand in zip(names, candidates):
        if LongTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_LongTrackIsolation",
                extra_particles=make_long_pions,
                ref_particles=cand,
                selection=cut)
        if TTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_TTrackIsolation",
                extra_particles=make_ttrack_pions,
                ref_particles=cand,
                selection=cut)
        if DownstreamTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_DownstreamTrackIsolation",
                extra_particles=make_down_pions,
                ref_particles=cand,
                selection=cut)
        if UpstreamTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_UpstreamTrackIsolation",
                extra_particles=make_up_pions,
                ref_particles=cand,
                selection=cut)
        if NeutralIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_NeutralIsolation",
                extra_particles=make_photons,
                ref_particles=cand,
                selection=cut)
        if PiZerosIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_PiZerosIsolation",
                extra_particles=make_merged_pi0s,
                ref_particles=cand,
                selection=cut)
    return extra_outputs
