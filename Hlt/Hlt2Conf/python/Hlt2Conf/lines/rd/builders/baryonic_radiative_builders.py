###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of builders used for radiative b-decays.

List of the builders:
    make_rd_xibm - Builder for Xi_b- -> Xi- Gamma
    make_rd_obm - Builder for Omega_b- -> Omega- Gamma
"""

from GaudiKernel.SystemOfUnits import GeV, MeV

import Functors as F
from Functors.math import in_range

from Hlt2Conf.algorithms_thor import ParticleCombiner

from PyConf import configurable

# Define the masses of composite particles.
_Obm_M = 6046.1
_Xibm_M = 5797.0


@configurable
def make_rd_xibm(
        xims,
        photons,
        pvs,
        name='rd_xibm',
        comb_mass_window=1200. * MeV,
        mass_window=1000. * MeV,
        pt_min=1.5 * GeV,
        p_min=30 * GeV,
        sum_pt_min=5 * GeV,
):
    combination_cut = F.require_all(
        in_range(_Xibm_M - comb_mass_window, F.MASS,
                 _Xibm_M + comb_mass_window),
        F.SUM(F.PT) > sum_pt_min)

    composite_cut = F.require_all(
        in_range(_Xibm_M - mass_window, F.MASS, _Xibm_M + mass_window),
        F.PT > pt_min, F.P > p_min)

    return ParticleCombiner(
        [xims, photons],
        name=name,
        DecayDescriptor="[Xi_b- -> Xi- gamma]cc",
        CombinationCut=combination_cut,
        CompositeCut=composite_cut,
        ParticleCombiner="ParticleAdder",
    )


@configurable
def make_rd_obm(
        omegams,
        photons,
        pvs,
        name='rd_obm',
        comb_mass_window=1200. * MeV,
        mass_window=1000. * MeV,
        pt_min=1.5 * GeV,
        p_min=30 * GeV,
        sum_pt_min=5 * GeV,
):
    combination_cut = F.require_all(
        in_range(_Obm_M - comb_mass_window, F.MASS, _Obm_M + comb_mass_window),
        F.SUM(F.PT) > sum_pt_min)

    composite_cut = F.require_all(
        in_range(_Obm_M - mass_window, F.MASS, _Obm_M + mass_window),
        F.PT > pt_min, F.P > p_min)

    return ParticleCombiner(
        [omegams, photons],
        name=name,
        DecayDescriptor="[Omega_b- -> Omega- gamma]cc",
        CombinationCut=combination_cut,
        CompositeCut=composite_cut,
        ParticleCombiner="ParticleAdder",
    )
