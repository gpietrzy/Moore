###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the Hb->V0ll(h) exclusive lines, with LL and DD KS and L0
Includes:
    B0 -> J/psi(1S) KS0
    [B+ -> J/psi(1S) KS0 pi+]cc
    [Lambda_b0 -> J/psi(1S) Lambda0]cc
with:
    J/psi(1S) -> e+ e-
    J/psi(1S) -> mu+ mu-
    [J/psi(1S) -> mu+ e-]cc

Contact: Harry Cliff (harry.victor.cliff@cern.ch)
"""
from GaudiKernel.SystemOfUnits import MeV

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import (make_pvs)

from Hlt2Conf.lines.rd.builders.rdbuilder_thor import (
    make_rd_detached_dimuon, make_rd_detached_dielectron, make_rd_detached_mue,
    make_rd_ks0_lls, make_rd_lambda_lls, make_rd_detached_pions)

from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter, _RD_MONITORING_VARIABLES
from Hlt2Conf.lines.rd.builders.b_to_v0ll_builder import (
    make_b2ksll, make_lambdab2lambdall, make_b2kspill, make_ksttokspi)

all_lines = {}

_min_pid_mu = 0.0
_min_pid_e = None

##########################
# Definition of the lines
##########################

##########################
# B0 -> KS0 l+ l-
##########################


@register_line_builder(all_lines)
def b2ksmumull_line(name="Hlt2RD_BdToKSMuMu_LL", prescale=1):
    """B0 -> KS0 mu+ mu- LL line"""

    # get the PVs
    pvs = make_pvs()

    # get the Kshorts
    kshorts = make_rd_ks0_lls(bpvvdchi2_min=15.)

    # get the detached dimuons
    detached_dimuons = make_rd_detached_dimuon(
        pidmu_muon_min=_min_pid_mu,
        vchi2pdof_max=9.,
        am_max=5500. * MeV,
        pt_muon_min=800. * MeV)

    # make the candidate
    B2KsMuMu = make_b2ksll(
        dileptons=detached_dimuons,
        Kshorts=kshorts,
        pvs=pvs,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kshorts, B2KsMuMu],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def b2kseell_line(name="Hlt2RD_BdToKSEE_LL", prescale=1):
    """B0 -> KS0 e+ e- LL line"""

    # get the Kshorts
    kshorts = make_rd_ks0_lls(bpvvdchi2_min=15.)

    # get the PVs
    pvs = make_pvs()

    # get the detached dielectrons
    detached_dielectrons = make_rd_detached_dielectron(
        pid_e_min=_min_pid_e,
        vfaspfchi2ndof_max=9.,
        am_max=5500. * MeV,
        pt_e_min=500 * MeV)

    # make the candidate
    B2KsEE = make_b2ksll(
        dileptons=detached_dielectrons,
        Kshorts=kshorts,
        pvs=pvs,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kshorts, B2KsEE],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def b2ksmuell_line(name="Hlt2RD_BdToKSMuE_LL", prescale=1):
    """B0 -> KS0 mu+ e- LL line"""

    # get the PVs
    pvs = make_pvs()

    # get the Kshorts
    kshorts = make_rd_ks0_lls(bpvvdchi2_min=15.)

    # get the detached mues
    detached_mue = make_rd_detached_mue(
        max_dilepton_mass=5500. * MeV,
        min_PIDe=_min_pid_e,
        min_pt_e=500 * MeV,
        min_pt_mu=800. * MeV,
        max_vchi2ndof=9.,
    )

    # make the candidate
    B2KsMuE = make_b2ksll(
        dileptons=detached_mue,
        Kshorts=kshorts,
        pvs=pvs,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kshorts, B2KsMuE],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


#############################
# Lambda_b0 -> Lambda0 l+ l-
#############################


@register_line_builder(all_lines)
def lambdab2lambdamumull_line(name="Hlt2RD_LbToLMuMu_LL", prescale=1):
    """Lambda_b0 -> Lambda0 mu+ mu- LL line"""

    # get the Kshorts
    lambdas = make_rd_lambda_lls(bpvvdchi2_min=15.)

    # get the PVs
    pvs = make_pvs()

    # get the detached dimuons
    detached_dimuons = make_rd_detached_dimuon(
        pidmu_muon_min=_min_pid_mu,
        vchi2pdof_max=9.,
        am_max=5500. * MeV,
        pt_muon_min=800. * MeV)

    # make the candidate
    Lb2LMuMu = make_lambdab2lambdall(
        dileptons=detached_dimuons,
        Lambdas=lambdas,
        pvs=pvs,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [lambdas, Lb2LMuMu],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lambdab2lambdaeell_line(name="Hlt2RD_LbToLEE_LL", prescale=1):
    """Lambda_b0 -> Lambda0 e+ e- LL line"""

    # get the Kshorts
    lambdas = make_rd_lambda_lls(bpvvdchi2_min=15.)

    # get the PVs
    pvs = make_pvs()

    # get the detached dielectrons
    detached_dielectrons = make_rd_detached_dielectron(
        pid_e_min=_min_pid_e,
        vfaspfchi2ndof_max=9.,
        am_max=5500. * MeV,
        pt_e_min=500 * MeV)

    # make the candidate
    Lb2LEE = make_lambdab2lambdall(
        dileptons=detached_dielectrons,
        Lambdas=lambdas,
        pvs=pvs,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [lambdas, Lb2LEE],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lambdab2lambdamuell_line(name="Hlt2RD_LbToLMuE_LL", prescale=1):
    """Lambda_b0 -> Lambda0 mu+ e- LL line"""

    # get the Kshorts
    lambdas = make_rd_lambda_lls(bpvvdchi2_min=15.)

    # get the PVs
    pvs = make_pvs()

    # get the detached mues
    detached_mue = make_rd_detached_mue(
        max_dilepton_mass=5500. * MeV,
        min_PIDe=_min_pid_e,
        min_pt_e=500 * MeV,
        min_pt_mu=800. * MeV,
        max_vchi2ndof=9.,
    )

    # make the candidate
    Lb2LMuE = make_lambdab2lambdall(
        dileptons=detached_mue,
        Lambdas=lambdas,
        pvs=pvs,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [lambdas, Lb2LMuE],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


##########################
# B+ -> KS0 pi+ l+ l-
##########################


@register_line_builder(all_lines)
def b2kspimumull_line(name="Hlt2RD_BuToKSPipMuMu_LL", prescale=1):
    """B+ -> KS0 pi+ mu+ mu- LL line"""

    # get the Kshorts
    kshorts = make_rd_ks0_lls(bpvvdchi2_min=15.)

    # get the pions
    pions = make_rd_detached_pions(
        pt_min=400. * MeV,
        mipchi2dvprimary_min=9.,
    )

    # get the PVs
    pvs = make_pvs()

    # get the Kstars
    kstars = make_ksttokspi(Kshorts=kshorts, pions=pions, pvs=pvs)

    # get the detached dimuons
    detached_dimuons = make_rd_detached_dimuon(
        pidmu_muon_min=_min_pid_mu,
        vchi2pdof_max=9.,
        am_max=5500. * MeV,
        pt_muon_min=800. * MeV)

    # make the candidate
    B2KsPiMuMu = make_b2kspill(
        dileptons=detached_dimuons,
        Kstars=kstars,
        pvs=pvs,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kshorts, B2KsPiMuMu],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def b2kspieell_line(name="Hlt2RD_BuToKSPipEE_LL", prescale=1):
    """B+ -> KS0 pi+ e+ e- LL line"""

    # get the Kshorts
    kshorts = make_rd_ks0_lls(bpvvdchi2_min=15.)

    # get the pions
    pions = make_rd_detached_pions(
        pt_min=400. * MeV,
        mipchi2dvprimary_min=9.,
    )

    # get the PVs
    pvs = make_pvs()

    # get the Kstars
    kstars = make_ksttokspi(Kshorts=kshorts, pions=pions, pvs=pvs)

    # get the detached dielectrons
    detached_dielectrons = make_rd_detached_dielectron(
        pid_e_min=_min_pid_e,
        vfaspfchi2ndof_max=9.,
        am_max=5500. * MeV,
        pt_e_min=500 * MeV)

    # make the candidate
    B2KsPiEE = make_b2kspill(
        dileptons=detached_dielectrons,
        Kstars=kstars,
        pvs=pvs,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kshorts, B2KsPiEE],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def b2kspimuell_line(name="Hlt2RD_BuToKSPipMuE_LL", prescale=1):
    """B+ -> KS0 pi+ mu+ e- LL line"""

    # get the Kshorts
    kshorts = make_rd_ks0_lls(bpvvdchi2_min=15.)

    # get the pions
    pions = make_rd_detached_pions(
        pt_min=400. * MeV,
        mipchi2dvprimary_min=9.,
    )

    # get the PVs
    pvs = make_pvs()

    # get the Kstars
    kstars = make_ksttokspi(Kshorts=kshorts, pions=pions, pvs=pvs)

    # get the detached mues
    detached_mue = make_rd_detached_mue(
        max_dilepton_mass=5500. * MeV,
        min_PIDe=_min_pid_e,
        min_pt_e=500 * MeV,
        min_pt_mu=800. * MeV,
        max_vchi2ndof=9.,
    )

    # make the candidate
    B2KsPiMuE = make_b2kspill(
        dileptons=detached_mue,
        Kstars=kstars,
        pvs=pvs,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kshorts, B2KsPiMuE],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )
