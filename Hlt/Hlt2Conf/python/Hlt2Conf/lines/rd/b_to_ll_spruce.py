###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Registration of B2ll(') spruce lines for the RD working group.
Needed to develop isolation tools. 
It contains also B2HH as this is an important normalisation.
- B -> Mu Mu
- B -> Mu E
- B -> E E
- B -> h h

author: Titus Mombächer
date: 25.10.2021
"""

from Moore.config import register_line_builder
from Moore.lines import SpruceLine

from RecoConf.reconstruction_objects import upfront_reconstruction

from .builders.rdbuilder_thor import make_rd_detached_dihadron
from .builders.b_to_ll_builders import filter_B2MuMu, filter_B2MuE, filter_B2EE

sprucing_lines = {}


@register_line_builder(sprucing_lines)
def SpruceRD_BToMuMu(name="SpruceRD_BToMuMu", prescale=1):
    """
    Register B2mumu (B0 and B_s0) line (opposite sign)
    """

    dimuons = filter_B2MuMu()

    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [dimuons],
        prescale=prescale,
        hlt2_filter_code="Hlt2RD_BToMuMuDecision")


@register_line_builder(sprucing_lines)
def SpruceRD_BToMuMu_SameSign(name="SpruceRD_BToMuMu_SameSign", prescale=1):
    """
    Register B2mumu (B0 and B_s0) same sign line
    """

    dimuons = filter_B2MuMu(same_sign=True)

    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [dimuons],
        prescale=prescale,
        hlt2_filter_code="Hlt2RD_BToMuMu_SameSignDecision")


@register_line_builder(sprucing_lines)
def SpruceRD_BToEE(name="SpruceRD_BToEE", prescale=1):
    """
    Register B2EE (B0 and B_s0) line (opposite sign)
    """

    dielectrons = filter_B2EE()

    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [dielectrons],
        prescale=prescale,
        hlt2_filter_code="Hlt2RD_BToEEDecision")


@register_line_builder(sprucing_lines)
def SpruceRD_BToEE_SameSign(name="SpruceRD_BToEE_SameSign", prescale=1):
    """
    Register B2EE (B0 and B_s0) same sign line
    """

    dielectrons = filter_B2EE(same_sign=True)

    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [dielectrons],
        prescale=prescale,
        hlt2_filter_code="Hlt2RD_BToEE_SameSignDecision")


@register_line_builder(sprucing_lines)
def SpruceRD_BToMuE(name="SpruceRD_BToMuE", prescale=1):
    """
    Register B2MuE (B0 and B_s0) line (opposite sign)
    """

    emu = filter_B2MuE()

    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [emu],
        prescale=prescale,
        hlt2_filter_code="Hlt2RD_BToMuEDecision")


@register_line_builder(sprucing_lines)
def SpruceRD_BToMuE_SameSign(name="SpruceRD_BToMuE_SameSign", prescale=1):
    """
    Register B2MuE (B0 and B_s0) same sign line
    """

    emu = filter_B2MuE(same_sign=True)

    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [emu],
        prescale=prescale,
        hlt2_filter_code="Hlt2RD_BToMuE_SameSignDecision")


@register_line_builder(sprucing_lines)
def SpruceRD_BTohh(name="SpruceRD_BTohh", prescale=1):
    """
    Register B2hh (B0 and B_s0) line (opposite sign)
    """

    dihadrons = make_rd_detached_dihadron()

    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [dihadrons],
        prescale=prescale,
        hlt2_filter_code="Hlt2RD_BTohhDecision")
