###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Lines to study strange decays:

* KS0 -> l+ l-
* KS0 -> (X0) mu+ mu-
* KS0 -> l+(pi+) l-(pi-) l'+ l'-
* [KS0 -> pi+ l- nu_l~]cc
* [K+ -> pi+ l+ l-]cc

* [Lambda0 -> p l- nu_l~]cc
* [Sigma+ -> p l(')+ l-]cc
* [Xi0 -> p pi-]cc
* [Xi- -> p pi- pi-]cc
* [Xi- -> Lambda0 l- \nu_l~]cc
* [Omega- -> Lambda0 pi-]cc

Contact: Miguel Ramos Pernas (miguel.ramos.pernas@cern.ch)

TODO:

- Replace PID variables by ProbNN once we have a proper tuning
"""
from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter
from Hlt2Conf.lines.rd.builders import strange_builders
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.reconstruction_objects import make_pvs

all_lines = {}

#
# These two lines define particles to be used as proxies for exotic signatures. The
# vertex fitter works differently for resonances and long-lived particles, so lines
# must be fed with these strings with caution. e.g. in the decay
# KS0 -> J/psi(1S) J/psi(1S), the vertex of the KS0 might end-up being built with
# the descendants of the J/psi(1S) particles rather than from the extrapolation of
# the J/psi(1S) trajectories, whereas for KS0 -> J/psi(1S) KS0 it will be built
# using the J/psi(1S) descendants and the KS0 trajectory. Despite we must explicitly
# specify containers for each part of a decay descriptor, we can not provide
# two different containers with the same PID (e.g. combining two J/psi(1S) where
# the first is built from muons and the second with electrons). Therefore we provide
# an alternative.
#
SHORT_LIVED = 'phasespa'  # tau=0ns, self-cc
LONG_LIVED = 'geantino'  # stable, self-cc
LONG_LIVED_ALT = 'opticalphoton'  # stable, self-cc

###################################
# CONTROL AND NORMALIZATION MODES #
###################################


@register_line_builder(all_lines)
@configurable
def ks02pipi_line(name="Hlt2RD_KS0ToPiPi", prescale=1e-4):
    """
    Normalization mode for KS0 decays with two particles in the
    final state
    """
    pvs = make_pvs()
    pions = strange_builders.make_pions(pvs)
    ks0 = strange_builders.make_combination(
        'KS0 -> pi+ pi-', [pions, pions],
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


@register_line_builder(all_lines)
@configurable
def ks02pipi_detached_line(name="Hlt2RD_KS0ToPiPi_Detached", prescale=1e-3):
    """ Normalization mode for KS0 decays that do not come from a PV """
    pvs = make_pvs()
    pions = strange_builders.make_pions(pvs)
    ks0 = strange_builders.make_combination(
        'KS0 -> pi+ pi-', [pions, pions],
        pvs,
        build_requirements=strange_builders.
        requirements_for_detached_combination,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


@register_line_builder(all_lines)
@configurable
def ks02pipi_loose_line(name="Hlt2RD_KS0ToPiPi_Loose", prescale=1e-5):
    """ Control mode for analyses using KS0 decays from a PV with loose requirements """
    pvs = make_pvs()
    pions = strange_builders.make_loose_pions(pvs)
    ks0 = strange_builders.make_combination(
        'KS0 -> pi+ pi-', [pions, pions],
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


@register_line_builder(all_lines)
@configurable
def ks02pipi_detached_tight_line(name="Hlt2RD_KS0ToPiPi_Detached_Tight",
                                 prescale=1e-2):
    """ Normalization mode for KS0 decays that do not come from a PV """
    pvs = make_pvs()
    pions = strange_builders.make_tight_pions(pvs)
    ks0 = strange_builders.make_combination(
        'KS0 -> pi+ pi-', [pions, pions],
        pvs,
        build_requirements=strange_builders.
        requirements_for_detached_combination,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


@register_line_builder(all_lines)
@configurable
def ks02mumu_sb_line(name="Hlt2RD_KS0ToMuMu_SB", prescale=1e-4):
    """
    Select doubly misidentified KS0 -> pi+ pi- candidates for the
    Hlt2RD_KS0ToMuMu trigger line
    """
    pvs = make_pvs()
    muons = strange_builders.make_muons(pvs)

    # must ensure that the upper mass bound coincides with the lower mass bound
    # of the signal lines

    comb_mass_min = strange_builders.dimuon_min_mass()

    comb_mass_max, _ = strange_builders.ks0_tight_mass_bounds()

    ks0 = strange_builders.make_combination(
        'KS0 -> mu+ mu-', [muons, muons],
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=(comb_mass_min, comb_mass_max),
        name=f'{name}_Combiner')

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


@register_line_builder(all_lines)
@configurable
def ks02mumu_ss_line(name="Hlt2RD_KS0ToMuMu_SS", prescale=1):
    """
    Select same-sign candidates for the Hlt2RD_KS0ToMuMu trigger line
    """
    pvs = make_pvs()
    muons = strange_builders.make_muons(pvs)
    ks0 = strange_builders.make_combination(
        '[KS0 -> mu+ mu+]cc', [muons, muons],
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


@register_line_builder(all_lines)
@configurable
def ks02emu_ss_line(name="Hlt2RD_KS0ToEMu_SS", prescale=1):
    """
    Select same-sign candidates for the Hlt2RD_KS0ToEMu trigger line
    """
    pvs = make_pvs()
    electrons = strange_builders.make_electrons(pvs)
    muons = strange_builders.make_muons(pvs)
    ks0 = strange_builders.make_combination(
        '[KS0 -> mu+ e+]cc', [muons, electrons],
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


@register_line_builder(all_lines)
@configurable
def ks02ee_ss_line(name="Hlt2RD_KS0ToEE_SS", prescale=1):
    """
    Select same-sign candidates for the Hlt2RD_KS0ToEE trigger line
    """
    pvs = make_pvs()

    mass_range = strange_builders.ks0_mass_bounds()

    ks0 = strange_builders.filter_combination(
        strange_builders.make_dielectron(
            'KS0',
            pvs,
            mass_range=mass_range,
            opposite_sign=False,
            electron_requirements='default'),
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=mass_range,
        name=f'{name}_DielectronFilter')

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


@register_line_builder(all_lines)
@configurable
def ks02pimu_line(name="Hlt2RD_KS0ToPiMu", prescale=1e-2):
    """
    Select [KS0 -> pi+ mu- (nu_mu~)]cc candidates
    """
    pvs = make_pvs()
    pions = strange_builders.make_pions(pvs)
    muons = strange_builders.make_muons(pvs)
    _, mass_max = strange_builders.ks0_mass_bounds()
    ks0 = strange_builders.make_combination(
        '[KS0 -> pi+ mu-]cc', [pions, muons],
        pvs,
        build_requirements=strange_builders.
        requirements_for_detached_combination,
        mass_range=(strange_builders.pion_muon_min_mass(), mass_max),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


@register_line_builder(all_lines)
@configurable
def ks02pimu_loose_line(name="Hlt2RD_KS0ToPiMu_Loose", prescale=1e-3):
    """
    Select [KS0 -> pi+ mu- (nu_mu~)]cc candidates
    """
    pvs = make_pvs()
    pions = strange_builders.make_loose_pions(pvs)
    muons = strange_builders.make_loose_muons(pvs)
    _, mass_max = strange_builders.ks0_mass_bounds()
    ks0 = strange_builders.make_combination(
        '[KS0 -> pi+ mu-]cc', [pions, muons],
        pvs,
        build_requirements=strange_builders.
        requirements_for_detached_combination,
        mass_range=(strange_builders.pion_muon_min_mass(), mass_max),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


@register_line_builder(all_lines)
@configurable
def ks02pie_line(name="Hlt2RD_KS0ToPiE", prescale=1e-3):
    """
    Select [KS0 -> pi+ e- (nu_e~)]cc candidates
    """
    pvs = make_pvs()
    pions = strange_builders.make_pions(pvs)
    electrons = strange_builders.make_electrons(pvs)
    _, mass_max = strange_builders.ks0_mass_bounds()
    ks0 = strange_builders.make_combination(
        '[KS0 -> pi+ e-]cc', [pions, electrons],
        pvs,
        build_requirements=strange_builders.
        requirements_for_detached_combination,
        mass_range=(strange_builders.pion_electron_min_mass(), mass_max),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


@register_line_builder(all_lines)
@configurable
def ks02pie_loose_line(name="Hlt2RD_KS0ToPiE_Loose", prescale=1e-4):
    """
    Select [KS0 -> pi+ e- (nu_e~)]cc candidates
    """
    pvs = make_pvs()
    pions = strange_builders.make_loose_pions(pvs)
    electrons = strange_builders.make_loose_electrons(pvs)
    _, mass_max = strange_builders.ks0_mass_bounds()
    ks0 = strange_builders.make_combination(
        '[KS0 -> pi+ e-]cc', [pions, electrons],
        pvs,
        build_requirements=strange_builders.
        requirements_for_detached_combination,
        mass_range=(strange_builders.pion_electron_min_mass(), mass_max),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


@register_line_builder(all_lines)
@configurable
def ks02x0mumu_ss_line(name="Hlt2RD_KS0ToX0MuMu_SS_Inclusive"):
    """
    Select same-sign candidates for the Hlt2RD_KS0ToX0MuMu_Inclusive
    trigger line
    """
    pvs = make_pvs()
    muons = strange_builders.make_muons(pvs)
    _, mass_max = strange_builders.ks0_mass_bounds()
    ks0 = strange_builders.make_combination(
        '[KS0 -> mu+ mu+]cc', [muons, muons],
        pvs,
        build_requirements=strange_builders.
        requirements_for_detached_combination,
        mass_range=(strange_builders.dimuon_min_mass(), mass_max),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


@register_line_builder(all_lines)
@configurable
def kplus2pipipi_line(name="Hlt2RD_KpToPiPiPi", prescale=1e-1):
    """
    Control mode for decays with three charged particles in the
    final state
    """
    pvs = make_pvs()
    pions = strange_builders.make_pions(pvs)
    kplus = strange_builders.make_combination(
        '[K+ -> pi+ pi+ pi-]cc', [pions, pions, pions],
        pvs,
        build_requirements=strange_builders.
        requirements_for_prompt_combination,
        mass_range=strange_builders.kplus_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


@register_line_builder(all_lines)
@configurable
def ks02pipiee_line(name="Hlt2RD_KS0ToPiPiEE"):
    """ Normalization mode for four-body KS0 decays """
    pvs = make_pvs()
    pions = strange_builders.make_loose_pions(pvs)

    mass_min, mass_max = strange_builders.ks0_mass_bounds()

    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        pvs,
        mass_range=(strange_builders.dielectron_min_mass(), mass_max),
        electron_requirements='loose')

    ks0 = strange_builders.make_combination(
        f'KS0 -> {SHORT_LIVED} pi+ pi-', [dielectron, pions, pions],
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=(mass_min, mass_max),
        name=f'{name}_Combiner')

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


@register_line_builder(all_lines)
@configurable
def lambda02ppi_line(name="Hlt2RD_Lambda0ToPPi", prescale=1e-4):
    """ Control mode for analyses using Lambda0 decays """
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [strange_builders.build_lambda0(make_pvs())],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


@register_line_builder(all_lines)
@configurable
def lambda02ppi_tight_line(name="Hlt2RD_Lambda0ToPPi_Tight", prescale=1e-3):
    """ Control mode for analyses using Lambda0 decays """
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() +
        [strange_builders.build_tight_lambda0(make_pvs())],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


@register_line_builder(all_lines)
@configurable
def sigmaplus2ppi0_resolved_line(name="Hlt2RD_SigmaPlusToPPi0Resolved"):
    """ Normalization mode for Sigma+ decays """
    pvs = make_pvs()
    protons = strange_builders.make_protons(pvs)
    pi0 = strange_builders.make_resolved_neutral_pions(pvs)
    sigma = strange_builders.make_combination(
        '[Sigma+ -> p+ pi0]cc', [protons, pi0],
        pvs,
        mass_range=strange_builders.sigma_mass_bounds(),
        build_requirements=strange_builders.
        requirements_for_prompt_neutral_combination,
        can_reco_vertex=False,
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.
        MONITORING_VARIABLES_CONTROL_NO_VERTEX,
    )


@register_line_builder(all_lines)
@configurable
def sigmaplus2ppi0_merged_line(name="Hlt2RD_SigmaPlusToPPi0Merged"):
    """ Normalization mode for Sigma+ decays """
    pvs = make_pvs()
    protons = strange_builders.make_protons(pvs)
    pi0 = strange_builders.make_merged_neutral_pions(pvs)
    sigma = strange_builders.make_combination(
        '[Sigma+ -> p+ pi0]cc', [protons, pi0],
        pvs,
        mass_range=strange_builders.sigma_mass_bounds(),
        build_requirements=strange_builders.
        requirements_for_prompt_neutral_combination,
        can_reco_vertex=False,
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.
        MONITORING_VARIABLES_CONTROL_NO_VERTEX,
    )


@register_line_builder(all_lines)
@configurable
def xi02l0pi0_resolved_line(name="Hlt2RD_Xi0ToLambdaPi0Resolved"):
    """ Normalization mode for Xi0 decays """
    pvs = make_pvs()
    l0 = strange_builders.build_tight_lambda0(pvs)
    pi0 = strange_builders.make_resolved_neutral_pions(pvs)
    xi = strange_builders.make_combination(
        '[Xi0 -> Lambda0 pi0]cc', [l0, pi0],
        pvs,
        mass_range=strange_builders.xi_mass_bounds(),
        build_requirements=strange_builders.
        requirements_for_prompt_neutral_combination,
        can_reco_vertex=False,
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        monitoring_variables=strange_builders.
        MONITORING_VARIABLES_CONTROL_NO_VERTEX,
    )


@register_line_builder(all_lines)
@configurable
def xi02l0pi0_merged_line(name="Hlt2RD_Xi0ToLambdaPi0Merged"):
    """ Normalization mode for Xi0 decays """
    pvs = make_pvs()
    l0 = strange_builders.build_tight_lambda0(pvs)
    pi0 = strange_builders.make_merged_neutral_pions(pvs)
    xi = strange_builders.make_combination(
        '[Xi0 -> Lambda0 pi0]cc', [l0, pi0],
        pvs,
        mass_range=strange_builders.xi_mass_bounds(),
        build_requirements=strange_builders.
        requirements_for_prompt_neutral_combination,
        can_reco_vertex=False,
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        monitoring_variables=strange_builders.
        MONITORING_VARIABLES_CONTROL_NO_VERTEX,
    )


@register_line_builder(all_lines)
@configurable
def ximinus2l0pi_line(name="Hlt2RD_XiMinusToLambdaPi"):
    """ Normalization mode for Xi- decays """
    pvs = make_pvs()
    l0 = strange_builders.build_tight_lambda0(pvs)
    pions = strange_builders.make_tight_pions(pvs)

    xi = strange_builders.make_combination(
        '[Xi- -> Lambda0 pi-]cc', [l0, pions],
        pvs,
        build_requirements=strange_builders.
        requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.xi_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


@register_line_builder(all_lines)
@configurable
def omegaminus2l0k_line(name="Hlt2RD_OmegaMinusToLambdaK"):
    """ Normalization mode for Omega- decays """
    pvs = make_pvs()
    l0 = strange_builders.build_lambda0(pvs)
    kaons = strange_builders.make_kaons(pvs)
    omega = strange_builders.make_combination(
        '[Omega- -> Lambda0 K-]cc', [l0, kaons],
        pvs,
        build_requirements=strange_builders.
        requirements_for_prompt_combination,
        mass_range=strange_builders.omega_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [omega],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
    )


#####################
# KS0 TRIGGER LINES #
#####################


@register_line_builder(all_lines)
@configurable
def ks02mumu_line(name="Hlt2RD_KS0ToMuMu"):
    """
    Select KS0 -> mu+ mu- candidates.

    Control trigger lines:

    - Hlt2RD_KS0ToPiPi => normalization
    - Hlt2RD_KS0ToPiMu => control
    - Hlt2RD_KS0ToMuMu_SB => background
    - Hlt2RD_KS0ToMuMu_SS => background
    """
    pvs = make_pvs()
    muons = strange_builders.make_muons(pvs)
    ks0 = strange_builders.make_combination(
        'KS0 -> mu+ mu-', [muons, muons],
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_tight_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def ks02emu_line(name="Hlt2RD_KS0ToEMu"):
    """
    Select [KS0 -> e+ mu-]cc candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPi => normalization
    - Hlt2RD_KS0ToPiMu => control
    - Hlt2RD_KS0ToPiE => control
    - Hlt2RD_KS0ToEMu_SS => background
    """
    pvs = make_pvs()
    electrons = strange_builders.make_electrons(pvs)
    muons = strange_builders.make_muons(pvs)
    ks0 = strange_builders.make_combination(
        '[KS0 -> mu+ e-]cc', [muons, electrons],
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def ks02ee_line(name="Hlt2RD_KS0ToEE"):
    """
    Select KS0 -> e+ e- candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPi => normalization
    - Hlt2RD_KS0ToPiE => control
    - Hlt2RD_KS0ToEE_SS => background
    """
    pvs = make_pvs()

    mass_range = strange_builders.ks0_mass_bounds()

    ks0 = strange_builders.filter_combination(
        strange_builders.make_dielectron(
            'KS0', pvs, mass_range=mass_range,
            electron_requirements='default'),
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=mass_range,
        name=f'{name}_DielectronFilter')

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def ks02x0mumu_line(name="Hlt2RD_KS0ToX0MuMu_Inclusive"):
    """
    Select KS0 -> X0 mu+ mu- candidates, where the neutral particle
    is not reconstructed. This line is focused on the decays where
    X0 is either a photon or a pi0.

    Control trigger lines:

    - Hlt2RD_KS0ToPiPi_Detached => normalization
    - Hlt2RD_KS0ToPiMu => control
    - Hlt2RD_KS0ToX0MuMu_SS_Inclusive => background
    """
    pvs = make_pvs()
    muons = strange_builders.make_muons(pvs)
    _, mass_max = strange_builders.ks0_mass_bounds()
    ks0 = strange_builders.make_combination(
        'KS0 -> mu+ mu-', [muons, muons],
        pvs,
        build_requirements=strange_builders.
        requirements_for_detached_combination,
        mass_range=(strange_builders.dimuon_min_mass(), mass_max),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def ks02pimu_tight_line(name="Hlt2RD_KS0ToPiMu_Tight"):
    """
    Select [KS0 -> pi+ mu- (nu_mu~)]cc candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPi_Detached_Tight => normalization
    """
    pvs = make_pvs()
    pions = strange_builders.make_tight_pions(pvs)
    muons = strange_builders.make_tight_muons(pvs)
    _, mass_max = strange_builders.ks0_mass_bounds()
    ks0 = strange_builders.make_combination(
        '[KS0 -> pi+ mu-]cc', [pions, muons],
        pvs,
        build_requirements=strange_builders.
        requirements_for_detached_combination,
        mass_range=(strange_builders.pion_muon_min_mass(), mass_max),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def ks02pie_tight_line(name="Hlt2RD_KS0ToPiE_Tight"):
    """
    Select [KS0 -> pi+ e- (nu_e~)]cc candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPi_Detached_Tight => normalization
    """
    pvs = make_pvs()
    pions = strange_builders.make_tight_pions(pvs)
    electrons = strange_builders.make_tight_electrons(pvs)
    _, mass_max = strange_builders.ks0_mass_bounds()
    ks0 = strange_builders.make_combination(
        '[KS0 -> pi+ mu-]cc', [pions, electrons],
        pvs,
        build_requirements=strange_builders.
        requirements_for_detached_combination,
        mass_range=(strange_builders.pion_electron_min_mass(), mass_max),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def ks02mumumumu_line(name="Hlt2RD_KS0ToMuMuMuMu"):
    """
    Select KS0 -> mu+ mu- mu+ mu- candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPiEE => normalization
    - Hlt2RD_KS0ToPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiMu_Loose => control
    """
    pvs = make_pvs()
    muons = strange_builders.make_loose_muons(pvs)
    ks0 = strange_builders.make_combination(
        'KS0 -> mu+ mu+ mu- mu-', [muons, muons, muons, muons],
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def ks02pipimumu_line(name="Hlt2RD_KS0ToPiPiMuMu"):
    """
    Select KS0 -> pi+ pi- mu+ mu- candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPiEE => normalization
    - Hlt2RD_KS0ToPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiMu_Loose => control
    """
    pvs = make_pvs()
    pions = strange_builders.make_loose_pions(pvs)
    muons = strange_builders.make_loose_muons(pvs)
    ks0 = strange_builders.make_combination(
        'KS0 -> mu+ mu- pi+ pi-', [muons, muons, pions, pions],
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def ks02mumuee_line(name="Hlt2RD_KS0ToMuMuEE"):
    """
    Select KS0 -> mu+ mu- e+ e- candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPiEE => normalization
    - Hlt2RD_KS0ToPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiMu_Loose => control
    - Hlt2RD_KS0ToPiE_Loose => control
    """
    pvs = make_pvs()
    muons = strange_builders.make_loose_muons(pvs)

    mass_min, mass_max = strange_builders.ks0_mass_bounds()

    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        pvs,
        mass_range=(strange_builders.dielectron_min_mass(), mass_max),
        electron_requirements='loose')

    ks0 = strange_builders.make_combination(
        f'KS0 -> mu+ mu- {SHORT_LIVED}', [muons, muons, dielectron],
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=(mass_min, mass_max),
        name=f'{name}_Combiner')

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def ks02eeee_line(name="Hlt2RD_KS0ToEEEE"):
    """
    Select KS0 -> e+ e- e+ e- candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPiEE => normalization
    - Hlt2RD_KS0ToPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiE_Loose => control
    """
    pvs = make_pvs()

    mass_min, mass_max = strange_builders.ks0_mass_bounds()

    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        pvs,
        mass_range=(strange_builders.dielectron_min_mass(), mass_max),
        electron_requirements='loose')

    # Note that despite we ensure that within a pair of electrons there are
    # not two particles adding the same bremsstrahlung photon, we can not
    # do the same when we combine the four
    ks0 = strange_builders.make_combination(
        f'KS0 -> {SHORT_LIVED} {SHORT_LIVED}', [dielectron, dielectron],
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=(mass_min, mass_max),
        name=f'{name}_Combiner')

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def ks02mumuemu_line(name="Hlt2RD_KS0ToMuMuEMu"):
    """
    Select [KS0 -> mu+ mu- e+ mu-]cc candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPiEE => normalization
    - Hlt2RD_KS0ToPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiMu_Loose => control
    - Hlt2RD_KS0ToPiE_Loose => control
    """
    pvs = make_pvs()
    muons = strange_builders.make_loose_muons(pvs)
    electrons = strange_builders.make_loose_electrons(pvs)
    ks0 = strange_builders.make_combination(
        '[KS0 -> mu+ mu+ mu- e-]cc', [muons, muons, muons, electrons],
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def ks02mumumumu_intermediate_neutral_line(
        name="Hlt2RD_KS0ToMuMuMuMu_IntermediateNeutral"):
    """
    Select KS0 -> mu+ mu- mu+ mu- candidates where muons can come from
    an intermediate neutral particle.

    Control trigger lines:

    - Hlt2RD_KS0ToPiPiEE => normalization
    - Hlt2RD_KS0ToPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiMu_Loose => control
    """
    pvs = make_pvs()
    mass_min, mass_max = strange_builders.ks0_mass_bounds()
    x0 = strange_builders.build_dimuon_intermediate_neutral(
        LONG_LIVED, pvs, mass_max=mass_max)
    ks0 = strange_builders.make_combination(
        f'KS0 -> {LONG_LIVED} {LONG_LIVED}', [x0, x0],
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=(mass_min, mass_max),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def ks02mumuee_intermediate_neutral_line(
        name="Hlt2RD_KS0ToMuMuEE_IntermediateNeutral"):
    """
    Select KS0 -> mu+ mu- e+ e- candidates where electrons can come from
    an intermediate neutral particle.

    Control trigger lines:

    - Hlt2RD_KS0ToPiPiEE => normalization
    - Hlt2RD_KS0ToPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiMu_Loose => control
    - Hlt2RD_KS0ToPiE_Loose => control
    """
    pvs = make_pvs()

    mass_min, mass_max = strange_builders.ks0_mass_bounds()

    x01 = strange_builders.build_dimuon_intermediate_neutral(
        LONG_LIVED, pvs, mass_max=mass_max)
    x02 = strange_builders.build_dielectron_intermediate_neutral(
        LONG_LIVED_ALT, pvs, mass_max=mass_max)

    ks0 = strange_builders.make_combination(
        f'KS0 -> {LONG_LIVED} {LONG_LIVED_ALT}', [x01, x02],
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=(mass_min, mass_max),
        name=f'{name}_Combiner')

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def ks02eeee_intermediate_neutral_line(
        name="Hlt2RD_KS0ToEEEE_IntermediateNeutral"):
    """
    Select KS0 -> e+ e- e+ e- candidates where electrons can come from
    an intermediate neutral particle.

    Control trigger lines:

    - Hlt2RD_KS0ToPiPiEE => normalization
    - Hlt2RD_KS0ToPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiE_Loose => control
    """
    pvs = make_pvs()

    mass_min, mass_max = strange_builders.ks0_mass_bounds()

    x0 = strange_builders.build_dielectron_intermediate_neutral(
        LONG_LIVED, pvs, mass_max=mass_max)

    # Note that despite we ensure that within a pair of electrons there are
    # not two particles adding the same bremsstrahlung photon, we can not
    # do the same when we combine the four
    ks0 = strange_builders.make_combination(
        f'KS0 -> {LONG_LIVED} {LONG_LIVED}', [x0, x0],
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=(mass_min, mass_max),
        name=f'{name}_Combiner')

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def ks02mumuemu_intermediate_neutral_line(
        name="Hlt2RD_KS0ToMuMuEMu_IntermediateNeutral"):
    """
    Select [KS0 -> mu+ mu- e+ mu-]cc candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPiEE => normalization
    - Hlt2RD_KS0ToPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiMu_Loose => control
    - Hlt2RD_KS0ToPiE_Loose => control
    """
    pvs = make_pvs()
    muons = strange_builders.make_loose_muons(pvs)
    electrons = strange_builders.make_loose_electrons(pvs)

    mass_min, mass_max = strange_builders.ks0_mass_bounds()

    x01 = strange_builders.make_combination(
        f'[{LONG_LIVED} -> mu+ e-]cc', [muons, electrons],
        pvs,
        build_requirements=strange_builders.
        requirements_for_detached_combination,
        mass_range=(strange_builders.muon_electron_min_mass(), mass_max),
        name=f'{name}_IntermediateNeutral_Combiner')

    x02 = strange_builders.build_dimuon_intermediate_neutral(
        LONG_LIVED_ALT, pvs, mass_max=mass_max)

    ks0 = strange_builders.make_combination(
        f'[KS0 -> {LONG_LIVED} {LONG_LIVED_ALT}]cc', [x01, x02],
        pvs,
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=(mass_min, mass_max),
        name=f'{name}_Combiner')

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


####################
# K+ TRIGGER LINES #
####################


@register_line_builder(all_lines)
@configurable
def kplus2pimumu_line(name="Hlt2RD_KpToPiMuMu"):
    """
    Select [K+ -> pi+ mu+ mu-]cc candidates

    Control trigger lines:

    - Hlt2RD_KpToPiPiPi => normalization
    """
    pvs = make_pvs()
    pions = strange_builders.make_pions(pvs)
    muons = strange_builders.make_muons(pvs)
    kplus = strange_builders.make_combination(
        '[K+ -> mu+ mu- pi+]cc', [muons, muons, pions],
        pvs,
        build_requirements=strange_builders.
        requirements_for_prompt_combination,
        mass_range=strange_builders.kplus_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def kplus2piee_line(name="Hlt2RD_KpToPiEE"):
    """
    Select [K+ -> pi+ e+ e-]cc candidates

    Control trigger lines:

    - Hlt2RD_KpToPiPiPi => normalization
    - Hlt2RD_KS0ToPiE_Loose => control
    """
    pvs = make_pvs()
    pions = strange_builders.make_pions(pvs)
    mass_min, mass_max = strange_builders.kplus_mass_bounds()
    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        pvs,
        mass_range=(strange_builders.dielectron_min_mass(), mass_max),
        electron_requirements='default')
    kplus = strange_builders.make_combination(
        f'[K+ -> {SHORT_LIVED} pi+]cc', [dielectron, pions],
        pvs,
        build_requirements=strange_builders.
        requirements_for_prompt_combination,
        mass_range=(mass_min, mass_max),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


#########################
# LAMBDA0 TRIGGER LINES #
#########################


@register_line_builder(all_lines)
@configurable
def lambda02pmu_line(name="Hlt2RD_Lambda0ToPMu"):
    """
    Select [Lambda0 -> p+ mu- nu_mu~]cc candidates

    Control trigger lines:

    - Hlt2RD_Lambda0ToPPi_Tight => normalization
    """
    pvs = make_pvs()
    protons = strange_builders.make_tight_protons(pvs)
    muons = strange_builders.make_tight_muons(pvs)
    l0 = strange_builders.make_combination(
        '[Lambda0 -> p+ mu-]cc', [protons, muons],
        pvs,
        build_requirements=strange_builders.
        tight_requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.lambda0_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [l0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def lambda02pe_line(name="Hlt2RD_Lambda0ToPE"):
    """
    Select [Lambda0 -> p+ e- nu_e~]cc candidates

    Control trigger lines:

    - Hlt2RD_Lambda0ToPPi_Tight => normalization
    """
    pvs = make_pvs()
    protons = strange_builders.make_tight_protons(pvs)
    electrons = strange_builders.make_tight_electrons(pvs)
    l0 = strange_builders.make_combination(
        '[Lambda0 -> p+ e-]cc', [protons, electrons],
        pvs,
        build_requirements=strange_builders.
        tight_requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.lambda0_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [l0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def lambda02ppiee_line(name="Hlt2RD_Lambda0ToPPiEE"):
    """
    Select [Lambda0 -> p+ pi- e+ e-]cc candidates

    Control trigger lines:

    - Hlt2RD_Lambda0ToPPi_Tight => normalization
    """
    pvs = make_pvs()
    protons = strange_builders.make_tight_protons(pvs)
    pions = strange_builders.make_tight_pions(pvs)

    mass_min, mass_max = strange_builders.lambda0_mass_bounds()

    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        pvs,
        mass_range=(strange_builders.dielectron_min_mass(), mass_max),
        electron_requirements='tight')

    l0 = strange_builders.make_combination(
        f'[Lambda0 -> p+ pi- {SHORT_LIVED}]cc', [protons, pions, dielectron],
        pvs,
        build_requirements=strange_builders.
        tight_requirements_for_detached_combination_no_ip,
        mass_range=(mass_min, mass_max),
        name=f'{name}_Combiner')

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [l0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


########################
# SIGMA+ TRIGGER LINES #
########################


@register_line_builder(all_lines)
@configurable
def sigmaplus2pmumu_line(name="Hlt2RD_SigmaPlusToPMuMu"):
    """
    Select [Sigma+ -> p+ mu+ mu-]cc candidates

    Control trigger lines:

    - Hlt2RD_SigmaPlusToPPi0Resolved => normalization
    - Hlt2RD_SigmaPlusToPPi0Merged => normalization
    - Hlt2RD_KpToPiPiPi => control
    """
    pvs = make_pvs()
    protons = strange_builders.make_protons(pvs)
    muons = strange_builders.make_muons(pvs)
    sigma = strange_builders.make_combination(
        '[Sigma+ -> p+ mu+ mu-]cc', [protons, muons, muons],
        pvs,
        build_requirements=strange_builders.
        requirements_for_prompt_combination,
        mass_range=strange_builders.sigma_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def sigmaplus2pee_line(name="Hlt2RD_SigmaPlusToPEE"):
    """
    Select [Sigma+ -> p+ e+ e-]cc candidates

    Control trigger lines:

    - Hlt2RD_SigmaPlusToPPi0Resolved => normalization
    - Hlt2RD_SigmaPlusToPPi0Merged => normalization
    - Hlt2RD_KpToPiPiPi => control
    """
    pvs = make_pvs()
    protons = strange_builders.make_protons(pvs)

    mass_min, mass_max = strange_builders.sigma_mass_bounds()

    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        pvs,
        mass_range=(strange_builders.dielectron_min_mass(), mass_max),
        electron_requirements='default')

    sigma = strange_builders.make_combination(
        f'[Sigma+ -> p+ {SHORT_LIVED}]cc', [protons, dielectron],
        pvs,
        build_requirements=strange_builders.
        requirements_for_prompt_combination,
        mass_range=(mass_min, mass_max),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def sigmaplus2pemu_line(name="Hlt2RD_SigmaPlusToPEMu"):
    """
    Select [Sigma+ -> p+ [mu+ e-]cc]cc candidates

    Control trigger lines:

    - Hlt2RD_SigmaPlusToPPi0Resolved => normalization
    - Hlt2RD_SigmaPlusToPPi0Merged => normalization
    - Hlt2RD_KpToPiPiPi => control
    """
    pvs = make_pvs()
    protons = strange_builders.make_protons(pvs)
    muons = strange_builders.make_muons(pvs)
    electrons = strange_builders.make_electrons(pvs, add_brem=True)
    mass_min, mass_max = strange_builders.sigma_mass_bounds()
    x0 = strange_builders.make_combination(
        f'[{SHORT_LIVED} -> mu+ e-]cc', [muons, electrons],
        pvs,
        build_requirements=strange_builders.NO_REQUIREMENTS,
        mass_range=(strange_builders.muon_electron_min_mass(), mass_max),
        name=f'{name}_IntermediateNeutral_Combiner')
    sigma = strange_builders.make_combination(
        f'[Sigma+ -> p+ {SHORT_LIVED}]cc', [protons, x0],
        pvs,
        build_requirements=strange_builders.
        requirements_for_prompt_combination,
        mass_range=(mass_min, mass_max),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


#####################
# XI0 TRIGGER LINES #
#####################


@register_line_builder(all_lines)
@configurable
def xi02ppi_line(name="Hlt2RD_Xi0ToPPi"):
    """
    Select [Xi0 -> p+ pi-]cc candidates

    Control trigger lines:

    - Hlt2RD_Xi0ToLambdaPi0Resolved => normalization
    - Hlt2RD_Xi0ToLambdaPi0Merged => normalization
    - Hlt2RD_Lambda0ToPPi_Tight => control
    """
    pvs = make_pvs()
    protons = strange_builders.make_tight_protons(pvs)
    pions = strange_builders.make_tight_pions(pvs)
    xi = strange_builders.make_combination(
        '[Xi0 -> p+ pi-]cc', [protons, pions],
        pvs,
        build_requirements=strange_builders.
        requirements_for_prompt_combination,
        mass_range=strange_builders.xi_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


#####################
# XI- TRIGGER LINES #
#####################


@register_line_builder(all_lines)
@configurable
def ximinus2ppipi_line(name="Hlt2RD_XiMinusToPPiPi"):
    """
    Select [Xi- -> p+ pi- pi-]cc candidates

    Control trigger lines:

    - Hlt2RD_XiMinusToLambdaPi => normalization
    """
    pvs = make_pvs()
    protons = strange_builders.make_tight_protons(pvs)
    pions = strange_builders.make_tight_pions(pvs)
    xi = strange_builders.make_combination(
        '[Xi- -> p+ pi- pi-]cc', [protons, pions, pions],
        pvs,
        build_requirements=strange_builders.
        requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.xi_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def ximinus2l0munu_line(name="Hlt2RD_XiMinusToLambdaMu"):
    """
    Select [Xi- -> Lambda0 (-> p+ pi-) mu+ nu_mu]cc candidates

    Control trigger lines:

    - Hlt2RD_XiMinusToLambdaPi => normalization
    - Hlt2RD_Lambda0ToPPi_Tight => control
    """
    pvs = make_pvs()
    l0 = strange_builders.build_tight_lambda0(pvs)
    muons = strange_builders.make_tight_muons(pvs)
    xi = strange_builders.make_combination(
        '[Xi- -> Lambda0 mu-]cc', [l0, muons],
        pvs,
        build_requirements=strange_builders.
        requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.xi_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


@register_line_builder(all_lines)
@configurable
def ximinus2l0e_line(name="Hlt2RD_XiMinusToLambdaE"):
    """
    Select [Xi- -> Lambda0 (-> p+ pi-) e+ nu_e]cc candidates

    Control trigger lines:

    - Hlt2RD_XiMinusToLambdaPi => normalization
    - Hlt2RD_Lambda0ToPPi_Tight => control
    """
    pvs = make_pvs()
    l0 = strange_builders.build_tight_lambda0(pvs)
    electrons = strange_builders.make_tight_electrons(pvs)
    xi = strange_builders.make_combination(
        '[Xi- -> Lambda0 e-]cc', [l0, electrons],
        pvs,
        build_requirements=strange_builders.
        requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.xi_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )


########################
# OMEGA- TRIGGER LINES #
########################


@register_line_builder(all_lines)
@configurable
def omegaminus2l0pi_line(name="Hlt2RD_OmegaMinusToLambdaPi"):
    """
    Select [Omega- -> Lambda0 (-> p+ pi-) pi-]cc candidates

    Control trigger lines:

    - Hlt2RD_OmegaMinusToLambdaK => normalization
    - Hlt2RD_Lambda0ToPPi => control
    """
    pvs = make_pvs()
    l0 = strange_builders.build_lambda0(pvs)
    pions = strange_builders.make_pions(pvs)
    omega = strange_builders.make_combination(
        '[Omega- -> Lambda0 pi-]cc', [l0, pions],
        pvs,
        build_requirements=strange_builders.
        requirements_for_prompt_combination,
        mass_range=strange_builders.omega_mass_bounds(),
        name=f'{name}_Combiner')
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [omega],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
    )
