###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""
from Hlt2Conf.lines.b_to_open_charm.utils import check_process

from GaudiKernel.SystemOfUnits import MeV, GeV, picosecond

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import cbaryon_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder


##############################################
# Form the Lb->LcPi
##############################################
@check_process
def make_LbToLcpPi_LcpToPKPi(process):
    pion = basic_builder.make_tight_pions(pi_pidk_max=0)
    cbaryon = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_lb(
        particles=[cbaryon, pion],
        descriptors=['[Lambda_b0 -> Lambda_c+ pi-]cc'],
        am_min=5350 * MeV,
        am_max=7000 * MeV,
        am_min_vtx=5350 * MeV,
        am_max_vtx=7000 * MeV,
        sum_pt_min=6 * GeV,
        bpvltime_min=0.3 * picosecond)
    return line_alg


# Form the Lb->LcPi, Lc -> p h h
# Lc -> p K K
@check_process
def make_LbToLcpPi_LcpToPKK(process):
    pion = basic_builder.make_tight_pions()
    cbaryon = cbaryon_builder.make_lc_to_pkk()
    line_alg = b_builder.make_lb(
        particles=[cbaryon, pion],
        descriptors=['[Lambda_b0 -> Lambda_c+ pi-]cc'])
    return line_alg


# WS: Lc -> p K K
@check_process
def make_LbToLcpPiWS_LcpToPKK(process):
    pion = basic_builder.make_tight_pions()
    cbaryon = cbaryon_builder.make_lc_to_pkk()
    line_alg = b_builder.make_lb(
        particles=[cbaryon, pion],
        descriptors=['[Lambda_b0 -> Lambda_c+ pi+]cc'])
    return line_alg


# Lc -> p pi pi
@check_process
def make_LbToLcpPi_LcpToPPiPi(process):
    pion = basic_builder.make_tight_pions()
    cbaryon = cbaryon_builder.make_lc_to_ppipi()
    line_alg = b_builder.make_lb(
        particles=[cbaryon, pion],
        descriptors=['[Lambda_b0 -> Lambda_c+ pi-]cc'])
    return line_alg


# WS: Lc -> p pi pi
@check_process
def make_LbToLcpPiWS_LcpToPPiPi(process):
    pion = basic_builder.make_tight_pions()
    cbaryon = cbaryon_builder.make_lc_to_ppipi()
    line_alg = b_builder.make_lb(
        particles=[cbaryon, pion],
        descriptors=['[Lambda_b0 -> Lambda_c+ pi+]cc'])
    return line_alg


# Lb -> Lc h, Lc -> Lambda h
@check_process
def make_LbToLcpK_LcpToLambdaLLK(process):
    kaon = basic_builder.make_tight_kaons()
    cbaryon = cbaryon_builder.make_lc_to_lambdak(
        lm=basic_builder.make_lambda_LL())
    line_alg = b_builder.make_lb(
        particles=[cbaryon, kaon],
        descriptors=['[Lambda_b0 -> Lambda_c+ K-]cc'])
    return line_alg


@check_process
def make_LbToLcpK_LcpToLambdaDDK(process):
    kaon = basic_builder.make_tight_kaons()
    cbaryon = cbaryon_builder.make_lc_to_lambdak(
        lm=basic_builder.make_lambda_DD())
    line_alg = b_builder.make_lb(
        particles=[cbaryon, kaon],
        descriptors=['[Lambda_b0 -> Lambda_c+ K-]cc'])
    return line_alg


@check_process
def make_LbToLcpK_LcpToLambdaLLPi(process):
    kaon = basic_builder.make_tight_kaons()
    cbaryon = cbaryon_builder.make_lc_to_lambdapi(
        lm=basic_builder.make_lambda_LL())
    line_alg = b_builder.make_lb(
        particles=[cbaryon, kaon],
        descriptors=['[Lambda_b0 -> Lambda_c+ K-]cc'])
    return line_alg


@check_process
def make_LbToLcpK_LcpToLambdaDDPi(process):
    kaon = basic_builder.make_tight_kaons()
    cbaryon = cbaryon_builder.make_lc_to_lambdapi(
        lm=basic_builder.make_lambda_DD())
    line_alg = b_builder.make_lb(
        particles=[cbaryon, kaon],
        descriptors=['[Lambda_b0 -> Lambda_c+ K-]cc'])
    return line_alg


@check_process
def make_LbToLcpPi_LcpToLambdaLLK(process):
    pion = basic_builder.make_tight_pions()
    cbaryon = cbaryon_builder.make_lc_to_lambdak(
        lm=basic_builder.make_lambda_LL())
    line_alg = b_builder.make_lb(
        particles=[cbaryon, pion],
        descriptors=['[Lambda_b0 -> Lambda_c+ pi-]cc'])
    return line_alg


@check_process
def make_LbToLcpPi_LcpToLambdaDDK(process):
    pion = basic_builder.make_tight_pions()
    cbaryon = cbaryon_builder.make_lc_to_lambdak(
        lm=basic_builder.make_lambda_DD())
    line_alg = b_builder.make_lb(
        particles=[cbaryon, pion],
        descriptors=['[Lambda_b0 -> Lambda_c+ pi-]cc'])
    return line_alg


@check_process
def make_LbToLcpPi_LcpToLambdaLLPi(process):
    pion = basic_builder.make_tight_pions()
    cbaryon = cbaryon_builder.make_lc_to_lambdapi(
        lm=basic_builder.make_lambda_LL())
    line_alg = b_builder.make_lb(
        particles=[cbaryon, pion],
        descriptors=['[Lambda_b0 -> Lambda_c+ pi-]cc'])
    return line_alg


@check_process
def make_LbToLcpPi_LcpToLambdaDDPi(process):
    pion = basic_builder.make_tight_pions()
    cbaryon = cbaryon_builder.make_lc_to_lambdapi(
        lm=basic_builder.make_lambda_DD())
    line_alg = b_builder.make_lb(
        particles=[cbaryon, pion],
        descriptors=['[Lambda_b0 -> Lambda_c+ pi-]cc'])
    return line_alg


# Lb -> Lc h, Lc -> p KS
@check_process
def make_LbToLcpK_LcpToPKsLL(process):
    kaon = basic_builder.make_tight_kaons()
    cbaryon = cbaryon_builder.make_lc_to_pks(ks=basic_builder.make_ks_LL())
    line_alg = b_builder.make_lb(
        particles=[cbaryon, kaon],
        descriptors=['[Lambda_b0 -> Lambda_c+ K-]cc'])
    return line_alg


@check_process
def make_LbToLcpK_LcpToPKsDD(process):
    kaon = basic_builder.make_tight_kaons()
    cbaryon = cbaryon_builder.make_lc_to_pks(ks=basic_builder.make_ks_DD())
    line_alg = b_builder.make_lb(
        particles=[cbaryon, kaon],
        descriptors=['[Lambda_b0 -> Lambda_c+ K-]cc'])
    return line_alg


@check_process
def make_LbToLcpPi_LcpToPKsLL(process):
    pion = basic_builder.make_tight_pions()
    cbaryon = cbaryon_builder.make_lc_to_pks(ks=basic_builder.make_ks_LL())
    line_alg = b_builder.make_lb(
        particles=[cbaryon, pion],
        descriptors=['[Lambda_b0 -> Lambda_c+ pi-]cc'])
    return line_alg


@check_process
def make_LbToLcpPi_LcpToPKsDD(process):
    pion = basic_builder.make_tight_pions()
    cbaryon = cbaryon_builder.make_lc_to_pks(ks=basic_builder.make_ks_DD())
    line_alg = b_builder.make_lb(
        particles=[cbaryon, pion],
        descriptors=['[Lambda_b0 -> Lambda_c+ pi-]cc'])
    return line_alg


##############################################
# Form the Lb -> Xic+ K-
##############################################
@check_process
def make_LbToXicpK_XicpToPKPi(process):
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_tight_kaons()
    cbaryon = cbaryon_builder.make_xicp_to_pkpi()
    line_alg = b_builder.make_lb(
        particles=[cbaryon, kaon], descriptors=['[Lambda_b0 -> Xi_c+ K-]cc'])
    return line_alg


##############################################
# Form the Xib0 -> Xic+ Pi-
##############################################
@check_process
def make_Xib0ToXicpPi_XicpToPKPi(process):
    pion = basic_builder.make_tight_pions()
    cbaryon = cbaryon_builder.make_xicp_to_pkpi()
    line_alg = b_builder.make_xib(
        particles=[cbaryon, pion], descriptors=['[Xi_b0 -> Xi_c+ pi-]cc'])
    return line_alg


##############################################
# Form the Xib- -> Xic0 Pi-
##############################################
@check_process
def make_XibmToXic0Pi_Xic0ToPKKPi(process):
    pion = basic_builder.make_tight_pions()
    cbaryon = cbaryon_builder.make_xic0_to_pkkpi()
    line_alg = b_builder.make_xib(
        particles=[cbaryon, pion], descriptors=['[Xi_b- -> Xi_c0 pi-]cc'])
    return line_alg


##############################################
# Form the Xib- -> Xic0 Pi+
##############################################
@check_process
def make_XibmToXic0PiWS_Xic0ToPKKPi(process):
    pion = basic_builder.make_tight_pions()
    cbaryon = cbaryon_builder.make_xic0_to_pkkpi()
    line_alg = b_builder.make_xib(
        particles=[cbaryon, pion], descriptors=['[Xi_b- -> Xi_c0 pi+]cc'])
    return line_alg


##############################################
# Form the Omegab- -> Omegac0 Pi-
##############################################
@check_process
def make_OmbmToOmc0Pi_Omc0ToPKKPi(process):
    pion = basic_builder.make_tight_pions()
    cbaryon = cbaryon_builder.make_omegac0_to_pkkpi()
    line_alg = b_builder.make_omegab(
        particles=[cbaryon, pion],
        descriptors=['[Omega_b- -> Omega_c0 pi-]cc'],
    )
    return line_alg


##############################################
# Form the Omegab- -> Omegac0 Pi+ (WS)
##############################################
@check_process
def make_OmbmToOmc0PiWS_Omc0ToPKKPi(process):
    pion = basic_builder.make_tight_pions()
    cbaryon = cbaryon_builder.make_omegac0_to_pkkpi()
    line_alg = b_builder.make_omegab(
        particles=[cbaryon, pion],
        descriptors=['[Omega_b- -> Omega_c0 pi+]cc'],
    )
    return line_alg


##############################################
# Form the Omegab- -> Omegac0 Pi- Gamma
# 1. Hadronic part to obtain a vertex, and require
# it to have a non-negiligble b-decay lifetime to
# control the rate.
# 2. Add the neutral state to cut on the mass window.
##############################################
@check_process
def make_OmbmToOmc0PiGamma_Omc0ToPKKPi(process):
    pion = basic_builder.make_tightpid_tight_pions()
    cbaryon = cbaryon_builder.make_omegac0_to_pkkpi()
    hadronic = b_builder.make_loose_omegab(
        particles=[cbaryon, pion],
        descriptors=['[Lambda_b0 -> Omega_c0 pi-]cc'],
        am_min=0. * MeV,
        am_min_vtx=0. * MeV)
    gamma = basic_builder.make_photons(et_min=2000 * MeV)
    line_alg = b_builder.make_omegab_with_neutral(
        particles=[hadronic, gamma],
        descriptor='[Omega_b- -> Lambda_b0 gamma]cc',
    )
    return line_alg


##############################################
# Form the Omegab- -> Omegac0 Pi+ Gamma (WS)
# 1. Hadronic part to obtain a vertex, and require
# it to have a non-negiligble b-decay lifetime to
# control the rate.
# 2. Add the neutral state to cut on the mass window.
##############################################
@check_process
def make_OmbmToOmc0PiGammaWS_Omc0ToPKKPi(process):
    pion = basic_builder.make_tightpid_tight_pions()
    cbaryon = cbaryon_builder.make_omegac0_to_pkkpi()
    hadronic = b_builder.make_loose_omegab(
        particles=[cbaryon, pion],
        descriptors=['[Lambda_b0 -> Omega_c0 pi+]cc'],
        am_min=0. * MeV,
        am_min_vtx=0. * MeV)
    gamma = basic_builder.make_photons(et_min=2000 * MeV)
    line_alg = b_builder.make_omegab_with_neutral(
        particles=[hadronic, gamma],
        descriptor='[Omega_b- -> Lambda_b0 gamma]cc',
    )
    return line_alg


##############################################
# Form the Lb->LcK
##############################################
@check_process
def make_LbToLcpK_LcpToPKPi(process):
    kaon = basic_builder.make_tight_kaons()
    cbaryon = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_lb(
        particles=[cbaryon, kaon],
        descriptors=['[Lambda_b0 -> Lambda_c+ K-]cc'])
    return line_alg


##############################################
# Form the Xib0 -> Xic+ K-
##############################################
@check_process
def make_Xib0ToXicpK_XicpToPKPi(process):
    kaon = basic_builder.make_tight_kaons()
    cbaryon = cbaryon_builder.make_xicp_to_pkpi()
    line_alg = b_builder.make_xib(
        particles=[cbaryon, kaon], descriptors=['[Xi_b0 -> Xi_c+ K-]cc'])
    return line_alg


##############################################
# Form the Xib- -> Xic0 K-
##############################################
@check_process
def make_XibmToXic0K_Xic0ToPKKPi(process):
    kaon = basic_builder.make_tight_kaons()
    cbaryon = cbaryon_builder.make_xic0_to_pkkpi()
    line_alg = b_builder.make_xib(
        particles=[cbaryon, kaon], descriptors=['[Xi_b- -> Xi_c0 K-]cc'])
    return line_alg


##############################################
# Form the Omegab- -> Omegac0 K-
##############################################
@check_process
def make_OmbmToOmc0K_Omc0ToPKKPi(process):
    kaon = basic_builder.make_tight_kaons()
    cbaryon = cbaryon_builder.make_omegac0_to_pkkpi()
    line_alg = b_builder.make_omegab(
        particles=[cbaryon, kaon], descriptors=['[Omega_b- -> Omega_c0 K-]cc'])
    return line_alg


###########################################################
# Form the Xi_bc+ -> Xi_cc++ Pi-,
# Xi_cc++ --> Xic+ pi+
#         --> Lc+ K- pi+ pi+
##########################################################
@check_process
def make_XibcpToXiccppPi_XiccppToXicpPi_XicpToPKPi(process):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions()
    elif process == 'hlt2':
        pion = basic_builder.make_tightpid_tight_pions()
    cbaryon = cbaryon_builder.make_xiccpp_to_xicppi()
    line_alg = b_builder.make_xibc2cx(
        particles=[cbaryon, pion], descriptors=['[Xi_bc+ -> Xi_cc++ pi-]cc'])
    return line_alg


@check_process
def make_XibcpToXiccppPi_XiccppToLcpKPiPi_LcpToPKPi(process):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions()
    elif process == 'hlt2':
        pion = basic_builder.make_tightpid_tight_pions()
    cbaryon = cbaryon_builder.make_xiccpp_to_lcpkmpippip()
    line_alg = b_builder.make_xibc2cx(
        particles=[cbaryon, pion], descriptors=['[Xi_bc+ -> Xi_cc++ pi-]cc'])
    return line_alg


###########################################################
# Form the Xi_bc0 -> Xi_cc+ Pi-, Xi_cc+ --> Xic0 pi+
# Xi_cc+ ->  Xic0 pi+
#        ->  Lc+ K- pi+
#        ->  D+ p K-
##########################################################
@check_process
def make_Xibc0ToXiccpPi_XiccpToXic0Pi_Xic0ToPKKPi(process):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions()
    elif process == 'hlt2':
        pion = basic_builder.make_tightpid_tight_pions()
    cbaryon = cbaryon_builder.make_xiccp_to_xic0pi()
    line_alg = b_builder.make_xibc2cx(
        particles=[cbaryon, pion], descriptors=['[Xi_bc0 -> Xi_cc+ pi-]cc'])
    return line_alg


@check_process
def make_Xibc0ToXiccpPi_XiccpToLcpKmPip_LcpToPKPi(process):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions()
    elif process == 'hlt2':
        pion = basic_builder.make_tightpid_tight_pions()
    cbaryon = cbaryon_builder.make_xiccp_to_lcpkmpip()
    line_alg = b_builder.make_xibc2cx(
        particles=[cbaryon, pion], descriptors=['[Xi_bc0 -> Xi_cc+ pi-]cc'])
    return line_alg


@check_process
def make_Xibc0ToXiccpPi_XiccpToPDpKm_DpToPipPipKm(process):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions()
    elif process == 'hlt2':
        pion = basic_builder.make_tightpid_tight_pions()
    cbaryon = cbaryon_builder.make_xiccp_to_dppk()
    line_alg = b_builder.make_xibc2cx(
        particles=[cbaryon, pion], descriptors=['[Xi_bc0 -> Xi_cc+ pi-]cc'])
    return line_alg


###########################################################
# Form the Xi_bc0 -> Xi_c+ Pi-, Xi_c+ --> p K- pi+
##########################################################
@check_process
def make_Xibc0ToXicpPi_XicpToPKPi(process):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions()
        cbaryon = cbaryon_builder.make_xicp_to_pkpi()
    elif process == 'hlt2':
        pion = basic_builder.make_tightpid_tight_pions()
        cbaryon = cbaryon_builder.make_tight_xicp_to_pkpi_for_xibc()
    line_alg = b_builder.make_xibc2cx(
        particles=[cbaryon, pion], descriptors=['[Xi_bc0 -> Xi_c+ pi-]cc'])
    return line_alg


###########################################################
# Form the Xi_bc0 -> Lc+ K-, Lc+ --> p K- pi+
##########################################################
@check_process
def make_Xibc0ToLcpK_LcpToPKPi(process):
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons()
        cbaryon = cbaryon_builder.make_lc_to_pkpi()
    elif process == 'hlt2':
        kaon = basic_builder.make_tightpid_tight_kaons(k_pidk_min=-2)
        cbaryon = cbaryon_builder.make_tight_lc_to_pkpi_for_xibc()
    line_alg = b_builder.make_xibc2cx(
        particles=[cbaryon, kaon], descriptors=['[Xi_bc0 -> Lambda_c+ K-]cc'])
    return line_alg


###########################################################
# Form the Xi_bc+ -> Xi_c0 Pi+, Xi_c0 --> p K- K- pi+
##########################################################
@check_process
def make_XibcpToXic0Pi_Xic0ToPKKPi(process):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions()
        cbaryon = cbaryon_builder.make_xic0_to_pkkpi()
    elif process == 'hlt2':
        pion = basic_builder.make_tightpid_tight_pions()
        cbaryon = cbaryon_builder.make_tight_xic0_to_pkkpi_for_xibc()
    line_alg = b_builder.make_xibc2cx(
        particles=[cbaryon, pion], descriptors=['[Xi_bc+ -> Xi_c0 pi+]cc'])
    return line_alg


###########################################################
# Form the Xi_bc0 -> Lc+ Pi-, Lc+ --> p K- pi+
##########################################################
@check_process
def make_Xibc0ToLcpPi_LcpToPKPi(process):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions()
        cbaryon = cbaryon_builder.make_lc_to_pkpi()
    elif process == 'hlt2':
        pion = basic_builder.make_tightpid_tight_pions()
        cbaryon = cbaryon_builder.make_tight_lc_to_pkpi_for_xibc()
    line_alg = b_builder.make_xibc2cx(
        particles=[cbaryon, pion], descriptors=['[Xi_bc0 -> Lambda_c+ pi-]cc'])
    return line_alg


##############################################################
# Form the Omega_bc0 -> Xi_c+ K-, Xi_c+ --> p K- pi+
##############################################################
@check_process
def make_Ombc0ToXicpK_XicpToPKPi(process):
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons()
        cbaryon = cbaryon_builder.make_xicp_to_pkpi()
    elif process == 'hlt2':
        kaon = basic_builder.make_tightpid_tight_kaons(k_pidk_min=-2)
        cbaryon = cbaryon_builder.make_tight_xicp_to_pkpi_for_xibc()
    line_alg = b_builder.make_xibc2cx(
        particles=[cbaryon, kaon], descriptors=['[Omega_bc0 -> Xi_c+ K-]cc'])
    return line_alg
