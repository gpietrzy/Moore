###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC BToLcpbarh lines
"""
from GaudiKernel.SystemOfUnits import GeV

from Hlt2Conf.lines.b_to_open_charm.utils import check_process

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import cbaryon_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder


###########################################################
# Form the B+ -> Lcbar- p+ pi+, Lcbar- --> pbar K+ pi-
##########################################################
@check_process
def make_BuToLcmPPi_LcmToPKPi(process):
    if process == 'spruce':
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        lc = cbaryon_builder.make_lc_to_pkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None)
    elif process == 'hlt2':
        pion = basic_builder.make_soft_pions(pi_pidk_max=0)
        proton = basic_builder.make_soft_protons(p_pidp_min=0)
        lc = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_b2chh(
        particles=[proton, pion, lc],
        descriptors=['[B+ -> p+ pi+ Lambda_c~-]cc'],
        sum_pt_min=6 * GeV)
    return line_alg


###########################################################
# Form the B+ -> Lcbar- p+ K+, Lcbar- --> pbar K+ pi-
##########################################################
@check_process
def make_BuToLcmPK_LcmToPKPi(process):
    if process == 'spruce':
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        lc = cbaryon_builder.make_lc_to_pkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_soft_kaons(k_pidk_min=0)
        proton = basic_builder.make_soft_protons(p_pidp_min=0)
        lc = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_b2chh(
        particles=[proton, kaon, lc],
        descriptors=['[B+ -> p+ K+ Lambda_c~-]cc'],
        sum_pt_min=6 * GeV)
    return line_alg


###########################################################
# Form the B+ -> Lc+ pbar- pi+, Lc+ --> p K- pi+
##########################################################
@check_process
def make_BuToLcpPPi_LcpToPKPi(process):
    if process == 'spruce':
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        lc = cbaryon_builder.make_lc_to_pkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None)
    elif process == 'hlt2':
        pion = basic_builder.make_soft_pions(pi_pidk_max=0)
        proton = basic_builder.make_soft_protons(p_pidp_min=0)
        lc = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_b2chh(
        particles=[proton, pion, lc],
        descriptors=['[B+ -> p~- pi+ Lambda_c+]cc'],
        sum_pt_min=6 * GeV)
    return line_alg


###########################################################
# Form the B+ -> Lc+ pbar- K+, Lc+- --> p K- pi+
##########################################################
@check_process
def make_BuToLcpPK_LcpToPKPi(process):
    if process == 'spruce':
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        lc = cbaryon_builder.make_lc_to_pkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_tightpid_soft_kaons()
        proton = basic_builder.make_tightpid_soft_protons()
        lc = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_b2chh(
        particles=[proton, kaon, lc],
        descriptors=['[B+ -> p~- K+ Lambda_c+]cc'])
    return line_alg


###########################################################
# Form the B+ -> Xicbar- p+ pi+, Xicbar- --> pbar K+ pi-
##########################################################
@check_process
def make_BuToXicmPPi_XicmToPKPi(process):
    if process == 'spruce':
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        xic = cbaryon_builder.make_xicp_to_pkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None)
    elif process == 'hlt2':
        pion = basic_builder.make_soft_pions(pi_pidk_max=0)
        proton = basic_builder.make_soft_protons(p_pidp_min=0)
        xic = cbaryon_builder.make_xicp_to_pkpi()
    line_alg = b_builder.make_b2chh(
        particles=[proton, pion, xic],
        descriptors=['[B+ -> p+ pi+ Xi_c~-]cc'],
        sum_pt_min=6 * GeV)
    return line_alg


###########################################################
# Form the B+ -> Xicbar- p+ K+, Xicbar- --> pbar K+ pi-
##########################################################
@check_process
def make_BuToXicmPK_XicmToPKPi(process):
    if process == 'spruce':
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        xic = cbaryon_builder.make_xicp_to_pkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_soft_kaons(k_pidk_min=0)
        proton = basic_builder.make_soft_protons(p_pidp_min=0)
        xic = cbaryon_builder.make_xicp_to_pkpi()
    line_alg = b_builder.make_b2chh(
        particles=[proton, kaon, xic],
        descriptors=['[B+ -> p+ K+ Xi_c~-]cc'],
        sum_pt_min=6 * GeV)
    return line_alg


###########################################################
# Form the B+ -> Xic+ pbar- pi+, Xic+ --> p K- pi+
##########################################################
@check_process
def make_BuToXicpPPi_XicpToPKPi(process):
    if process == 'spruce':
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        xic = cbaryon_builder.make_xicp_to_pkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None)
    elif process == 'hlt2':
        pion = basic_builder.make_soft_pions(pi_pidk_max=0)
        proton = basic_builder.make_soft_protons(p_pidp_min=0)
        xic = cbaryon_builder.make_xicp_to_pkpi()
    line_alg = b_builder.make_b2chh(
        particles=[proton, pion, xic],
        descriptors=['[B+ -> p~- pi+ Xi_c+]cc'],
        sum_pt_min=6 * GeV)
    return line_alg


###########################################################
# Form the B+ -> Xic+ pbar- K+, Xic+- --> p K- pi+
##########################################################
@check_process
def make_BuToXicpPK_XicpToPKPi(process):
    if process == 'spruce':
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        xic = cbaryon_builder.make_xicp_to_pkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_soft_kaons(k_pidk_min=0)
        proton = basic_builder.make_soft_protons(p_pidp_min=0)
        xic = cbaryon_builder.make_xicp_to_pkpi()
    line_alg = b_builder.make_b2chh(
        particles=[proton, kaon, xic],
        descriptors=['[B+ -> p~- K+ Xi_c+]cc'],
        sum_pt_min=6 * GeV)
    return line_alg


##############################################################
# Form the B(s)0 --> Xi_c0 p- pi+, Xi_c0 --> p K- K- pi+
##############################################################
@check_process
def make_BdToXic0PPi_Xic0ToPKKPi(process):
    if process == 'spruce':
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
        xic0 = cbaryon_builder.make_xic0_to_pkkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None)
    elif process == 'hlt2':
        proton = basic_builder.make_soft_protons()
        pion = basic_builder.make_soft_pions()
        xic0 = cbaryon_builder.make_xic0_to_pkkpi()
    line_alg = b_builder.make_b2chh(
        particles=[proton, pion, xic0],
        descriptors=['[B0 -> p~- pi+ Xi_c0]cc'])
    return line_alg


##############################################################
# Form the B(s)0 --> Xi_c0 p- K+, Xi_c0 --> p K- K- pi+
##############################################################
@check_process
def make_BdToXic0PK_Xic0ToPKKPi(process):
    if process == 'spruce':
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
        xic0 = cbaryon_builder.make_xic0_to_pkkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None)
    elif process == 'hlt2':
        proton = basic_builder.make_soft_protons()
        kaon = basic_builder.make_soft_kaons()
        xic0 = cbaryon_builder.make_xic0_to_pkkpi()
    line_alg = b_builder.make_b2chh(
        particles=[proton, kaon, xic0], descriptors=['[B0 -> p~- K+ Xi_c0]cc'])
    return line_alg


##############################################################
# Form the B(s)0 --> Omega_c0 p- pi+, Omega_c0 --> p K- K- pi+
##############################################################
@check_process
def make_BdToOmc0PPi_Omc0ToPKKPi(process):
    if process == 'spruce':
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
        omegac0 = cbaryon_builder.make_omegac0_to_pkkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None)
    elif process == 'hlt2':
        proton = basic_builder.make_soft_protons()
        pion = basic_builder.make_soft_pions()
        omegac0 = cbaryon_builder.make_omegac0_to_pkkpi()
    line_alg = b_builder.make_b2chh(
        particles=[proton, pion, omegac0],
        descriptors=['[B0 -> p~- pi+ Omega_c0]cc'])
    return line_alg


##############################################################
# Form the B(s)0 --> Omega_c0 p- K+, Omega_c0 --> p K- K- pi+
##############################################################
@check_process
def make_BdToOmc0PK_Omc0ToPKKPi(process):
    if process == 'spruce':
        proton = basic_builder.make_soft_protons(p_pidp_min=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
        omegac0 = cbaryon_builder.make_omegac0_to_pkkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None)
    elif process == 'hlt2':
        proton = basic_builder.make_soft_protons()
        kaon = basic_builder.make_soft_kaons()
        omegac0 = cbaryon_builder.make_omegac0_to_pkkpi()
    line_alg = b_builder.make_b2chh(
        particles=[proton, kaon, omegac0],
        descriptors=['[B0 -> p~- K+ Omega_c0]cc'])
    return line_alg
