###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Definition of B2OC selections for the Dh topology

Non trivial imports:
basic builders for D mesons and h, prefilters, BDT post filter

Returns:
every function returns a line_alg

"""
from GaudiKernel.SystemOfUnits import MeV

from Hlt2Conf.lines.b_to_open_charm.filters import b_sigmanet_filter

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.b_to_open_charm.utils import check_process

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder

from GaudiKernel.SystemOfUnits import mm

##############################################
# B0->D*munu, D*->D0pi
##############################################


@check_process
def make_BdToDstmMuNu_DstmToD0Pi_D0ToKsLLHH(process, MVAcut=0.7):
    muon = basic_builder.make_muons()
    dzero = d_builder.make_dzero_to_kshh(
        pi_pidk_max=20, k_pidk_min=-10, k_shorts=basic_builder.make_ks_LL())
    dzerost = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2x(
        particles=[dzerost, muon],
        descriptors=['B0 -> D*(2010)- mu+', 'B0 -> D*(2010)+ mu-'],
        am_min=2500 * MeV,
        am_max=5000 * MeV,
        am_min_vtx=2500 * MeV,
        am_max_vtx=5000 * MeV,
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVAcut)
    return line_alg


@check_process
def make_BdToDstmMuNu_DstmToD0Pi_D0ToKsDDHH(process, MVAcut=0.7):
    muon = basic_builder.make_muons()
    dzero = d_builder.make_dzero_to_kshh(
        pi_pidk_max=20, k_pidk_min=-10, k_shorts=basic_builder.make_ks_DD())
    dzerost = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2x(
        particles=[dzerost, muon],
        descriptors=['B0 -> D*(2010)- mu+', 'B0 -> D*(2010)+ mu-'],
        am_min=2500 * MeV,
        am_max=5000 * MeV,
        am_min_vtx=2500 * MeV,
        am_max_vtx=5000 * MeV,
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVAcut)
    return line_alg


@check_process
def make_BdToDstmMuNu_DstmToD0Pi_D0ToHHHH(process, MVAcut=0.7):
    muon = basic_builder.make_muons()
    dzero = d_builder.make_dzero_to_hhhh(pi_pidk_max=20, k_pidk_min=-10)
    dzerost = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2x(
        particles=[dzerost, muon],
        descriptors=['B0 -> D*(2010)- mu+', 'B0 -> D*(2010)+ mu-'],
        am_min=2500 * MeV,
        am_max=5000 * MeV,
        am_min_vtx=2500 * MeV,
        am_max_vtx=5000 * MeV,
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVAcut)
    return line_alg


@check_process
def make_BdToDstmMuNu_DstmToD0Pi_D0ToKsLLHHPi0Resolved(process, MVAcut=0.7):
    muon = basic_builder.make_muons()
    pi0 = basic_builder.make_resolved_pi0s()
    dzero = d_builder.make_dzero_to_kshhpi0(
        pi_pidk_max=20,
        k_pidk_min=-10,
        k_shorts=basic_builder.make_ks_LL(),
        pi0=pi0)
    dzerost = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2x(
        particles=[dzerost, muon],
        descriptors=['B0 -> D*(2010)- mu+', 'B0 -> D*(2010)+ mu-'],
        am_min=2500 * MeV,
        am_max=5000 * MeV,
        am_min_vtx=2500 * MeV,
        am_max_vtx=5000 * MeV,
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVAcut)
    return line_alg


@check_process
def make_BdToDstmMuNu_DstmToD0Pi_D0ToKsDDHHPi0Resolved(process, MVAcut=0.7):
    muon = basic_builder.make_muons()
    pi0 = basic_builder.make_resolved_pi0s()
    dzero = d_builder.make_dzero_to_kshhpi0(
        pi_pidk_max=20,
        k_pidk_min=-10,
        k_shorts=basic_builder.make_ks_DD(),
        pi0=pi0)
    dzerost = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2x(
        particles=[dzerost, muon],
        descriptors=['B0 -> D*(2010)- mu+', 'B0 -> D*(2010)+ mu-'],
        am_min=2500 * MeV,
        am_max=5000 * MeV,
        am_min_vtx=2500 * MeV,
        am_max_vtx=5000 * MeV,
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVAcut)
    return line_alg


@check_process
def make_BdToDstmMuNu_DstmToD0Pi_D0ToKsLLHHPi0Merged(process, MVAcut=0.7):
    muon = basic_builder.make_muons()
    pi0 = basic_builder.make_merged_pi0s()
    dzero = d_builder.make_dzero_to_kshhpi0(
        pi_pidk_max=20,
        k_pidk_min=-10,
        k_shorts=basic_builder.make_ks_LL(),
        pi0=pi0)
    dzerost = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2x(
        particles=[dzerost, muon],
        descriptors=['B0 -> D*(2010)- mu+', 'B0 -> D*(2010)+ mu-'],
        am_min=2500 * MeV,
        am_max=5000 * MeV,
        am_min_vtx=2500 * MeV,
        am_max_vtx=5000 * MeV,
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVAcut)
    return line_alg


@check_process
def make_BdToDstmMuNu_DstmToD0Pi_D0ToKsDDHHPi0Merged(process, MVAcut=0.7):
    muon = basic_builder.make_muons()
    pi0 = basic_builder.make_merged_pi0s()
    dzero = d_builder.make_dzero_to_kshhpi0(
        pi_pidk_max=20,
        k_pidk_min=-10,
        k_shorts=basic_builder.make_ks_DD(),
        pi0=pi0)
    dzerost = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2x(
        particles=[dzerost, muon],
        descriptors=['B0 -> D*(2010)- mu+', 'B0 -> D*(2010)+ mu-'],
        am_min=2500 * MeV,
        am_max=5000 * MeV,
        am_min_vtx=2500 * MeV,
        am_max_vtx=5000 * MeV,
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVAcut)
    return line_alg
