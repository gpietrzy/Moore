###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Lines for SL decays of doubly charmed baryons.

  1. Xicc+    -> Xic0     mu nu
  2. Xicc++   -> Lambdac+ mu nu
  3. Xicc++   -> Xic+     mu nu
  4. Omegacc+ -> Omegac0  mu nu
  5. Omegacc+ -> Xic0     mu nu

where the daughter charmed baryon is reconstructed via:

  Lambdac+ -> p K- pi+
  Xic+     -> p K- pi+
  Xic0     -> p K- K- pi+
  Oemgac0  -> p K- K- pi+

Additional normalization channels for Omegacc decays are used:

  6. Omegacc+ -> Omegac0 pi+
  7. Omegacc+ -> Xic0    pi+

TODO: add requirements on tracks chi2/ndf, HLT1 filtering
"""
import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.standard_particles import (
    make_has_rich_long_pions, make_has_rich_long_kaons,
    make_has_rich_long_protons, make_ismuon_long_muon)
from Hlt2Conf.algorithms_thor import (ParticleFilter, ParticleCombiner)
from .prefilters import charm_prefilters

all_lines = {}

###############################################################################
# Track filters
###############################################################################


def filter_long_pions():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 180 * MeV,
                F.P > 2 * GeV,
                F.MINIPCHI2CUT(IPChi2Cut=4., Vertices=make_pvs()),
                F.PID_K < 5.,
            ), ),
    )


def filter_long_kaons():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 250 * MeV,
                F.P > 3 * GeV,
                F.MINIPCHI2CUT(IPChi2Cut=4., Vertices=make_pvs()),
                F.PID_K > 5.,
            ), ),
    )


def filter_long_protons():
    return ParticleFilter(
        make_has_rich_long_protons(),
        F.FILTER(
            F.require_all(
                F.PT > 400 * MeV,
                F.P > 9 * GeV,
                F.MINIPCHI2CUT(IPChi2Cut=4., Vertices=make_pvs()),
                F.PID_P > 5.,
                F.PID_P - F.PID_K > 0.,
            ), ),
    )


def filter_long_muons():
    return ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.PT > 1000 * MeV,
                F.P > 6 * GeV,
                F.MINIPCHI2CUT(IPChi2Cut=2., Vertices=make_pvs()),
                F.PID_MU > 0.,
            ), ),
    )


###############################################################################
# Basic combiners
###############################################################################


def make_lc_to_pkpi(protons, kaons, pions):
    """Return a Lambda_c+ -> p K- pi+ decay maker."""
    pvs = make_pvs()
    combination12_code = F.require_all(
        F.MAXDOCACUT(0.15 * mm), F.MASS < 2242 * MeV)
    combination_code = F.require_all(
        in_range(2191 * MeV, F.MASS, 2381 * MeV),
        F.MAX(F.MINIPCHI2(pvs)) > 9., F.MAXDOCACUT(0.15 * mm), F.PT > 2 * GeV)
    vertex_code = F.require_all(F.CHI2DOF < 6.,
                                in_range(2211 * MeV, F.MASS, 2361 * MeV),
                                F.BPVFDCHI2(pvs) > 9.,
                                F.BPVDIRA(pvs) > 0.9)
    return ParticleCombiner(
        Inputs=[protons, kaons, pions],
        name='Charm_CCBaryonToCBaryonSl_make_lc_to_pkpi_{hash}',
        DecayDescriptor="[Lambda_c+ -> p+ K- pi+]cc",
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


def make_xicp_to_pkpi(protons, kaons, pions):
    """Return a Xi_c+ -> p K- pi+ decay maker."""
    pvs = make_pvs()
    combination12_code = F.require_all(
        F.MAXDOCACUT(0.15 * mm), F.MASS < 2430 * MeV)
    combination_code = F.require_all(
        in_range(2368 * MeV, F.MASS, 2568 * MeV),
        F.MAX(F.MINIPCHI2(pvs)) > 9., F.MAXDOCACUT(0.15 * mm), F.PT > 2 * GeV)
    vertex_code = F.require_all(F.CHI2DOF < 6.,
                                in_range(2388 * MeV, F.MASS, 2548 * MeV),
                                F.BPVFDCHI2(pvs) > 12.,
                                F.BPVDIRA(pvs) > 0.9)
    return ParticleCombiner(
        Inputs=[protons, kaons, pions],
        name='Charm_CCBaryonToCBaryonSl_make_xicp_to_pkpi_{hash}',
        DecayDescriptor="[Xi_c+ -> p+ K- pi+]cc",
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


def make_xic0_to_pkkpi(protons, kaons, pions):
    """Return a Xi_c0 -> p K- K- pi+ decay maker."""
    pvs = make_pvs()
    combination12_code = F.require_all(
        F.MAXDOCACUT(0.15 * mm), F.MASS < 1930 * MeV)
    combination123_code = F.require_all(
        F.MAXDOCACUT(0.15 * mm), F.MASS < 2421 * MeV)
    combination_code = F.require_all(
        in_range(2380 * MeV, F.MASS, 2560 * MeV),
        F.MAX(F.MINIPCHI2(pvs)) > 12., F.MAXDOCACUT(0.15 * mm), F.PT > 2 * GeV)
    vertex_code = F.require_all(F.CHI2DOF < 6.,
                                in_range(2400 * MeV, F.MASS, 2540 * MeV),
                                F.BPVFDCHI2(pvs) > 6.,
                                F.BPVDIRA(pvs) > 0.9)
    return ParticleCombiner(
        Inputs=[protons, kaons, kaons, pions],
        name='Charm_CCBaryonToCBaryonSl_make_xic0_to_pkkpi_{hash}',
        DecayDescriptor="[Xi_c0 -> p+ K- K- pi+]cc",
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


def make_omegac0_to_pkkpi(protons, kaons, pions):
    """Return a Omega_c0 -> p K- K- pi+ decay maker."""
    pvs = make_pvs()
    combination12_code = F.require_all(
        F.MAXDOCACUT(0.15 * mm), F.MASS < 2155 * MeV)
    combination123_code = F.require_all(
        F.MAXDOCACUT(0.15 * mm), F.MASS < 2646 * MeV)
    combination_code = F.require_all(
        in_range(2605 * MeV, F.MASS, 2785 * MeV),
        F.MAX(F.MINIPCHI2(pvs)) > 12., F.MAXDOCACUT(0.15 * mm), F.PT > 2 * GeV)
    vertex_code = F.require_all(F.CHI2DOF < 6.,
                                in_range(2625 * MeV, F.MASS, 2765 * MeV),
                                F.BPVFDCHI2(pvs) > 9.,
                                F.BPVDIRA(pvs) > 0.9)
    return ParticleCombiner(
        Inputs=[protons, kaons, kaons, pions],
        name='Charm_CCBaryonToCBaryonSl_make_omegac0_to_pkkpi_{hash}',
        DecayDescriptor="[Omega_c0 -> p+ K- K- pi+]cc",
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


def make_cc_baryon(baryon, particle, name, decay_descriptor, comb_m_max,
                   comb_pt_min, doca_max, vchi2dof_max, dz1_min, bpvdira_min):
    """Combine a charm baryon with muon."""
    pvs = make_pvs()
    combination_code = F.require_all(F.PT > comb_pt_min,
                                     F.MAXDOCACUT(doca_max))
    vertex_code = F.require_all(F.CHI2DOF < vchi2dof_max,
                                F.BPVCORRM(pvs) < comb_m_max,
                                F.BPVDIRA(pvs) > bpvdira_min,
                                F.CHILD(1, F.END_VZ) - F.END_VZ > dz1_min)

    return ParticleCombiner([baryon, particle],
                            name=name,
                            DecayDescriptor=decay_descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


###############################################################################
# Lines definitions
###############################################################################


@register_line_builder(all_lines)
def xiccpp_to_lcpmu_line(name="Hlt2Charm_XiccppToLcpMupNu", prescale=1):
    muons = filter_long_muons()
    kaons = filter_long_kaons()
    pions = filter_long_pions()
    protons = filter_long_protons()
    lambdac = make_lc_to_pkpi(protons=protons, kaons=kaons, pions=pions)
    line_alg = make_cc_baryon(
        baryon=lambdac,
        particle=muons,
        name='Charm_CCBaryonToCBaryonSl_make_cc_baryon_XiccToLcMu_{hash}',
        decay_descriptor="[Xi_cc++ -> Lambda_c+ mu+]cc",
        comb_m_max=4.8 * GeV,
        comb_pt_min=2 * GeV,
        doca_max=0.2 * mm,
        dz1_min=-0.5 * mm,
        vchi2dof_max=9.,
        bpvdira_min=0.99)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, protons, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccpp_to_xicpmu_line(name="Hlt2Charm_XiccppToXicpMupNu", prescale=1):
    muons = filter_long_muons()
    kaons = filter_long_kaons()
    pions = filter_long_pions()
    protons = filter_long_protons()
    xicp = make_xicp_to_pkpi(protons=protons, kaons=kaons, pions=pions)
    line_alg = make_cc_baryon(
        baryon=xicp,
        particle=muons,
        name='Charm_CCBaryonToCBaryonSl_make_cc_baryon_XiccToXicpMu_{hash}',
        decay_descriptor="[Xi_cc++ -> Xi_c+ mu+]cc",
        comb_m_max=4.8 * GeV,
        comb_pt_min=2 * GeV,
        doca_max=0.2 * mm,
        dz1_min=-0.5 * mm,
        vchi2dof_max=9.,
        bpvdira_min=0.99)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, protons, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccp_to_xic0mu_line(name="Hlt2Charm_XiccpToXic0MupNu", prescale=1):
    muons = filter_long_muons()
    kaons = filter_long_kaons()
    pions = filter_long_pions()
    protons = filter_long_protons()
    xic0 = make_xic0_to_pkkpi(protons=protons, kaons=kaons, pions=pions)
    line_alg = make_cc_baryon(
        baryon=xic0,
        particle=muons,
        name='Charm_CCBaryonToCBaryonSl_make_cc_baryon_XiccToXic0Mu_{hash}',
        decay_descriptor="[Xi_cc+ -> Xi_c0 mu+]cc",
        comb_m_max=4.8 * GeV,
        comb_pt_min=2 * GeV,
        doca_max=0.2 * mm,
        dz1_min=-0.5 * mm,
        vchi2dof_max=9.,
        bpvdira_min=0.99)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, protons, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccpp_to_lcpmu_wsline(name="Hlt2Charm_XiccppToLcpMumNu_WS", prescale=0.5):
    muons = filter_long_muons()
    kaons = filter_long_kaons()
    pions = filter_long_pions()
    protons = filter_long_protons()
    lambdac = make_lc_to_pkpi(protons=protons, kaons=kaons, pions=pions)
    line_alg = make_cc_baryon(
        baryon=lambdac,
        particle=muons,
        name='Charm_CCBaryonToCBaryonSl_make_cc_baryon_XiccToLcMuWS_{hash}',
        decay_descriptor="[Xi_cc++ -> Lambda_c+ mu-]cc",
        comb_m_max=4.8 * GeV,
        comb_pt_min=2 * GeV,
        doca_max=0.2 * mm,
        dz1_min=-0.5 * mm,
        vchi2dof_max=9.,
        bpvdira_min=0.99)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, protons, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccpp_to_xicpmu_wsline(name="Hlt2Charm_XiccppToXicpMumNu_WS", prescale=1):
    muons = filter_long_muons()
    kaons = filter_long_kaons()
    pions = filter_long_pions()
    protons = filter_long_protons()
    xicp = make_xicp_to_pkpi(protons=protons, kaons=kaons, pions=pions)
    line_alg = make_cc_baryon(
        baryon=xicp,
        particle=muons,
        name='Charm_CCBaryonToCBaryonSl_make_cc_baryon_XiccToXicpMuWS_{hash}',
        decay_descriptor="[Xi_cc++ -> Xi_c+ mu-]cc",
        comb_m_max=4.8 * GeV,
        comb_pt_min=2 * GeV,
        doca_max=0.2 * mm,
        dz1_min=-0.5 * mm,
        vchi2dof_max=9.,
        bpvdira_min=0.99)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, protons, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccp_to_xic0mu_wsline(name="Hlt2Charm_XiccpToXic0MumNu_WS", prescale=1):
    muons = filter_long_muons()
    kaons = filter_long_kaons()
    pions = filter_long_pions()
    protons = filter_long_protons()
    xic0 = make_xic0_to_pkkpi(protons=protons, kaons=kaons, pions=pions)
    line_alg = make_cc_baryon(
        baryon=xic0,
        particle=muons,
        name='Charm_CCBaryonToCBaryonSl_make_cc_baryon_XiccToXicpMuWS_{hash}',
        decay_descriptor="[Xi_cc+ -> Xi_c0 mu-]cc",
        comb_m_max=4.8 * GeV,
        comb_pt_min=2 * GeV,
        doca_max=0.2 * mm,
        dz1_min=-0.5 * mm,
        vchi2dof_max=9.,
        bpvdira_min=0.99)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, protons, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def omegaccp_to_omegac0mu_line(name="Hlt2Charm_OccpToOc0MupNu", prescale=1):
    muons = filter_long_muons()
    kaons = filter_long_kaons()
    pions = filter_long_pions()
    protons = filter_long_protons()
    omegac0 = make_omegac0_to_pkkpi(protons=protons, kaons=kaons, pions=pions)
    line_alg = make_cc_baryon(
        baryon=omegac0,
        particle=muons,
        name='Charm_CCBaryonToCBaryonSl_make_cc_baryon_OmccToOmc0Mu_{hash}',
        decay_descriptor="[Omega_cc+ -> Omega_c0 mu+]cc",
        comb_m_max=5.1 * GeV,
        comb_pt_min=2 * GeV,
        doca_max=0.2 * mm,
        dz1_min=-0.5 * mm,
        vchi2dof_max=9.,
        bpvdira_min=0.99)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, protons, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def omegaccp_to_xic0mu_line(name="Hlt2Charm_OccpToXic0MupNu", prescale=1):
    muons = filter_long_muons()
    kaons = filter_long_kaons()
    pions = filter_long_pions()
    protons = filter_long_protons()
    xic0 = make_xic0_to_pkkpi(protons=protons, kaons=kaons, pions=pions)
    line_alg = make_cc_baryon(
        baryon=xic0,
        particle=muons,
        name='Charm_CCBaryonToCBaryonSl_make_cc_baryon_OmccToXic0Mu_{hash}',
        decay_descriptor="[Omega_cc+ -> Xi_c0 mu+]cc",
        comb_m_max=5.1 * GeV,
        comb_pt_min=2 * GeV,
        doca_max=0.2 * mm,
        dz1_min=-0.5 * mm,
        vchi2dof_max=9.,
        bpvdira_min=0.99)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, protons, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def omegaccp_to_omegac0mu_wsline(name="Hlt2Charm_OccpToOc0MumNu_WS",
                                 prescale=1):
    muons = filter_long_muons()
    kaons = filter_long_kaons()
    pions = filter_long_pions()
    protons = filter_long_protons()
    omegac0 = make_omegac0_to_pkkpi(protons=protons, kaons=kaons, pions=pions)
    line_alg = make_cc_baryon(
        baryon=omegac0,
        particle=muons,
        name='Charm_CCBaryonToCBaryonSl_make_cc_baryon_OmccToOmc0MuWS_{hash}',
        decay_descriptor="[Omega_cc+ -> Omega_c0 mu-]cc",
        comb_m_max=5.1 * GeV,
        comb_pt_min=2 * GeV,
        doca_max=0.2 * mm,
        dz1_min=-0.5 * mm,
        vchi2dof_max=9.,
        bpvdira_min=0.99)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, protons, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def omegaccp_to_xic0mu_wsline(name="Hlt2Charm_OccpToXic0MumNu_WS", prescale=1):
    muons = filter_long_muons()
    kaons = filter_long_kaons()
    pions = filter_long_pions()
    protons = filter_long_protons()
    xic0 = make_xic0_to_pkkpi(protons=protons, kaons=kaons, pions=pions)
    line_alg = make_cc_baryon(
        baryon=xic0,
        particle=muons,
        name='Charm_CCBaryonToCBaryonSl_make_cc_baryon_OmccToXic0MuWS_{hash}',
        decay_descriptor="[Omega_cc+ -> Xi_c0 mu-]cc",
        comb_m_max=5.1 * GeV,
        comb_pt_min=2 * GeV,
        doca_max=0.2 * mm,
        dz1_min=-0.5 * mm,
        vchi2dof_max=9.,
        bpvdira_min=0.99)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, protons, line_alg],
        prescale=prescale)
