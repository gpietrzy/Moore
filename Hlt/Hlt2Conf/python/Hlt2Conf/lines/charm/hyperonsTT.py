###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of Xi_c+/Xi_c0 -> Lambda/Xi- X HLT2 lines
The Lambda -> proton pi are reconstrcted based on T-tracks

2-body lines:
---------------------
- Xic0 -> Xi pi
  Hlt2Charm_Xic0ToXimPip_TTL_L
  Hlt2Charm_Xic0ToXimPip_TTD_L
  Hlt2Charm_Xic0ToXimPip_TTD_D

3-body lines:
---------------------
- Xic+ -> Xi pi pi
  Hlt2Charm_XicpToXimPipPip_TTL_LL
  Hlt2Charm_XicpToXimPipPip_TTD_LL
  Hlt2Charm_XicpToXimPipPip_TTD_DD
- Xic0 -> L K- pi+
  Hlt2Charm_Xic0ToL0KmPip_TT_LL
  Hlt2Charm_Xic0ToL0KmPip_TT_DD
"""
import Functors as F
from GaudiKernel.SystemOfUnits import (GeV, MeV, mm)

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs
from ...standard_particles import (
    make_has_rich_long_pions, make_long_pions, make_has_rich_down_pions,
    make_down_pions, make_has_rich_long_kaons, make_has_rich_down_kaons)
from ...algorithms_thor import ParticleFilter, ParticleCombiner
from .prefilters import charm_prefilters

from Hlt2Conf.standard_particles import (make_ttrack_pions,
                                         make_ttrack_protons)

from RecoConf.ttrack_selections_reco import make_ttrack_MVAfiltered_protoparticles

from Hlt2Conf.lines.bandq.builders.V0_to_TT_builders import (
    _make_V0TT, make_ttrack_pions_for_V0, make_ttrack_protons_for_V0)


# re-definitions of functors
def _DZ_CHILD(i):
    return F.CHILD(i, F.END_VZ) - F.END_VZ


def _MIPCHI2_MIN(cut, pvs=make_pvs):
    return F.MINIPCHI2CUT(IPChi2Cut=cut, Vertices=pvs())


def _make_long_pions_for_xi():
    pvs = make_pvs()
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(F.require_all(F.PT > 100 * MeV,
                               F.MINIPCHI2(pvs) < 20.)))


def _make_down_pions_for_xi():
    pvs = make_pvs()
    return ParticleFilter(
        make_down_pions(),
        F.FILTER(F.require_all(F.PT > 100 * MeV,
                               F.MINIPCHI2(pvs) < 20.)))


#########################################################
## Local bachelor particle filters used at least twice ##
#########################################################
def _make_loose_pions_for_charm():
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 250 * MeV, F.P > 2.0 * GeV,
                          F.MINIPCHI2(pvs) < 3., F.PID_K < 5.), ),
    )


def _make_loose_kaons_for_charm():
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 500 * MeV, F.P > 10 * GeV,
                          F.MINIPCHI2(pvs) < 2., F.PID_K > -2.)))


def _make_loose_down_pions_for_charm():
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_down_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 150 * MeV,
                F.P > 2 * GeV,
                F.MINIPCHI2(pvs) < 3.,
                F.PID_K < 5.,
            )))


def _make_loose_down_kaons_for_charm():
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_down_kaons(),
        F.FILTER(
            F.require_all(F.PT > 150 * MeV, F.P > 2 * GeV,
                          F.MINIPCHI2(pvs) < 2., F.PID_K > -2.)))


def _make_mva_pions():
    return make_ttrack_pions(
        make_protoparticles=make_ttrack_MVAfiltered_protoparticles)


def _make_mva_protons():
    return make_ttrack_protons(
        make_protoparticles=make_ttrack_MVAfiltered_protoparticles)


def _make_ttrack_pions():
    return make_ttrack_pions_for_V0(
        pt_min=400. * MeV,
        p_min=5. * GeV,
        minipchi2_min=500.,
        make_pions=_make_mva_pions)


def _make_ttrack_protons():
    return make_ttrack_protons_for_V0(
        pt_min=1. * GeV,
        p_min=25. * GeV,
        minip_max=200.,
        minipchi2_max=10000.,
        minipchi2_min=50.,
        make_protons=_make_mva_protons)


def _make_tt_lambdas():
    """Make Lambda -> p+ pi- from t-tracks."""
    pvs = make_pvs()
    ttrack_pions = _make_ttrack_pions()
    ttrack_protons = _make_ttrack_protons()
    descriptors = "[Lambda0 -> p+ pi-]cc"
    return _make_V0TT(
        particles=[ttrack_protons, ttrack_pions],
        descriptors=descriptors,
        pvs=pvs,
        maxdoca=30.,
        maxdocachi2=80.,
        p_min=25 * GeV,
        max_chi2=5.,
        bpv_ip_chi2_max=5.,
        RKextrapolation=True,
        name="Charm_Hyperons_Lambda0RKTTCombiner_{hash}")


# This will always ever be called with ll_lambdas=_make_ll_lambdas_for_hyperon.
def _make_ttl_xis():
    pvs = make_pvs()
    tt_lambdas = _make_tt_lambdas()
    pions = _make_long_pions_for_xi()
    return ParticleCombiner(
        [tt_lambdas, pions],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name="Charm_Hyperons_Xim_TTL_{hash}",
        CombinationCut=F.require_all(
            F.MASS < 1800 * MeV,
            F.PT > 1. * GeV,
            F.P > 30. * GeV,
            F.MAXDOCACUT(20.0 * mm),
            F.MAXDOCACHI2CUT(3.0),
        ),
        CompositeCut=F.require_all(
            F.MASS < 1700 * MeV,
            F.PT > 1.1 * GeV,
            F.P > 35. * GeV,
            F.CHI2DOF < 3.,
            _DZ_CHILD(1) > 2000 * mm,
            F.BPVVDZ(pvs) > 15 * mm,
            F.BPVIPCHI2(pvs) < 15,
            F.BPVFDCHI2(pvs) > 160.,
        ),
    )


def _make_ttd_xis():
    pvs = make_pvs()
    tt_lambdas = _make_tt_lambdas()
    pions = _make_down_pions_for_xi()
    return ParticleCombiner(
        [tt_lambdas, pions],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name="Charm_Hyperons_Xim_TTD_{hash}",
        CombinationCut=F.require_all(
            F.MASS < 1800 * MeV,
            F.PT > 1. * GeV,
            F.P > 25. * GeV,
            F.MAXDOCACUT(25.0 * mm),
            F.MAXDOCACHI2CUT(3.0),
        ),
        CompositeCut=F.require_all(
            F.MASS < 1700 * MeV,
            F.PT > 1.1 * GeV,
            F.P > 30. * GeV,
            F.CHI2DOF < 3.,
            _DZ_CHILD(1) > 1000 * mm,
            F.BPVVDZ(pvs) > 100. * mm,
            F.BPVIPCHI2(pvs) < 10.,
            F.BPVFDCHI2(pvs) > 20.,
        ),
    )


all_lines = {}


# Xic0 -> Xi- pi+.  Xi- for TTL, pi+ for L
@register_line_builder(all_lines)
def xicz_to_ximpi_ttl_l_line(name="Hlt2Charm_Xic0ToXimPip_TTL_L"):
    pvs = make_pvs()
    ttl_xis = _make_ttl_xis()
    long_xics_pions = _make_loose_pions_for_charm()
    xic0s = ParticleCombiner(
        [ttl_xis, long_xics_pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi+]cc",
        name="Charm_Hyperons_Xic0ToXimPip_TTL_L_{hash}",
        CombinationCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 3000 * MeV),
            F.PT > 1. * GeV,
            F.P > 40. * GeV,
            F.MAXDOCACUT(10 * mm),
            F.MAXDOCACHI2CUT(3.),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2300 * MeV, F.MASS, 2800 * MeV),
            F.PT > 2. * GeV,
            F.P > 45. * GeV,
            F.CHI2DOF < 3.,
            _DZ_CHILD(1) > 50 * mm,
            F.BPVVDZ(pvs) > 1. * mm,
            F.BPVFDCHI2(pvs) > 5,
            F.BPVIPCHI2(pvs) < 10.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        #hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=charm_prefilters() + [
            long_xics_pions,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            _make_tt_lambdas(), ttl_xis, xic0s
        ])


# Xic0 -> Xi- pi+.  Xi- for TTD, pi+ for L
@register_line_builder(all_lines)
def xicz_to_ximpi_ttd_l_line(name="Hlt2Charm_Xic0ToXimPip_TTD_L"):
    pvs = make_pvs()
    ttd_xis = _make_ttd_xis()
    long_xics_pions = _make_loose_pions_for_charm()
    xic0s = ParticleCombiner(
        [ttd_xis, long_xics_pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi+]cc",
        name="Charm_Hyperons_Xic0ToXimPip_TTD_L_{hash}",
        CombinationCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 3000 * MeV),
            F.PT > 1. * GeV,
            F.P > 40. * GeV,
            F.MAXDOCACUT(10 * mm),
            F.MAXDOCACHI2CUT(3.),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2300 * MeV, F.MASS, 2800 * MeV),
            F.PT > 2. * GeV,
            F.P > 45. * GeV,
            F.CHI2DOF < 3.,
            _DZ_CHILD(1) > 400. * mm,
            F.BPVVDZ(pvs) > 4. * mm,
            F.BPVFDCHI2(pvs) > 5,
            F.BPVIPCHI2(pvs) < 10.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        #hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=charm_prefilters() + [
            long_xics_pions,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            _make_tt_lambdas(), ttd_xis, xic0s
        ])


# Xic0 -> Xi- pi+.  Xi- for TTD, pi+ for D
@register_line_builder(all_lines)
def xicz_to_ximpi_ttd_d_line(name="Hlt2Charm_Xic0ToXimPip_TTD_D"):
    pvs = make_pvs()
    ttd_xis = _make_ttd_xis()
    down_xics_pions = _make_loose_down_pions_for_charm()
    xic0s = ParticleCombiner(
        [ttd_xis, down_xics_pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi+]cc",
        name="Charm_Hyperons_Xic0ToXimPip_TTD_D_{hash}",
        CombinationCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 3000 * MeV),
            F.PT > 1. * GeV,
            F.P > 25. * GeV,
            F.MAXDOCACUT(20 * mm),
            F.MAXDOCACHI2CUT(3.),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2300 * MeV, F.MASS, 2800 * MeV),
            F.PT > 1.2 * GeV,
            F.P > 30. * GeV,
            F.CHI2DOF < 3.,
            _DZ_CHILD(1) > 100. * mm,
            F.BPVVDZ(pvs) > 0. * mm,
            F.BPVFDCHI2(pvs) > 5.,
            F.BPVIPCHI2(pvs) < 30.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        #hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=charm_prefilters() + [
            down_xics_pions,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            _make_tt_lambdas(), ttd_xis, xic0s
        ])


# Xic+ -> Xi- pi+ pi+.  Xi- for TTL, pi+pi+ for LL
@register_line_builder(all_lines)
def xicp_to_ximpipi_ttl_ll_line(name="Hlt2Charm_XicpToXimPipPip_TTL_LL"):
    pvs = make_pvs()
    ttl_xis = _make_ttl_xis()
    long_xics_pions = _make_loose_pions_for_charm()
    xicps = ParticleCombiner(
        [ttl_xis, long_xics_pions, long_xics_pions],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ pi+]cc",
        name="Charm_Hyperons_XicpToXimPipPip_TTL_LL_{hash}",
        Combination12Cut=F.require_all(
            F.math.in_range(1460 * MeV, F.MASS, 2600 * MeV),
            F.MAXDOCACUT(20 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 3000 * MeV),
            F.PT > 1. * GeV,
            F.P > 40. * GeV,
            F.DOCA(1, 3) < 20 * mm,
            F.DOCA(2, 3) < 0.2 * mm,
            F.MAXDOCACHI2CUT(5.),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2300 * MeV, F.MASS, 2800 * MeV),
            F.PT > 2. * GeV,
            F.P > 45. * GeV,
            F.CHI2DOF < 5.,
            _DZ_CHILD(1) > 60 * mm,
            F.BPVVDZ(pvs) > 0. * mm,
            F.BPVFDCHI2(pvs) > 5.,
            F.BPVIPCHI2(pvs) < 30.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        #hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=charm_prefilters() + [
            long_xics_pions,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            _make_tt_lambdas(), ttl_xis, xicps
        ])


# Xic+ -> Xi- pi+ pi+.  Xi- for TTD, pi+pi+ for LL
@register_line_builder(all_lines)
def xicp_to_ximpipi_ttd_ll_line(name="Hlt2Charm_XicpToXimPipPip_TTD_LL"):
    pvs = make_pvs()
    ttd_xis = _make_ttd_xis()
    long_xics_pions = _make_loose_pions_for_charm()
    xicps = ParticleCombiner(
        [ttd_xis, long_xics_pions, long_xics_pions],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ pi+]cc",
        name="Charm_Hyperons_XicpToXimPipPip_TTD_LL_{hash}",
        Combination12Cut=F.require_all(
            F.math.in_range(1460 * MeV, F.MASS, 2600 * MeV),
            F.MAXDOCACUT(20 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 3000 * MeV),
            F.PT > 1. * GeV,
            F.P > 40. * GeV,
            F.DOCA(1, 3) < 20 * mm,
            F.DOCA(2, 3) < 0.2 * mm,
            F.MAXDOCACHI2CUT(5.),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2300 * MeV, F.MASS, 2800 * MeV),
            F.PT > 2.5 * GeV,
            F.P > 45. * GeV,
            F.CHI2DOF < 5.,
            _DZ_CHILD(1) > 200. * mm,
            F.BPVVDZ(pvs) > 0. * mm,
            F.BPVFDCHI2(pvs) > 5.,
            F.BPVIPCHI2(pvs) < 10.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        #hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=charm_prefilters() + [
            long_xics_pions,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            _make_tt_lambdas(), ttd_xis, xicps
        ])


# Xic+ -> Xi- pi+ pi+.  Xi- for TTD, pi+pi+ for DD
@register_line_builder(all_lines)
def xicp_to_ximpipi_ttd_dd_line(name="Hlt2Charm_XicpToXimPipPip_TTD_DD"):
    pvs = make_pvs()
    ttd_xis = _make_ttd_xis()
    down_xics_pions = _make_loose_down_pions_for_charm()
    xicps = ParticleCombiner(
        [ttd_xis, down_xics_pions, down_xics_pions],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ pi+]cc",
        name="Charm_Hyperons_XicpToXimPipPip_TTD_DD_{hash}",
        Combination12Cut=F.require_all(
            F.math.in_range(1460 * MeV, F.MASS, 2600 * MeV),
            F.MAXDOCACUT(25 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 3000 * MeV),
            F.PT > 0.8 * GeV,
            F.P > 20. * GeV,
            F.DOCA(1, 3) < 25 * mm,
            F.DOCA(2, 3) < 1.2 * mm,
            F.MAXDOCACHI2CUT(30.),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2300 * MeV, F.MASS, 2800 * MeV),
            F.PT > 1. * GeV,
            F.P > 30. * GeV,
            F.CHI2DOF < 10.,
            _DZ_CHILD(1) > 5 * mm,
            F.BPVVDZ(pvs) > 0. * mm,
            F.BPVFDCHI2(pvs) > 5.,
            F.BPVIPCHI2(pvs) < 60.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        #hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=charm_prefilters() + [
            down_xics_pions,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            _make_tt_lambdas(), ttd_xis, xicps
        ])


# Xic0 -> Lambda K- pi+.  Lambda for TT, K-pi+ for LL
@register_line_builder(all_lines)
def xicz_to_lkpi_tt_ll_line(name="Hlt2Charm_Xic0ToL0KmPip_TT_LL"):
    pvs = make_pvs()
    tt_lambdas = _make_tt_lambdas()
    long_xicz_pions = _make_loose_pions_for_charm()
    long_xicz_kaons = _make_loose_kaons_for_charm()
    xic0s = ParticleCombiner(
        [tt_lambdas, long_xicz_kaons, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- pi+]cc",
        name="Charm_Hyperons_Xic0ToL0KmPip_TT_LL_{hash}",
        Combination12Cut=F.require_all(
            F.math.in_range(1600 * MeV, F.MASS, 2800 * MeV),
            F.MAXDOCACUT(15 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 3000 * MeV),
            F.PT > 1.5 * GeV,
            F.P > 40. * GeV,
            F.DOCA(1, 3) < 15 * mm,
            F.DOCA(2, 3) < 0.1 * mm,
            F.MAXDOCACHI2CUT(5.),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2300 * MeV, F.MASS, 2800 * MeV),
            F.PT > 2.5 * GeV,
            F.P > 50. * GeV,
            F.CHI2DOF < 3.,
            _DZ_CHILD(1) > 3000. * mm,
            F.BPVVDZ(pvs) > 1.5 * mm,
            F.BPVFDCHI2(pvs) > 9.,
            F.BPVIPCHI2(pvs) < 10.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        #hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=charm_prefilters() + [
            long_xicz_pions, long_xicz_kaons,
            _make_ttrack_protons(),
            _make_ttrack_pions(), tt_lambdas, xic0s
        ])


# Xic0 -> Lambda K- pi+.  Lambda for TT, K-pi+ for DD
@register_line_builder(all_lines)
def xicz_to_lkpi_tt_dd_line(name="Hlt2Charm_Xic0ToL0KmPip_TT_DD"):
    pvs = make_pvs()
    tt_lambdas = _make_tt_lambdas()
    down_xicz_pions = _make_loose_down_pions_for_charm()
    down_xicz_kaons = _make_loose_down_kaons_for_charm()
    xic0s = ParticleCombiner(
        [tt_lambdas, down_xicz_kaons, down_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- pi+]cc",
        name="Charm_Hyperons_Xic0ToL0KmPip_TT_DD_{hash}",
        Combination12Cut=F.require_all(
            F.math.in_range(1600 * MeV, F.MASS, 2800 * MeV),
            F.MAXDOCACUT(15 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 3000 * MeV),
            F.PT > 0.8 * GeV,
            F.P > 20. * GeV,
            F.DOCA(1, 3) < 30 * mm,
            F.DOCA(2, 3) < 1.2 * mm,
            F.MAXDOCACHI2CUT(10.),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2300 * MeV, F.MASS, 2800 * MeV),
            F.PT > 1.0 * GeV,
            F.P > 25. * GeV,
            F.CHI2DOF < 10.,
            _DZ_CHILD(1) > 10. * mm,
            F.BPVVDZ(pvs) > 0. * mm,
            F.BPVFDCHI2(pvs) > 5.,
            F.BPVIPCHI2(pvs) < 30.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        #hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=charm_prefilters() + [
            down_xicz_pions, down_xicz_kaons,
            _make_ttrack_protons(),
            _make_ttrack_pions(), tt_lambdas, xic0s
        ])
