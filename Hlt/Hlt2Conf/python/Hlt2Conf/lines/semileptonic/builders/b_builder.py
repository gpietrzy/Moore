###############################################################################
# (c) Copyright 2020-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Semileptonic B builder, based on B2OC code.
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, GeV, mm
from Hlt2Conf.algorithms_thor import ParticleCombiner
from PyConf import configurable
from RecoConf.reconstruction_objects import make_pvs


@configurable
def make_b2xclnu(particles,
                 descriptor,
                 name='BToXcLNuCombiner_{hash}',
                 make_pvs=make_pvs,
                 comb_m_min=2200 * MeV,
                 comb_m_max=8000 * MeV,
                 comb_docachi2_max=10.,
                 mother_m_min=2200 * MeV,
                 mother_m_max=8000 * MeV,
                 vchi2pdof_max=9.0,
                 bpvdira_min=0.999,
                 b_d_dzs_min=-2.0 * mm,
                 pt_min=0 * MeV,
                 p_min=0 * MeV,
                 bpvvdz_min=None):
    '''
    Base SL b-hadron decay builder.
    '''
    pvs = make_pvs()
    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max), F.PT > pt_min, F.P > p_min)
    if comb_docachi2_max is not None:
        combination_code = F.require_all(combination_code,
                                         F.MAXDOCACHI2CUT(comb_docachi2_max))

    vertex_code = F.require_all(
        in_range(mother_m_min, F.MASS, mother_m_max),
        F.CHI2DOF < vchi2pdof_max,
        F.BPVDIRA(pvs) > bpvdira_min)

    if bpvvdz_min is not None:
        vertex_code = F.require_all(vertex_code, F.BPVVDZ(pvs) > bpvvdz_min)
    if b_d_dzs_min is not None:
        #charm_endvtx_z = F.CHILD(1, F.END_VZ)  #Alternative to MINTREE
        charm_endvtx_z = F.MINTREE(
            F.IS_ABS_ID('D_s-') | F.IS_ABS_ID('D+') | F.IS_ABS_ID('D0')
            | F.IS_ABS_ID('Lambda_c+') | F.IS_ABS_ID('Omega_c0')
            | F.IS_ABS_ID('Xi_c+') | F.IS_ABS_ID('Xi_c0')
            | F.IS_ABS_ID('J/psi(1S)'), F.END_VZ)

        vertex_code = F.require_all(vertex_code,
                                    charm_endvtx_z - F.END_VZ > b_d_dzs_min)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_b2xulnu(particles,
                 descriptor,
                 name='BToXuLNuCombiner_{hash}',
                 make_pvs=make_pvs,
                 comb_m_min=1000 * MeV,
                 comb_m_max=10000 * MeV,
                 vchi2pdof_max=4.0,
                 bpvdira_min=0.994,
                 pt_min=1500 * MeV,
                 bpvfdchi2_min=120,
                 mcorr_min=2500.0 * MeV,
                 mcorr_max=7000.0 * MeV,
                 comb_doca_max=None):
    '''
    SL b-hadron decay builder specialized in b->u transitions.
    '''
    pvs = make_pvs()
    combination_code = F.require_all(in_range(comb_m_min, F.MASS, comb_m_max))
    if comb_doca_max is not None:
        combination_code = F.require_all(combination_code,
                                         F.MAXDOCACUT(comb_doca_max))

    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.BPVDIRA(pvs) > bpvdira_min, F.PT > pt_min,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        in_range(mcorr_min, F.BPVCORRM(pvs), mcorr_max))

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_b2xtaunu(particles,
                  descriptor,
                  name='BToXTauNuCombiner_{hash}',
                  make_pvs=make_pvs,
                  m_min=0. * MeV,
                  m_max=10000. * MeV,
                  comb_m_min=0. * MeV,
                  comb_m_max=11000. * MeV,
                  comb_doca_max=0.5 * mm,
                  vchi2pdof_max=6,
                  bpvdira_min=0.999,
                  bpvfdchi2_min=50):
    '''
    SL b-hadron decay builder specialized in semi-tauonic decays.
    '''
    pvs = make_pvs()
    combination_code = F.require_all(in_range(comb_m_min, F.MASS, comb_m_max))

    if comb_doca_max is not None:
        combination_code = F.require_all(combination_code,
                                         F.MAXDOCACUT(comb_doca_max))
    if bpvfdchi2_min is not None:
        vertex_code = F.BPVFDCHI2(pvs) > bpvfdchi2_min

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vchi2pdof_max,
        F.BPVDIRA(pvs) > bpvdira_min)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_b2hhmunu(
        particles,
        descriptors,
        pvs,
        name='B2HHMuNuCombiner_{hash}',
        m_min=1800 * MeV,
        m_max=5300 * MeV,
        fdchi2_min=50,
        sum_pt_min=5 * GeV,
        vtx_chi2pdof_max=15.,
        bpvdira_min=0.999,
):
    '''
    SL b-hadron builder to a final state of two NR hadrons and muon
    '''

    combination_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.SUM(F.PT) > sum_pt_min)
    vertex_code = F.require_all(
        F.BPVFDCHI2(pvs) > fdchi2_min, F.CHI2DOF < vtx_chi2pdof_max,
        F.BPVDIRA(pvs) > bpvdira_min)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptors,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_b2lllnu(particles,
                 descriptor,
                 name='BToLLLNuCombiner_{hash}',
                 make_pvs=make_pvs,
                 comb_m_min=1000. * MeV,
                 comb_m_max=10000. * MeV,
                 vchi2pdof_max=4.0,
                 bpvdira_min=0.99,
                 pt=1200. * MeV,
                 bpvfdchi2_min=30.,
                 mcorr_min=2500.0 * MeV,
                 mcorr_max=10000.0 * MeV,
                 comb_doca_max=20.):
    '''
    SL b-hadron decay builder specialized in B(c)+ -> l+ l+ l- nu transitions.
    '''
    pvs = make_pvs()

    combination_code = F.require_all(in_range(comb_m_min, F.MASS, comb_m_max))
    if comb_doca_max is not None:
        combination_code = F.require_all(combination_code,
                                         F.MAXDOCACUT(comb_doca_max))

    vertex_code = F.require_all(
        F.PT > pt, F.CHI2DOF < vchi2pdof_max,
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        in_range(mcorr_min, F.BPVCORRM(pvs), mcorr_max))

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_bc2bsx(descriptors,
                bs_particles,
                bach_particles,
                points_to_pv,
                name="BcToBsXMaker",
                p_min=10000 * MeV,
                pt_min=3000 * MeV,
                vchi2pdof_max=20,
                bpvvdz_min=0.08 * mm,
                bpvdira_min_tight=0.99,
                bpvdira_min_loose=0.9):
    '''
    SL and hadronic Bc decay builder
    Loose bpvdira if semileptonic, tight if hadronic
    '''

    if points_to_pv:
        bpvdira_min = bpvdira_min_tight
    else:
        bpvdira_min = bpvdira_min_loose

    particles = [bs_particles, bach_particles]

    return make_b2xclnu(
        particles,
        descriptors,
        name=name,
        comb_docachi2_max=None,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        b_d_dzs_min=None,
        pt_min=pt_min,
        p_min=p_min,
        bpvvdz_min=bpvvdz_min)


@configurable
def make_bs2hh(particles,
               descriptor,
               name="Bs2HHCombiner",
               am_min=5000 * MeV,
               am_max=6000 * MeV,
               vchi2pdof_max=25,
               bpvdira_min=0.9,
               bpvvdchi2_min=100,
               bpvdz_min=2):
    '''
    SL Bs builder to a final state of two mesons
    '''
    pvs = make_pvs()

    combination_code = F.require_all(in_range(am_min, F.MASS, am_max))
    mother_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max, bpvdira_min < F.BPVDIRA(pvs),
        bpvvdchi2_min < F.BPVFDCHI2(pvs), bpvdz_min < F.BPVVDZ(pvs))

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=mother_code)
