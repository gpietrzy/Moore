###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import MeV, GeV
from RecoConf.reconstruction_objects import make_pvs
from .builders.base_builder import make_muons_from_b, make_fake_muons_from_b_notIsMuon, make_protons_from_b, make_fake_protons_from_b
from .builders.b_builder import make_b2hhmunu
from .builders.hh_builder import make_hh
"""
SL lines for the B->pplnu decays. The decay descriptors have a J/psi->p+p~- decay, to have the protons come from the B vertex
"""


def make_b2ppbarmunu(process):
    """
    Selection for the B->ppmunu and muonic B->pptaunu decays
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    pvs = make_pvs()
    protons = make_protons_from_b(pt_min=800 * MeV, mipchi2_min=9.)

    ppbar = make_hh([protons, protons], "J/psi(1S) -> p+ p~-", pvs)
    muons = make_muons_from_b(p_min=3. * GeV)
    line_alg = make_b2hhmunu([ppbar, muons], "[B+ -> J/psi(1S) mu+]cc", pvs)

    return line_alg


def make_b2ppbarmunu_ss(process):
    """
    Selection for the B->ppmunu and muonic B->pptaunu decays with same-sign protons
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    pvs = make_pvs()
    protons = make_protons_from_b(pt_min=800 * MeV, mipchi2_min=9.)
    ppbar = make_hh([protons, protons], "[J/psi(1S) -> p+ p+]cc", pvs)
    muons = make_muons_from_b(p_min=3. * GeV)
    line_alg = make_b2hhmunu([ppbar, muons], "[B+ -> J/psi(1S) mu+]cc", pvs)

    return line_alg


def make_b2ppbarmunu_fakep(process):
    """
    Selection for the B->ppmunu and muonic B->pptaunu decays with a fake proton of opposite sign to muon
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    pvs = make_pvs()
    protons = make_protons_from_b(pt_min=800 * MeV, mipchi2_min=9.)
    fake_protons = make_fake_protons_from_b(pt_min=800 * MeV, mipchi2_min=9.)

    ppbar = make_hh([protons, fake_protons], "[J/psi(1S) -> p+ p~-]cc", pvs)
    muons = make_muons_from_b(p_min=3. * GeV)
    line_alg = make_b2hhmunu([ppbar, muons], "[B+ -> J/psi(1S) mu+]cc", pvs)

    return line_alg


def make_b2ppbarmunu_fakemu(process):
    """
    Selection for the B->ppmunu and muonic B->pptaunu decays with a fake muon
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    pvs = make_pvs()
    protons = make_protons_from_b(pt_min=800 * MeV, mipchi2_min=9.)

    ppbar = make_hh([protons, protons], "J/psi(1S) -> p+ p~-", pvs)
    muons = make_fake_muons_from_b_notIsMuon(p_min=3. * GeV)
    line_alg = make_b2hhmunu([ppbar, muons], "[B+ -> J/psi(1S) mu+]cc", pvs)

    return line_alg
