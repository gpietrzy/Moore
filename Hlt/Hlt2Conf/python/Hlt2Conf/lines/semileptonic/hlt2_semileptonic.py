###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Booking of semileptonic hlt2 lines, notice PROCESS = 'hlt2'

Output:
updated dictionary of hlt2_lines

Note:
    Line builders have ``PROCESS`` as argument to allow ad hoc settings.
"""
from Moore.config import HltLine, register_line_builder
from .builders import sl_line_prefilter

from .HbToHcTauNu_TauToPiPiPiNu import (
    make_bptod0taunu_d0tok3pi_tautopipipinu,
    make_b0todptaunu_dptokpipi_tautopipipinu,
    make_bctojpsitaunu_jpsitomumu_tautopipipinu,
    make_bptod0taunu_d0tokpi_tautopipipinu,
    make_bstodstaunu_dstokkpi_tautopipipinu,
    make_lbtolctaunu_lctopkpi_tautopipipinu,
    make_lbtoptaunu_tautopipipinu,
)
from .HbToHuLNu import (
    make_lbtopmunu,
    make_lbtopmunu_fakep,
    make_lbtopmunu_fakemu,
    make_lbtopmunu_ss,
    make_lbtopmunu_ss_fakep,
    make_lbtopmunu_ss_fakemu,
    make_b0topimunu,
    make_b0topimunu_fakemu,
    make_b0topimunu_fakek,
    make_bstokmunu,
    make_bstokmunu_fakemu,
    make_bstokmunu_fakek,
)
from .HbToHuTauNu_TauToPiPiPiNu import (
    make_bstoktaunu_hadronic,
    make_b0topitaunu_hadronic,
)
from .HbToHuTauNu_TauToLNuNu import (
    make_bstoktaunu_muonic,
    make_bstoktaunu_muonic_fakemu,
    make_bstoktaunu_muonic_fakek,
)
from .HbToHHLNu import (
    make_b2ppbarmunu,
    make_b2ppbarmunu_ss,
    make_b2ppbarmunu_fakep,
    make_b2ppbarmunu_fakemu,
)
from .HbToHcTauNu_TauToLNuNu import (
    make_bctojpsitaunu_jpsitomumu_tautolnunu,
    make_bctojpsitaunu_jpsitomumu_tautolnunu_fakelepton,
    make_b0todptaunu_dptokpipi_tautolnunu,
    make_b0todptaunu_dptokpipi_tautolnunu_fakelepton,
    make_butod0taunu_d0tokpi_tautolnunu,
    make_butod0taunu_d0tokpi_tautolnunu_fakelepton,
    make_butod0taunu_d0tok3pi_tautolnunu,
    make_butod0taunu_d0tok3pi_tautolnunu_fakelepton,
    make_bstodstaunu_dstokkpi_tautolnunu,
    make_bstodstaunu_dstokkpi_tautolnunu_fakelepton,
    make_lbtolctaunu_lctopkpi_tautolnu,
    make_lbtolctaunu_lctopkpi_tautolnu_fakelepton,
    make_xib0toxicplustaunu_xicplustopkpi_tautolnunu,
    make_xib0toxicplustaunu_xicplustopkpi_tautolnunu_fakelepton,
    make_xibminustoxic0taunu_xic0topkkpi_tautolnunu,
    make_xibminustoxic0taunu_xic0topkkpi_tautolnunu_fakelepton,
    make_omegabtoomegactaunu_omegactopkkpi_tautolnunu,
    make_omegabtoomegactaunu_omegactopkkpi_tautolnunu_fakelepton,
    make_lbtolctaunu_lctoV0h_tautolnu,
    make_lbtolctaunu_lctoV0h_tautolnu_fakelepton,
)
from .HbToHcLNu import (
    make_bctojpsilnu_jpsitomumu,
    make_bctojpsilnu_jpsitomumu_fakelepton,
    make_butod0lnu_d0tokpi,
    make_butod0lnu_d0tokpi_fakelepton,
    make_b0todplnu_dptokpipi,
    make_b0todplnu_dptokpipi_fakelepton,
    make_butod0lnu_d0tok3pi,
    make_butod0lnu_d0tok3pi_fakelepton,
    make_bstodslnu_dstokkpi,
    make_bstodslnu_dstokkpi_fakelepton,
    make_lbtolclnu_lctopkpi,
    make_lbtolclnu_lctopkpi_fakelepton,
    make_bctod0lnu_d0tokpi,
    make_bctod0lnu_d0tokpi_fakelepton,
    make_xib0toxicpluslnu_xicplustopkpi,
    make_xib0toxicpluslnu_xicplustopkpi_fakelepton,
    make_xibminustoxic0lnu_xic0topkkpi,
    make_xibminustoxic0lnu_xic0topkkpi_fakelepton,
    make_omegabtoomegaclnu_omegactopkkpi,
    make_omegabtoomegaclnu_omegactopkkpi_fakelepton,
    make_lbtolclnu_lctoV0h,
    make_lbtolclnu_lctoV0h_fakelepton,
)
from .HbToLLLNu import (
    make_b2mumumunu, make_b2emumunu, make_b2mueenu, make_b2eeenu,
    make_b2taumumunu_3pi, make_b2taueenu_3pi, make_b2mumumunu_ss,
    make_b2emumunu_ss, make_b2mueenu_ss, make_b2eeenu_ss,
    make_b2taumumunu_3pi_ss, make_b2taueenu_3pi_ss,
    make_b2mumumunu_trifakemuon, make_b2mumumunu_onefakemuon,
    make_b2emumunu_fakeelectron, make_b2mueenu_fakemuon,
    make_b2eeenu_trifakeelectron, make_b2eeenu_onefakeelectron)
from .HbToHbprimeLNu import make_bctobsx

PROCESS = 'hlt2'
hlt2_lines = {}
all_lines = hlt2_lines


@register_line_builder(hlt2_lines)
def hlt2_bptod0taunu_d0tokpipipi_tautopipipinu_line(
        name="Hlt2SLB_BuToD0TauNu_D0ToK3Pi_TauToPiPiPiNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B+ -> D~0 tau+, D~0 -> K- Pi- Pi+ Pi+, hadronic tau decay.
    """
    line_alg = make_bptod0taunu_d0tok3pi_tautopipipinu(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctojpsitaunu_jpsitomumu_tautomununu_line(
        name="Hlt2SLB_BcToJpsiTauNu_JpsiToMuMu_TauToMuNuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Bc -> J/psi(1S)(-> mu mu) tau(-> mu nu nu) nu.
    """
    line_alg = make_bctojpsitaunu_jpsitomumu_tautolnunu(
        process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctojpsitaunu_jpsitomumu_fakemuon_line(
        name="Hlt2SLB_BcToJpsiTauNu_JpsiToMuMu_FakeMuon",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Bc -> J/psi(1S)(-> mu mu) tau(-> mu nu nu) nu, with a fake muon.
    """
    line_alg = make_bctojpsitaunu_jpsitomumu_tautolnunu_fakelepton(
        process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctojpsitaunu_jpsitomumu_tautoenunu_line(
        name="Hlt2SLB_BcToJpsiTauNu_JpsiToMuMu_TauToENuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Bc -> J/psi(1S)(-> mu mu) tau(-> e nu nu) nu.
    """
    line_alg = make_bctojpsitaunu_jpsitomumu_tautolnunu(
        process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctojpsitaunu_jpsitomumu_fakeelectron_line(
        name="Hlt2SLB_BcToJpsiTauNu_JpsiToMuMu_FakeElectron",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Bc -> J/psi(1S)(-> mu mu) tau(-> e nu nu) nu, with a fake electron.
    """
    line_alg = make_bctojpsitaunu_jpsitomumu_tautolnunu_fakelepton(
        process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b0todptaunu_dptokpipi_tautomununu_line(
        name="Hlt2SLB_B0ToDpTauNu_DpToKPiPi_TauToMuNuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K pi pi) tau(-> mu nu nu) nu.
    """
    line_alg = make_b0todptaunu_dptokpipi_tautolnunu(
        process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b0todptaunu_dptokpipi_fakemuon_line(
        name="Hlt2SLB_B0ToDpTauNu_DpToKPiPi_FakeMuon",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K pi pi) tau(-> mu nu nu) nu, with a fake muon.
    """
    line_alg = make_b0todptaunu_dptokpipi_tautolnunu_fakelepton(
        process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b0todptaunu_dptokpipi_tautoenunu_line(
        name="Hlt2SLB_B0ToDpTauNu_DpToKPiPi_TauToENuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K pi pi) tau(-> e nu nu) nu.
    """
    line_alg = make_b0todptaunu_dptokpipi_tautolnunu(
        process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b0todptaunu_dptokpipi_fakeelectron_line(
        name="Hlt2SLB_B0ToDpTauNu_DpToKPiPi_FakeElectron",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K pi pi) tau(-> e nu nu) nu, with a fake electron.
    """
    line_alg = make_b0todptaunu_dptokpipi_tautolnunu_fakelepton(
        process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_butod0taunu_d0tokpi_tautomununu_line(
        name="Hlt2SLB_BuToD0TauNu_D0ToKPi_TauToMuNuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi) tau(-> mu nu nu) nu.
    """
    line_alg = make_butod0taunu_d0tokpi_tautolnunu(
        process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_butod0taunu_d0tokpi_fakemuon_line(
        name="Hlt2SLB_BuToD0TauNu_D0ToKPi_FakeMuon",
        prescale=0.1,
        persistreco=True):
    """
   SL Hlt2 line for the decay B+ -> D0(-> K pi) tau(-> mu nu nu) nu, with a fake muon.
   """
    line_alg = make_butod0taunu_d0tokpi_tautolnunu_fakelepton(
        process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_butod0taunu_d0tokpi_tautoenunu_line(
        name="Hlt2SLB_BuToD0TauNu_D0ToKPi_TauToENuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi) tau(-> e nu nu) nu.
    """
    line_alg = make_butod0taunu_d0tokpi_tautolnunu(process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_butod0taunu_d0tokpi_fakeelectron_line(
        name="Hlt2SLB_BuToD0TauNu_D0ToKPi_FakeElectron",
        prescale=0.1,
        persistreco=True):
    """
   SL Hlt2 line for the decay B+ -> D0(-> K pi) tau(-> e nu nu) nu, with a fake electron.
   """
    line_alg = make_butod0taunu_d0tokpi_tautolnunu_fakelepton(
        process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_butod0taunu_d0tok3pi_tautomununu_line(
        name="Hlt2SLB_BuToD0TauNu_D0ToK3Pi_TauToMuNuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi pi pi) tau(-> mu nu nu) nu.
    """
    line_alg = make_butod0taunu_d0tok3pi_tautolnunu(
        process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_butod0taunu_d0tok3pi_fakemuon_line(
        name="Hlt2SLB_BuToD0TauNu_D0ToK3Pi_FakeMuon",
        prescale=0.1,
        persistreco=True):
    """
   SL Hlt2 line for the decay B+ -> D0(-> K pi pi pi) tau(-> mu nu nu) nu, with a fake muon.
   """
    line_alg = make_butod0taunu_d0tok3pi_tautolnunu_fakelepton(
        process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_butod0taunu_d0tok3pi_tautoenunu_line(
        name="Hlt2SLB_BuToD0TauNu_D0ToK3Pi_TauToENuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi pi pi) tau(-> e nu nu) nu.
    """
    line_alg = make_butod0taunu_d0tok3pi_tautolnunu(
        process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_butod0taunu_d0tok3pi_fakeelectron_line(
        name="Hlt2SLB_BuToD0TauNu_D0ToK3Pi_FakeElectron",
        prescale=0.1,
        persistreco=True):
    """
   SL Hlt2 line for the decay B+ -> D0(-> K pi pi pi) tau(-> e nu nu) nu, with a fake electron.
   """
    line_alg = make_butod0taunu_d0tok3pi_tautolnunu_fakelepton(
        process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bstodstaunu_dstokkpi_tautomununu_line(
        name="Hlt2SLB_BsToDsTauNu_DsToKKPi_TauToMuNuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Bs0 -> Ds-(-> K K pi) tau+(-> mu nu nu) nu, and combinatorial (same sign).
    """
    line_alg = make_bstodstaunu_dstokkpi_tautolnunu(
        process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bstodstaunu_dstokkpi_fakemuon_line(
        name="Hlt2SLB_BsToDsTauNu_DpToKKPi_FakeMuon",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Bs0 -> Ds-(-> K K pi) tau+(-> mu nu nu) nu and combinatorial (same sign), with a fake muon.
    """
    line_alg = make_bstodstaunu_dstokkpi_tautolnunu_fakelepton(
        process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bstodstaunu_dstokkpi_tautoenunu_line(
        name="Hlt2SLB_BsToDsTauNu_DsToKKPi_TauToENuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Bs0 -> Ds-(-> K K pi) tau+(-> e nu nu) nu.
    """
    line_alg = make_bstodstaunu_dstokkpi_tautolnunu(
        process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bstodstaunu_dstokkpi_fakeelectron_line(
        name="Hlt2SLB_BsToDsTauNu_DpToKKPi_FakeElectron",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Bs0 -> Ds-(-> K K pi) tau+(-> e nu nu) nu, with a fake electron.
    """
    line_alg = make_bstodstaunu_dstokkpi_tautolnunu_fakelepton(
        process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctopkpi_line(name="Hlt2SLB_LbToLcMuNu_LcToPKPi",
                                  prescale=1,
                                  persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p K pi) mu nu.
    """
    line_alg = make_lbtolclnu_lctopkpi(process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctopkpi_fakemuon_line(
        name="Hlt2SLB_LbToLcMuNu_LcToPKPi_FakeMuon",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p K pi) mu nu, with a fake muon.
    """
    line_alg = make_lbtolclnu_lctopkpi_fakelepton(process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcenu_lctopkpi_line(name="Hlt2SLB_LbToLcENu_LcToPKPi",
                                 prescale=1,
                                 persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p K pi) e nu.
    """
    line_alg = make_lbtolclnu_lctopkpi(process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcenu_lctopkpi_fakeelectron_line(
        name="Hlt2SLB_LbToLcENu_LcToPKPi_FakeElectron",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p K pi) e nu, with a fake electron.
    """
    line_alg = make_lbtolclnu_lctopkpi_fakelepton(process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopkpi_tautomunu_line(
        name="Hlt2SLB_LbToLcTauNu_LcToPKPi_TauToMuNuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p K pi) tau-(-> mu nu nu) nu.
    """
    line_alg = make_lbtolctaunu_lctopkpi_tautolnu(process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopkpi_tautomunu_fakemuon_line(
        name="Hlt2SLB_LbToLcTauNu_LcToPKPi_FakeMuon",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p K pi) tau-(-> mu nu nu) nu, with a fake muon.
    """
    line_alg = make_lbtolctaunu_lctopkpi_tautolnu_fakelepton(
        process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopkpi_tautoenu_line(
        name="Hlt2SLB_LbToLcTauNu_LcToPKPi_TauToENuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p K pi) tau-(-> e nu nu) nu.
    """
    line_alg = make_lbtolctaunu_lctopkpi_tautolnu(process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])
    #extra_outputs=[("Lc", lcs), ("Tau", tauons)])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopkpi_tautoenu_fakeelectron_line(
        name="Hlt2SLB_LbToLcTauNu_LcToPKPi_FakeElectron",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p K pi) tau-(-> e nu nu) nu, with a fake electron.
    """
    line_alg = make_lbtolctaunu_lctopkpi_tautolnu_fakelepton(
        process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctojpsimunu_jpsitomumu_line(name="Hlt2SLB_BcToJpsiMuNu_JpsiToMuMu",
                                      persistreco=True,
                                      prescale=1):
    """
    SL Hlt2 line for the decay Bc+ -> Jpsi(-> mu mu) mu nu.
    """
    line_alg = make_bctojpsilnu_jpsitomumu(process=PROCESS, lepton="mu")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctojpsimunu_jpsitomumu_fakemuon_line(
        name="Hlt2SLB_BcToJpsiMuNu_JpsiToMuMu_FakeMuon",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Bc+ -> Jpsi(-> mu mu) mu nu, with a fake muon.
    """
    line_alg = make_bctojpsilnu_jpsitomumu_fakelepton(
        process=PROCESS, lepton="mu")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctojpsienu_jpsitomumu_line(name="Hlt2SLB_BcToJpsiENu_JpsiToMuMu",
                                     prescale=1,
                                     persistreco=True):
    """
    SL Hlt2 line for the decay Bc+ -> Jpsi(-> mu mu) e nu.
    """
    line_alg = make_bctojpsilnu_jpsitomumu(process=PROCESS, lepton="e")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctojpsienu_jpsitomumu_fakeelectron_line(
        name="Hlt2SLB_BcToJpsiENu_JpsiToMuMu_FakeElectron",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Bc+ -> Jpsi(-> mu mu) e nu, with a fake electron.
    """
    line_alg = make_bctojpsilnu_jpsitomumu_fakelepton(
        process=PROCESS, lepton="e")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_butod0munu_d0tokpi_line(name="Hlt2SLB_BuToD0MuNu_D0ToKPi",
                                 prescale=1,
                                 persistreco=True):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi) mu nu.
    """
    line_alg = make_butod0lnu_d0tokpi(process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_butod0munu_d0tokpi_fakemuon_line(
        name="Hlt2SLB_BuToD0MuNu_D0ToKPi_FakeMuon", prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi) mu nu, with a fake muon.
    """
    line_alg = make_butod0lnu_d0tokpi_fakelepton(process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_butod0enu_d0tokpi_line(name="Hlt2SLB_BuToD0ENu_D0ToKPi",
                                prescale=1,
                                persistreco=True):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi) e nu.
    """
    line_alg = make_butod0lnu_d0tokpi(process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def butod0enu_d0tokpi_fakeelectron_line(
        name="Hlt2SLB_BuToD0ENu_D0ToKPi_FakeElectron",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi) e nu, with a fake electron.
    """
    line_alg = make_butod0lnu_d0tokpi_fakelepton(process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_butod0munu_d0tok3pi_line(name="Hlt2SLB_BuToD0MuNu_D0ToK3Pi",
                                  prescale=1,
                                  persistreco=True):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi pi pi) mu nu.
    """
    line_alg = make_butod0lnu_d0tok3pi(process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_butod0munu_d0tok3pi_fakemuon_line(
        name="Hlt2SLB_BuToD0MuNu_D0ToK3Pi_FakeMuon",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi pi pi) mu nu, with a fake muon.
    """
    line_alg = make_butod0lnu_d0tok3pi_fakelepton(process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_butod0enu_d0tok3pi_line(name="Hlt2SLB_BuToD0ENu_D0ToK3Pi",
                                 prescale=1,
                                 persistreco=True):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi pi pi) e nu.
    """
    line_alg = make_butod0lnu_d0tok3pi(process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_butod0enu_d0tok3pi_fakeelectron_line(
        name="Hlt2SLB_BuToD0ENu_D0ToK3Pi_FakeElectron",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi pi pi) e nu, with a fake electron.
    """
    line_alg = make_butod0lnu_d0tok3pi_fakelepton(process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b0todpmunu_dptokpipi_line(name="Hlt2SLB_B0ToDpMuNu_DpToKPiPi",
                                   prescale=1,
                                   persistreco=True):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K pi pi) mu nu.
    """
    line_alg = make_b0todplnu_dptokpipi(process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b0todpmunu_dptokpipi_fakemuon_line(
        name="Hlt2SLB_B0ToDpMuNu_DpToKPiPi_FakeMuon",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K pi pi) mu nu, with a fake muon.
    """
    line_alg = make_b0todplnu_dptokpipi_fakelepton(
        process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b0todpenu_dptokpipi_line(name="Hlt2SLB_B0ToDpENu_DpToKPiPi",
                                  prescale=1,
                                  persistreco=True):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K pi pi) e nu.
    """
    line_alg = make_b0todplnu_dptokpipi(process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b0todpenu_dptokpipi_fakeelectron_line(
        name="Hlt2SLB_B0ToDpENu_DpToKPiPi_FakeElectron",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K pi pi) e nu, with a fake electron.
    """
    line_alg = make_b0todplnu_dptokpipi_fakelepton(process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bstodsmunu_dstokkpi_line(name="Hlt2SLB_BsToDsMuNu_DsToKKPi",
                                  prescale=1,
                                  persistreco=True):
    """
    SL Hlt2 line for the decay B_s0 -> Ds(-> K K pi) mu nu.
    """
    line_alg = make_bstodslnu_dstokkpi(process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bstodsmunu_dstokkpi_fakemuon_line(
        name="Hlt2SLB_BsToDsMuNu_DsToKKPi_FakeMuon",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B_s0 -> Ds(-> K K pi) mu nu, with a fake muon.
    """
    line_alg = make_bstodslnu_dstokkpi_fakelepton(process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bstodsenu_dstokkpi_line(name="Hlt2SLB_BsToDsENu_DsToKKPi",
                                 prescale=1,
                                 persistreco=True):
    """
    SL Hlt2 line for the decay B_s0 -> Ds(-> K K pi) e nu.
    """
    line_alg = make_bstodslnu_dstokkpi(process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bstodsenu_dstokkpi_fakeelectron_line(
        name="Hlt2SLB_BsToDsENu_DsToKKPi_FakeElectron",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B_s0 -> Ds(-> K K pi) e nu, with a fake electron.
    """
    line_alg = make_bstodslnu_dstokkpi_fakelepton(process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_xib0toxicplusenu_xicplustopkpi_line(
        name="Hlt2SLB_Xib0ToXicplusENu_XicplusTopKPi",
        prescale=1.,
        persistreco=True):
    """
    SL Hlt2 line for the decays:
        Xi_b0 -> Xi_c+ (-> p K- pi+) e- nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) e+ nu (Wrong sign)
    """
    line_alg = make_xib0toxicpluslnu_xicplustopkpi(process=PROCESS, lepton="e")
    #Note following does not work if line_alg is defined as function in different file
    # extra_outputs=[("ext_tr_long", isolation_particles())])
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_xib0toxicplusmunu_xicplustopkpi_line(
        name="Hlt2SLB_Xib0ToXicplusMuNu_XicplusTopKPi",
        prescale=1.,
        persistreco=True):
    """
    SL Hlt2 line for the decays:
        Xi_b0 -> Xi_c+ (-> p K- pi+) mu- nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) mu+ nu (Wrong sign)
    """
    line_alg = make_xib0toxicpluslnu_xicplustopkpi(
        process=PROCESS, lepton="mu")
    #Note following does not work if line_alg is defined as function in different file
    # extra_outputs=[("ext_tr_long", isolation_particles())])
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_xib0toxicplustaunu_xicplustopkpi_tautoenunu_line(
        name="Hlt2SLB_Xib0ToXicplusTauNu_XicplusTopKPi_TauToENuNu",
        prescale=1.,
        persistreco=True):
    """
    SL Hlt2 line for the decays:
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau- (-> e- nu nu) nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau+ (-> e+ nu nu) nu (Wrong sign)
    """
    line_alg = make_xib0toxicplustaunu_xicplustopkpi_tautolnunu(
        process=PROCESS, lepton="e")
    #Note following does not work if line_alg is defined as function in different file
    # extra_outputs=[("ext_tr_long", isolation_particles())])
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_xib0toxicplustaunu_xicplustopkpi_tautomununu_line(
        name="Hlt2SLB_Xib0ToXicplusTauNu_XicplusTopKPi_TauToMuNuNu",
        prescale=1.,
        persistreco=True):
    """
    SL Hlt2 line for the decays:
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau- (-> mu- nu nu) nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau+ (-> mu+ nu nu) nu (Wrong sign)
    """
    line_alg = make_xib0toxicplustaunu_xicplustopkpi_tautolnunu(
        process=PROCESS, lepton="mu")
    #Note following does not work if line_alg is defined as function in different file
    # extra_outputs=[("ext_tr_long", isolation_particles())])
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_xib0toxicplusenu_xicplustopkpi_fakeelectron_line(
        name="Hlt2SLB_Xib0ToXicplusENu_XicplusTopKPi_FakeElectron",
        prescale=0.35,
        persistreco=True):
    """
    SL Hlt2 line for the decays with a fake electron:
        Xi_b0 -> Xi_c+ (-> p K- pi+) e- nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) e+ nu (Wrong sign)
    """
    line_alg = make_xib0toxicpluslnu_xicplustopkpi_fakelepton(
        process=PROCESS, lepton="e")
    #Note following does not work if line_alg is defined as function in different file
    # extra_outputs=[("ext_tr_long", isolation_particles())])
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_xib0toxicplusmunu_xicplustopkpi_fakemuon_line(
        name="Hlt2SLB_Xib0ToXicplusMuNu_XicplusTopKPi_FakeMuon",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decays with fake muon:
        Xi_b0 -> Xi_c+ (-> p K- pi+) mu- nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) mu+ nu (Wrong sign)
    """
    line_alg = make_xib0toxicpluslnu_xicplustopkpi_fakelepton(
        process=PROCESS, lepton="mu")
    #Note following does not work if line_alg is defined as function in different file
    # extra_outputs=[("ext_tr_long", isolation_particles())])
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_xib0toxicplustaunu_xicplustopkpi_tautoenunu_fakeelectron_line(
        name="Hlt2SLB_Xib0ToXicplusTauNu_XicplusTopKPi_FakeElectron",
        prescale=.1,
        persistreco=True):
    """
    SL Hlt2 line for the decays with a fake electron:
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau- (-> e- nu nu) nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau+ (-> e+ nu nu) nu (Wrong sign)
    """
    line_alg = make_xib0toxicplustaunu_xicplustopkpi_tautolnunu_fakelepton(
        process=PROCESS, lepton="e")
    #Note following does not work if line_alg is defined as function in different file
    # extra_outputs=[("ext_tr_long", isolation_particles())])
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_xib0toxicplustaunu_xicplustopkpi_tautomununu_fakemuon_line(
        name="Hlt2SLB_Xib0ToXicplusTauNu_XicplusTopKPi_FakeMuon",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decays with a fake muon:
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau- (-> mu- nu nu) nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau+ (-> mu+ nu nu) nu (Wrong sign)
    """
    line_alg = make_xib0toxicplustaunu_xicplustopkpi_tautolnunu_fakelepton(
        process=PROCESS, lepton="mu")
    #Note following does not work if line_alg is defined as function in different file
    # extra_outputs=[("ext_tr_long", isolation_particles())])
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_xibminustoxic0enu_xic0topkkpi_line(
        name="Hlt2SLB_XibminusToXic0ENu_Xic0TopKKPi",
        prescale=1.,
        persistreco=True):
    """
    SL Hlt2 line for the decays:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) e- nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) e+ nu (Wrong sign)
    """
    line_alg = make_xibminustoxic0lnu_xic0topkkpi(process=PROCESS, lepton="e")
    #Note following does not work if line_alg is defined as function in different file
    # extra_outputs=[("ext_tr_long", isolation_particles())])
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_xibminustoxic0munu_xic0topkkpi_line(
        name="Hlt2SLB_XibminusToXic0MuNu_Xic0TopKKPi",
        prescale=1.,
        persistreco=True):
    """
    SL Hlt2 line for the decays:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) mu- nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) mu+ nu (Wrong sign)
    """
    line_alg = make_xibminustoxic0lnu_xic0topkkpi(process=PROCESS, lepton="mu")
    #Note following does not work if line_alg is defined as function in different file
    # extra_outputs=[("ext_tr_long", isolation_particles())])
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_xibminustoxic0taunu_xic0topkkpi_tautoenunu_line(
        name="Hlt2SLB_XibminusToXic0TauNu_Xic0TopKKPi_TauToENuNu",
        prescale=1.,
        persistreco=True):
    """
    SL Hlt2 line for the decays:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau- (-> e- nu nu) nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau+ (-> e+ nu nu) nu (Wrong sign)
    """
    line_alg = make_xibminustoxic0taunu_xic0topkkpi_tautolnunu(
        process=PROCESS, lepton="e")
    #Note following does not work if line_alg is defined as function in different file
    # extra_outputs=[("ext_tr_long", isolation_particles())])
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_xibminustoxic0taunu_xic0topkkpi_tautomununu_line(
        name="Hlt2SLB_XibminusToXic0TauNu_Xic0TopKKPi_TauToMuNuNu",
        prescale=1.,
        persistreco=True):
    """
    SL Hlt2 line for the decays:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau- (-> mu- nu nu) nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau+ (-> mu+ nu nu) nu (Wrong sign)
    """
    line_alg = make_xibminustoxic0taunu_xic0topkkpi_tautolnunu(
        process=PROCESS, lepton="mu")
    #Note following does not work if line_alg is defined as function in different file
    # extra_outputs=[("ext_tr_long", isolation_particles())])
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_xibminustoxic0enu_xic0topkkpi_fakeelectron_line(
        name="Hlt2SLB_XibminusToXic0ENu_Xic0TopKKPi_FakeElectron",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decays with fake electron:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) e- nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) e+ nu (Wrong sign)
    """
    line_alg = make_xibminustoxic0lnu_xic0topkkpi_fakelepton(
        process=PROCESS, lepton="e")
    #Note following does not work if line_alg is defined as function in different file
    # extra_outputs=[("ext_tr_long", isolation_particles())])
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_xibminustoxic0munu_xic0topkkpi_fakemuon_line(
        name="Hlt2SLB_XibminusToXic0MuNu_Xic0TopKKPi_FakeMuon",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decays with fake muon:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) mu- nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) mu+ nu (Wrong sign)
    """
    line_alg = make_xibminustoxic0lnu_xic0topkkpi_fakelepton(
        process=PROCESS, lepton="mu")
    #Note following does not work if line_alg is defined as function in different file
    # extra_outputs=[("ext_tr_long", isolation_particles())])
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_xibminustoxic0taunu_xic0topkkpi_tautoenunu_fakeelectron_line(
        name="Hlt2SLB_XibminusToXic0TauNu_Xic0TopKKPi_FakeElectron",
        prescale=1.,
        persistreco=True):
    """
    SL Hlt2 line for the decays with fake electron:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau- (-> e- nu nu) nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau+ (-> e+ nu nu) nu (Wrong sign)
    """
    line_alg = make_xibminustoxic0taunu_xic0topkkpi_tautolnunu_fakelepton(
        process=PROCESS, lepton="e")
    #Note following does not work if line_alg is defined as function in different file
    # extra_outputs=[("ext_tr_long", isolation_particles())])
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_xibminustoxic0taunu_xic0topkkpi_tautomununu_fakemuon_line(
        name="Hlt2SLB_XibminusToXic0TauNu_Xic0TopKKPi_FakeMuon",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decays with fake muon:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau- (-> mu- nu nu) nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau+ (-> mu+ nu nu) nu (Wrong sign)
    """
    line_alg = make_xibminustoxic0taunu_xic0topkkpi_tautolnunu_fakelepton(
        process=PROCESS, lepton="mu")
    #Note following does not work if line_alg is defined as function in different file
    # extra_outputs=[("ext_tr_long", isolation_particles())])
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctod0munu_d0tokpi_line(name="Hlt2SLB_BcToD0MuNu_D0ToKPi",
                                 prescale=1.,
                                 persistreco=True):
    """
    SL Hlt2 line for the decay Bc+ -> D0(-> K pi) mu nu.
    """
    line_alg = make_bctod0lnu_d0tokpi(process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctod0enu_d0tokpi_line(name="Hlt2SLB_BcToD0ENu_D0ToKPi",
                                prescale=1.,
                                persistreco=True):
    """
    SL Hlt2 line for the decay Bc+ -> D0(-> K pi) e nu.
    """
    line_alg = make_bctod0lnu_d0tokpi(process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctod0munu_d0tokpi_fakemuon_line(
        name="Hlt2SLB_BcToD0MuNu_D0ToKPi_FakeMuon", prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Bc+ -> D0(-> K pi) mu nu.
    """
    line_alg = make_bctod0lnu_d0tokpi_fakelepton(process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctod0enu_d0tokpi_fakeelectron_line(
        name="Hlt2SLB_BcToD0ENu_D0ToKPi_FakeElectron",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Bc+ -> D0(-> K pi) e nu.
    """
    line_alg = make_bctod0lnu_d0tokpi_fakelepton(process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b0todptaunu_dptokpipi_tautopipipinu_line(
        name="Hlt2SLB_B0ToDpTauNu_DpToKPiPi_TauToPiPiPiNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B0 -> D- tau+, hadronic tau decay.
    """
    line_alg = make_b0todptaunu_dptokpipi_tautopipipinu(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctojpsitaunu_jpsitomumu_tautopipipinu_line(
        name="Hlt2SLB_BcToJpsiTauNu_JpsiToMuMu_TauToPiPiPiNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B_c+ -> J/psi(1S) tau+, hadronic tau decay.
    """
    line_alg = make_bctojpsitaunu_jpsitomumu_tautopipipinu(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bptod0taunu_d0tokpi_tautopipipinu_line(
        name="Hlt2SLB_BuToD0TauNu_D0ToKPi_TauToPiPiPiNu",
        prescale=0.59,
        persistreco=True):
    """
    SL Hlt2 line for the decay B+ -> D~0 tau+, hadronic tau decay.
    """
    line_alg = make_bptod0taunu_d0tokpi_tautopipipinu(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bstodstaunu_dstokkpi_tautopipipinu_line(
        name="Hlt2SLB_BsToDsTauNu_DsToKKPi_TauToPiPiPiNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay B_s0 -> D_s- tau+, hadronic tau decay.
    """
    line_alg = make_bstodstaunu_dstokkpi_tautopipipinu(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopkpi_tautopipipinu_line(
        name="Hlt2SLB_LbToLcTauNu_LcTopKPi_TauToPiPiPiNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lambda_b0 -> Lambda_c+ tau-, hadronic tau decay.
    """
    line_alg = make_lbtolctaunu_lctopkpi_tautopipipinu(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtoptaunu_tautopipipinu_line(name="Hlt2SLB_LbTopTauNu_TauToPiPiPiNu",
                                       prescale=0.43,
                                       persistreco=True):
    """
    SL Hlt2 line for the decay Lambda_b0 -> p+ tau-, hadronic tau decay.
    """
    line_alg = make_lbtoptaunu_tautopipipinu(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b0topimunu_line(name="Hlt2SLB_B2XuMuNuB02Pi",
                         prescale=1,
                         persistreco=True):
    """
    SL Hlt2 line for the decay B0 -> pi mu nu.
    """
    line_alg = make_b0topimunu(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b0topimunu_fakemu_line(name="Hlt2SLB_B2XuMuNuB02pi_NoPIDMu",
                                prescale=0.02,
                                persistreco=True):
    """
    SL Hlt2 line for the decay B0 -> pi mu nu, with a fake muon.
    """
    line_alg = make_b0topimunu_fakemu(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b0topimunu_fakek_line(name="Hlt2SLB_B2XuMuNuB02pi_NoPIDPi",
                               prescale=0.02,
                               persistreco=True):
    """
    SL Hlt2 line for the decay B0 -> pi mu, with a fake pions.
    """
    line_alg = make_b0topimunu_fakek(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b0topitaunu_hadronic_line(name="Hlt2SLB_B2XuTauNu_HadronicB02Pi",
                                   prescale=0.1,
                                   persistreco=True):
    """
    SL Hlt2 line for the decay B0 -> pi tau (->pi+pi+pi-)
    """
    line_alg = make_b0topitaunu_hadronic(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtopmunu_line(name="Hlt2SLB_LbToPMuNu", prescale=1,
                        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> p mu nu.
    """
    line_alg = make_lbtopmunu(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtopmunu_fakep_line(name="Hlt2SLB_Lb2pMuNuFakep",
                              prescale=0.020,
                              persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> p mu nu, with a fake proton.
    """
    line_alg = make_lbtopmunu_fakep(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtopmunu_fakemu_line(name="Hlt2SLB_Lb2pMuNuFakemu",
                               prescale=0.05,
                               persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> p mu nu, with a fake muon.
    """
    line_alg = make_lbtopmunu_fakemu(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtopmunu_ss_line(name="Hlt2SLB_Lb2pMuNuSS",
                           prescale=1,
                           persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> p mu nu (same-sign).
    """
    line_alg = make_lbtopmunu_ss(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtopmunu_ss_fakep_line(name="Hlt2SLB_Lb2pMuNuFakeSSp",
                                 prescale=0.020,
                                 persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> p mu nu (same-sign), with a fake proton.
    """
    line_alg = make_lbtopmunu_ss_fakep(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtopmunu_ss_fakemu_line(name="Hlt2SLB_Lb2pMuNuFakeSSmu",
                                  prescale=0.05,
                                  persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> p mu nu (same-sign), with a fake muon.
    """
    line_alg = make_lbtopmunu_ss_fakemu(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bstokmunu_line(name="Hlt2SLB_B2XuMuNuBs2K",
                        prescale=1,
                        persistreco=True):
    """
    SL Hlt2 line for the decay Bs0 -> K mu nu.
    """
    line_alg = make_bstokmunu(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bstokmunu_fakemu_line(name="Hlt2SLB_B2XuMuNuBs2K_NoPIDMu",
                               prescale=0.02,
                               persistreco=True):
    """
    SL Hlt2 line for the decay Bs0 -> K mu nu, with a fake muon.
    """
    line_alg = make_bstokmunu_fakemu(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bstokmunu_fakek_line(name="Hlt2SLB_B2XuMuNuBs2K_NoPIDK",
                              prescale=0.02,
                              persistreco=True):
    """
    SL Hlt2 line for the decay Bs0 -> K mu, with a fake kaon.
    """
    line_alg = make_bstokmunu_fakek(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bstoktaunu_hadronic_line(name="Hlt2SLB_B2XuTauNu_HadronicBs2K",
                                  prescale=0.1,
                                  persistreco=True):
    """
    SL Hlt2 line for the decay Bs0 -> K tau (->pi+pi+pi-)
    """
    line_alg = make_bstoktaunu_hadronic(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bstoktaunu_muonic_line(name="Hlt2SLB_B2XuTauNu_MuonicBs2K",
                                prescale=1,
                                persistreco=True):
    """
    SL Hlt2 line for the decay Bs0 -> K tau(mu nu nu ) nu.
    """
    line_alg = make_bstoktaunu_muonic(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bstoktaunu_muonic_fakemu_line(
        name="Hlt2SLB_B2XuTauNu_MuonicBs2K_NoPIDMu",
        prescale=0.02,
        persistreco=True):
    """
    SL Hlt2 line for the decay Bs0 -> K tau(mu nu nu) nu, with a fake muon.
    """
    line_alg = make_bstoktaunu_muonic_fakemu(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bstoktaunu_muonic_fakek_line(
        name="Hlt2SLB_B2XuTauNu_MuonicBs2K_NoPIDK",
        prescale=0.02,
        persistreco=True):
    """
    SL Hlt2 line for the decay Bs0 -> K tau(mu nu nu) , with a fake kaon.
    """
    line_alg = make_bstoktaunu_muonic_fakek(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2ppbarmunu_line(name="Hlt2SLB_B2PPbarMuNu",
                          prescale=1,
                          persistreco=True):
    """
    SL Hlt2 line for the B->ppmunu and muonic B->pptaunu decays
    """
    line_alg = make_b2ppbarmunu(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2ppbarmunu_ss_line(name="Hlt2SLB_B2PPbarMuNu_SS",
                             prescale=1,
                             persistreco=True):
    """
    SL Hlt2 line for the B->ppmunu and muonic B->pptaunu decays with same-sign protons
    """
    line_alg = make_b2ppbarmunu_ss(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2ppbarmunu_fakep_line(name="Hlt2SLB_B2PPbarMuNu_FakeP",
                                prescale=1,
                                persistreco=True):
    """
    SL Hlt2 line for the B->ppmunu and muonic B->pptaunu decays with a fake proton of opposite sign to muon
    """
    line_alg = make_b2ppbarmunu_fakep(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2ppbarmunu_fakemu_line(name="Hlt2SLB_B2PPbarMuNu_FakeMu",
                                 prescale=1,
                                 persistreco=True):
    """
    SL Hlt2 line for the B->ppmunu and muonic B->pptaunu decays with a fake muon
    """
    line_alg = make_b2ppbarmunu_fakemu(process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2mumumunu_line(name="Hlt2SLB_B2MuMuMuNu",
                         prescale=1,
                         persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> mu+mu-mu+nu decays
    """
    line_alg = make_b2mumumunu(name=name, process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2emumunu_line(name="Hlt2SLB_B2EMuMuNu", prescale=1,
                        persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> e+mu-mu+nu decays
    """
    line_alg = make_b2emumunu(name=name, process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2mueenu_line(name="Hlt2SLB_B2MuEENu", prescale=1, persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> mu+e-e+nu decays
    """
    line_alg = make_b2mueenu(name=name, process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2eeenu_line(name="Hlt2SLB_B2EEENu", prescale=1, persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> e+e-e+nu decays
    """
    line_alg = make_b2eeenu(name=name, process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2taumumunu_3pi_line(name="Hlt2SLB_B2TauMuMuNu_3Pi",
                              prescale=1,
                              persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> tau+mu-mu+nu decays with hadronic tau->3pi
    """
    line_alg = make_b2taumumunu_3pi(name=name, process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2taueenu_3pi_line(name="Hlt2SLB_B2TauEENu_3Pi",
                            prescale=1,
                            persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> tau+e-e+nu decays with hadronic tau->3pi
    """
    line_alg = make_b2taueenu_3pi(name=name, process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2mumumunu_ss_line(name="Hlt2SLB_B2MuMuMuNu_SS",
                            prescale=1,
                            persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> mu+mu+mu+nu decays: same-sign combinations
    """
    line_alg = make_b2mumumunu_ss(name=name, process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2emumunu_ss_line(name="Hlt2SLB_B2EMuMuNu_SS",
                           prescale=1,
                           persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> e+mu+mu+nu decays: same-sign combinations
    """
    line_alg = make_b2emumunu_ss(name=name, process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2mueenu_ss_line(name="Hlt2SLB_B2MuEENu_SS",
                          prescale=1,
                          persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> mu+e+e+nu decays: same-sign combinations
    """
    line_alg = make_b2mueenu_ss(name=name, process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2eeenu_ss_line(name="Hlt2SLB_B2EEENu_SS",
                         prescale=1,
                         persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> e+e+e+nu decays: same-sign combinations
    """
    line_alg = make_b2eeenu_ss(name=name, process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2taumumunu_3pi_ss_line(name="Hlt2SLB_B2TauMuMuNu_3Pi_SS",
                                 prescale=1,
                                 persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> tau+mu+mu+nu decays with hadronic tau->3pi: same-sign combinations
    """
    line_alg = make_b2taumumunu_3pi_ss(name=name, process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2taueenu_3pi_ss(name="Hlt2SLB_B2TauEENu_3Pi_SS",
                          prescale=1,
                          persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> tau+e+e+nu decays with hadronic tau->3pi: same-sign combinations
    """
    line_alg = make_b2taueenu_3pi_ss(name=name, process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2mumumunu_trifakemuon_line(name="Hlt2SLB_B2MuMuMuNu_TriFakeMuon",
                                     prescale=0.01,
                                     persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> mu+mu-mu+nu decays: three fake muons
    """
    line_alg = make_b2mumumunu_trifakemuon(name=name, process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2mumumunu_onefakemuon_line(name="Hlt2SLB_B2MuMuMuNu_OneFakeMuon",
                                     prescale=0.1,
                                     persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> mu+mu-mu+nu decays: one fake muon
    """
    line_alg = make_b2mumumunu_onefakemuon(name=name, process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2emumunu_fakeeelectron_line(name="Hlt2SLB_B2EMuMuNu_FakeElectron",
                                      prescale=0.1,
                                      persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> e+mu-mu+nu decays: fake electron
    """
    line_alg = make_b2emumunu_fakeelectron(name=name, process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2mueenu_fakemuon_line(name="Hlt2SLB_B2MuEENu_FakeMuon",
                                prescale=0.05,
                                persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> mu+e-e+nu decays: fake muon
    """
    line_alg = make_b2mueenu_fakemuon(name=name, process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2eeenu_trifakeelectron_line(name="Hlt2SLB_B2EEENu_TriFakeElectron",
                                      prescale=0.01,
                                      persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> e+e-e+nu decays: three fake electrons
    """
    line_alg = make_b2eeenu_trifakeelectron(name=name, process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_b2eeenu_onefakeelectron_line(name="Hlt2SLB_B2EEENu_OneFakeElectron",
                                      prescale=0.05,
                                      persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> e+e-e+nu decays: one fake electron
    """
    line_alg = make_b2eeenu_onefakeelectron(name=name, process=PROCESS)
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_omegabtoomegacmunu_omegactopkkpi_line(
        name="Hlt2SLB_OmegabToOmegacMuNu_OmegacToPKKPi",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay [Omegab- -> Omegac0(-> p K- K- pi+) mu nu]cc.
    """
    line_alg = make_omegabtoomegaclnu_omegactopkkpi(
        process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_omegabtoomegacenu_omegactopkkpi_line(
        name="Hlt2SLB_OmegabToOmegacENu_OmegacToPKKPi",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay [Omegab- -> Omegac0(-> p K- K- pi+) e nu]cc.
    """
    line_alg = make_omegabtoomegaclnu_omegactopkkpi(
        process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_omegabtoomegacmunu_omegactopkkpi_fakemuon_line(
        name="Hlt2SLB_OmegabToOmegacMuNu_OmegacToPKKPi_FakeMuon",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay [Omegab- -> Omegac0(-> p K- K- pi+) mu nu]cc with a fake muon.
    """
    line_alg = make_omegabtoomegaclnu_omegactopkkpi_fakelepton(
        process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_omegabtoomegacenu_omegactopkkpi_fakeelectron_line(
        name="Hlt2SLB_OmegabToOmegacENu_OmegacToPKKPi_FakeElectron",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay [Omegab- -> Omegac0(-> p K- K- pi+) e nu]cc with a fake electron.
    """
    line_alg = make_omegabtoomegaclnu_omegactopkkpi_fakelepton(
        process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_omegabtoomegactaunu_omegactopkkpi_tautomunu_line(
        name="Hlt2SLB_OmegabToOmegacTauNu_OmegacToPKKPi_TauToMuNuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Omegab- -> Omegac0(-> p K- K- pi+) tau-(-> mu nu nu) nu.
    """
    line_alg = make_omegabtoomegactaunu_omegactopkkpi_tautolnunu(
        process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_omegabtoomegactaunu_omegactopkkpi_tautomunu_fakemuon_line(
        name="Hlt2SLB_OmegabToOmegacTauNu_OmegacToPKKPi_FakeMuon",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Omegab- -> Omegac0(-> p K- K- pi+) tau-(-> mu nu nu) nu, with a fake muon.
    """
    line_alg = make_omegabtoomegactaunu_omegactopkkpi_tautolnunu_fakelepton(
        process=PROCESS, lepton="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_omegabtoomegactaunu_omegactopkkpi_tautoenu_line(
        name="Hlt2SLB_OmegabToOmegacTauNu_OmegacToPKKPi_TauToENuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Omegab- -> Omegac0(-> p K- K- pi+) tau-(-> e nu nu) nu.
    """
    line_alg = make_omegabtoomegactaunu_omegactopkkpi_tautolnunu(
        process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_omegabtoomegactaunu_omegactopkkpi_tautoenu_fakeelectron_line(
        name="Hlt2SLB_OmegabToOmegacTauNu_OmegacToPKKPi_FakeElectron",
        prescale=0.1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Omegab- -> Omegac0(-> p K- K- pi+) tau-(-> e nu nu) nu, with a fake electron.
    """
    line_alg = make_omegabtoomegactaunu_omegactopkkpi_tautolnunu_fakelepton(
        process=PROCESS, lepton="e")
    return HltLine(
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg],
        persistreco=persistreco)


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctopksLL_line(name="Hlt2SLB_LbToLcMuNu_LcToPKSLL",
                                   prescale=1,
                                   persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) mu nu.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolclnu_lctoV0h(
        process=PROCESS, V0_name="KS0", V0_type="LL", lepton_name="mu")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctopksDD_line(name="Hlt2SLB_LbToLcMuNu_LcToPKSDD",
                                   prescale=1,
                                   persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) mu nu.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'downstream' tracks ('DD' category).
    """
    line_alg = make_lbtolclnu_lctoV0h(
        process=PROCESS, V0_name="KS0", V0_type="DD", lepton_name="mu")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcenu_lctopksLL_line(name="Hlt2SLB_LbToLcENu_LcToPKSLL",
                                  prescale=1,
                                  persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) e nu.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolclnu_lctoV0h(
        process=PROCESS, V0_name="KS0", V0_type="LL", lepton_name="e")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcenu_lctopksDD_line(name="Hlt2SLB_LbToLcENu_LcToPKSDD",
                                  prescale=1,
                                  persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) e nu.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'downstream' tracks ('DD' category).
    """
    line_alg = make_lbtolclnu_lctoV0h(
        process=PROCESS, V0_name="KS0", V0_type="DD", lepton_name="e")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctopksLL_fakemuon_line(
        name="Hlt2SLB_LbToLcMuNu_LcToPKSLL_FakeMuon", prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) mu nu, with a fake muon.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolclnu_lctoV0h_fakelepton(
        process=PROCESS, V0_name="KS0", V0_type="LL", lepton_name="mu")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctopksDD_fakemuon_line(
        name="Hlt2SLB_LbToLcMuNu_LcToPKSDD_FakeMuon", prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) mu nu, with a fake muon.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'downstream' tracks ('DD' category).
    """
    line_alg = make_lbtolclnu_lctoV0h_fakelepton(
        process=PROCESS, V0_name="KS0", V0_type="DD", lepton_name="mu")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcenu_lctopksLL_fakeelectron_line(
        name="Hlt2SLB_LbToLcENu_LcToPKSLL_FakeElectron",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) e nu, with a fake electron.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolclnu_lctoV0h_fakelepton(
        process=PROCESS, V0_name="KS0", V0_type="LL", lepton_name="e")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcenu_lctopksDD_fakeelectron_line(
        name="Hlt2SLB_LbToLcENu_LcToPKSDD_FakeElectron",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) e nu, with a fake electron.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'downstream' tracks ('DD' category).
    """
    line_alg = make_lbtolclnu_lctoV0h_fakelepton(
        process=PROCESS, V0_name="KS0", V0_type="DD", lepton_name="e")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopksLL_tautomunu_line(
        name="Hlt2SLB_LbToLcTauNu_LcToPKSLL_TautoMuNuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) tau (-> mu nu nu) nu.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu(
        process=PROCESS, V0_name="KS0", V0_type="LL", lepton_name="mu")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopksDD_tautomunu_line(
        name="Hlt2SLB_LbToLcTauNu_LcToPKSDD_TautoMuNuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) tau (-> mu nu nu) nu.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('DD' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu(
        process=PROCESS, V0_name="KS0", V0_type="DD", lepton_name="mu")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopksLL_tautoenu_line(
        name="Hlt2SLB_LbToLcTauNu_LcToPKSLL_TautoENuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) tau (-> e nu nu) nu.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu(
        process=PROCESS, V0_name="KS0", V0_type="LL", lepton_name="e")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopksDD_tautoenu_line(
        name="Hlt2SLB_LbToLcTauNu_LcToPKSDD_TautoENuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) tau (-> e nu nu) nu.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('DD' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu(
        process=PROCESS, V0_name="KS0", V0_type="DD", lepton_name="e")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopksLL_tautomunu_fakemuon_line(
        name="Hlt2SLB_LbToLcTauNu_LcToPKSLL_FakeMuon",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) tau (-> mu nu nu) nu, with a fake muon.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu_fakelepton(
        process=PROCESS, V0_name="KS0", V0_type="LL", lepton_name="mu")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopksDD_tautomunu_fakemuon_line(
        name="Hlt2SLB_LbToLcTauNu_LcToPKSDD_FakeMuon",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) tau (-> mu nu nu) nu, with a fake muon.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('DD' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu_fakelepton(
        process=PROCESS, V0_name="KS0", V0_type="DD", lepton_name="mu")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopksLL_tautoenu_fakeelectron_line(
        name="Hlt2SLB_LbToLcTauNu_LcToPKSLL_FakeElectron",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) tau (-> e nu nu) nu, with a fake electron.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu_fakelepton(
        process=PROCESS, V0_name="KS0", V0_type="LL", lepton_name="e")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopksDD_tautoenu_fakeelectron_line(
        name="Hlt2SLB_LbToLcTauNu_LcToPKSDD_FakeElectron",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) tau (-> e nu nu) nu, with a fake electron.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('DD' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu_fakelepton(
        process=PROCESS, V0_name="KS0", V0_type="DD", lepton_name="e")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctolambdapiLL_line(
        name="Hlt2SLB_LbToLcMuNu_LcToLambdaPiLL", prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) mu nu.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolclnu_lctoV0h(
        process=PROCESS, V0_name="Lambda0", V0_type="LL", lepton_name="mu")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctolambdapiDD_line(
        name="Hlt2SLB_LbToLcMuNu_LcToLambdaPiDD", prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) mu nu.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'downstream' tracks ('DD' category).
    """
    line_alg = make_lbtolclnu_lctoV0h(
        process=PROCESS, V0_name="Lambda0", V0_type="DD", lepton_name="mu")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcenu_lctolambdapiLL_line(name="Hlt2SLB_LbToLcENu_LcToLambdaPiLL",
                                       prescale=1,
                                       persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) e nu.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolclnu_lctoV0h(
        process=PROCESS, V0_name="Lambda0", V0_type="LL", lepton_name="e")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcenu_lctolambdapiDD_line(name="Hlt2SLB_LbToLcENu_LcToLambdaPiDD",
                                       prescale=1,
                                       persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) e nu.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'downstream' tracks ('DD' category).
    """
    line_alg = make_lbtolclnu_lctoV0h(
        process=PROCESS, V0_name="Lambda0", V0_type="DD", lepton_name="e")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctolambdapiLL_fakemuon_line(
        name="Hlt2SLB_LbToLcMuNu_LcToLambdaPiLL_FakeMuon",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) mu nu, with a fake muon.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolclnu_lctoV0h_fakelepton(
        process=PROCESS, V0_name="Lambda0", V0_type="LL", lepton_name="mu")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctolambdapiDD_fakemuon_line(
        name="Hlt2SLB_LbToLcMuNu_LcToLambdaPiDD_FakeMuon",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) mu nu, with a fake muon.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'downstream' tracks ('DD' category).
    """
    line_alg = make_lbtolclnu_lctoV0h_fakelepton(
        process=PROCESS, V0_name="Lambda0", V0_type="DD", lepton_name="mu")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcenu_lctolambdapiLL_fakeelectron_line(
        name="Hlt2SLB_LbToLcENu_LcToLambdaPiLL_FakeElectron",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) e nu, with a fake electron.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolclnu_lctoV0h_fakelepton(
        process=PROCESS, V0_name="Lambda0", V0_type="LL", lepton_name="e")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolcenu_lctolambdapiDD_fakeelectron_line(
        name="Hlt2SLB_LbToLcENu_LcToLambdaPiDD_FakeElectron",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) e nu, with a fake electron.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'downstream' tracks ('DD' category).
    """
    line_alg = make_lbtolclnu_lctoV0h_fakelepton(
        process=PROCESS, V0_name="Lambda0", V0_type="DD", lepton_name="e")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctolambdapiLL_tautomunu_line(
        name="Hlt2SLB_LbToLcTauNu_LcToLambdaPiLL_TautoMuNuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) tau (-> mu nu nu) nu.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu(
        process=PROCESS, V0_name="Lambda0", V0_type="LL", lepton_name="mu")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctolambdapiDD_tautomunu_line(
        name="Hlt2SLB_LbToLcTauNu_LcToLambdaPiDD_TautoMuNuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) tau (-> mu nu nu) nu.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('DD' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu(
        process=PROCESS, V0_name="Lambda0", V0_type="DD", lepton_name="mu")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctolambdapiLL_tautoenu_line(
        name="Hlt2SLB_LbToLcTauNu_LcToLambdaPiLL_TautoENuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) tau (-> e nu nu) nu.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu(
        process=PROCESS, V0_name="Lambda0", V0_type="LL", lepton_name="e")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctolambdapiDD_tautoenu_line(
        name="Hlt2SLB_LbToLcTauNu_LcToLambdaPiDD_TautoENuNu",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) tau (-> e nu nu) nu.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('DD' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu(
        process=PROCESS, V0_name="Lambda0", V0_type="DD", lepton_name="e")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctolambdapiLL_tautomunu_fakemuon_line(
        name="Hlt2SLB_LbToLcTauNu_LcToLambdaPiLL_FakeMuon",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) tau (-> mu nu nu) nu, with a fake muon.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu_fakelepton(
        process=PROCESS, V0_name="Lambda0", V0_type="LL", lepton_name="mu")
    return HltLine(
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg],
        persistreco=persistreco)


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctolambdapiDD_tautomunu_fakemuon_line(
        name="Hlt2SLB_LbToLcTauNu_LcToLambdaPiDD_FakeMuon",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) tau (-> mu nu nu) nu, with a fake muon.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('DD' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu_fakelepton(
        process=PROCESS, V0_name="Lambda0", V0_type="DD", lepton_name="mu")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctolambdapiLL_tautoenu_fakeelectron_line(
        name="Hlt2SLB_LbToLcTauNu_LcToLambdaPiLL_FakeElectron",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) tau (-> e nu nu) nu, with a fake electron.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu_fakelepton(
        process=PROCESS, V0_name="Lambda0", V0_type="LL", lepton_name="e")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctolambdapiDD_tautoenu_fakeelectron_line(
        name="Hlt2SLB_LbToLcTauNu_LcToLambdaPiDD_FakeElectron",
        prescale=1,
        persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) tau (-> e nu nu) nu, with a fake electron.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('DD' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu_fakelepton(
        process=PROCESS, V0_name="Lambda0", V0_type="DD", lepton_name="e")
    return HltLine(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobsmunu_bstodspi_line(name="Hlt2SLB_BcToBsMuNu_BsToDsPi",
                                  persistreco=True,
                                  prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs mu nu with Bs -> Ds pi
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsMuNu", BsDecay="BsToDsPi")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobsmunu_bstodsk_line(name="Hlt2SLB_BcToBsMuNu_BsToDsK",
                                 persistreco=True,
                                 prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs mu nu with Bs -> Ds K
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsMuNu", BsDecay="BsToDsK")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobsmunu_bstojpsiphi_line(name="Hlt2SLB_BcToBsMuNu_BsToJpsiPhi",
                                     persistreco=True,
                                     prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs mu nu with Bs -> J/psi phi
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsMuNu", BsDecay="BsToJpsiPhi")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobsmunu_bstokk_line(name="Hlt2SLB_BcToBsMuNu_BsToKK",
                                persistreco=True,
                                prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs mu nu with Bs -> K- K+
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsMuNu", BsDecay="BsToKK")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobsmunu_bstokpi_line(name="Hlt2SLB_BcToBsMuNu_BsToKPi",
                                 persistreco=True,
                                 prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs mu nu with Bs -> K+ pi- and CC
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsMuNu", BsDecay="BsToKPi")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobsmunu_bstopipi_line(name="Hlt2SLB_BcToBsMuNu_BsToPiPi",
                                  persistreco=True,
                                  prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs mu nu with Bs -> pi+ pi-
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsMuNu", BsDecay="BsToPiPi")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobsenu_bstodspi_line(name="Hlt2SLB_BcToBsENu_BsToDsPi",
                                 persistreco=True,
                                 prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs e nu with Bs -> Ds pi
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsENu", BsDecay="BsToDsPi")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobsenu_bstodsk_line(name="Hlt2SLB_BcToBsENu_BsToDsK",
                                persistreco=True,
                                prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs e nu with Bs -> Ds K
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsENu", BsDecay="BsToDsK")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobsenu_bstojpsiphi_line(name="Hlt2SLB_BcToBsENu_BsToJpsiPhi",
                                    persistreco=True,
                                    prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs e nu with Bs -> J/psi phi
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsENu", BsDecay="BsToJpsiPhi")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobsenu_bstokk_line(name="Hlt2SLB_BcToBsENu_BsToKK",
                               persistreco=True,
                               prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs e nu with Bs -> K- K+
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsENu", BsDecay="BsToKK")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobsenu_bstokpi_line(name="Hlt2SLB_BcToBsENu_BsToKPi",
                                persistreco=True,
                                prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs e nu with Bs -> K+ pi- and CC
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsENu", BsDecay="BsToKPi")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobsenu_bstopipi_line(name="Hlt2SLB_BcToBsENu_BsToPiPi",
                                 persistreco=True,
                                 prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs e nu with Bs -> pi+ pi-
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsENu", BsDecay="BsToPiPi")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobspi_bstodspi_line(name="Hlt2SLB_BcToBsPi_BsToDsPi",
                                persistreco=True,
                                prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs pi with Bs -> Ds pi
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsPi", BsDecay="BsToDsPi")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobspi_bstodsk_line(name="Hlt2SLB_BcToBsPi_BsToDsK",
                               persistreco=True,
                               prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs pi with Bs -> Ds K
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsPi", BsDecay="BsToDsK")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobspi_bstojpsiphi_line(name="Hlt2SLB_BcToBsPi_BsToJpsiPhi",
                                   persistreco=True,
                                   prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs pi with Bs -> J/psi phi
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsPi", BsDecay="BsToJpsiPhi")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobspi_bstokk_line(name="Hlt2SLB_BcToBsPi_BsToKK",
                              persistreco=True,
                              prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs pi with Bs -> K+ K-
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsPi", BsDecay="BsToKK")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobspi_bstokpi_line(name="Hlt2SLB_BcToBsPi_BsToKPi",
                               persistreco=True,
                               prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs pi with Bs -> K+ pi-
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsPi", BsDecay="BsToKPi")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobspi_bstopipi_line(name="Hlt2SLB_BcToBsPi_BsToPiPi",
                                persistreco=True,
                                prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs pi with Bs -> pi+ pi-
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsPi", BsDecay="BsToPiPi")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobsk_bstodspi_line(name="Hlt2SLB_BcToBsK_BsToDsPi",
                               persistreco=True,
                               prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs K with Bs -> Ds pi
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsK", BsDecay="BsToDsPi")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobsk_bstodsk_line(name="Hlt2SLB_BcToBsK_BsToDsK",
                              persistreco=True,
                              prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs K with Bs -> Ds K
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsK", BsDecay="BsToDsK")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobsk_bstojpsiphi_line(name="Hlt2SLB_BcToBsK_BsToJpsiPhi",
                                  persistreco=True,
                                  prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs K with Bs -> J/psi phi
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsK", BsDecay="BsToJpsiPhi")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobsk_bstokk_line(name="Hlt2SLB_BcToBsK_BsToKK",
                             persistreco=True,
                             prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs K with Bs -> K+ K-
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsK", BsDecay="BsToKK")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobsk_bstokpi_line(name="Hlt2SLB_BcToBsK_BsToKPi",
                              persistreco=True,
                              prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs K with Bs -> K+ pi- and CC
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsK", BsDecay="BsToKPi")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])


@register_line_builder(hlt2_lines)
def hlt2_bctobsk_bstopipi_line(name="Hlt2SLB_BcToBsK_BsToPiPi",
                               persistreco=True,
                               prescale=1.0):
    """
    SL Hlt2 line for the decay Bc -> Bs K with Bs -> pi+ pi-
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsK", BsDecay="BsToPiPi")
    return HltLine(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg])
