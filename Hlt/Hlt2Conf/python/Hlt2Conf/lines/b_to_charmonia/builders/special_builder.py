###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of special descriptor for non-default Bs meson decays 
that can be shared in the B2CC selections, and therefore are defined centrally.
"""
from Hlt2Conf.algorithms_thor import ParticleCombiner
from PyConf import configurable
from RecoConf.reconstruction_objects import make_pvs
import Functors as F


@configurable
def make_X2BsLep(particles,
                 descriptor,
                 name='B2CC_X2Bsmu_Combiner_{hash}',
                 pvs=make_pvs):
    """
    A X->BLep decay maker, where X
    is a dummy particle
    """
    combination_code = F.ALL
    vertex_code = F.ALL
    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)
