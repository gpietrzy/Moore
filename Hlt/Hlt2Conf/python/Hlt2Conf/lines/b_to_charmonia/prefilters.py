###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Common prefilters used by all lines
"""
from RecoConf.event_filters import require_pvs, require_gec
from RecoConf.reconstruction_objects import (make_pvs, upfront_reconstruction)


# make sure we have a PV
# at the momement GEC not mandatory
def b2cc_prefilters(require_GEC=False):

    if require_GEC:
        return upfront_reconstruction() + [
            require_gec(), require_pvs(make_pvs())
        ]
    else:
        return upfront_reconstruction() + [require_pvs(make_pvs())]
