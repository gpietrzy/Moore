###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Booking of B2CC sprucing lines, notice PROCESS = 'spruce'

Output:
updated dictionary of sprucing_lines

"""
from Moore.config import SpruceLine, register_line_builder

from PyConf import configurable
from .prefilters import b2cc_prefilters
from Hlt2Conf.lines.b_to_charmonia import b_to_jpsix

PROCESS = 'spruce'
sprucing_lines = {}


@register_line_builder(sprucing_lines)
@configurable
def BsToJpsiPhi_JpsiToMuMu_sprucing_line(
        name='SpruceB2CC_BsToJpsiPhi_Detached', prescale=1):
    """
     Bs0 --> Jpsi(-> mu+ mu-) phi spruce line
    """
    line_alg = b_to_jpsix.make_BsToJpsiPhi_detached_line(process=PROCESS)

    return SpruceLine(
        name=name, prescale=prescale, algs=b2cc_prefilters() + line_alg)


@register_line_builder(sprucing_lines)
@configurable
def BsToJpsiKstarWide_line(name='SpruceB2CC_BsToJpsiKstarWide', prescale=1):
    """
     Bs0 --> Jpsi(-> mu+ mu-) Kst spruce line
    """
    line_alg = b_to_jpsix.make_BsToJpsiKstar_line(process=PROCESS)

    return SpruceLine(
        name=name, prescale=prescale, algs=b2cc_prefilters() + line_alg)
