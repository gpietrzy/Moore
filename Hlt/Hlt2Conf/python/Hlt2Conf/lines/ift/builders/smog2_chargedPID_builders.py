###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of builders for SMOG2 HLT2 lines for charged PID calibration
"""
from Hlt2Conf.lines.ift.builders.smog2_builders import (
    sv_in_smog2,
    make_smog2_common_particles,
    bpv_in_smog2,
)
from Hlt2Conf.standard_particles import make_has_rich_long_kaons
from Hlt2Conf.standard_particles import make_long_pions, make_long_protons, make_long_kaons
from Hlt2Conf.standard_particles import make_down_pions, make_down_protons, make_down_kaons
from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner

from GaudiKernel.SystemOfUnits import MeV, GeV, mm, ns
import Functors as F
from Functors.math import in_range
from RecoConf.reconstruction_objects import make_pvs

all_lines = {}

_MASSWINDOW_KS = [(497.7 - 50) * MeV, (497.7 + 50) * MeV]
_MASSWINDOW_KStar = [(891.67 - 300) * MeV, (891.67 + 300) * MeV]
_MASSWINDOW_LAMBDA0 = [(1115.683 - 25) * MeV, (1115.683 + 25) * MeV]

_MASSWINDOW_COMB_PHI = [(1019.445 - 40) * MeV, (1019.445 + 40) * MeV]
_MASSWINDOW_VERTEX_PHI = [(1019.445 - 20) * MeV, (1019.445 + 20) * MeV]

_MASSWINDOW_OMEGA = [(1672 - 25) * MeV, (1672 + 25) * MeV]
_MASSWINDOW_XI = [(1321.71 - 25) * MeV, (1321.71 + 25) * MeV]

_MASS_KS = 497.7 * MeV
_MASS_Lambda0 = 1115.683 * MeV
_MASS_PHI = 1019.445 * MeV


def make_probe_kaon(probe_charge, min_p, min_pt, max_trchi2dof):
    kaons = make_smog2_common_particles(
        make_long_kaons,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        min_pt=min_pt,
    )
    return ParticleFilter(kaons, Cut=F.FILTER(probe_charge))


def make_tag_kaon(tag_charge, min_p, min_pt, max_trchi2dof, max_ghostprob,
                  min_pidk):
    kaons = make_smog2_common_particles(
        make_has_rich_long_kaons,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        min_pt=min_pt,
        pid_cut=min_pidk,
        particle="Kaon",
    )
    return ParticleFilter(
        kaons,
        Cut=F.FILTER(F.require_all(tag_charge, F.GHOSTPROB < max_ghostprob)))


def make_dihadron(pvs, particle1, particle2, name, descriptor, mmincomb,
                  mmaxcomb, mminver, mmaxver, apt_min, maxsdoca,
                  vchi2pdof_max):

    combination_code = F.require_all(F.PT > apt_min,
                                     in_range(mmincomb, F.MASS, mmaxcomb),
                                     F.MAXSDOCACUT(maxsdoca))
    vertex_code = F.require_all(
        in_range(mminver, F.MASS, mmaxver),
        F.CHI2DOF < vchi2pdof_max,
        sv_in_smog2(),
        bpv_in_smog2(pvs),
    )

    return ParticleCombiner(
        Inputs=[particle1, particle2],
        DecayDescriptor=descriptor,
        name=name,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


def make_detached_dihadron_ll(
        pvs,
        particle1,
        particle2,
        name,
        descriptors,
        mmin,
        mmax,
        apt_min,
        vchi2pdof_max,
        end_vz_max,
        bpvvdchi2_min,
        bpvltime_min,
        parent_bpvipchi2_max,
        ks_veto_window=None,
        lambda_veto_window=None,
):
    combination_code = F.require_all(F.PT > apt_min,
                                     in_range(mmin, F.MASS, mmax))

    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.END_VZ < end_vz_max,
        F.BPVFDCHI2(pvs()) > bpvvdchi2_min,
        F.BPVLTIME(pvs()) > bpvltime_min,
        bpv_in_smog2(pvs),
    )

    if parent_bpvipchi2_max is not None:
        vertex_code &= (F.BPVIPCHI2(pvs()) < parent_bpvipchi2_max)

    if ks_veto_window is not None:
        vertex_code &= ((F.MASSWITHHYPOTHESES(
            ("pi+", "pi-")) < _MASS_KS - ks_veto_window) |
                        (F.MASSWITHHYPOTHESES(
                            ("pi+", "pi-")) > _MASS_KS + ks_veto_window))

    if lambda_veto_window is not None:
        vertex_code &= (
            (F.MASSWITHHYPOTHESES(
                ("p+", "pi-")) < _MASS_Lambda0 - lambda_veto_window) |
            (F.MASSWITHHYPOTHESES(
                ("p+", "pi-")) > _MASS_Lambda0 + lambda_veto_window))
        vertex_code &= (
            (F.MASSWITHHYPOTHESES(
                ("pi+", "p~-")) < _MASS_Lambda0 - lambda_veto_window) |
            (F.MASSWITHHYPOTHESES(
                ("pi+", "p~-")) > _MASS_Lambda0 + lambda_veto_window))

    return ParticleCombiner(
        Inputs=[particle1, particle2],
        DecayDescriptor=descriptors,
        name=name,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def make_detached_dihadron_dd(
        pvs,
        particle1,
        particle2,
        name,
        descriptors,
        mmin,
        mmax,
        apt_min,
        vchi2pdof_max,
        bpvvdz_min,
        bpvvdz_max,
        bpvvdchi2_min,
        parent_bpvipchi2_max,
        ks_veto_window=None,
        lambda_veto_window=None,
):
    combination_code = F.require_all(F.PT > apt_min,
                                     in_range(mmin, F.MASS, mmax))
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.BPVVDZ(pvs()) > bpvvdz_min,
        F.BPVVDZ(pvs()) < bpvvdz_max,
        F.BPVFDCHI2(pvs()) > bpvvdchi2_min,
        bpv_in_smog2(pvs),
    )

    if parent_bpvipchi2_max is not None:
        vertex_code &= (F.BPVIPCHI2(pvs()) < parent_bpvipchi2_max)

    if ks_veto_window is not None:
        vertex_code &= ((F.MASSWITHHYPOTHESES(
            ("pi+", "pi-")) < _MASS_KS - ks_veto_window) |
                        (F.MASSWITHHYPOTHESES(
                            ("pi+", "pi-")) > _MASS_KS + ks_veto_window))

    if lambda_veto_window is not None:
        vertex_code &= (
            (F.MASSWITHHYPOTHESES(
                ("p+", "pi-")) < _MASS_Lambda0 - lambda_veto_window) |
            (F.MASSWITHHYPOTHESES(
                ("p+", "pi-")) > _MASS_Lambda0 + lambda_veto_window))
        vertex_code &= (
            (F.MASSWITHHYPOTHESES(
                ("pi+", "p~-")) < _MASS_Lambda0 - lambda_veto_window) |
            (F.MASSWITHHYPOTHESES(
                ("pi+", "p~-")) > _MASS_Lambda0 + lambda_veto_window))

    return ParticleCombiner(
        Inputs=[particle1, particle2],
        DecayDescriptor=descriptors,
        name=name,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def make_smog2_ks2pipi_ll_line(
        pvs=make_pvs,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin=_MASSWINDOW_KS[0],
        mmax=_MASSWINDOW_KS[1],
        apt_min=0 * MeV,
        vchi2pdof_max=30.0,
        end_vz_max=2200 * mm,
        bpvvdchi2_min=0,
        bpvltime_min=0.002 * ns,
        parent_bpvipchi2_max=75,
        lambda_veto_window=9 * MeV,
        process="hlt2",
        name="smog2_ks2pipi_ll",
):
    descriptor = "KS0 -> pi+ pi-"
    pions = make_smog2_common_particles(
        make_long_pions,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        pvs=pvs,
        min_bpvipchi2=min_bpvipchi2,
    )

    return make_detached_dihadron_ll(
        pvs,
        pions,
        pions,
        name,
        descriptor,
        mmin,
        mmax,
        apt_min,
        vchi2pdof_max,
        end_vz_max,
        bpvvdchi2_min,
        bpvltime_min,
        parent_bpvipchi2_max,
        lambda_veto_window=lambda_veto_window,
    )


def make_smog2_kstar2kpi_line(
        pvs=make_pvs,
        min_p=2 * GeV,
        max_trchi2dof=5,
        minPIDKaon=None,
        maxPIDKaon=None,
        min_bpvipchi2=25,
        mmin=_MASSWINDOW_KStar[0],
        mmax=_MASSWINDOW_KStar[1],
        mminver=_MASSWINDOW_KStar[0],
        mmaxver=_MASSWINDOW_KStar[1],
        apt_min=0 * MeV,
        maxsdoca=10 * mm,
        vchi2pdof_max=16.,
        process="hlt2",
        name='smog2_kstar2kpi',
):
    descriptor = '[K*(892)0 -> K+ pi-]cc'

    pions = make_smog2_common_particles(
        make_particles=make_long_pions,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        pvs=pvs,
        pid_cut=maxPIDKaon,
        particle='Pion')
    kaons = make_smog2_common_particles(
        make_particles=make_long_kaons,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        pvs=pvs,
        pid_cut=minPIDKaon,
        particle='Kaon')

    return make_dihadron(pvs, kaons, pions, name, descriptor, mmin, mmax,
                         mminver, mmaxver, apt_min, maxsdoca, vchi2pdof_max)


def make_smog2_ks2pipi_dd_line(
        pvs=make_pvs,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin=_MASSWINDOW_KS[0],
        mmax=_MASSWINDOW_KS[1],
        apt_min=0 * MeV,
        vchi2pdof_max=30.0,
        bpvvdz_min=400 * mm,
        bpvvdz_max=2800 * mm,
        bpvvdchi2_min=0,
        parent_bpvipchi2_max=150,
        lambda_veto_window=9 * MeV,
        process="hlt2",
        name="smog2_ks2pipi_dd",
):
    descriptor = "KS0 -> pi+ pi-"
    pions = make_smog2_common_particles(
        make_down_pions,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        pvs=pvs,
        min_bpvipchi2=min_bpvipchi2,
    )

    return make_detached_dihadron_dd(
        pvs,
        pions,
        pions,
        name,
        descriptor,
        mmin,
        mmax,
        apt_min,
        vchi2pdof_max,
        bpvvdz_min,
        bpvvdz_max,
        bpvvdchi2_min,
        parent_bpvipchi2_max,
        lambda_veto_window=lambda_veto_window,
    )


def make_smog2_L02ppi_ll_line(
        pvs=make_pvs,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin=_MASSWINDOW_LAMBDA0[0],
        mmax=_MASSWINDOW_LAMBDA0[1],
        apt_min=0 * MeV,
        vchi2pdof_max=30.0,
        end_vz_max=2200 * mm,
        bpvvdchi2_min=0,
        bpvltime_min=0 * ns,
        parent_bpvipchi2_max=200,
        ks_veto_window=20 * MeV,
        process="hlt2",
        name="smog2_L02ppi_ll",
):

    descriptor = "[Lambda0 -> p+ pi-]cc"

    pions = make_smog2_common_particles(
        make_long_pions,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        pvs=pvs,
        min_bpvipchi2=min_bpvipchi2,
    )
    protons = make_smog2_common_particles(
        make_long_protons,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        pvs=pvs,
        min_bpvipchi2=min_bpvipchi2,
    )

    return make_detached_dihadron_ll(
        pvs,
        protons,
        pions,
        name,
        descriptor,
        mmin,
        mmax,
        apt_min,
        vchi2pdof_max,
        end_vz_max,
        bpvvdchi2_min,
        bpvltime_min,
        parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
        lambda_veto_window=None,
    )


def make_smog2_L02ppi_dd_line(
        pvs=make_pvs,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin=_MASSWINDOW_LAMBDA0[0],
        mmax=_MASSWINDOW_LAMBDA0[1],
        apt_min=0 * MeV,
        vchi2pdof_max=30.0,
        bpvvdz_min=400,
        bpvvdz_max=2800,
        bpvvdchi2_min=0,
        parent_bpvipchi2_max=500,
        ks_veto_window=20 * MeV,
        process="hlt2",
        name="smog2_L02ppi_dd",
):
    descriptor = "[Lambda0 -> p+ pi-]cc"
    pions = make_smog2_common_particles(
        make_down_pions,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        pvs=pvs,
        min_bpvipchi2=min_bpvipchi2,
    )
    protons = make_smog2_common_particles(
        make_down_protons,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        pvs=pvs,
        min_bpvipchi2=min_bpvipchi2,
    )

    return make_detached_dihadron_dd(
        pvs,
        protons,
        pions,
        name,
        descriptor,
        mmin,
        mmax,
        apt_min,
        vchi2pdof_max,
        bpvvdz_min,
        bpvvdz_max,
        bpvvdchi2_min,
        parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
        lambda_veto_window=None,
    )


def make_smog2_phi2kk(name,
                      pvs=make_pvs,
                      min_p=2 * GeV,
                      min_pt=380 * MeV,
                      max_trchi2dof=5,
                      max_ghostprob=0.25,
                      min_pidk=0,
                      mmincomb=_MASSWINDOW_COMB_PHI[0],
                      mmaxcomb=_MASSWINDOW_COMB_PHI[1],
                      mminver=_MASSWINDOW_VERTEX_PHI[0],
                      mmaxver=_MASSWINDOW_VERTEX_PHI[1],
                      apt_min=0 * MeV,
                      maxsdoca=10 * mm,
                      vchi2pdof_max=16.,
                      process="hlt2"):

    descriptor = 'phi(1020) -> K+ K-'

    kaons = make_smog2_common_particles(
        make_particles=make_long_kaons,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        pvs=pvs,
        pid_cut=min_pidk,
        particle='Kaon')

    return make_dihadron(pvs, kaons, kaons, name, descriptor, mmincomb,
                         mmaxcomb, mminver, mmaxver, apt_min, maxsdoca,
                         vchi2pdof_max)


def make_smog2_phi2kk_calib(name,
                            pvs=make_pvs,
                            min_p=2 * GeV,
                            min_pt=380 * MeV,
                            max_trchi2dof=5,
                            max_ghostprob=0.25,
                            min_pidk=15,
                            mmincomb=_MASSWINDOW_COMB_PHI[0],
                            mmaxcomb=_MASSWINDOW_COMB_PHI[1],
                            mminver=_MASSWINDOW_VERTEX_PHI[0],
                            mmaxver=_MASSWINDOW_VERTEX_PHI[1],
                            apt_min=0 * MeV,
                            maxsdoca=10 * mm,
                            vchi2pdof_max=16.,
                            process="hlt2"):

    tag_charge = F.CHARGE > 0 if "Kmprobe" in name else F.CHARGE < 0
    probe_charge = F.CHARGE < 0 if "Kmprobe" in name else F.CHARGE > 0

    descriptor = "phi(1020) -> K+ K-" if "Kmprobe" in name else "phi(1020) -> K- K+"

    return make_dihadron(
        pvs,
        make_tag_kaon(tag_charge, min_p, min_pt, max_trchi2dof, max_ghostprob,
                      min_pidk),
        make_probe_kaon(probe_charge, min_p, min_pt, max_trchi2dof),
        name,
        descriptor,
        mmincomb,
        mmaxcomb,
        mminver,
        mmaxver,
        apt_min,
        maxsdoca,
        vchi2pdof_max,
    )


def make_smog2_omega2L0K_lll_line(
        pvs=make_pvs,
        min_p=2 * GeV,
        min_pt=100 * MeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin_Omega=_MASSWINDOW_OMEGA[0],
        mmax_Omega=_MASSWINDOW_OMEGA[1],
        mmin_L0=_MASSWINDOW_LAMBDA0[0],
        mmax_L0=_MASSWINDOW_LAMBDA0[1],
        apt_min=0 * MeV,
        vchi2pdof_max=30.0,
        end_vz_max=2200 * mm,
        bpvvdchi2_min=5,
        bpvltime_min=0 * ns,
        parent_bpvipchi2_max=1000,
        ks_veto_window=None,
        process="hlt2",
        name_Omega="smog2_Omega2L0K_lll",
        name_L0="smog2_lambda2ppi_ll_loose_no_ks_veto",
):
    lambdas = make_smog2_L02ppi_ll_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin_L0,
        mmax=mmax_L0,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        end_vz_max=end_vz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvltime_min=bpvltime_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
        process=process,
        name=name_L0,
    )

    kaons = make_smog2_common_particles(
        make_long_kaons,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        min_pt=min_pt,
        min_bpvipchi2=min_bpvipchi2,
    )

    descriptor = "[Omega- -> Lambda0 K-]cc"
    return make_detached_dihadron_ll(
        pvs=pvs,
        particle1=lambdas,
        particle2=kaons,
        name=name_Omega,
        descriptors=descriptor,
        mmin=mmin_Omega,
        mmax=mmax_Omega,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        end_vz_max=end_vz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvltime_min=bpvltime_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
    )


def make_smog2_omega2L0K_ddl_line(
        pvs=make_pvs,
        min_p=2 * GeV,
        min_pt=100 * MeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin_Omega=_MASSWINDOW_OMEGA[0],
        mmax_Omega=_MASSWINDOW_OMEGA[1],
        mmin_L0=_MASSWINDOW_LAMBDA0[0],
        mmax_L0=_MASSWINDOW_LAMBDA0[1],
        apt_min=0 * MeV,
        vchi2pdof_max=30.0,
        end_vz_max=2200 * mm,
        bpvvdz_min=400 * mm,
        bpvvdz_max=2800 * mm,
        bpvvdchi2_min=5,
        bpvltime_min=0 * ns,
        parent_bpvipchi2_max=1000,
        ks_veto_window=None,
        process="hlt2",
        name_Omega="smog2_Omega2L0K_ddl",
        name_L0="smog2_lambda2ppi_dd_loose_no_ks_veto",
):
    lambdas = make_smog2_L02ppi_dd_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin_L0,
        mmax=mmax_L0,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bpvvdz_max=bpvvdz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
        process=process,
        name=name_L0,
    )

    kaons = make_smog2_common_particles(
        make_long_kaons,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        min_pt=min_pt,
        min_bpvipchi2=min_bpvipchi2,
    )

    descriptor = "[Omega- -> Lambda0 K-]cc"
    return make_detached_dihadron_ll(
        pvs=pvs,
        particle1=lambdas,
        particle2=kaons,
        name=name_Omega,
        descriptors=descriptor,
        mmin=mmin_Omega,
        mmax=mmax_Omega,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        end_vz_max=end_vz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvltime_min=bpvltime_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
    )


def make_smog2_omega2L0K_ddd_line(
        pvs=make_pvs,
        min_p=2 * GeV,
        min_pt=100 * MeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin_Omega=_MASSWINDOW_OMEGA[0],
        mmax_Omega=_MASSWINDOW_OMEGA[1],
        mmin_L0=_MASSWINDOW_LAMBDA0[0],
        mmax_L0=_MASSWINDOW_LAMBDA0[1],
        apt_min=0 * MeV,
        vchi2pdof_max=30.0,
        bpvvdz_min=400 * mm,
        bpvvdz_max=2800 * mm,
        bpvvdchi2_min=0,
        parent_bpvipchi2_max=1000,
        ks_veto_window=None,
        process="hlt2",
        name_Omega="smog2_Omega2L0K_ddd",
        name_L0="smog2_lambda2ppi_dd_loose_no_ks_veto",
):
    lambdas = make_smog2_L02ppi_dd_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin_L0,
        mmax=mmax_L0,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bpvvdz_max=bpvvdz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
        process=process,
        name=name_L0,
    )

    kaons = make_smog2_common_particles(
        make_down_kaons,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        min_pt=min_pt,
        min_bpvipchi2=min_bpvipchi2,
    )

    descriptor = "[Omega- -> Lambda0 K-]cc"
    return make_detached_dihadron_dd(
        pvs=pvs,
        particle1=lambdas,
        particle2=kaons,
        name=name_Omega,
        descriptors=descriptor,
        mmin=mmin_Omega,
        mmax=mmax_Omega,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bpvvdz_max=bpvvdz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
    )


def make_smog2_xi2L0pi_lll_line(
        pvs=make_pvs,
        min_p=2 * GeV,
        min_pt=100 * MeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin_Xi=_MASSWINDOW_XI[0],
        mmax_Xi=_MASSWINDOW_XI[1],
        mmin_L0=_MASSWINDOW_LAMBDA0[0],
        mmax_L0=_MASSWINDOW_LAMBDA0[1],
        apt_min=0 * MeV,
        vchi2pdof_max=25.0,
        end_vz_max=2200 * mm,
        bpvvdchi2_min=5,
        bpvltime_min=0 * ns,
        parent_bpvipchi2_max=1000,
        ks_veto_window=None,
        process="hlt2",
        name_Xi="smog2_Xi2L0pi_lll",
        name_L0="smog2_lambda2ppi_ll_loose_no_ks_veto",
):
    lambdas = make_smog2_L02ppi_ll_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin_L0,
        mmax=mmax_L0,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        end_vz_max=end_vz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvltime_min=bpvltime_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
        process=process,
        name=name_L0,
    )

    pions = make_smog2_common_particles(
        make_long_pions,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        min_pt=min_pt,
        min_bpvipchi2=min_bpvipchi2,
    )

    descriptor = "[Xi- -> Lambda0 pi-]cc"
    return make_detached_dihadron_ll(
        pvs=pvs,
        particle1=lambdas,
        particle2=pions,
        name=name_Xi,
        descriptors=descriptor,
        mmin=mmin_Xi,
        mmax=mmax_Xi,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        end_vz_max=end_vz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvltime_min=bpvltime_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
    )


def make_smog2_xi2L0pi_ddl_line(
        pvs=make_pvs,
        min_p=2 * GeV,
        min_pt=100 * MeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin_Xi=_MASSWINDOW_XI[0],
        mmax_Xi=_MASSWINDOW_XI[1],
        mmin_L0=_MASSWINDOW_LAMBDA0[0],
        mmax_L0=_MASSWINDOW_LAMBDA0[1],
        apt_min=0 * MeV,
        vchi2pdof_max=30.0,
        end_vz_max=2200 * mm,
        bpvvdz_min=400 * mm,
        bpvvdz_max=2800 * mm,
        bpvvdchi2_min=5,
        bpvltime_min=0 * ns,
        parent_bpvipchi2_max=1000,
        ks_veto_window=None,
        process="hlt2",
        name_Xi="smog2_Xi2L0pi_ddl",
        name_L0="smog2_lambda2ppi_dd_loose_no_ks_veto",
):
    lambdas = make_smog2_L02ppi_dd_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin_L0,
        mmax=mmax_L0,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bpvvdz_max=bpvvdz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
        process=process,
        name=name_L0,
    )

    pions = make_smog2_common_particles(
        make_long_pions,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        min_pt=min_pt,
        min_bpvipchi2=min_bpvipchi2,
    )

    descriptor = "[Xi- -> Lambda0 pi-]cc"
    return make_detached_dihadron_ll(
        pvs=pvs,
        particle1=lambdas,
        particle2=pions,
        name=name_Xi,
        descriptors=descriptor,
        mmin=mmin_Xi,
        mmax=mmax_Xi,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        end_vz_max=end_vz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvltime_min=bpvltime_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
    )


def make_smog2_xi2L0pi_ddd_line(
        pvs=make_pvs,
        min_p=2 * GeV,
        min_pt=100 * MeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin_Xi=_MASSWINDOW_XI[0],
        mmax_Xi=_MASSWINDOW_XI[1],
        mmin_L0=_MASSWINDOW_LAMBDA0[0],
        mmax_L0=_MASSWINDOW_LAMBDA0[1],
        apt_min=0 * MeV,
        vchi2pdof_max=30.0,
        bpvvdz_min=400 * mm,
        bpvvdz_max=2800 * mm,
        bpvvdchi2_min=5,
        parent_bpvipchi2_max=1000,
        ks_veto_window=None,
        process="hlt2",
        name_Xi="smog2_Xi2L0pi_ddd",
        name_L0="smog2_lambda2ppi_dd_{hash}",
):
    lambdas = make_smog2_L02ppi_dd_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin_L0,
        mmax=mmax_L0,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdchi2_min=bpvvdchi2_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
        process=process,
        name=name_L0,
    )

    pions = make_smog2_common_particles(
        make_down_pions,
        max_trchi2dof=max_trchi2dof,
        min_p=min_p,
        min_pt=min_pt,
        min_bpvipchi2=min_bpvipchi2,
    )

    descriptor = "[Xi- -> Lambda0 pi-]cc"
    return make_detached_dihadron_dd(
        pvs=pvs,
        particle1=lambdas,
        particle2=pions,
        name=name_Xi,
        descriptors=descriptor,
        mmin=mmin_Xi,
        mmax=mmax_Xi,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bpvvdz_max=bpvvdz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
    )
