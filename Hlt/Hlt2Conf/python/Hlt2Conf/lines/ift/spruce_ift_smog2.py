###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of SMOG2 sprucing lines
"""

from Moore.config import SpruceLine, register_line_builder
from PyConf import configurable

from RecoConf.reconstruction_objects import upfront_reconstruction
from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.event_filters import require_gec
PROCESS = "spruce"
sprucing_lines = {}


@register_line_builder(sprucing_lines)
@configurable
def smog2_passthrough_PV_spruceline(
        name='SpruceIFT_SMOG2Passthrough_PV_in_SMOG2',
        prescale=1,
        persistreco=True):
    pvs = make_pvs
    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs),
        hlt2_filter_code='Hlt2IFT_SMOG2Passthrough_PV_in_SMOG2Decision',
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def smog2_passthrough_spruceline(name='SpruceIFT_SMOG2GECPassthrough',
                                 prescale=1,
                                 persistreco=True):
    gec = require_gec()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [gec],
        prescale=prescale,
        hlt2_filter_code='Hlt2IFT_SMOG2GECPassthroughDecision',
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def smog2_lumi_passthrough_spruceline(name='SpruceIFT_SMOG2LumiPassthrough',
                                      prescale=1,
                                      persistreco=True):
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction(),
        hlt2_filter_code='Hlt2IFT_SMOG2LumiPassthroughDecision',
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(sprucing_lines)
@configurable
def smog2_minbias_passthrough_spruceline(name='SpruceIFT_SMOG2MBPassthrough',
                                         prescale=1,
                                         persistreco=True):
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction(),
        hlt2_filter_code='Hlt2IFT_SMOG2MBPassthroughDecision',
        prescale=prescale,
        persistreco=persistreco,
    )
