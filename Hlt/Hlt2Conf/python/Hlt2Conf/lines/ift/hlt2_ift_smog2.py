###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of SMOG2 HLT2 lines
"""
from PyConf import configurable
from Moore.lines import Hlt2Line
from Moore.config import register_line_builder

from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.reconstruction_objects import upfront_reconstruction
from RecoConf.event_filters import require_gec

PROCESS = "hlt2"
all_lines = {}

_hlt1_SMOG2_lines = [
    "Hlt1_SMOG2_MinimumBiasDecision",
    "Hlt1_SMOG2_SingleTrackDecision",
    "Hlt1_SMOG2_SingleMuonDecision",
    "Hlt1_SMOG2_eta2ppDecision",
    "Hlt1_SMOG2_D2KpiDecision",
    "Hlt1_SMOG2_2BodyGenericDecision",
    "Hlt1_SMOG2_DiMuonHighMassDecision",
    "Hlt1GECPassThrough_LowMult5Decision",
    "Hlt1_BESMOG2_NoBiasDecision",
    "Hlt1_BESMOG2_LowMult10Decision",
    "Hlt1_SMOG2_MinimumBiasDecision",
    "Hlt1Passthrough_PV_in_SMOG2Decision",
]

_hlt1_lowMult_SMOG2_lines = [
    "Hlt1GECPassThrough_LowMult5Decision", "Hlt1_BESMOG2_LowMult10Decision"
]  # For luminosity measurement with p-e scattering
_hlt1_minbias_SMOG2_lines = [
    "Hlt1_SMOG2_MinimumBiasDecision", "Hlt1Passthrough_PV_in_SMOG2Decision",
    "Hlt1_BESMOG2_NoBiasDecision"
]


@register_line_builder(all_lines)
@configurable
def smog2_passthrough_PV(name='Hlt2IFT_SMOG2Passthrough_PV_in_SMOG2',
                         prescale=0.01,
                         persistreco=True):
    pvs = make_pvs

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs),
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_passthrough(name='Hlt2IFT_SMOG2GECPassthrough',
                      prescale=0.01,
                      persistreco=True):
    gec = require_gec()
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [gec],
        hlt1_filter_code=_hlt1_SMOG2_lines,
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_lumi_passthrough(name='Hlt2IFT_SMOG2LumiPassthrough',
                           prescale=1,
                           persistreco=True):
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction(),
        hlt1_filter_code=_hlt1_lowMult_SMOG2_lines,
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_minbias_passthrough(name='Hlt2IFT_SMOG2MBPassthrough',
                              prescale=1,
                              persistreco=True):
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction(),
        hlt1_filter_code=_hlt1_minbias_SMOG2_lines,
        prescale=prescale,
        persistreco=persistreco,
    )
