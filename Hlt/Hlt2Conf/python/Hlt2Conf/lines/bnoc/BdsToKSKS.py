###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Lines to select the decay B(s)0 -> KS0 KS0 and KS0 -> pi+ pi-.
The KS0 can be reconstructed either from long or downstream tracks, resulting
in the combinations LLLL, LLDD, DDDD.

"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, mm, picosecond
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.algorithms_thor import ParticleCombiner
from Hlt2Conf.lines.bnoc.builders.basic_builder import make_KS_LL, make_KS_DD
from PyConf import configurable


@configurable
def make_BdsToKsKsLLLL(process):
    pvs = make_pvs()
    kshorts = make_KS_LL()
    line_alg = ParticleCombiner(
        [kshorts, kshorts],
        DecayDescriptor="B_s0 -> KS0 KS0",
        CombinationCut=F.require_all(
            in_range(4400 * MeV, F.MASS, 6100 * MeV),
            F.MAXDOCACUT(1 * mm),
            F.SUM(F.PT) > 1500 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(4600 * MeV, F.MASS, 5900 * MeV),
            F.CHI2DOF < 20,
            F.BPVDIRA(pvs) > 0.999,
            F.BPVLTIME(pvs) > 0.1 * picosecond,
        ),
    )
    return [kshorts, line_alg]


@configurable
def make_BdsToKsKsLLDD(process):
    pvs = make_pvs()
    ll_kshorts = make_KS_LL()
    dd_kshorts = make_KS_DD()
    line_alg = ParticleCombiner(
        [dd_kshorts, ll_kshorts],
        DecayDescriptor="B_s0 -> KS0 KS0",
        AllowDiffInputsForSameIDChildren=True,
        CombinationCut=F.require_all(
            in_range(4200 * MeV, F.MASS, 6300 * MeV),
            F.MAXDOCACUT(4 * mm),
            F.SUM(F.PT) > 1500 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(4400 * MeV, F.MASS, 6100 * MeV), F.CHI2DOF < 30,
            F.BPVDIRA(pvs) > 0.999,
            F.BPVLTIME(pvs) > 0.1 * picosecond),
    )
    return [ll_kshorts, dd_kshorts, line_alg]


@configurable
def make_BdsToKsKsDDDD(process):
    pvs = make_pvs()
    dd_kshorts = make_KS_DD()
    line_alg = ParticleCombiner(
        [dd_kshorts, dd_kshorts],
        DecayDescriptor="B_s0 -> KS0 KS0",
        CombinationCut=F.require_all(
            in_range(4200 * MeV, F.MASS, 6300 * MeV),
            F.MAXDOCACUT(4 * mm),
            F.SUM(F.PT) > 1500 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(4400 * MeV, F.MASS, 6100 * MeV), F.CHI2DOF < 40,
            F.BPVDIRA(pvs) > 0.999,
            F.BPVLTIME(pvs) > 0.1 * picosecond),
    )
    return [dd_kshorts, line_alg]
