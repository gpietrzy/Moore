###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Booking of BnoC sprucing lines. This is where sprucing lines are actually booked
"""
from Hlt2Conf.lines.bnoc.utils import update_makers, make_default_lines, make_prescaled_lines, make_flavour_tagging_lines, make_custom_lines

from Hlt2Conf.lines.bnoc import bbaryon_to_hyperon_4h
from Hlt2Conf.lines.bnoc import bbaryon_to_l0hhh
from Hlt2Conf.lines.bnoc import bbaryon_to_lightbaryon_h
from Hlt2Conf.lines.bnoc import bbaryon_to_lightbaryon_hh
from Hlt2Conf.lines.bnoc import bbaryon_to_phh
from Hlt2Conf.lines.bnoc import bbaryon_to_phhh
from Hlt2Conf.lines.bnoc import BcTohhh
from Hlt2Conf.lines.bnoc import BdsTohhhh
from Hlt2Conf.lines.bnoc import BdsToKShh
from Hlt2Conf.lines.bnoc import BdsToKSKS
from Hlt2Conf.lines.bnoc import BdsToKSpp
from Hlt2Conf.lines.bnoc import BdsToVV
from Hlt2Conf.lines.bnoc import BdTohh
from Hlt2Conf.lines.bnoc import BdToppbarhh
from Hlt2Conf.lines.bnoc import bTohh
from Hlt2Conf.lines.bnoc import BuTohhh
from Hlt2Conf.lines.bnoc import BuToKSh
from Hlt2Conf.lines.bnoc import BuToKShhh

PROCESS = 'spruce'
sprucing_lines = {}

line_makers = {}
update_makers(line_makers, bbaryon_to_hyperon_4h)
update_makers(line_makers, bbaryon_to_l0hhh)
update_makers(line_makers, bbaryon_to_lightbaryon_h)
update_makers(line_makers, bbaryon_to_lightbaryon_hh)
update_makers(line_makers, bbaryon_to_phh)
update_makers(line_makers, bbaryon_to_phhh)
update_makers(line_makers, BcTohhh)
update_makers(line_makers, BdsTohhhh)
update_makers(line_makers, BdsToKShh)
update_makers(line_makers, BdsToKSKS)
update_makers(line_makers, BdsToKSpp)
update_makers(line_makers, BdsToVV)
update_makers(line_makers, BdTohh)
update_makers(line_makers, BdToppbarhh)
update_makers(line_makers, bTohh)
update_makers(line_makers, BuTohhh)
update_makers(line_makers, BuToKSh)
update_makers(line_makers, BuToKShhh)

# Automate creation of sprucing lines for all Hlt2 lines that have persistreco=True
from Hlt2Conf.lines.bnoc.hlt2_bnoc import flavour_tagging_lines as hlt2_ft_lines
from Hlt2Conf.lines.bnoc.hlt2_bnoc import custom_lines as hlt2_custom_lines

# Automatically add HLT2 decision filtering for HLT2 lines with persistreco=True
spruce_hlt2_filters = {}
persistreco_custom_lines = {}

for decay in hlt2_ft_lines:
    spruce_hlt2_filters[decay] = [f'Hlt2BnoC_{decay}Decision']

for decay, kwargs in hlt2_custom_lines.items():
    if 'flavour_tagging' in kwargs or 'persistreco' in kwargs:
        spruce_hlt2_filters[decay] = [f'Hlt2BnoC_{decay}Decision']
        persistreco_custom_lines[decay] = kwargs

# If you want your sprucing line to filter on Hlt2 lines, e.g.
# Hlt2Topo2BodyDecision, add them to this dictionary (will automatically check
# if the line has already been automatically added to spurce_hlt_filters, and
# will merge accordingly)
spruce_hlt2_extra_filters = {
    'Lb0ToPpKpKmKm': ['Hlt2Topo2BodyDecision', 'Hlt2Topo3BodyDecision'],
    'Lb0ToPpKmKpPim': ['Hlt2Topo2BodyDecision', 'Hlt2Topo3BodyDecision'],
    'Lb0ToPpKmPipPim': ['Hlt2Topo2BodyDecision', 'Hlt2Topo3BodyDecision'],
    'Lb0ToPpKpPimPim': ['Hlt2Topo2BodyDecision', 'Hlt2Topo3BodyDecision'],
    'Lb0ToPpPipPimPim': ['Hlt2Topo2BodyDecision', 'Hlt2Topo3BodyDecision'],
}

for decay, hlt2_lines in spruce_hlt2_extra_filters.items():
    if decay in spruce_hlt2_filters:
        spruce_hlt2_filters[decay] += hlt2_lines
    else:
        spruce_hlt2_filters[decay] = hlt2_lines

make_flavour_tagging_lines(
    process=PROCESS,
    line_dict=sprucing_lines,
    line_makers=line_makers,
    flavour_tagging_lines=hlt2_ft_lines,
    spruce_hlt2_filters=spruce_hlt2_filters)

make_custom_lines(
    process=PROCESS,
    line_dict=sprucing_lines,
    line_makers=line_makers,
    custom_lines=persistreco_custom_lines,
    spruce_hlt2_filters=spruce_hlt2_filters)

###############################################################################
# Add new lines in the appropriate list / dictionary below
# Note that persistreco=False by default (unless you specify
# flavour_tagging=True)
# Ensure that if you add a new module (e.g. BdsToVV.py) it is imported above
# and included in the `update_makers` calls
###############################################################################

default_lines = [
    'Lb0ToPpKpKmKm', 'Lb0ToPpKmKpPim', 'Lb0ToPpKmPipPim', 'Lb0ToPpKpPimPim',
    'Lb0ToPpPipPimPim'
]

flavour_tagging_lines = []

prescale_lines = {}

# Below are the lines which require a more custom configuration
# You may want to add a new line category if you have some specific use-case
# e.g. a line with persistreco=True but no flavour tagging to keep clear the
# different groups of lines we define.

# Lines requiring flavour tagging, GEC and topo
flavour_tagging_gec_topo_lines = {}

# Lines with GEC and topo but no flavour tagging
# The 2 (3) body MVA cuts can be specified rather than using the default
# 0.1 that all the below lines currently use (see
# flavour_tagging_gec_topo_lines for examples)
gec_topo_lines = {}

# If you add a new category of custom line, update the below dictionary
custom_lines = flavour_tagging_gec_topo_lines
custom_lines.update(gec_topo_lines)

make_default_lines(
    process=PROCESS,
    line_dict=sprucing_lines,
    line_makers=line_makers,
    default_lines=default_lines,
    spruce_hlt2_filters=spruce_hlt2_filters)

make_prescaled_lines(
    process=PROCESS,
    line_dict=sprucing_lines,
    line_makers=line_makers,
    prescaled_lines=prescale_lines,
    spruce_hlt2_filters=spruce_hlt2_filters)

make_flavour_tagging_lines(
    process=PROCESS,
    line_dict=sprucing_lines,
    line_makers=line_makers,
    flavour_tagging_lines=flavour_tagging_lines,
    spruce_hlt2_filters=spruce_hlt2_filters)

make_custom_lines(
    process=PROCESS,
    line_dict=sprucing_lines,
    line_makers=line_makers,
    custom_lines=custom_lines,
    spruce_hlt2_filters=spruce_hlt2_filters)
