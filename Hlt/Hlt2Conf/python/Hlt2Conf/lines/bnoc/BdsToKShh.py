###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of B0(s) -> KShh lines.
"""

from Hlt2Conf.lines.bnoc.builders.basic_builder import make_soft_pions, make_soft_kaons
from Hlt2Conf.lines.bnoc.builders.basic_builder import make_bbaryon_ks0_ll, make_bbaryon_ks0_dd
from Hlt2Conf.lines.bnoc.builders.b_builder import make_b2kshh
from Hlt2Conf.lines.bnoc.utils import check_process


@check_process
def make_BdsToKSPipPim_LL(process):
    if process == 'spruce':
        pions = make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        pions = make_soft_pions()
    ks = make_bbaryon_ks0_ll()
    line_alg = make_b2kshh(
        particles=[ks, pions, pions],
        descriptor='B0 -> KS0 pi+ pi-',
        bpvipchi2_sum_min=1000.0)
    return [ks, line_alg]


@check_process
def make_BdsToKSPipPim_DD(process):
    if process == 'spruce':
        pions = make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        pions = make_soft_pions()
    ks = make_bbaryon_ks0_dd()
    line_alg = make_b2kshh(
        particles=[ks, pions, pions], descriptor='B0 -> KS0 pi+ pi-')
    return [ks, line_alg]


@check_process
def make_BdsToKSKpKm_LL(process):
    if process == 'spruce':
        kaons = make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaons = make_soft_kaons()
    ks = make_bbaryon_ks0_ll()
    line_alg = make_b2kshh(
        particles=[ks, kaons, kaons],
        descriptor='B0 -> KS0 K+ K-',
        bpvipchi2_sum_min=1000.0)
    return [ks, line_alg]


@check_process
def make_BdsToKSKpKm_DD(process):
    if process == 'spruce':
        kaons = make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaons = make_soft_kaons()
    ks = make_bbaryon_ks0_dd()
    line_alg = make_b2kshh(
        particles=[ks, kaons, kaons], descriptor='B0 -> KS0 K+ K-')
    return [ks, line_alg]


@check_process
def make_BdsToKSKpPim_LL(process):
    if process == 'spruce':
        kaons = make_soft_kaons(k_pidk_min=None)
        pions = make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        kaons = make_soft_kaons()
        pions = make_soft_pions()
    ks = make_bbaryon_ks0_ll()
    line_alg = make_b2kshh(
        particles=[ks, kaons, pions],
        descriptor='[B0 -> KS0 K+ pi-]cc',
        bpvipchi2_sum_min=1000.0)
    return [ks, line_alg]


@check_process
def make_BdsToKSKpPim_DD(process):
    if process == 'spruce':
        kaons = make_soft_kaons(k_pidk_min=None)
        pions = make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        kaons = make_soft_kaons()
        pions = make_soft_pions()
    ks = make_bbaryon_ks0_dd()
    line_alg = make_b2kshh(
        particles=[ks, kaons, pions], descriptor='[B0 -> KS0 K+ pi-]cc')
    return [ks, line_alg]
