###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of BnoC lines with a final signature as Hb- -> hyperon 4h

Xibm -> Xim 4pi / Xim 2K 2pi / Xim 2P 2pi
Ombm -> Omm 4pi / Omm 2K 2pi / Omm 2P 2pi
All lines are implemented as LL/DD

"""

from Hlt2Conf.lines.bnoc.utils import check_process
from Hlt2Conf.lines.bnoc.builders.basic_builder import make_bbaryon_detached_pions, make_bbaryon_detached_kaons, make_bbaryon_detached_protons, make_detached_down_pions, make_bbaryon_detached_down_kaons, make_veryloose_lambda_LL, make_loose_lambda_DD, make_xim_to_lambda_pi_lll, make_xim_to_lambda_pi_ddl, make_xim_to_lambda_pi_ddd, make_omegam_to_lambda_k_lll, make_omegam_to_lambda_k_ddl, make_omegam_to_lambda_k_ddd
from Hlt2Conf.lines.bnoc.builders.b_builder import make_bbaryon_5body

all_lines = {}

### LLL combinations for Xibm and Ombm


@check_process
def make_XibmToXimPipPipPimPim_LLL(process):
    if process == 'spruce':
        hyperons = make_xim_to_lambda_pi_lll(
            lambdas=make_veryloose_lambda_LL(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_xim_to_lambda_pi_lll(
            lambdas=make_veryloose_lambda_LL(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron1, hadron1],
        name="XibmToXimPipPipPimPim_LLL_Combiner",
        descriptor='[Xi_b- -> Xi- pi+ pi+ pi- pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_XibmToXimKpPipPimPim_LLL(process):
    if process == 'spruce':
        hyperons = make_xim_to_lambda_pi_lll(
            lambdas=make_veryloose_lambda_LL(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_xim_to_lambda_pi_lll(
            lambdas=make_veryloose_lambda_LL(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron2, hadron2, hadron2],
        name="XibmToXimKpPipPimPim_LLL_Combiner",
        descriptor='[Xi_b- -> Xi- K+ pi+ pi- pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_XibmToXimKpKmPipPim_LLL(process):
    if process == 'spruce':
        hyperons = make_xim_to_lambda_pi_lll(
            lambdas=make_veryloose_lambda_LL(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_xim_to_lambda_pi_lll(
            lambdas=make_veryloose_lambda_LL(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron2, hadron2],
        name="XibmToXimKpKmPipPim_LLL_Combiner",
        descriptor='[Xi_b- -> Xi- K+ K- pi+ pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_XibmToXimPpPmPipPim_LLL(process):
    if process == 'spruce':
        hyperons = make_xim_to_lambda_pi_lll(
            lambdas=make_veryloose_lambda_LL(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_protons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_xim_to_lambda_pi_lll(
            lambdas=make_veryloose_lambda_LL(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_protons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron2, hadron2],
        name="XibmToXimPpPmPipPim_LLL_Combiner",
        descriptor='[Xi_b- -> Xi- p+ p~- pi+ pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_XibmToXimKpKpPimPim_LLL(process):
    if process == 'spruce':
        hyperons = make_xim_to_lambda_pi_lll(
            lambdas=make_veryloose_lambda_LL(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_xim_to_lambda_pi_lll(
            lambdas=make_veryloose_lambda_LL(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron2, hadron2],
        name="XibmToXimKpKpPimPim_LLL_Combiner",
        descriptor='[Xi_b- -> Xi- K+ K+ pi- pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_OmbmToOmmPipPipPimPim_LLL(process):
    if process == 'spruce':
        hyperons = make_omegam_to_lambda_k_lll(
            lambdas=make_veryloose_lambda_LL(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_omegam_to_lambda_k_lll(
            lambdas=make_veryloose_lambda_LL(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron1, hadron1],
        name="OmbmToOmmPipPipPimPim_LLL_Combiner",
        descriptor='[Omega_b- -> Omega- pi+ pi+ pi- pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_OmbmToOmmKpPipPimPim_LLL(process):
    if process == 'spruce':
        hyperons = make_omegam_to_lambda_k_lll(
            lambdas=make_veryloose_lambda_LL(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_omegam_to_lambda_k_lll(
            lambdas=make_veryloose_lambda_LL(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron2, hadron2, hadron2],
        name="OmbmToOmmKpPipPimPim_LLL_Combiner",
        descriptor='[Omega_b- -> Omega- K+ pi+ pi- pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_OmbmToOmmKpKmPipPim_LLL(process):
    if process == 'spruce':
        hyperons = make_omegam_to_lambda_k_lll(
            lambdas=make_veryloose_lambda_LL(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_omegam_to_lambda_k_lll(
            lambdas=make_veryloose_lambda_LL(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron2, hadron2],
        name="OmbmToOmmKpKmPipPim_LLL_Combiner",
        descriptor='[Omega_b- -> Omega- K+ K- pi+ pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_OmbmToOmmKpKpPimPim_LLL(process):
    if process == 'spruce':
        hyperons = make_omegam_to_lambda_k_lll(
            lambdas=make_veryloose_lambda_LL(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_omegam_to_lambda_k_lll(
            lambdas=make_veryloose_lambda_LL(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron2, hadron2],
        name="OmbmToOmmKpKpPimPim_LLL_Combiner",
        descriptor='[Omega_b- -> Omega- K+ K+ pi- pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_OmbmToOmmPpPmPipPim_LLL(process):
    if process == 'spruce':
        hyperons = make_omegam_to_lambda_k_lll(
            lambdas=make_veryloose_lambda_LL(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_protons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_omegam_to_lambda_k_lll(
            lambdas=make_veryloose_lambda_LL(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_protons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron2, hadron2],
        name="OmbmToOmmPpPmPipPim_LLL_Combiner",
        descriptor='[Omega_b- -> Omega- p+ p~- pi+ pi-]cc')
    return [hyperons, line_alg]


### DDL combinations for Xibm and Ombm


@check_process
def make_XibmToXimPipPipPimPim_DDL(process):
    if process == 'spruce':
        hyperons = make_xim_to_lambda_pi_ddl(
            lambdas=make_loose_lambda_DD(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_xim_to_lambda_pi_ddl(
            lambdas=make_loose_lambda_DD(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron1, hadron1],
        name="XibmToXimPipPipPimPim_DDL_Combiner",
        descriptor='[Xi_b- -> Xi- pi+ pi+ pi- pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_XibmToXimKpPipPimPim_DDL(process):
    if process == 'spruce':
        hyperons = make_xim_to_lambda_pi_ddl(
            lambdas=make_loose_lambda_DD(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_xim_to_lambda_pi_ddl(
            lambdas=make_loose_lambda_DD(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron2, hadron2, hadron2],
        name="XibmToXimKpPipPimPim_DDL_Combiner",
        descriptor='[Xi_b- -> Xi- K+ pi+ pi- pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_XibmToXimKpKmPipPim_DDL(process):
    if process == 'spruce':
        hyperons = make_xim_to_lambda_pi_ddl(
            lambdas=make_loose_lambda_DD(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_xim_to_lambda_pi_ddl(
            lambdas=make_loose_lambda_DD(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron2, hadron2],
        name="XibmToXimKpKmPipPim_DDL_Combiner",
        descriptor='[Xi_b- -> Xi- K+ K- pi+ pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_XibmToXimPpPmPipPim_DDL(process):
    if process == 'spruce':
        hyperons = make_xim_to_lambda_pi_ddl(
            lambdas=make_loose_lambda_DD(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_protons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_xim_to_lambda_pi_ddl(
            lambdas=make_loose_lambda_DD(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_protons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron2, hadron2],
        name="XibmToXimPpPmPipPim_DDL_Combiner",
        descriptor='[Xi_b- -> Xi- p+ p~- pi+ pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_XibmToXimKpKpPimPim_DDL(process):
    if process == 'spruce':
        hyperons = make_xim_to_lambda_pi_ddl(
            lambdas=make_loose_lambda_DD(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_xim_to_lambda_pi_ddl(
            lambdas=make_loose_lambda_DD(),
            pions=make_bbaryon_detached_pions(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron2, hadron2],
        name="XibmToXimKpKpPimPim_DDL_Combiner",
        descriptor='[Xi_b- -> Xi- K+ K+ pi- pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_OmbmToOmmPipPipPimPim_DDL(process):
    if process == 'spruce':
        hyperons = make_omegam_to_lambda_k_ddl(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_omegam_to_lambda_k_ddl(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron1, hadron1],
        name="OmbmToOmmPipPipPimPim_DDL_Combiner",
        descriptor='[Omega_b- -> Omega- pi+ pi+ pi- pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_OmbmToOmmKpPipPimPim_DDL(process):
    if process == 'spruce':
        hyperons = make_omegam_to_lambda_k_ddl(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_omegam_to_lambda_k_ddl(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron2, hadron2, hadron2],
        name="OmbmToOmmKpPipPimPim_DDL_Combiner",
        descriptor='[Omega_b- -> Omega- K+ pi+ pi- pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_OmbmToOmmKpKmPipPim_DDL(process):
    if process == 'spruce':
        hyperons = make_omegam_to_lambda_k_ddl(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_omegam_to_lambda_k_ddl(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron2, hadron2],
        name="OmbmToOmmKpKmPipPim_DDL_Combiner",
        descriptor='[Omega_b- -> Omega- K+ K- pi+ pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_OmbmToOmmKpKpPimPim_DDL(process):
    if process == 'spruce':
        hyperons = make_omegam_to_lambda_k_ddl(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_omegam_to_lambda_k_ddl(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron2, hadron2],
        name="OmbmToOmmKpKpPimPim_DDL_Combiner",
        descriptor='[Omega_b- -> Omega- K+ K+ pi- pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_OmbmToOmmPpPmPipPim_DDL(process):
    if process == 'spruce':
        hyperons = make_omegam_to_lambda_k_ddl(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_protons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_omegam_to_lambda_k_ddl(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_kaons(pid=None))
        hadron1 = make_bbaryon_detached_protons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron2, hadron2],
        name="OmbmToOmmPpPmPipPim_DDL_Combiner",
        descriptor='[Omega_b- -> Omega- p+ p~- pi+ pi-]cc')
    return [hyperons, line_alg]


### DDD combinations for Xibm and Ombm


@check_process
def make_XibmToXimPipPipPimPim_DDD(process):
    if process == 'spruce':
        hyperons = make_xim_to_lambda_pi_ddd(
            lambdas=make_loose_lambda_DD(),
            pions=make_detached_down_pions(pi_pidk_max=None))
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_xim_to_lambda_pi_ddd(
            lambdas=make_loose_lambda_DD(),
            pions=make_detached_down_pions(pi_pidk_max=None))
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron1, hadron1],
        name="XibmToXimPipPipPimPim_DDD_Combiner",
        descriptor='[Xi_b- -> Xi- pi+ pi+ pi- pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_XibmToXimKpPipPimPim_DDD(process):
    if process == 'spruce':
        hyperons = make_xim_to_lambda_pi_ddd(
            lambdas=make_loose_lambda_DD(),
            pions=make_detached_down_pions(pi_pidk_max=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_xim_to_lambda_pi_ddd(
            lambdas=make_loose_lambda_DD(),
            pions=make_detached_down_pions(pi_pidk_max=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron2, hadron2, hadron2],
        name="XibmToXimKpPipPimPim_DDD_Combiner",
        descriptor='[Xi_b- -> Xi- K+ pi+ pi- pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_XibmToXimKpKmPipPim_DDD(process):
    if process == 'spruce':
        hyperons = make_xim_to_lambda_pi_ddd(
            lambdas=make_loose_lambda_DD(),
            pions=make_detached_down_pions(pi_pidk_max=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_xim_to_lambda_pi_ddd(
            lambdas=make_loose_lambda_DD(),
            pions=make_detached_down_pions(pi_pidk_max=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron2, hadron2],
        name="XibmToXimKpKmPipPim_DDD_Combiner",
        descriptor='[Xi_b- -> Xi- K+ K- pi+ pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_XibmToXimPpPmPipPim_DDD(process):
    if process == 'spruce':
        hyperons = make_xim_to_lambda_pi_ddd(
            lambdas=make_loose_lambda_DD(),
            pions=make_detached_down_pions(pi_pidk_max=None))
        hadron1 = make_bbaryon_detached_protons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_xim_to_lambda_pi_ddd(
            lambdas=make_loose_lambda_DD(),
            pions=make_detached_down_pions(pi_pidk_max=None))
        hadron1 = make_bbaryon_detached_protons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron2, hadron2],
        name="XibmToXimPpPmPipPim_DDD_Combiner",
        descriptor='[Xi_b- -> Xi- p+ p~- pi+ pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_XibmToXimKpKpPimPim_DDD(process):
    if process == 'spruce':
        hyperons = make_xim_to_lambda_pi_ddd(
            lambdas=make_loose_lambda_DD(),
            pions=make_detached_down_pions(pi_pidk_max=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_xim_to_lambda_pi_ddd(
            lambdas=make_loose_lambda_DD(),
            pions=make_detached_down_pions(pi_pidk_max=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron2, hadron2],
        name="XibmToXimKpKpPimPim_DDD_Combiner",
        descriptor='[Xi_b- -> Xi- K+ K+ pi- pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_OmbmToOmmPipPipPimPim_DDD(process):
    if process == 'spruce':
        hyperons = make_omegam_to_lambda_k_ddd(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_down_kaons(pid=None))
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_omegam_to_lambda_k_ddd(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_down_kaons(pid=None))
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron1, hadron1],
        name="OmbmToOmmPipPipPimPim_DDD_Combiner",
        descriptor='[Omega_b- -> Omega- pi+ pi+ pi- pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_OmbmToOmmKpPipPimPim_DDD(process):
    if process == 'spruce':
        hyperons = make_omegam_to_lambda_k_ddd(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_down_kaons(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_omegam_to_lambda_k_ddd(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_down_kaons(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron2, hadron2, hadron2],
        name="OmbmToOmmKpPipPimPim_DDD_Combiner",
        descriptor='[Omega_b- -> Omega- K+ pi+ pi- pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_OmbmToOmmKpKmPipPim_DDD(process):
    if process == 'spruce':
        hyperons = make_omegam_to_lambda_k_ddd(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_down_kaons(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_omegam_to_lambda_k_ddd(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_down_kaons(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron2, hadron2],
        name="OmbmToOmmKpKmPipPim_DDD_Combiner",
        descriptor='[Omega_b- -> Omega- K+ K- pi+ pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_OmbmToOmmKpKpPimPim_DDD(process):
    if process == 'spruce':
        hyperons = make_omegam_to_lambda_k_ddd(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_down_kaons(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_omegam_to_lambda_k_ddd(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_down_kaons(pid=None))
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron2, hadron2],
        name="OmbmToOmmKpKpPimPim_DDD_Combiner",
        descriptor='[Omega_b- -> Omega- K+ K+ pi- pi-]cc')
    return [hyperons, line_alg]


@check_process
def make_OmbmToOmmPpPmPipPim_DDD(process):
    if process == 'spruce':
        hyperons = make_omegam_to_lambda_k_ddd(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_down_kaons(pid=None))
        hadron1 = make_bbaryon_detached_protons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        hyperons = make_omegam_to_lambda_k_ddd(
            lambdas=make_loose_lambda_DD(),
            kaons=make_bbaryon_detached_down_kaons(pid=None))
        hadron1 = make_bbaryon_detached_protons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_5body(
        particles=[hyperons, hadron1, hadron1, hadron2, hadron2],
        name="OmbmToOmmPpPmPipPim_DDD_Combiner",
        descriptor='[Omega_b- -> Omega- p+ p~- pi+ pi-]cc')
    return [hyperons, line_alg]
