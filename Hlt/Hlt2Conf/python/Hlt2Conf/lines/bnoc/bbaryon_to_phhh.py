###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of following BNOC lines:
b-baryon -> proton 3h lines
Implemented:
Lb0/Xib0 -> pKKK
Lb0/Xib0 -> pKKpi
Lb0/Xib0 -> pKpipi
Lb0/Xib0 -> ppipipi

b-baryon -> proton proton anti-proton h lines

b-baryon -> Lambda0 2h lines

"""
from GaudiKernel.SystemOfUnits import MeV

from Hlt2Conf.lines.bnoc.utils import check_process
from Hlt2Conf.lines.bnoc.builders.basic_builder import make_bbaryon_detached_pions, make_bbaryon_detached_kaons, make_bbaryon_detached_protons, make_phi, make_bbaryon_ks0_ll, make_bbaryon_ks0_dd, make_veryloose_lambda_LL, make_loose_lambda_DD
from Hlt2Conf.lines.bnoc.builders.b_builder import make_bbaryon_2body, make_bbaryon_3body, make_bbaryon_4body

all_lines = {}


@check_process
def make_Lb0ToPpKpKmKm(process):
    if process == 'spruce':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == 'hlt2':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_4body(
        particles=[protons, hadron1, hadron1, hadron1],
        name="Lb0ToPpKpKmKm_Combiner",
        descriptor='[Lambda_b0 -> p+ K+ K- K-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return line_alg


@check_process
def make_Lb0ToPpKmKpPim(process):
    if process == 'spruce':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[protons, hadron1, hadron1, hadron2],
        name="Lb0ToPpKmKpPim_Combiner",
        descriptor='[Lambda_b0 -> p+ K- K+ pi-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return line_alg


@check_process
def make_Lb0ToPpKmKmPip(process):
    if process == 'spruce':
        protons = make_bbaryon_detached_protons(),
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[protons, hadron1, hadron1, hadron2],
        name="Lb0ToPpKmKmPip_Combiner",
        descriptor='[Lambda_b0 -> p+ K- K- pi+]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return line_alg


@check_process
def make_Lb0ToPpKmPipPim(process):
    if process == 'spruce':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[protons, hadron1, hadron2, hadron2],
        name="Lb0ToPpKmPipPim_Combiner",
        descriptor='[Lambda_b0 -> p+ K- pi+ pi-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return line_alg


@check_process
def make_Lb0ToPpKpPimPim(process):
    if process == 'spruce':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[protons, hadron1, hadron2, hadron2],
        name="Lb0ToPpKpPimPim_Combiner",
        descriptor='[Lambda_b0 -> p+ K+ pi- pi-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return line_alg


@check_process
def make_Lb0ToPpPipPimPim(process):
    if process == 'spruce':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[protons, hadron1, hadron1, hadron1],
        name="Lb0ToPpPimPipPim_Combiner",
        descriptor='[Lambda_b0 -> p+ pi+ pi- pi-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return line_alg


@check_process
def make_Lb0ToPpPipPimPim_NoPID(process):
    if process == 'spruce':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_pions(pid=None)
    line_alg = make_bbaryon_4body(
        particles=[protons, hadron1, hadron1, hadron1],
        name="Lb0ToPpPimPipPim_PersistReco_Combiner",
        descriptor='[Lambda_b0 -> p+ pi+ pi- pi-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return line_alg


# b-baryon -> proton proton anti-proton h


@check_process
def make_Lb0ToPpPpPmPim(process):
    if process == 'spruce':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[protons, protons, protons, hadron1],
        name="Lb0ToPpPpPmPim_Combiner",
        descriptor='[Lambda_b0 -> p+ p+ p~- pi-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return line_alg


@check_process
def make_Lb0ToPpPpPmKm(process):
    if process == 'spruce':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == 'hlt2':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_4body(
        particles=[protons, protons, protons, hadron1],
        name="Lb0ToPpPpPmKm_Combiner",
        descriptor='[Lambda_b0 -> p+ p+ p~- K-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return line_alg


# b-baryon -> p KS0 h LL / DD


@check_process
def make_Lb0ToPpKSPim_LL(process):
    if process == 'spruce':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_ks0_ll()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_ks0_ll()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[protons, hadron1, hadron2],
        name="Lb0ToPpKSPim_LL_Combiner",
        descriptor='[Lambda_b0 -> p+ KS0 pi-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [hadron1, line_alg]


@check_process
def make_Lb0ToPpKSKm_LL(process):
    if process == 'spruce':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_ks0_ll()
        hadron2 = make_bbaryon_detached_kaons(pid=None)
    elif process == 'hlt2':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_ks0_ll()
        hadron2 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_3body(
        particles=[protons, hadron1, hadron2],
        name="Lb0ToPpKSKm_LL_Combiner",
        descriptor='[Lambda_b0 -> p+ KS0 K-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [hadron1, line_alg]


@check_process
def make_Lb0ToPpKSPim_DD(process):
    if process == 'spruce':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_ks0_dd()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_ks0_dd()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[protons, hadron1, hadron2],
        name="Lb0ToPpKSPim_LL_Combiner",
        descriptor='[Lambda_b0 -> p+ KS0 pi-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [hadron1, line_alg]


@check_process
def make_Lb0ToPpKSKm_DD(process):
    if process == 'spruce':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_ks0_dd()
        hadron2 = make_bbaryon_detached_kaons(pid=None)
    elif process == 'hlt2':
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_ks0_dd()
        hadron2 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_3body(
        particles=[protons, hadron1, hadron2],
        name="Lb0ToPpKSKm_DD_Combiner",
        descriptor='[Lambda_b0 -> p+ KS0 K-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [hadron1, line_alg]


# b-baryon -> Lambda0 2h LL


@check_process
def make_Lb0ToL0PipPim_LL(process):
    if process == 'spruce':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron1],
        name="Lb0ToL0PipPim_LL_Combiner",
        descriptor='[Lambda_b0 -> Lambda0 pi+ pi-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [lambda0s, line_alg]


@check_process
def make_Lb0ToL0KpPim_LL(process):
    if process == 'spruce':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron2],
        name="Lb0ToL0KpPim_LL_Combiner",
        descriptor='[Lambda_b0 -> Lambda0 K+ pi-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [lambda0s, line_alg]


@check_process
def make_Lb0ToL0KmPip_LL(process):
    if process == 'spruce':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron2],
        name="Lb0ToL0KmPip_LL_Combiner",
        descriptor='[Lambda_b0 -> Lambda0 K- pi+]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [lambda0s, line_alg]


@check_process
def make_Lb0ToL0KpKm_LL(process):
    if process == 'spruce':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == 'hlt2':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron1],
        name="Lb0ToL0KpKm_LL_Combiner",
        descriptor='[Lambda_b0 -> Lambda0 K+ K-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [lambda0s, line_alg]


@check_process
def make_Lb0ToL0PpPm_LL(process):
    if process == 'spruce':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_protons(pid=None)
    elif process == 'hlt2':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_protons()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron1],
        name="Lb0ToL0PpPm_LL_Combiner",
        descriptor='[Lambda_b0 -> Lambda0 p+ p~-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [lambda0s, line_alg]


# b-baryon -> Lambda0 2h DD


@check_process
def make_Lb0ToL0PipPim_DD(process):
    if process == 'spruce':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron1],
        name="Lb0ToL0PipPim_DD_Combiner",
        descriptor='[Lambda_b0 -> Lambda0 pi+ pi-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [lambda0s, line_alg]


@check_process
def make_Lb0ToL0KpPim_DD(process):
    if process == 'spruce':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron2],
        name="Lb0ToL0KpPim_DD_Combiner",
        descriptor='[Lambda_b0 -> Lambda0 K+ pi-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [lambda0s, line_alg]


@check_process
def make_Lb0ToL0KmPip_DD(process):
    if process == 'spruce':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron2],
        name="Lb0ToL0KmPip_DD_Combiner",
        descriptor='[Lambda_b0 -> Lambda0 K- pi+]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [lambda0s, line_alg]


@check_process
def make_Lb0ToL0KpKm_DD(process):
    if process == 'spruce':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == 'hlt2':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron1],
        name="Lb0ToL0KpKm_DD_Combiner",
        descriptor='[Lambda_b0 -> Lambda0 K+ K-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [lambda0s, line_alg]


@check_process
def make_Lb0ToL0PpPm_DD(process):
    if process == 'spruce':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_protons(pid=None)
    elif process == 'hlt2':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_protons()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron1],
        name="Lb0ToL0PpPm_DD_Combiner",
        descriptor='[Lambda_b0 -> Lambda0 p+ p~-]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [lambda0s, line_alg]


# b-baryon -> Lambda0 phi LL/DD


@check_process
def make_Lb0ToL0Phi_LL(process):
    if process == 'spruce':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_phi(k_p_min=500 * MeV, k_pt_min=0 * MeV)
    elif process == 'hlt2':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_phi(k_p_min=500 * MeV, k_pt_min=0 * MeV)
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1],
        name="Lb0ToL0Phi_LL_Combiner",
        descriptor='[Lambda_b0 -> Lambda0 phi(1020)]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [hadron1, lambda0s, line_alg]


@check_process
def make_Lb0ToL0Phi_DD(process):
    if process == 'spruce':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_phi(k_p_min=500 * MeV, k_pt_min=0 * MeV)
    elif process == 'hlt2':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_phi(k_p_min=500 * MeV, k_pt_min=0 * MeV)
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1],
        name="Lb0ToL0Phi_DD_Combiner",
        descriptor='[Lambda_b0 -> Lambda0 phi(1020)]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [hadron1, lambda0s, line_alg]


# b-baryon -> Lambda0 KS LLLL / LLDD / DDLL / DDDD


@check_process
def make_Lb0ToL0KS_LLLL(process):
    if process == 'spruce':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_ks0_ll()
    elif process == 'hlt2':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_ks0_ll()
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1],
        name="Lb0ToL0KS_LLLL_Combiner",
        descriptor='[Lambda_b0 -> Lambda0 KS0]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [hadron1, lambda0s, line_alg]


@check_process
def make_Lb0ToL0KS_LLDD(process):
    if process == 'spruce':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_ks0_dd()
    elif process == 'hlt2':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_ks0_dd()
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1],
        name="Lb0ToL0KS_LLDD_Combiner",
        descriptor='[Lambda_b0 -> Lambda0 KS0]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [hadron1, lambda0s, line_alg]


@check_process
def make_Lb0ToL0KS_DDLL(process):
    if process == 'spruce':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_ks0_ll()
    elif process == 'hlt2':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_ks0_ll()
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1],
        name="Lb0ToL0KS_DDLL_Combiner",
        descriptor='[Lambda_b0 -> Lambda0 KS0]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [hadron1, lambda0s, line_alg]


@check_process
def make_Lb0ToL0KS_DDDD(process):
    if process == 'spruce':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_ks0_dd()
    elif process == 'hlt2':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_ks0_dd()
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1],
        name="Lb0ToL0KS_DDDD_Combiner",
        descriptor='[Lambda_b0 -> Lambda0 KS0]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [hadron1, lambda0s, line_alg]
