###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Booking of BnoC hlt2 lines. This is where Hlt2 lines are actually booked
"""
from Hlt2Conf.lines.bnoc.utils import update_makers, make_default_lines, make_prescaled_lines, make_flavour_tagging_lines, make_custom_lines

from Hlt2Conf.lines.bnoc import bbaryon_to_hyperon_4h
from Hlt2Conf.lines.bnoc import bbaryon_to_l0hhh
from Hlt2Conf.lines.bnoc import bbaryon_to_lightbaryon_h
from Hlt2Conf.lines.bnoc import bbaryon_to_lightbaryon_hh
from Hlt2Conf.lines.bnoc import bbaryon_to_phh
from Hlt2Conf.lines.bnoc import bbaryon_to_phhh
from Hlt2Conf.lines.bnoc import BcTohhh
from Hlt2Conf.lines.bnoc import BdsTohhhh
from Hlt2Conf.lines.bnoc import BdsToKShh
from Hlt2Conf.lines.bnoc import BdsToKSKS
from Hlt2Conf.lines.bnoc import BdsToKSpp
from Hlt2Conf.lines.bnoc import BdsToVV
from Hlt2Conf.lines.bnoc import BdTohh
from Hlt2Conf.lines.bnoc import BdToppbarhh
from Hlt2Conf.lines.bnoc import bTohh
from Hlt2Conf.lines.bnoc import BuTohhh
from Hlt2Conf.lines.bnoc import BuToKSh
from Hlt2Conf.lines.bnoc import BuToKShhh

PROCESS = 'hlt2'
hlt2_lines = {}

line_makers = {}
update_makers(line_makers, bbaryon_to_hyperon_4h)
update_makers(line_makers, bbaryon_to_l0hhh)
update_makers(line_makers, bbaryon_to_lightbaryon_h)
update_makers(line_makers, bbaryon_to_lightbaryon_hh)
update_makers(line_makers, bbaryon_to_phh)
update_makers(line_makers, bbaryon_to_phhh)
update_makers(line_makers, BcTohhh)
update_makers(line_makers, BdsTohhhh)
update_makers(line_makers, BdsToKShh)
update_makers(line_makers, BdsToKSKS)
update_makers(line_makers, BdsToKSpp)
update_makers(line_makers, BdsToVV)
update_makers(line_makers, BdTohh)
update_makers(line_makers, BdToppbarhh)
update_makers(line_makers, bTohh)
update_makers(line_makers, BuTohhh)
update_makers(line_makers, BuToKSh)
update_makers(line_makers, BuToKShhh)

###############################################################################
# Add new lines in the appropriate list / dictionary below
# Note that persistreco=False by default (unless you specify
# flavour_tagging=True)
# Ensure that if you add a new module (e.g. BdsToVV.py) it is imported above
# and included in the `update_makers` calls
###############################################################################

# Note, if you intend to run sprucing on an HLT2 line declared here, you must
# set persistreco=True
# persistreco=True should also be used for flavour tagging lines (automatic)

default_lines = [
    'BdsToKstzPhi',
    'BdsToKstzRho',
    'BdToPPbarKK',
    'BdToPPbarKPi',
    'BdToPPbarPiPi',
    'BuToKpPi0',
    'BuToPipPi0',
    'Lb0ToL0KS_DDDD',
    'Lb0ToL0KS_DDLL',
    'Lb0ToL0KS_LLDD',
    'Lb0ToL0KS_LLLL',
    'Lb0ToL0KmPip_DD',
    'Lb0ToL0KmPip_LL',
    'Lb0ToL0KpKm_DD',
    'Lb0ToL0KpKm_LL',
    'Lb0ToL0KpPim_DD',
    'Lb0ToL0KpPim_LL',
    'Lb0ToL0Phi_DD',
    'Lb0ToL0Phi_LL',
    'Lb0ToL0PipPim_DD',
    'Lb0ToL0PipPim_LL',
    'Lb0ToL0PpPm_DD',
    'Lb0ToL0PpPm_LL',
    'Lb0ToPpKSKm_DD',
    'Lb0ToPpKSKm_LL',
    'Lb0ToPpKSPim_DD',
    'Lb0ToPpKSPim_LL',
    'Lb0ToPpPipPimPim_NoPID',
    'Lb0ToPpPpPmKm',
    'Lb0ToPpPpPmPim',
    'LbToXimKp_DDD',
    'LbToXimKp_DDL',
    'LbToXimKp_LLL',
    'OmbmToOmmKS_DDDDD',
    'OmbmToOmmKS_DDDLL',
    'OmbmToOmmKS_DDLDD',
    'OmbmToOmmKS_DDLLL',
    'OmbmToOmmKS_LLLLL',
    'OmbmToOmmKmPip_DDD',
    'OmbmToOmmKmPip_DDL',
    'OmbmToOmmKmPip_LLL',
    'OmbmToOmmKpKmPipPim_DDD',
    'OmbmToOmmKpKmPipPim_DDL',
    'OmbmToOmmKpKmPipPim_LLL',
    'OmbmToOmmKpKm_DDD',
    'OmbmToOmmKpKm_DDL',
    'OmbmToOmmKpKm_LLL',
    'OmbmToOmmKpKpPimPim_DDD',
    'OmbmToOmmKpKpPimPim_DDL',
    'OmbmToOmmKpKpPimPim_LLL',
    'OmbmToOmmKpPim_DDD',
    'OmbmToOmmKpPim_DDL',
    'OmbmToOmmKpPim_LLL',
    'OmbmToOmmKpPipPimPim_DDD',
    'OmbmToOmmKpPipPimPim_DDL',
    'OmbmToOmmKpPipPimPim_LLL',
    'OmbmToOmmPipPim_DDD',
    'OmbmToOmmPipPim_DDL',
    'OmbmToOmmPipPim_LLL',
    'OmbmToOmmPipPipPimPim_DDD',
    'OmbmToOmmPipPipPimPim_DDL',
    'OmbmToOmmPipPipPimPim_LLL',
    'OmbmToOmmPpPmPipPim_DDD',
    'OmbmToOmmPpPmPipPim_DDL',
    'OmbmToOmmPpPmPipPim_LLL',
    'OmbmToOmmPpPm_DDD',
    'OmbmToOmmPpPm_DDL',
    'OmbmToOmmPpPm_LLL',
    'Xib0ToOmmKp_DDD',
    'Xib0ToOmmKp_DDL',
    'Xib0ToOmmKp_LLL',
    'Xib0ToXimPip_DDD',
    'Xib0ToXimPip_DDL',
    'Xib0ToXimPip_LLL',
    'XibmToL0KmKmPip_DD',
    'XibmToL0KmKmPip_LL',
    'XibmToL0KmKpPim_DD',
    'XibmToL0KmKpPim_LL',
    'XibmToL0KmPipPim_DD',
    'XibmToL0KmPipPim_LL',
    'XibmToL0Km_DD',
    'XibmToL0Km_LL',
    'XibmToL0KpKmKm_DD',
    'XibmToL0KpKmKm_LL',
    'XibmToL0KpPimPim_DD',
    'XibmToL0KpPimPim_LL',
    'XibmToL0Pim_DD',
    'XibmToL0Pim_LL',
    'XibmToL0PipPimPim_DD',
    'XibmToL0PipPimPim_LL',
    'XibmToL0PpPmPim_DD',
    'XibmToL0PpPmPim_LL',
    'XibmToPpKSKmKm_DD',
    'XibmToPpKSKmKm_LL',
    'XibmToPpKSKmPim_DD',
    'XibmToPpKSKmPim_LL',
    'XibmToPpKSPimPim_DD',
    'XibmToPpKSPimPim_LL',
    'XibmToPpKmKm',
    'XibmToPpKmPim',
    'XibmToPpPimPim',
    'XibmToXimKS_DDDDD',
    'XibmToXimKS_DDDLL',
    'XibmToXimKS_DDLDD',
    'XibmToXimKS_DDLLL',
    'XibmToXimKS_LLLDD',
    'XibmToXimKS_LLLLL',
    'XibmToXimKmPip_DDD',
    'XibmToXimKmPip_DDL',
    'XibmToXimKmPip_LLL',
    'XibmToXimKpKmPipPim_DDD',
    'XibmToXimKpKmPipPim_DDL',
    'XibmToXimKpKmPipPim_LLL',
    'XibmToXimKpKm_DDD',
    'XibmToXimKpKm_DDL',
    'XibmToXimKpKm_LLL',
    'XibmToXimKpKpPimPim_DDD',
    'XibmToXimKpKpPimPim_DDL',
    'XibmToXimKpKpPimPim_LLL',
    'XibmToXimKpPim_DDD',
    'XibmToXimKpPim_DDL',
    'XibmToXimKpPim_LLL',
    'XibmToXimKpPipPimPim_DDD',
    'XibmToXimKpPipPimPim_DDL',
    'XibmToXimKpPipPimPim_LLL',
    'XibmToXimPipPim_DDD',
    'XibmToXimPipPim_DDL',
    'XibmToXimPipPim_LLL',
    'XibmToXimPipPipPimPim_DDD',
    'XibmToXimPipPipPimPim_DDL',
    'XibmToXimPipPipPimPim_LLL',
    'XibmToXimPpPmPipPim_DDD',
    'XibmToXimPpPmPipPim_DDL',
    'XibmToXimPpPmPipPim_LLL',
    'XibmToXimPpPm_DDD',
    'XibmToXimPpPm_DDL',
    'XibmToXimPpPm_LLL',
]

flavour_tagging_lines = [
    'BdsToKSPi0_LL',
    'BdsToPhiPhi',
    'BdsToKstzKstzb',
    'BdsToPhiRho',
    'BdsToRhoRho',
    'BdToHH',
    'BdsToKsKsLLLL',
    'BdsToKsKsLLDD',
    'BdsToKsKsDDDD',
]

# Currently no prescale lines
# I leave a commented out example to show how to add one
prescale_lines = {
    #'BdsToKstzRho': {'prescale': 0.1},
}

# Below are the lines which require a more custom configuration
# You may want to add a new line category if you have some specific use-case
# e.g. a line with persistreco=True but no flavour tagging to keep clear the
# different groups of lines we define.

# Lines requiring flavour tagging, GEC and topo
flavour_tagging_gec_topo_lines = {
    'BdsToPipPimPipPim': {
        'flavour_tagging': True,
        'require_GEC': True,
        'require_topo': True,
        'min_twobody_mva': 0.95,
        'min_threebody_mva': 0.95
    },
    'BdsToKpPimPipPim': {
        'flavour_tagging': True,
        'require_GEC': True,
        'require_topo': True,
        'min_twobody_mva': 0.99,
        'min_threebody_mva': 0.99
    },
    'BdsToKpKmPipPim': {
        'flavour_tagging': True,
        'require_GEC': True,
        'require_topo': True,
        'min_twobody_mva': 0.95,
        'min_threebody_mva': 0.95
    },
    'BdsToKpKmKpPim': {
        'flavour_tagging': True,
        'require_GEC': True,
        'require_topo': True,
        'min_twobody_mva': 0.95,
        'min_threebody_mva': 0.95
    },
    'BdsToKpKmKpKm': {
        'flavour_tagging': True,
        'require_GEC': True,
        'require_topo': True,
        'min_twobody_mva': 0.95,
        'min_threebody_mva': 0.95
    },
    'BdsToKSPipPim_LL': {
        'flavour_tagging': True,
        'require_GEC': True,
        'require_topo': True,
        'min_twobody_mva': 0.1,
        'min_threebody_mva': 0.1
    },
    'BdsToKSPipPim_DD': {
        'flavour_tagging': True,
        'require_GEC': True,
        'require_topo': True,
        'min_twobody_mva': 0.1,
        'min_threebody_mva': 0.1
    },
    'BdsToKSKpKm_LL': {
        'flavour_tagging': True,
        'require_GEC': True,
        'require_topo': True,
        'min_twobody_mva': 0.1,
        'min_threebody_mva': 0.1
    },
    'BdsToKSKpKm_DD': {
        'flavour_tagging': True,
        'require_GEC': True,
        'require_topo': True,
        'min_twobody_mva': 0.1,
        'min_threebody_mva': 0.1
    },
    'BdsToKSKpPim_LL': {
        'flavour_tagging': True,
        'require_GEC': True,
        'require_topo': True,
        'min_twobody_mva': 0.1,
        'min_threebody_mva': 0.1
    },
    'BdsToKSKpPim_DD': {
        'flavour_tagging': True,
        'require_GEC': True,
        'require_topo': True,
        'min_twobody_mva': 0.1,
        'min_threebody_mva': 0.1
    },
    'BdsToPpPmKS_LL': {
        'flavour_tagging': True,
        'require_GEC': True,
        'require_topo': True,
        'min_twobody_mva': 0.1,
        'min_threebody_mva': 0.1
    },
    'BdsToPpPmKS_DD': {
        'flavour_tagging': True,
        'require_GEC': True,
        'require_topo': True,
        'min_twobody_mva': 0.1,
        'min_threebody_mva': 0.1
    },
}

# Lines with GEC and topo but no flavour tagging
# The 2 (3) body MVA cuts can be specified rather than using the default
# 0.1 that all the below lines currently use (see
# flavour_tagging_gec_topo_lines for examples)
gec_topo_lines = {
    'BuToKSPiPiPi_LL': {
        'require_GEC': True,
        'require_topo': True
    },
    'BuToKSPiPiPi_DD': {
        'require_GEC': True,
        'require_topo': True
    },
    'BuToKSKpPiPi_LL': {
        'require_GEC': True,
        'require_topo': True
    },
    'BuToKSKpPiPi_DD': {
        'require_GEC': True,
        'require_topo': True
    },
    'BuToKSKmPiPi_LL': {
        'require_GEC': True,
        'require_topo': True
    },
    'BuToKSKmPiPi_DD': {
        'require_GEC': True,
        'require_topo': True
    },
    'BuToKSKKPip_LL': {
        'require_GEC': True,
        'require_topo': True
    },
    'BuToKSKKPip_DD': {
        'require_GEC': True,
        'require_topo': True
    },
    'BuToKSKKPim_LL': {
        'require_GEC': True,
        'require_topo': True
    },
    'BuToKSKKPim_DD': {
        'require_GEC': True,
        'require_topo': True
    },
    'BuToKSKKK_LL': {
        'require_GEC': True,
        'require_topo': True
    },
    'BuToKSKKK_DD': {
        'require_GEC': True,
        'require_topo': True
    },
    'BuToKKK': {
        'require_GEC': True,
        'require_topo': True,
        'min_twobody_mva': 0.13,
        'min_threebody_mva': 0.13
    },
    'BcToKKK': {
        'require_GEC': True,
        'require_topo': True
    },
    'BuToLambdapLL': {
        'require_GEC': True,
        'require_topo': True
    },
    'BuToLambdapDD': {
        'require_GEC': True,
        'require_topo': True
    },
    'BcToLambdapLL': {
        'require_GEC': True,
        'require_topo': True
    },
    'BcToLambdapDD': {
        'require_GEC': True,
        'require_topo': True
    },
    'BuToKsLLPi': {
        'require_GEC': True,
        'require_topo': True
    },
    'BuToKsDDPi': {
        'require_GEC': True,
        'require_topo': True
    },
    'BcToKsLLPi': {
        'require_GEC': True,
        'require_topo': True
    },
    'BcToKsDDPi': {
        'require_GEC': True,
        'require_topo': True
    },
    'BuToKsLLK': {
        'require_GEC': True,
        'require_topo': True
    },
    'BuToKsDDK': {
        'require_GEC': True,
        'require_topo': True
    },
    'BcToKsLLK': {
        'require_GEC': True,
        'require_topo': True
    },
    'BcToKsDDK': {
        'require_GEC': True,
        'require_topo': True
    },
}

# If you add a new category of custom line, update the below dictionary
custom_lines = flavour_tagging_gec_topo_lines
custom_lines.update(gec_topo_lines)

make_default_lines(
    process=PROCESS,
    line_dict=hlt2_lines,
    line_makers=line_makers,
    default_lines=default_lines)

make_prescaled_lines(
    process=PROCESS,
    line_dict=hlt2_lines,
    line_makers=line_makers,
    prescaled_lines=prescale_lines)

make_flavour_tagging_lines(
    process=PROCESS,
    line_dict=hlt2_lines,
    line_makers=line_makers,
    flavour_tagging_lines=flavour_tagging_lines)

make_custom_lines(
    process=PROCESS,
    line_dict=hlt2_lines,
    line_makers=line_makers,
    custom_lines=custom_lines)
