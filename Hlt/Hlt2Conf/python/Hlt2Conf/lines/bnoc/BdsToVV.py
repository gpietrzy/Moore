###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B0(s) -> VV, V -> hh HLT2 lines.
* V: phi, kstar, rho
"""

from GaudiKernel.SystemOfUnits import MeV, mm, GeV, picosecond

from PyConf import configurable
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.bnoc.builders.basic_builder import make_KS_LL, make_wide_kstar0, make_phi, make_rho0, make_tight_kaons, make_tight_pions, make_merged_pi0s
from Hlt2Conf.lines.bnoc.builders.combiners import make_twobody, make_phixtwobody
from Hlt2Conf.lines.bnoc.builders.b_builder import make_b2Kpi
from Hlt2Conf.lines.bnoc.utils import check_process


@configurable
def _make_BdsToPhiPhi(
        particles,
        descriptor,
        am_min=4950 * MeV,
        am_max=6000 * MeV,
        vtx_am_min=5000 * MeV,
        vtx_am_max=5950 * MeV,
        make_pvs=make_pvs,
        ptproduct=1.5 * GeV * GeV,
        ltime_min=0.2 * picosecond,
        vchi2pdof_max=15,
):

    return make_phixtwobody(
        particles,
        descriptor,
        am_min,
        am_max,
        vtx_am_min,
        vtx_am_max,
        name="BdsToPhiPhiCombiner_{hash}",
        make_pvs=make_pvs,
        ptproduct=ptproduct,
        vchi2pdof_max=vchi2pdof_max,
    )


@check_process
@configurable
def make_BdsToPhiPhi(process):
    phi = make_phi(am_min=995 * MeV, am_max=1045 * MeV)
    line_alg = _make_BdsToPhiPhi(
        particles=[phi, phi],
        make_pvs=make_pvs,
        descriptor="B_s0 -> phi(1020) phi(1020)")
    return [phi, line_alg]


@configurable
def _make_BdsToKstzKstzb(particles,
                         descriptor,
                         am_min=4900 * MeV,
                         am_max=6000 * MeV,
                         vtx_am_min=4950 * MeV,
                         vtx_am_max=5950 * MeV,
                         make_pvs=make_pvs,
                         asumpt_min=2500. * MeV,
                         adoca12_max=0.08 * mm,
                         vchi2pdof_max=16,
                         mipchi2_max=25,
                         bpvvdchi2_min=81,
                         bpvdira_min=0.999):

    return make_twobody(
        particles,
        descriptor,
        am_min,
        am_max,
        vtx_am_min,
        vtx_am_max,
        name="BdsToKstzKstzbCombiner_{hash}",
        make_pvs=make_pvs,
        asumpt_min=asumpt_min,
        adoca12_max=adoca12_max,
        vchi2pdof_max=vchi2pdof_max,
        mipchi2_max=mipchi2_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvdira_min=bpvdira_min)


@check_process
@configurable
def make_BdsToKstzKstzb(process):
    kstar = make_wide_kstar0(
        make_kaons=make_tight_kaons,
        pi_pidk=5.,
        k_pidk_min=-5.,
        bpvvdchi2_min=16,
        vchi2pdof_max=13,
        adoca12_max=0.15 * mm,
        motherpt_min=900 * MeV,
        mipchi2_min=20.)
    line_alg = _make_BdsToKstzKstzb(
        particles=[kstar, kstar],
        make_pvs=make_pvs,
        descriptor='B_s0 -> K*(892)0 K*(892)~0')
    return [kstar, line_alg]


@configurable
def _make_BdsToKstzPhi(particles,
                       descriptor,
                       am_min=5000 * MeV,
                       am_max=6000 * MeV,
                       vtx_am_min=4550 * MeV,
                       vtx_am_max=6750 * MeV,
                       make_pvs=make_pvs,
                       asumpt_min=1500 * MeV,
                       adoca12_max=0.3 * mm,
                       vchi2pdof_max=15,
                       mipchi2_max=25,
                       bpvdira_min=0.99):

    return make_twobody(
        particles,
        descriptor,
        am_min,
        am_max,
        vtx_am_min,
        vtx_am_max,
        name="BdsToKstzPhiCombiner_{hash}",
        make_pvs=make_pvs,
        adoca12_max=adoca12_max,
        vchi2pdof_max=vchi2pdof_max,
        mipchi2_max=mipchi2_max,
        bpvdira_min=bpvdira_min)


@check_process
@configurable
def make_BdsToKstzPhi(process):
    kstar = make_wide_kstar0(
        make_pions=make_tight_pions,
        make_kaons=make_tight_kaons,
        am_max=1800 * MeV,
        asumpt_min=500 * MeV)
    phi = make_phi(
        make_kaons=make_tight_kaons,
        k_pidk_min=-5.,
        k_p_min=1.5 * GeV,
        k_pt_min=500 * MeV,
        asumpt_min=500 * MeV,
        vchi2pdof_max=9)
    line_alg = _make_BdsToKstzPhi(
        particles=[phi, kstar],
        make_pvs=make_pvs,
        descriptor='[B0 -> phi(1020) K*(892)0]cc')
    return [kstar, phi, line_alg]


@configurable
def _make_BdsToKstzRho(particles,
                       descriptor,
                       am_min=4900 * MeV,
                       am_max=6000 * MeV,
                       vtx_am_min=4850 * MeV,
                       vtx_am_max=5950 * MeV,
                       make_pvs=make_pvs,
                       asumpt_min=2500. * MeV,
                       adoca12_max=0.15 * mm,
                       vchi2pdof_max=16,
                       mipchi2_max=25,
                       bpvvdchi2_min=36,
                       bpvdira_min=0.999):

    return make_twobody(
        particles,
        descriptor,
        am_min,
        am_max,
        vtx_am_min,
        vtx_am_max,
        name="BdsToKstzRhoCombiner_{hash}",
        make_pvs=make_pvs,
        asumpt_min=asumpt_min,
        adoca12_max=adoca12_max,
        vchi2pdof_max=vchi2pdof_max,
        mipchi2_max=mipchi2_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvdira_min=bpvdira_min)


@check_process
@configurable
def make_BdsToKstzRho(process):
    kstar = make_wide_kstar0(
        make_kaons=make_tight_kaons,
        pi_pidk=3.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvvdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.)
    rho = make_rho0(
        pi_pidk_max=0.,
        pi_p_min=1. * GeV,
        pi_pt_min=250. * MeV,
        am_min=280 * MeV,
        am_max=1300 * MeV,
        adoca12_max=0.3 * mm,
        bpvvdchi2_min=16,
        vchi2pdof_max=7.5,
        asumpt_min=500 * MeV,
        motherpt_min=500 * MeV,
        mipchi2_min=36.)
    line_alg = _make_BdsToKstzRho(
        particles=[kstar, rho],
        make_pvs=make_pvs,
        descriptor='[B0 -> K*(892)~0 rho(770)0]cc')
    return [kstar, rho, line_alg]


@configurable
def _make_BdsToPhiRho(particles,
                      descriptor,
                      am_min=4800 * MeV,
                      am_max=6000 * MeV,
                      vtx_am_min=4850 * MeV,
                      vtx_am_max=5950 * MeV,
                      make_pvs=make_pvs,
                      asumpt_min=2000. * MeV,
                      ptproduct_min=2.5 * GeV * GeV,
                      adoca12_max=0.09 * mm,
                      vchi2pdof_max=16,
                      mipchi2_max=25,
                      bpvvdchi2_min=90,
                      bpvdira_min=0.99):

    return make_twobody(
        particles,
        descriptor,
        am_min,
        am_max,
        vtx_am_min,
        vtx_am_max,
        name="BdsToPhiRhoCombiner",
        make_pvs=make_pvs,
        asumpt_min=asumpt_min,
        ptproduct_min=2.5 * GeV * GeV,
        adoca12_max=adoca12_max,
        vchi2pdof_max=vchi2pdof_max,
        mipchi2_max=mipchi2_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvdira_min=bpvdira_min)


@check_process
@configurable
def make_BdsToPhiRho(process):
    phi = make_phi(
        make_kaons=make_tight_kaons,
        k_pidk_min=-5.,
        adoca12_max=0.12 * mm,
        asumpt_min=1100 * MeV,
        bpvvdchi2_min=11,
        vchi2pdof_max=9,
        mipchi2_min=32.)
    rho = make_rho0(
        pi_pidk_max=5.,
        adoca12_max=0.12 * mm,
        bpvvdchi2_min=20,
        vchi2pdof_max=15,
        mipchi2_min=32.)
    line_alg = _make_BdsToPhiRho(
        particles=[rho, phi],
        make_pvs=make_pvs,
        descriptor='B_s0 -> rho(770)0 phi(1020)')
    return [phi, rho, line_alg]


@configurable
def _make_BdsToRhoRho(particles,
                      descriptor,
                      am_min=4950 * MeV,
                      am_max=6000 * MeV,
                      vtx_am_min=5000 * MeV,
                      vtx_am_max=5950 * MeV,
                      make_pvs=make_pvs,
                      asumpt_min=2500. * MeV,
                      adoca12_max=0.2 * mm,
                      vchi2pdof_max=16,
                      mipchi2_max=25,
                      bpvvdchi2_min=36,
                      bpvdira_min=0.999):

    return make_twobody(
        particles,
        descriptor,
        am_min,
        am_max,
        vtx_am_min,
        vtx_am_max,
        name="BdsToRhoRhoCombiner",
        make_pvs=make_pvs,
        asumpt_min=asumpt_min,
        adoca12_max=adoca12_max,
        vchi2pdof_max=vchi2pdof_max,
        mipchi2_max=mipchi2_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvdira_min=bpvdira_min)


@check_process
@configurable
def make_BdsToRhoRho(process):
    rho = make_rho0(
        pi_pidk_max=0.,
        pi_p_min=800 * MeV,
        pi_pt_min=250. * MeV,
        am_min=280 * MeV,
        am_max=1600 * MeV,
        adoca12_max=0.3 * mm,
        bpvvdchi2_min=16,
        vchi2pdof_max=7.5,
        asump_min=800 * MeV,
        asumpt_min=500 * MeV,
        motherpt_min=500 * MeV,
        mipchi2_min=36.)
    line_alg = _make_BdsToRhoRho(
        particles=[rho, rho],
        make_pvs=make_pvs,
        descriptor='B0 -> rho(770)0 rho(770)0')
    return [rho, line_alg]


@check_process
@configurable
def make_BdsToKSPi0_LL(process):
    ks = make_KS_LL(
        pi_p_min=0 * GeV,
        p_min=8000.0 * MeV,
        pt_min=500.0 * MeV,
        vchi2_max=10.0,
        bpvfdchi2_min=25,
        mipchi2_min=10.0)
    pions = make_merged_pi0s(p_min=5000 * MeV, pt_min=3500 * MeV)

    line_alg = make_b2Kpi(
        particles=[ks, pions],
        descriptor='B0 -> KS0 pi0',
        comb_m_min=4000 * MeV,
        comb_m_max=6200 * MeV,
        comb_pt_min=5000 * MeV,
        mtdocachi2_max=10.0,
        pt_min=4000 * MeV)
    return [ks, line_alg]
