###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of B&Q HLT2 lines

Lines with neutral inputs or downstream tracks are commented out for now
"""

from PyConf import configurable

from Moore.config import HltLine, register_line_builder

#get bandq builders
from Hlt2Conf.lines.bandq.builders.prefilters import make_prefilters
from Hlt2Conf.lines.bandq.builders import b_to_jpsiX_lines, \
    doublecharm, \
    b_for_spectroscopy
from Hlt2Conf.lines.bandq.builders import dps_lines
from Hlt2Conf.lines.bandq.builders import dimuon_lines
from Hlt2Conf.lines.bandq.builders import ccbargamma_conv_lines, ccbarmumu_lines
from Hlt2Conf.lines.bandq.builders import qqbar_to_hadrons
from Hlt2Conf.lines.bandq.builders import ppmumu_lines
from Hlt2Conf.lines.bandq.builders import bbaryon_to_lcdsX_lines
from Hlt2Conf.lines.bandq.builders import jpsi_to_lmdlmd_lines

PROCESS = 'hlt2'
all_lines = {}


@register_line_builder(all_lines)
@configurable
def ccbarToPpPmPrompt_line(name='Hlt2BandQ_ccbarToPpPmPrompt',
                           prescale=1,
                           persistreco=False):
    line_alg = qqbar_to_hadrons.make_prompt_ccbarToPpPm()
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def ccbarToPpPmHighPrompt_line(name='Hlt2BandQ_ccbarToPpPmHighPrompt',
                               prescale=1,
                               persistreco=False):
    line_alg = qqbar_to_hadrons.make_prompt_ccbarToPpPmHigh()
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def doubleCcbarToPpPmPrompt_line(name='Hlt2BandQ_doubleCcbarToPpPmPrompt',
                                 prescale=1,
                                 persistreco=False):
    line_alg = qqbar_to_hadrons.make_prompt_doubleCcbarToPpPm()
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def DiMuonHighMass_line(name='Hlt2BandQ_DiMuonHighMass',
                        prescale=1,
                        persistreco=True):
    line_alg = dimuon_lines.make_HighMass_dimuon()
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def DiMuonSameSignHighMass_line(name='Hlt2BandQ_DiMuonSameSignHighMass',
                                prescale=0.2,
                                persistreco=True):
    line_alg = dimuon_lines.make_HighMass_samesign_dimuon()
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def DiMuonInc_line(name='Hlt2BandQ_DiMuonInc', prescale=0.03,
                   persistreco=True):
    line_alg = dimuon_lines.make_loose_dimuon()
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def DiMuonSameSignInc_line(name='Hlt2BandQ_DiMuonSameSignInc',
                           prescale=0.1,
                           persistreco=True):
    line_alg = dimuon_lines.make_loose_samesign_dimuon()
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def DiMuonIncHighPT_line(name='Hlt2BandQ_DiMuonIncHighPT',
                         prescale=1,
                         persistreco=True):
    line_alg = dimuon_lines.make_tight_highpt_dimuon()
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def DiMuonSameSignIncHighPT_line(name='Hlt2BandQ_DiMuonSameSignIncHighPT',
                                 prescale=1,
                                 persistreco=True):
    line_alg = dimuon_lines.make_tight_highpt_samesign_dimuon()
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def DiMuonSoft_line(name='Hlt2BandQ_DiMuonSoft', prescale=1, persistreco=True):
    line_alg = dimuon_lines.make_soft_detached_dimuon()
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def JpsiToMuMuHighPt_line(name='Hlt2BandQ_DiMuonJPsiHighPT', prescale=1):
    line_alg = dimuon_lines.make_jpsi_highpt()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def UpsilonToMuMu_line(name='Hlt2BandQ_DiMuonUpsilon',
                       prescale=1,
                       persistreco=True):
    line_alg = dimuon_lines.make_upsilon()
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def ZToMuMu_line(name='Hlt2BandQ_DiMuonZ', prescale=1):
    line_alg = dimuon_lines.make_z()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def BcToJpsiPip_JpsiToMuMu_line(name='Hlt2BandQ_BcToJpsiPip_JpsiToMuMu',
                                prescale=1,
                                persistreco=True):
    """Bc+ --> Jpsi(-> mu+ mu-)  pi+ line"""
    line_alg = b_to_jpsiX_lines.make_BcToJpsiPip_JpsiToMuMu(process=PROCESS)
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def BcToPsi2SPip_Psi2SToMuMu_line(name='Hlt2BandQ_BcToPsi2SPip_Psi2SToMuMu',
                                  prescale=1):
    """Bc+ --> psi(2S)(-> mu+ mu-)  pi+ line"""
    line_alg = b_to_jpsiX_lines.make_BcToPsi2SPip_Psi2SToMuMu(process=PROCESS)
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def BuToJpsiKp_JpsiToMuMu_line(name='Hlt2BandQ_BpToJpsiKp_JpsiToMuMu',
                               prescale=1):
    """B+ --> Jpsi(-> mu+ mu-)  K+ line"""
    line_alg = b_to_jpsiX_lines.make_BuToJpsiKp_JpsiToMuMu(process=PROCESS)
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def BuToPsi2SKp_Psi2SToMuMu_line(name='Hlt2BandQ_BpToPsi2SKp_Psi2SToMuMu',
                                 prescale=1):
    """B+ --> psi(2S)(-> mu+ mu-)  K+ line"""
    line_alg = b_to_jpsiX_lines.make_BuToPsi2SKp_Psi2SToMuMu(process=PROCESS)
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def BuToJpsiPip_JpsiToMuMu_line(name='Hlt2BandQ_BpToJpsiPip_JpsiToMuMu',
                                prescale=1):
    """B+ --> Jpsi(-> mu+ mu-)  pi+ line"""
    line_alg = b_to_jpsiX_lines.make_BuToJpsiPip_JpsiToMuMu(process=PROCESS)
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def BuToPsi2SPip_Psi2SToMuMu_line(name='Hlt2BandQ_BpToPsi2SPip_Psi2SToMuMu',
                                  prescale=1):
    """B+ --> psi(2S)(-> mu+ mu-)  pi+ line"""
    line_alg = b_to_jpsiX_lines.make_BuToPsi2SPip_Psi2SToMuMu(process=PROCESS)
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def doubledimuon_dps_jpsijpsi_line(name='Hlt2BandQ_DoubleDiMuon_Jpsi_Jpsi_DPS',
                                   prescale=1,
                                   persistreco=True):
    """DoubleDiMuon DPS line"""
    line_alg = dps_lines.make_doubledimuon_dps_jpsijpsi()
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def doubledimuon_dps_psi2spsi2s_line(
        name='Hlt2BandQ_DoubleDiMuon_Psi2S_Psi2S_DPS',
        prescale=1,
        persistreco=True):
    """DoubleDiMuon DPS line"""
    line_alg = dps_lines.make_doubledimuon_dps_psi2spsi2s()
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def doubledimuon_dps_upsilonupsilon_line(
        name='Hlt2BandQ_DoubleDiMuon_Upsilon_Upsilon_DPS', prescale=1):
    """DoubleDiMuon DPS line"""
    line_alg = dps_lines.make_doubledimuon_dps_upsilonupsilon()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def doubledimuon_dps_jpsipsi2s_line(
        name='Hlt2BandQ_DoubleDiMuon_Jpsi_Psi2S_DPS', prescale=1,
        persistreco=True):
    """DoubleDiMuon DPS line"""
    line_alg = dps_lines.make_doubledimuon_dps_jpsipsi2s()
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def doubledimuon_dps_jpsiupsilon_line(
        name='Hlt2BandQ_DoubleDiMuon_Jpsi_Upsilon_DPS', prescale=1):
    """DoubleDiMuon DPS line"""
    line_alg = dps_lines.make_doubledimuon_dps_jpsiupsilon()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def doubledimuon_dps_psi2supsilon_line(
        name='Hlt2BandQ_DoubleDiMuon_Psi2S_Upsilon_DPS', prescale=1):
    """DoubleDiMuon DPS line"""
    line_alg = dps_lines.make_doubledimuon_dps_psi2supsilon()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def chic2jpsimumu_line(name="Hlt2BandQ_ChicToJpsiMuMu", prescale=1):
    line_alg = ccbarmumu_lines.make_chic2jpsimumu()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def doublecharm_samesign_line(name="Hlt2BandQ_DoubleCharmSameSign",
                              prescale=1,
                              persistreco=True):
    line_alg = doublecharm.make_doublecharm_samesign()
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def X2jpsimumu_line(name="Hlt2BandQ_XToJpsiMuMu", prescale=1):
    line_alg = ccbarmumu_lines.make_X2jpsimumu()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def doublecharm_oppositesign_line(name="Hlt2BandQ_DoubleCharmOppositeSign",
                                  prescale=1,
                                  persistreco=True):
    line_alg = doublecharm.make_doublecharm_oppositesign()
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def hc2jpsimumu_line(name="Hlt2BandQ_hcToJpsiMuMu", prescale=1):
    line_alg = ccbarmumu_lines.make_hc2jpsimumu()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


#@register_line_builder(all_lines)
#@configurable
#def X2chicmumu_line(name="Hlt2BandQ_XToChicMuMu", prescale=1):
#    line_alg = ccbarmumu_lines.make_X2chicmumu()
#    return HltLine(
#        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def chib2upsilonmumu_line(name="Hlt2BandQ_ChibToUpsilonMuMu", prescale=1):
    line_alg = ccbarmumu_lines.make_chib2upsilonmumu()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def bc2jpsimu_line(name="Hlt2BandQ_BcToJpsiMu_JpsiToMuMu",
                   prescale=1,
                   persistreco=True):
    line_alg = b_to_jpsiX_lines.make_BcToJpsiMu_JpsiToMuMu(process=PROCESS)
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


#@register_line_builder(all_lines)
#@configurable
#def ccbar2jpsigamma_convDD_line(name="Hlt2BandQ_CcbarToJpsiGamma_ConvDD",
#                                prescale=1):
#    line_alg = ccbargamma_conv_lines.make_ccbar2jpsigamma_convDD()
#    return HltLine(
#        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)

#@register_line_builder(all_lines)
#@configurable
#def bbbar2upsilongamma_convDD_line(
#        name="Hlt2BandQ_BbbarToUpsilonGamma_ConvDD", prescale=1):
#    line_alg = ccbargamma_conv_lines.make_bbbar2upsilongamma_convDD()
#    return HltLine(
#        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def ccbar2jpsigamma_convLL_line(name="Hlt2BandQ_CcbarToJpsiGamma_ConvLL",
                                prescale=1):
    line_alg = ccbargamma_conv_lines.make_ccbar2jpsigamma_convLL()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def bbbar2upsilongamma_convLL_line(name="Hlt2BandQ_BbbarToUpsilonGamma_ConvLL",
                                   prescale=1):
    line_alg = ccbargamma_conv_lines.make_bbbar2upsilongamma_convLL()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def BBbarToPhiPhi_line(name='Hlt2BandQ_BBbarToPhiPhi', prescale=1):
    """Upsilon(1S) -> phi(1020) phi(1020)"""
    line_alg = qqbar_to_hadrons.make_bbbar_to_phiphi()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def PPMuMu_Detached_line(name='Hlt2BandQ_PPMuMu_Detached', prescale=1):
    """detached p+ p~- mu+ mu-"""
    line_alg = ppmumu_lines.make_ppmumu_Detached()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def PPMuMu_Hc_line(name='Hlt2BandQ_PPMuMu_Hc', prescale=1):
    """p+ p~- mu+ mu- in charm mass region"""
    line_alg = ppmumu_lines.make_ppmumu_Hc()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def PPMuMu_High_line(name='Hlt2BandQ_PPMuMu_High', prescale=1):
    """p+ p~- mu+ mu- in high mass region"""
    line_alg = ppmumu_lines.make_ppmumu_High()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


###############################################
#### B for spectroscopy lines  #################
##  ------ fully reconstructed modes
@register_line_builder(all_lines)
@configurable
def Bu_for_spectroscopy_line(name="Hlt2BandQ_BuForSpectroscopy",
                             prescale=1,
                             persistreco=True):
    line_alg = b_for_spectroscopy.make_Bu(process=PROCESS)
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def Bd_for_spectroscopy_line(name="Hlt2BandQ_BdForSpectroscopy",
                             prescale=1,
                             persistreco=True):
    line_alg = b_for_spectroscopy.make_Bd(process=PROCESS)
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def Bs_for_spectroscopy_line(name="Hlt2BandQ_BsForSpectroscopy",
                             prescale=1,
                             persistreco=True):
    line_alg = b_for_spectroscopy.make_Bs(process=PROCESS)
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def Lb_for_spectroscopy_line(name="Hlt2BandQ_LbForSpectroscopy",
                             prescale=1,
                             persistreco=True):
    line_alg = b_for_spectroscopy.make_Lb(process=PROCESS)
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def Bc_for_spectroscopy_line(name="Hlt2BandQ_BcForSpectroscopy",
                             prescale=1,
                             persistreco=True):
    line_alg = b_for_spectroscopy.make_Bc(process=PROCESS)
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def Xibm_for_spectroscopy_line(name="Hlt2BandQ_XibmForSpectroscopy",
                               prescale=1,
                               persistreco=True):
    line_alg = b_for_spectroscopy.make_Xibm(process=PROCESS)
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def Xib0_for_spectroscopy_line(name="Hlt2BandQ_Xib0ForSpectroscopy",
                               prescale=1,
                               persistreco=True):
    line_alg = b_for_spectroscopy.make_Xib0(process=PROCESS)
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def Omegab_for_spectroscopy_line(name="Hlt2BandQ_OmegabForSpectroscopy",
                                 prescale=1,
                                 persistreco=True):
    line_alg = b_for_spectroscopy.make_Omegab(process=PROCESS)
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


##  -------- semileptonic modes


@register_line_builder(all_lines)
@configurable
def Bud_for_spectroscopy_SL_line(name="Hlt2BandQ_BudForSpectroscopySL",
                                 prescale=1,
                                 persistreco=True):
    line_alg = b_for_spectroscopy.make_Bud_SL(process=PROCESS)
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def Bs_for_spectroscopy_SL_line(name="Hlt2BandQ_BsForSpectroscopySL",
                                prescale=1,
                                persistreco=True):
    line_alg = b_for_spectroscopy.make_Bs_SL(process=PROCESS)
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def Bc_for_spectroscopy_SL_line(name="Hlt2BandQ_BcForSpectroscopySL",
                                prescale=1,
                                persistreco=True):
    line_alg = b_for_spectroscopy.make_Bc_SL(process=PROCESS)
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def Lb_for_spectroscopy_SL_line(name="Hlt2BandQ_LbForSpectroscopySL",
                                prescale=1,
                                persistreco=True):
    line_alg = b_for_spectroscopy.make_Lb_SL(process=PROCESS)
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def Xib_for_spectroscopy_SL_line(name="Hlt2BandQ_XibForSpectroscopySL",
                                 prescale=1,
                                 persistreco=True):
    line_alg = b_for_spectroscopy.make_Xib_SL(process=PROCESS)
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def Omegab_for_spectroscopy_SL_line(name="Hlt2BandQ_OmegabForSpectroscopySL",
                                    prescale=1,
                                    persistreco=True):
    line_alg = b_for_spectroscopy.make_Omegab_SL(process=PROCESS)
    return HltLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


from Hlt2Conf.lines.charmonium_to_dimuon_detached import make_detached_jpsi
from Hlt2Conf.lines.bandq.builders.b_to_jpsiX_TT_lines import make_Lb2JpsiLambdaTT, make_Bd2JpsiKsTT
from Moore.lines import Hlt2Line


@register_line_builder(all_lines)
@configurable
def Lb2JpsiLambdaTTLine(name='Hlt2BandQ_Lb2JpsiLambdaTT', prescale=1.):
    return Hlt2Line(
        # Name must start with either Hlt1 or Hlt2
        name=name,
        algs=make_prefilters() +
        [make_detached_jpsi(), make_Lb2JpsiLambdaTT()],
        prescale=prescale)


@register_line_builder(all_lines)
@configurable
def Bd2JpsiKsTTLine(name='Hlt2BandQ_Bd2JpsiKsTT', prescale=1.):
    return Hlt2Line(
        # Name must start with either Hlt1 or Hlt2
        name=name,
        algs=make_prefilters() + [make_detached_jpsi(),
                                  make_Bd2JpsiKsTT()],
        prescale=prescale)


@register_line_builder(all_lines)
@configurable
def XibToJpsiPKsLLK_JpsiToMuMu_line(
        name='Hlt2BandQ_XibToJpsiPKsLLK_JpsiToMuMu', prescale=1):
    """Xib0 --> Jpsi(-> mu+ mu-)  p+ KS0LL K- line"""
    line_alg = b_to_jpsiX_lines.make_XibToJpsiPKsLLK_JpsiToMuMu()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def XibToJpsiPKsDDK_JpsiToMuMu_line(
        name='Hlt2BandQ_XibToJpsiPKsDDK_JpsiToMuMu', prescale=1):
    """Xib0 --> Jpsi(-> mu+ mu-)  p+ KS0DD K- line"""
    line_alg = b_to_jpsiX_lines.make_XibToJpsiPKsDDK_JpsiToMuMu()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def XibToJpsiPKPi_JpsiToMuMu_line(name='Hlt2BandQ_XibToJpsiPKPi_JpsiToMuMu',
                                  prescale=1):
    """Xib- --> Jpsi(-> mu+ mu-)  p+ K- pi- line"""
    line_alg = b_to_jpsiX_lines.make_XibToJpsiPKPi_JpsiToMuMu()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def LbToLcDsmPiPi_line(name='Hlt2BandQ_LbToLcDsmPiPi', prescale=1):
    """Lb0 --> Lc+(-> p+ K- pi+)  Ds-(-> K- K+ pi-) pi+ pi- line"""
    line_alg = bbaryon_to_lcdsX_lines.make_LbToLcDsmPiPi()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def LbToLcDsmKK_line(name='Hlt2BandQ_LbToLcDsmKK', prescale=1):
    """Lb0 --> Lc+(-> p+ K- pi+)  Ds-(-> K- K+ pi-) K+ K- line"""
    line_alg = bbaryon_to_lcdsX_lines.make_LbToLcDsmKK()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def XibToLcDsmK_line(name='Hlt2BandQ_XibToLcDsmK', prescale=1):
    """Xib- --> Lc+(-> p+ K- pi+)  Ds-(-> K- K+ pi-) K- line"""
    line_alg = bbaryon_to_lcdsX_lines.make_XibToLcDsmK()
    return HltLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


from Hlt2Conf.lines.bandq.builders.jpsi_to_lmdlmd_lines import make_lambdall, make_lambdadd, make_lambdatt


@register_line_builder(all_lines)
@configurable
def Jpsi2LmdLmd_LLLL_line(name='Hlt2BandQ_Jpsi2LmdLmd_LLLL',
                          prescale=1,
                          persistreco=False):
    """Jpsi -> Lambda(LL) Lambda(LL)"""
    line_alg = jpsi_to_lmdlmd_lines.make_jpsi_to_lmdlmd_llll()
    return HltLine(
        name=name,
        algs=make_prefilters() + [make_lambdall(), line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def Jpsi2LmdLmd_LLDD_line(name='Hlt2BandQ_Jpsi2LmdLmd_LLDD',
                          prescale=1,
                          persistreco=False):
    """Jpsi -> Lambda(LL) Lambda(DD)"""
    line_alg = jpsi_to_lmdlmd_lines.make_jpsi_to_lmdlmd_lldd()
    return HltLine(
        name=name,
        algs=make_prefilters() + [make_lambdall(),
                                  make_lambdadd(), line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def Jpsi2LmdLmd_DDDD_line(name='Hlt2BandQ_Jpsi2LmdLmd_DDDD',
                          prescale=1,
                          persistreco=False):
    """Jpsi -> Lambda(DD) Lambda(DD)"""
    line_alg = jpsi_to_lmdlmd_lines.make_jpsi_to_lmdlmd_dddd()
    return HltLine(
        name=name,
        algs=make_prefilters() + [make_lambdadd(), line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def Jpsi2LmdLmd_TTLL_line(name='Hlt2BandQ_Jpsi2LmdLmd_TTLL',
                          prescale=1,
                          persistreco=False):
    """Jpsi -> Lambda(RK-TT) Lambda(LL)"""
    line_alg = jpsi_to_lmdlmd_lines.make_jpsi_to_lmdlmd_ttll()
    return HltLine(
        name=name,
        algs=make_prefilters() + [make_lambdall(),
                                  make_lambdatt(), line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def Jpsi2LmdLmd_TTDD_line(name='Hlt2BandQ_Jpsi2LmdLmd_TTDD',
                          prescale=1,
                          persistreco=False):
    """Jpsi -> Lambda(RK-TT) Lambda(DD)"""
    line_alg = jpsi_to_lmdlmd_lines.make_jpsi_to_lmdlmd_ttdd()
    return HltLine(
        name=name,
        algs=make_prefilters() + [make_lambdadd(),
                                  make_lambdatt(), line_alg],
        prescale=prescale,
        persistreco=persistreco)
