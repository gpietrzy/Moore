###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the Chic2JpsiMuMu line
"""

import Functors as F
from Functors.math import in_range

from Hlt2Conf.algorithms_thor import ParticleCombiner
from Functors import require_all

from GaudiKernel.SystemOfUnits import MeV

from PyConf import configurable

from Hlt2Conf.lines.charmonium_to_dimuon import make_jpsi_tight
from Hlt2Conf.lines.bandq.builders.dimuon_lines import make_upsilon_tight
from Hlt2Conf.lines.bandq.builders.neutral_particles import make_gamma2ee_DD, make_gamma2ee_LL

########################
#line registers        #
########################


@configurable
def make_ccbar2jpsigamma_convDD(name='bandq_ccbar2jpsigamma_convDD_{hash}',
                                descriptor="chi_c1(1P) -> J/psi(1S) gamma",
                                am_min=2900. * MeV,
                                am_max=4800. * MeV,
                                m_min=3000. * MeV,
                                m_max=4700. * MeV,
                                maxVertexChi2=25):
    jpsi = make_jpsi_tight()
    gamma = make_gamma2ee_DD()
    combination_code = in_range(am_min, F.MASS, am_max)
    vertex_code = require_all(
        in_range(m_min, F.MASS, am_max), F.CHI2 < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=[jpsi, gamma],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_ccbar2jpsigamma_convLL(name='bandq_ccbar2jpsigamma_convLL_{hash}',
                                descriptor="chi_c1(1P) -> J/psi(1S) gamma",
                                am_min=2900. * MeV,
                                am_max=4800. * MeV,
                                m_min=3000. * MeV,
                                m_max=4700. * MeV,
                                maxVertexChi2=25):
    jpsi = make_jpsi_tight()
    gamma = make_gamma2ee_LL()
    combination_code = in_range(am_min, F.MASS, am_max)
    vertex_code = require_all(
        in_range(m_min, F.MASS, am_max), F.CHI2 < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=[jpsi, gamma],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_bbbar2upsilongamma_convDD(
        name='bandq_bbbar2upsilongamma_convDD_{hash}',
        descriptor="chi_b0(1P) -> Upsilon(1S) gamma",
        am_min=9400. * MeV,
        am_max=13200. * MeV,
        m_min=9500. * MeV,
        m_max=13000. * MeV,
        maxVertexChi2=25):
    upsilon = make_upsilon_tight()
    gamma = make_gamma2ee_DD()
    combination_code = in_range(am_min, F.MASS, am_max)
    vertex_code = require_all(
        in_range(m_min, F.MASS, am_max), F.CHI2 < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=[upsilon, gamma],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_bbbar2upsilongamma_convLL(
        name='bandq_bbbar2upsilongamma_convLL_{hash}',
        descriptor="chi_b0(1P) -> Upsilon(1S) gamma",
        am_min=9400. * MeV,
        am_max=13200. * MeV,
        m_min=9500. * MeV,
        m_max=13000. * MeV,
        maxVertexChi2=25):
    upsilon = make_upsilon_tight()
    gamma = make_gamma2ee_LL()
    combination_code = in_range(am_min, F.MASS, am_max)
    vertex_code = require_all(
        in_range(m_min, F.MASS, am_max), F.CHI2 < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=[upsilon, gamma],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)
