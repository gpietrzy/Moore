###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from PyConf.tonic import configurable
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import (
    make_pvs,
    upfront_reconstruction,
)
from RecoConf.event_filters import require_pvs
from RecoConf.reconstruction_objects import make_pvs_v1
from Hlt2Conf.standard_particles import make_long_pions

from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter

from Hlt2Conf.probe_muons import make_velo_muons

all_lines = {}


@configurable
def filter_pions(particles, pvs=None, pt_min=0.4 * GeV):
    if pvs is not None:
        cut = F.require_all(
            F.P > 5000,
            F.PT > pt_min,
            F.ETA < 5.0,
            F.ETA > 1.5,
            F.MINIP(pvs) > 0.4 * mm,
        )
    else:
        cut = F.require_all(F.P > 5000, F.PT > pt_min, F.ETA < 5.0,
                            F.ETA > 1.5)

    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def filter_particles_velo(particles, pvs):
    cut = F.require_all(
        F.ETA < 4.5,
        F.ETA > 1.5,
        F.MINIP(pvs) > 0.5 * mm,
    )
    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def make_kshort_pi_muplus_specific_charge(pions,
                                          muons,
                                          decay_descriptor,
                                          comb_m_min=0 * MeV,
                                          comb_m_max=2650 * MeV,
                                          comb_maxdoca=0.15 * mm,
                                          name="KShortMuPiCombiner"):
    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.MAXDOCACUT(comb_maxdoca),
    )
    vertex_code = F.require_all(
        F.END_VRHO < 100 * mm,
        F.END_VZ > 30 * mm,
    )
    return ParticleCombiner(
        [pions, muons],
        name=name,
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


from Hlt2Conf.algorithms_thor import ParticleContainersMerger


def make_kshort_pi_muplus(pions, muons, **kwargs):
    ks_particle_combinations = [
        make_kshort_pi_muplus_specific_charge(
            pions,
            muons,
            "KS0 -> pi+ mu+",
            name="KShortMuPiCombiner_0",
            **kwargs),
        make_kshort_pi_muplus_specific_charge(
            pions,
            muons,
            "KS0 -> pi+ mu-",
            name="KShortMuPiCombiner_1",
            **kwargs),
        make_kshort_pi_muplus_specific_charge(
            pions,
            muons,
            "KS0 -> pi- mu-",
            name="KShortMuPiCombiner_2",
            **kwargs),
        make_kshort_pi_muplus_specific_charge(
            pions,
            muons,
            "KS0 -> pi- mu+",
            name="KShortMuPiCombiner_3",
            **kwargs),
    ]

    return ParticleContainersMerger(
        ks_particle_combinations, name="KS_combinations")


from PyConf.Algorithms import KSLongVeloFilter


@register_line_builder(all_lines)
@configurable
def kshort_velo_long_line(name="Hlt2KshortVeloLong", prescale=0.01):
    pvs = make_pvs()
    pions = filter_pions(make_long_pions(), pvs, pt_min=0.5 * GeV)
    muonsVelo = filter_particles_velo(make_velo_muons(), pvs)
    kshorts = make_kshort_pi_muplus(
        pions, muonsVelo, comb_m_max=1500,
        comb_m_min=0)  # probe should be on index 1

    filtered_kshorts = KSLongVeloFilter(
        InputParticle=kshorts,
        InputPVs=make_pvs_v1(),
        PVConstrainedMassMin=350.,
        PVConstrainedMassMax=625.,
        PVConstrainedProbePtMax=1000.,
        IPperpendicularMax=0.007,
        IPMax=0.8,
        name="TrackEffFilter",
    ).OutputParticles

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [require_pvs(make_pvs()), filtered_kshorts],
        prescale=prescale,
        monitoring_variables=["m"],
        persistreco=True)


@register_line_builder(all_lines)
@configurable
def kshort_velo_long_line_15(name="Hlt2KshortVeloLong_15", prescale=0.04):
    pvs = make_pvs()
    pions = filter_pions(make_long_pions(), pvs, pt_min=0.5 * GeV)
    muonsVelo = filter_particles_velo(make_velo_muons(), pvs)
    kshorts = make_kshort_pi_muplus(
        pions, muonsVelo, comb_m_max=1500,
        comb_m_min=0)  # probe should be on index 1

    filtered_kshorts = KSLongVeloFilter(
        InputParticle=kshorts,
        InputPVs=make_pvs_v1(),
        PVConstrainedMassMin=350.,
        PVConstrainedMassMax=625.,
        PVConstrainedProbePtMin=1000.,
        PVConstrainedProbePtMax=1500.,
        IPperpendicularMax=0.007,
        IPMax=0.8,
        name="TrackEffFilter_1",
    ).OutputParticles

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [require_pvs(make_pvs()), filtered_kshorts],
        prescale=prescale,
        monitoring_variables=["m"],
        persistreco=True)


@register_line_builder(all_lines)
@configurable
def kshort_velo_long_line_20(name="Hlt2KshortVeloLong_20", prescale=0.05):
    pvs = make_pvs()
    pions = filter_pions(make_long_pions(), pvs, pt_min=0.5 * GeV)
    muonsVelo = filter_particles_velo(make_velo_muons(), pvs)
    kshorts = make_kshort_pi_muplus(
        pions, muonsVelo, comb_m_max=1500,
        comb_m_min=0)  # probe should be on index 1

    filtered_kshorts = KSLongVeloFilter(
        InputParticle=kshorts,
        InputPVs=make_pvs_v1(),
        PVConstrainedMassMin=350.,
        PVConstrainedMassMax=625.,
        PVConstrainedProbePtMin=1500.,
        PVConstrainedProbePtMax=2000.,
        IPperpendicularMax=0.007,
        IPMax=0.8,
        name="TrackEffFilter_2",
    ).OutputParticles

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [require_pvs(make_pvs()), filtered_kshorts],
        prescale=prescale,
        monitoring_variables=["m"],
        persistreco=True)


@register_line_builder(all_lines)
@configurable
def kshort_velo_long_line_high(name="Hlt2KshortVeloLong_high", prescale=0.04):
    pvs = make_pvs()
    pions = filter_pions(make_long_pions(), pvs, pt_min=0.5 * GeV)
    muonsVelo = filter_particles_velo(make_velo_muons(), pvs)
    kshorts = make_kshort_pi_muplus(
        pions, muonsVelo, comb_m_max=1500,
        comb_m_min=0)  # probe should be on index 1

    filtered_kshorts = KSLongVeloFilter(
        InputParticle=kshorts,
        InputPVs=make_pvs_v1(),
        PVConstrainedMassMin=350.,
        PVConstrainedMassMax=625.,
        PVConstrainedProbePtMin=2000.,
        IPperpendicularMax=0.007,
        IPMax=0.8,
        name="TrackEffFilter_3",
    ).OutputParticles

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [require_pvs(make_pvs()), filtered_kshorts],
        prescale=prescale,
        monitoring_variables=["m"],
        persistreco=True)
