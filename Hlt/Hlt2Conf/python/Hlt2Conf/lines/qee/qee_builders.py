###############################################################################
# Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration          #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of useful QEE filters and builders
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV

from PyConf import configurable

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.standard_particles import make_ismuon_long_muon, make_long_muons
from Hlt2Conf.standard_particles import make_long_electrons_with_brem, make_down_electrons_no_brem
from Hlt2Conf.probe_muons import make_velomuon_muons, make_downstream_muons, make_muonut_muons
from Hlt2Conf.standard_jets import make_pf_particles, build_jets, tag_jets
from Hlt2Conf.lines.jets.topobits import make_topo_2body
from Hlt2Conf.standard_particles import make_photons
from Hlt2Conf.lines.qee.high_mass_dimuon import make_dimuon_novxt
from Hlt2Conf.lines.qee.high_mass_dielec import make_dielec_novxt
from Hlt2Conf.lines.qee.high_mass_dimuon_tracking_efficiency import VeloMuon_cuts, Downstream_cuts, UTMuon_cuts


@configurable
def muon_filter(min_pt=0. * GeV, require_muID=True):
    #A muon filter: PT

    code = (F.PT > min_pt)
    particles = make_ismuon_long_muon() if require_muID else make_long_muons()

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def elec_filter(min_pt=0. * GeV, min_electron_id=-1):
    #An electron filter: PT
    code = F.require_all(F.PT > min_pt, F.PID_E > min_electron_id)
    particles = make_long_electrons_with_brem()

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def elec_filter_down(min_pt=0. * GeV, min_electron_id=-1):
    #An down type electron filter: PT

    code = F.require_all(F.PT > min_pt, F.PID_E > min_electron_id)
    particles = make_down_electrons_no_brem()

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def make_jets(name='SimpleJets_{hash}',
              min_pt=10 * GeV,
              JetsByVtx=False,
              tags=None):
    #Build and tag jets

    pflow = make_pf_particles()
    svtags = make_topo_2body()
    jets = build_jets(pflow, JetsByVtx, name='JetBuilder' + name)

    if tags is not None:
        taggedjets = tag_jets(jets, svtags, name="Tags" + name)
        jets = taggedjets

    code = (F.PT > min_pt)

    return ParticleFilter(jets, F.FILTER(code))


@configurable
def make_qee_photons(name="qee_photons",
                     make_particles=make_photons,
                     pt_min=5. * GeV):
    code = (F.PT > pt_min)
    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_qee_gamma_DD(name="qee_gamma_DD",
                      descriptor="gamma -> e+ e-",
                      am_max=0.5 * GeV,
                      m_max=0.1 * GeV,
                      pt_min=5. * GeV,
                      min_elec_pt=0. * GeV,
                      min_elec_id=0.,
                      maxVertexChi2=16):

    electrons = elec_filter_down(
        min_pt=min_elec_pt, min_electron_id=min_elec_id)
    combination_code = (F.MASS < am_max)
    vertex_code = F.require_all(F.PT > pt_min, F.MASS < am_max,
                                F.CHI2 < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=[electrons, electrons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_qee_gamma_LL(name="qee_gamma_LL",
                      descriptor="gamma -> e+ e-",
                      am_max=0.5 * GeV,
                      m_max=0.1 * GeV,
                      pt_min=5. * GeV,
                      min_elec_pt=0. * GeV,
                      min_elec_id=0.,
                      maxVertexChi2=16):

    electrons = elec_filter(min_pt=min_elec_pt, min_electron_id=min_elec_id)
    combination_code = (F.MASS < am_max)
    vertex_code = F.require_all(F.PT > pt_min, F.MASS < am_max,
                                F.CHI2 < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=[electrons, electrons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_photons(photon_type="DD", pt_min=10. * GeV):

    if photon_type == "LL":
        gamma = make_qee_gamma_LL()
    elif photon_type == "DD":
        gamma = make_qee_gamma_DD()
    else:
        gamma = make_qee_photons()

    code = F.require_all(F.PT > pt_min)

    return ParticleFilter(gamma, F.FILTER(code))


@configurable
def lepton_filter(lepton_type="mu", min_pt=10. * GeV):

    if lepton_type == "mu":
        lepts = muon_filter(min_pt=min_pt)
    else:
        lepts = elec_filter(min_pt=min_pt)

    return lepts


@configurable
def Z_To_lepts_filter(lepton_type="mu", min_pt=10. * GeV):

    if lepton_type == "mu":
        muons_for_Z = muon_filter(min_pt=min_pt)
        zcand = make_dimuon_novxt(
            input_muon1=muons_for_Z,
            input_muon2=muons_for_Z,
            decaydescriptor="Z0 -> mu+ mu-",
            min_mass=40 * GeV)
    else:
        electrons_for_Z = elec_filter(min_pt=min_pt)
        zcand = make_dielec_novxt(
            input_elecs=electrons_for_Z,
            decaydescriptor="Z0 -> e+ e-",
            min_mass=40. * GeV)

    return zcand


def make_Z_trkeff_cand(trkeff_method=""):
    """
    Builds Z-like Tag&Probe Dimuon candidates.
    Use: Hlt2_ZTrkEff cuts to generate candidates for Spruce_ZTrkEff.
    Requires `cc` in decay descriptor to ensure probe='mu+' and probe='mu-' samples are both reconstructed.
    """
    decay_descriptor = "[Z0 -> mu- mu+]cc"
    name_prefix = "SpruceQEE_TrackEff_ZToMuMu"
    name = f"{name_prefix}_{trkeff_method}_Builder"

    probe, cuts = {
        "Downstream": (make_downstream_muons(), Downstream_cuts),
        "UTMuon": (make_muonut_muons(), UTMuon_cuts),
        "VeloMuon": (make_velomuon_muons(), VeloMuon_cuts)
    }[trkeff_method]

    filtered_probe = ParticleFilter(
        probe,
        F.FILTER(F.PT > cuts['min_ProbePT']),
        name=f"{name_prefix}_{trkeff_method}_ProbeFilter")
    filtered_tag = muon_filter(min_pt=cuts['min_TagPT'], require_muID=True)
    masscomb_cut = F.math.in_range(cuts['min_Z0mass'], F.MASS,
                                   cuts['max_Z0mass'])

    return ParticleCombiner(
        name=name,
        Inputs=[filtered_tag, filtered_probe],
        CombinationCut=masscomb_cut,
        CompositeCut=masscomb_cut,
        DecayDescriptor=decay_descriptor)
