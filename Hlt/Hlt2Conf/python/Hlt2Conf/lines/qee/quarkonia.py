###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the HLT2 Jpsi(1S)/Upsilon(1S)->MuMu lines.
All lines have ismuon.
"""
import Functors as F
from GaudiKernel.SystemOfUnits import MeV, GeV

from PyConf import configurable

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from RecoConf.event_filters import require_pvs

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.standard_particles import make_ismuon_long_muon

_JPSI_PDG_MASS_ = 3096.9 * MeV
_UPSILON_PDG_MASS_ = 9460.3 * MeV

_JPSI_WINDOW_ = 100. * MeV
_UPSILON_WINDOW_ = 400. * MeV

MIN_PT_MUON = 500 * MeV
MAX_TRCHI2NDOF_MUON = 5.
MAX_VCHI2NDOF = 10.

all_lines = {}


@configurable
def make_muons(
        name='qee_quarkonia_muons_{hash}',
        min_pT=0. * GeV,
        max_trchi2dof=None,
):
    muons = make_ismuon_long_muon()
    code = F.require_all(F.PT > min_pT, F.ISMUON, F.CHI2DOF < max_trchi2dof)

    return ParticleFilter(muons, name=name, Cut=F.FILTER(code))


def make_dimuon(pvs,
                name='qee_quarkonia_dimuon_{hash}',
                DecayDescriptor='J/psi(1S) -> mu+ mu-',
                min_pt_muon=0. * GeV,
                max_trchi2dof_muon=None,
                min_pt_dimuon=0. * GeV,
                min_mass_dimuon=0. * GeV,
                max_mass_dimuon=11. * GeV,
                max_vchi2ndof=25.,
                min_bpvdls=None,
                max_bpvdls=None):

    muons = make_muons(
        min_pT=min_pt_muon,
        max_trchi2dof=max_trchi2dof_muon,
    )

    combination_code = F.math.in_range(min_mass_dimuon, F.MASS,
                                       max_mass_dimuon)

    pvs = make_pvs()
    composite_code = F.require_all(F.PT > min_pt_dimuon,
                                   F.CHI2DOF < max_vchi2ndof)

    if min_bpvdls: composite_code &= F.BPVDLS(pvs) > min_bpvdls
    if max_bpvdls: composite_code &= F.BPVDLS(pvs) < max_bpvdls

    return ParticleCombiner(
        name=name,
        Inputs=[muons, muons],
        DecayDescriptor=DecayDescriptor,
        CombinationCut=F.require_all(combination_code),
        CompositeCut=F.require_all(composite_code))


@configurable
def make_jpsi(pvs, min_bpvdls=None, max_bpvdls=None):

    return make_dimuon(
        pvs=pvs,
        name='qee_jpsi_{hash}',
        DecayDescriptor='J/psi(1S) -> mu+ mu-',
        min_pt_muon=MIN_PT_MUON,
        max_trchi2dof_muon=MAX_TRCHI2NDOF_MUON,
        min_pt_dimuon=2000. * MeV,
        min_mass_dimuon=_JPSI_PDG_MASS_ - _JPSI_WINDOW_,
        max_mass_dimuon=_JPSI_PDG_MASS_ + _JPSI_WINDOW_,
        max_vchi2ndof=MAX_VCHI2NDOF,
        min_bpvdls=min_bpvdls,
        max_bpvdls=max_bpvdls)


@configurable
def make_upsilon(pvs):

    return make_dimuon(
        pvs=pvs,
        name='qee_upsilon_{hash}',
        DecayDescriptor='Upsilon(1S) -> mu+ mu-',
        min_pt_muon=MIN_PT_MUON,
        max_trchi2dof_muon=MAX_TRCHI2NDOF_MUON,
        min_pt_dimuon=0. * MeV,
        min_mass_dimuon=_UPSILON_PDG_MASS_ - _UPSILON_WINDOW_,
        max_mass_dimuon=_UPSILON_PDG_MASS_ + _UPSILON_WINDOW_,
        max_vchi2ndof=MAX_VCHI2NDOF,
        min_bpvdls=None,
        max_bpvdls=None)


@register_line_builder(all_lines)
@configurable
def jpsi_to_mu_mu_detached_line(name='Hlt2QEE_JpsiToMuMu_Detached',
                                prescale=1,
                                persistreco=False):
    """Jpsi (Detached) decay to two muons line"""
    pvs = make_pvs()
    jpsi = make_jpsi(pvs=pvs, min_bpvdls=3.)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), jpsi],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("m", "pt", "eta", "n_candidates"),
    )


@register_line_builder(all_lines)
@configurable
def jpsi_to_mu_mu_prompt_line(name='Hlt2QEE_JpsiToMuMu_Prompt',
                              prescale=0.1,
                              persistreco=False):
    """Jpsi (Prompt) decay to two muons line"""
    pvs = make_pvs()
    jpsi = make_jpsi(pvs=pvs, max_bpvdls=3.)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), jpsi],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("m", "pt", "eta", "n_candidates"),
    )


@register_line_builder(all_lines)
@configurable
def upsilon_to_mu_mu_line(name='Hlt2QEE_Upsilon1SToMuMu',
                          prescale=1,
                          persistreco=False):
    """Upsilon(1S) decay to two muons line"""
    pvs = make_pvs()
    u1s = make_upsilon(pvs=pvs)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), u1s],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("m", "pt", "eta", "n_candidates"),
    )
