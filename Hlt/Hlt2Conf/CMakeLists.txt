###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Hlt/Hlt2Conf
------------
#]=======================================================================]

gaudi_install(PYTHON)

gaudi_add_tests(pytest ${CMAKE_CURRENT_SOURCE_DIR}/python)
gaudi_add_tests(QMTest)

if(BINARY_TAG MATCHES "san$")
    # When running in sanitizer platforms, the tests take longer and
    # the test job gets killed at the 10h mark. Here we disable some
    # test to make the runtime more reasonable.

    # Disable some tests that take particularly long time
    set_property(
        TEST
            Hlt2Conf.sprucing.test_spruce_all_lines_realtime
            Hlt2Conf.sprucing.test_spruce_hlt2filter
            Hlt2Conf.sprucing.test_spruce_passthrough
            Hlt2Conf.sprucing.test_spruce_passthrough_check
            Hlt2Conf.streaming.test_hlt2_all_lines_with_reco_with_streams
            Hlt2Conf.streaming.test_hlt2_streaming
            Hlt2Conf.streaming.test_hlt2_streaming_check_A
            Hlt2Conf.streaming.test_hlt2_streaming_check_B
            Hlt2Conf.streaming.test_hlt2_streaming_check_Lumi
            Hlt2Conf.streaming.test_sprucing_streaming
            Hlt2Conf.streaming.test_sprucing_streaming_check_A
            Hlt2Conf.streaming.test_sprucing_streaming_check_B
            Hlt2Conf.streaming.test_sprucing_hlt2_check_output_with_reco_with_streams_mdf
            Hlt2Conf.test_hlt2_2or3bodytopo_realtime
            Hlt2Conf.test_persistreco_realtime
            Hlt2Conf.test_persistreco_output_realtime
            Hlt2Conf.test_turbo_hlt2_check_output_with_reco_with_streams_mdf
        PROPERTY DISABLED TRUE
    )
endif()

if(BUILD_TESTING AND USE_DD4HEP)
    # Disable some tests that are not yet dd4hep ready
    set_property(
        TEST
            # These need muon geometry v2 or depend on tests needing it
            Hlt2Conf.streaming.test_hlt2_streaming
            Hlt2Conf.streaming.test_hlt2_streaming_check_A
            Hlt2Conf.streaming.test_hlt2_streaming_check_B
            Hlt2Conf.streaming.test_hlt2_streaming_check_Lumi
            Hlt2Conf.streaming.test_sprucing_streaming
            Hlt2Conf.streaming.test_sprucing_streaming_check_A
            Hlt2Conf.streaming.test_sprucing_streaming_check_B
            Hlt2Conf.test_hlt2_2or3bodytopo_realtime
            Hlt2Conf.test_hlt2_2or3bodytopo_realtime_dst
            Hlt2Conf.test_hlt2_all_lines
            Hlt2Conf.test_hlt2_bandwidth_5streams
            Hlt2Conf.test_hlt2_flavourtagging_flavourtaggers
            Hlt2Conf.test_persistreco_check_flavourtags
            Hlt2Conf.test_hlt2_flavourtagging_sskaontagger
            Hlt2Conf.test_hlt2_line_example_with_extras
            Hlt2Conf.test_hlt2_noUT_trackefflines
            Hlt2Conf.test_hlt2_passthrough_persistreco
            Hlt2Conf.test_hlt2_passthrough_persistreco_output
            Hlt2Conf.test_hlt2_trackefflines
            Hlt2Conf.test_hlt2_with_hlt1_decisions
            Hlt2Conf.test_persistreco_realtime
            Hlt2Conf.test_persistreco_output_realtime
            Hlt2Conf.sprucing.test_spruce_example_realtime
            Hlt2Conf.sprucing.test_spruce_example_realtime_check
            Hlt2Conf.sprucing.test_spruce_example_realtime_dstinput
            Hlt2Conf.sprucing.test_spruce_example_realtime_dstinput_check
            Hlt2Conf.sprucing.test_spruce_example_realtime_dstinput_gaudirun
            Hlt2Conf.sprucing.test_spruce_example_realtime_dstinput_gaudirun_check
            Hlt2Conf.sprucing.test_spruce_example_realtime_extraoutputs
            Hlt2Conf.sprucing.test_spruce_example_realtime_extraoutputs_check
            Hlt2Conf.sprucing.test_spruce_alllines_realtime_gaudirun
            Hlt2Conf.sprucing.test_spruce_example_realtime_monitoring
            Hlt2Conf.sprucing.test_spruce_example_realtime_persistreco
            Hlt2Conf.sprucing.test_spruce_example_realtime_persistreco_check
            Hlt2Conf.sprucing.test_spruce_hlt2filter
            Hlt2Conf.sprucing.test_spruce_passthrough
            Hlt2Conf.sprucing.test_spruce_passthrough_check
            Hlt2Conf.sprucing.test_spruce_passthrough_dstinput
            Hlt2Conf.sprucing.test_spruce_passthrough_dstinput_check
            Hlt2Conf.sprucing.test_spruce_passthrough_dstinput_gaudirun
            Hlt2Conf.sprucing.test_spruce_passthrough_dstinput_gaudirun_check
            Hlt2Conf.sprucing.test_spruce_passthrough_gaudirun
            Hlt2Conf.sprucing.test_spruce_passthrough_gaudirun_check
            Hlt2Conf.test_hlt2_check_output
            Hlt2Conf.test_persistreco_fromfile
            Hlt2Conf.test_persistreco_output_fromfile
            # needs sim-20180530-vc-md100 as condition version
            Hlt2Conf.test_hlt2_reco_plus_thor_selections
            Hlt2Conf.test_hlt2_reco_plus_thor_selections_fastest
            Hlt2Conf.test_hlt2_reco_plus_thor_selections_legacy
            Hlt2Conf.test_hlt2_all_lines_with_reco
            Hlt2Conf.test_hlt2_check_output_with_reco
            Hlt2Conf.monitoring.test_hlt2_default_monitoring
            Hlt2Conf.streaming.test_hlt2_all_lines_with_reco_with_streams
            Hlt2Conf.streaming.test_hlt2_all_lines_with_reco_with_streams_mdf
            Hlt2Conf.test_turbo_hlt2_check_output_with_reco_with_streams_mdf
            Hlt2Conf.streaming.test_sprucing_hlt2_check_output_with_reco_with_streams_mdf
            Hlt2Conf.hlt2_pp_thor
            # These need dddb-20171126 tag, non existing in DD4hep
            Hlt2Conf.sprucing.test_spruce_all_lines_realtime
            Hlt2Conf.test_hlt2_standard_particles
            # needs sim-20171127-vc-md100 as condition version
            Hlt2Conf.test_hlt2_example
        PROPERTY
            DISABLED TRUE
    )
endif()

if(BUILD_TESTING AND NOT USE_DD4HEP)
    # Disable tests that should not run on detdesc platforms
    set_property(
        TEST
            Hlt2Conf.hlt2_SMOG2_thor_data_2022_HLT2rerun
            Hlt2Conf.hlt2_SMOG2_thor_data_2022_HLT2rerun_check_output
            Hlt2Conf.hlt2_test_duplicate_filters
            Hlt2Conf.sprucing.test_excl_spruce_2022_data
            Hlt2Conf.sprucing.test_pass_spruce_2022_data
            Hlt2Conf.sprucing.test_turcal_spruce_2022_data
            Hlt2Conf.sprucing.test_turcal_spruce_2022_data_pid_check
            Hlt2Conf.sprucing.test_turcal_spruce_2022_data_pid_rb_check
            Hlt2Conf.sprucing.test_turcal_spruce_2022_data_trackeff_check
            Hlt2Conf.sprucing.test_turcal_spruce_2022_data_trackeff_rb_check
            Hlt2Conf.sprucing.test_turcal_spruce_2022_data_monitoring_check
            Hlt2Conf.sprucing.test_turcal_spruce_2022_data_monitoring_rb_check
            Hlt2Conf.sprucing.test_excl_spruce_2022_data_b2cc_check
            Hlt2Conf.sprucing.test_excl_spruce_2022_data_b2oc_check
            Hlt2Conf.sprucing.test_excl_spruce_2022_data_bandq_check
            Hlt2Conf.sprucing.test_excl_spruce_2022_data_qee_check
            Hlt2Conf.sprucing.test_excl_spruce_2022_data_rd_check
            Hlt2Conf.sprucing.test_excl_spruce_2022_data_sl_check
            Hlt2Conf.sprucing.test_pass_spruce_2022_data_b2cc_check
            Hlt2Conf.sprucing.test_pass_spruce_2022_data_b2oc_check
            Hlt2Conf.sprucing.test_pass_spruce_2022_data_bandq_check
            Hlt2Conf.sprucing.test_pass_spruce_2022_data_bnoc_check
            Hlt2Conf.sprucing.test_pass_spruce_2022_data_charm_check
            Hlt2Conf.sprucing.test_pass_spruce_2022_data_qee_check
            Hlt2Conf.sprucing.test_pass_spruce_2022_data_rd_check
            Hlt2Conf.sprucing.test_pass_spruce_2022_data_sl_check
            Hlt2Conf.sprucing.test_excl_spruce_2023_1_data
            Hlt2Conf.sprucing.test_excl_spruce_2023_2_data
        PROPERTY
            DISABLED TRUE
    )
endif()

