###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_moore
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt1_muonid import make_muon_hits
from RecoConf.hlt1_tracking import (make_reco_pvs, make_PatPV3DFuture_pvs)
from RecoConf.hlt2_tracking import make_TrackBestTrackCreator_tracks, get_UpgradeGhostId_tool_no_UT
from RecoConf.hlt2_global_reco import make_fastest_reconstruction
from RecoConf.hlt2_global_reco import reconstruction as reco
from RecoConf.event_filters import require_gec
from Hlt2Conf.lines.iftPbPb import all_lines as ift_lines
from PyConf.Algorithms import PrHybridSeeding
from PyConf.packing import persistreco_writing_version

from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

dd4hepSvc = DD4hepSvc()
dd4hepSvc.DetectorList = [
    '/world', 'VP', 'FT', 'Magnet', 'Rich1', 'Rich2', 'Ecal', 'Hcal', 'Muon'
]  #no UT
dd4hepSvc.ConditionsLocation = 'git:/cvmfs/lhcb.cern.ch/lib/lhcb/git-conddb/lhcb-conditions-database.git'

options.output_file = 'hlt2_ion_data.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "hlt2_ion_data.tck.json"
options.histo_file = "hlt2_ion_data.root"
public_tools = []


def make_lines():
    lines = []
    for builder in ift_lines.values():
        has_gec = False
        node = builder().node
        for child in node.children:
            for grand_child in child.children:
                if "PrGECFilter" in grand_child.name:
                    has_gec = True
        if has_gec:
            lines.append(builder)
        else:
            print("WARNING: line %s has not GEC and will not be processed" %
                  node.name)

    return [builder() for builder in lines]

with reconstruction.bind(from_file=False),\
    require_gec.bind(cut=30000, skipUT=True), \
    make_fastest_reconstruction.bind(skipUT=True),\
    PrHybridSeeding.bind(RemoveBeamHole=True, RemoveClones_forLead=True),\
    reco.bind(make_reconstruction=make_fastest_reconstruction),\
    make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
    make_TrackBestTrackCreator_tracks.bind(max_ghost_prob=0.8),\
    get_UpgradeGhostId_tool_no_UT.bind(for_PbPb=True),\
    make_muon_hits.bind(geometry_version=3),\
    persistreco_writing_version.bind(version=1.1):
    config = run_moore(options, make_lines, public_tools)
