###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Example option on how to test on any specific lines in B2OC.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_b2oc_example.py
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
# TODO stateProvider_with_simplified_geom must go away from option files

###############################################################################
# B2OC: configure which lines to test
###############################################################################

# Run on ALL B2OC lines:
#from Hlt2Conf.lines.b_to_open_charm import all_lines

# Run on any specific line:
#from Hlt2Conf.lines.b_to_open_charm.hlt2_b2oc import BdToDsmK_DsmToHHH_Test_hlt2_line

# Book lines with generic functions
all_lines = {}
from Hlt2Conf.lines.b_to_open_charm.hlt2_b2oc import make_default_hlt2_lines
default_lines = [
    'BdToDsmK_DsmToHHH',
]
make_default_hlt2_lines(line_dict=all_lines, default_lines=default_lines)

###############################################################################
# configure input data set
###############################################################################

# TIP: to go from a BK path to a root-URL
# 1) lb-run LHCbDirac dirac-bookkeeping-get-files -B <BK path> # to get list of LFN's
# 2) lb-run LHCbDirac dirac-dms-lfn-accessURL --Protocol="xroot" -l <an LFN> # to get root-URL
# List of available upgrade MC is on
# https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/B2OCUpgrade#Upgrade_MC_event_types_available
# Some locations are listed in
# https://gitlab.cern.ch/lhcb-b2oc/b2ocupgrade/-/blob/master/Moore_input_files/
# Uncomment the following lines to configure input data from grid locations

#input_files = [
#    # MinBias 30000000 AFTER Hlt1
#    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000001_1.ldst',
#]
#options.input_files = input_files
#options.input_type = 'ROOT'
#options.simulation = True
#options.data_type = 'Upgrade'
#options.dddb_tag = 'dddb-20171126'
#options.conddb_tag = 'sim-20171127-vc-md100'

# Configure input data from testfileDB
# Comment the next line if configure input data above
options.set_input_and_conds_from_testfiledb('upgrade_minbias_hlt1_filtered')

# When running from Upgrade MC,
# must use the post-juggling locations of the raw event
options.input_raw_format = 4.3

# Set a reasonable number of events
options.evt_max = 100

# Write the output file
options.output_file = 'hlt2_b2oc_lines.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "hlt2_b2oc_example.tck.json"


def make_lines():
    lines = [builder() for builder in all_lines.values()]
    return lines


public_tools = [stateProvider_with_simplified_geom()]
config = run_moore(options, make_lines, public_tools)
