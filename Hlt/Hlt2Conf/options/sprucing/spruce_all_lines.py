###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Configuration file of a Sprucing throughput test on all Sprucing lines.
"""

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom, trackMasterExtrapolator_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

from PyConf.application import metainfo_repos

from Moore.streams_hlt2 import DETECTOR_RAW_BANK_TYPES, HLT1_REPORT_RAW_BANK_TYPES, HLT2_REPORT_RAW_BANK_TYPES
import Moore.streams_spruce

from Hlt2Conf.lines import sprucing_lines

import logging
log = logging.getLogger()

from Moore.persistence.hlt2_tistos import list_of_full_stream_lines
from Hlt2Conf.sprucing_settings.fixed_line_configs import lines_for_TISTOS_BW_Aug2023 as lines_for_TISTOS

metainfo_repos.global_bind(extra_central_tags=['key-ac5f2a28'])
options.input_raw_format = 0.3
options.input_process = 'Hlt2'
options.monitoring_file = "monitoring.json"
options.output_file = "spruce_all_lines.mdf"
options.output_type = "MDF"


def make_streams():
    "Makes each line its own stream"
    if 'SpruceB2OC_BdToDsmK_DsmToHHH_FEST' in sprucing_lines.keys():
        print("Removing SpruceB2OC_BdToDsmK_DsmToHHH_FEST")
        sprucing_lines.pop('SpruceB2OC_BdToDsmK_DsmToHHH_FEST')
    return {'AllStreams': [builder() for builder in sprucing_lines.values()]}


banks = DETECTOR_RAW_BANK_TYPES + HLT1_REPORT_RAW_BANK_TYPES + HLT2_REPORT_RAW_BANK_TYPES
banks = list(dict.fromkeys(banks).keys())

Moore.streams_spruce.stream_banks = {'AllStreams': banks}

public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom()
]

options.scheduler_legacy_mode = False

with reconstruction.bind(
        from_file=True,
        spruce=True), list_of_full_stream_lines.bind(lines=lines_for_TISTOS):
    config = run_moore(options, make_streams, public_tools, analytics=True)
