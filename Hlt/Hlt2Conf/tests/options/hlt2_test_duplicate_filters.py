###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
This is for a unit test relating to [Erroneous behaviour in lines that use same hlt_filters](https://gitlab.cern.ch/lhcb/Moore/-/issues/612)
This is a copy of the options used for the hlt2_pp_thor_data_2022.qmt test. I have changed the lines running only
"""

from Moore import options, run_moore
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_light_reco_pr_kf_without_UT
from Hlt2Conf.lines.charm.d0_to_hh import make_charm_kaons, make_dzeros
from Hlt2Conf.lines.charm.prefilters import charm_prefilters
from Moore.lines import Hlt2Line
from RecoConf.hlt2_tracking import (
    make_PrKalmanFilter_noUT_tracks, make_PrKalmanFilter_Seed_tracks,
    make_PrKalmanFilter_Velo_tracks, make_TrackBestTrackCreator_tracks,
    get_UpgradeGhostId_tool_no_UT)
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD, VPRetinaFullClusterDecoder
from RecoConf.hlt1_tracking import (
    make_VeloClusterTrackingSIMD, make_RetinaClusters,
    get_global_measurement_provider, make_velo_full_clusters)

from RecoConf.hlt1_muonid import make_muon_hits
from RecoConf.calorimeter_reconstruction import make_digits

options.histo_file = "histos_hlt2_pp_thor_data_2022.root"
options.output_file = "data_hlt2_pp_thor_data_2022.mdf"
options.output_type = "MDF"
options.event_store = 'EvtStoreSvc'
options.scheduler_legacy_mode = False
options.evt_max = 10000


##Define arbitrary line
def test1_line(name='Hlt2test_1_', prescale=1):
    kaons = make_charm_kaons()
    dzeros = make_dzeros(kaons, kaons, 'D0 -> K- K+')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dzeros],
        hlt1_filter_code=[
            "Hlt1TrackMVADecision", "Hlt1TwoTrackMVADecision",
            "Hlt1LowPtDiMuonDecision"
        ],
        prescale=prescale)


#Define the line again with a different name
def test2_line(name='Hlt2test_2_', prescale=1):
    kaons = make_charm_kaons()
    dzeros = make_dzeros(kaons, kaons, 'D0 -> K- K+')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dzeros],
        hlt1_filter_code=[
            "Hlt1TrackMVADecision", "Hlt1TwoTrackMVADecision",
            "Hlt1LowPtDiMuonDecision"
        ],
        prescale=prescale)


def make_lines():
    return [test1_line(), test2_line()]



with reconstruction.bind(from_file=False),\
     make_light_reco_pr_kf_without_UT.bind(skipRich=False, skipCalo=False, skipMuon=False),\
     make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=8.0),\
     make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=8.0),\
     make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=8.0),\
     make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=8.0),\
     make_VeloClusterTrackingSIMD.bind(algorithm=VeloRetinaClusterTrackingSIMD),\
     get_UpgradeGhostId_tool_no_UT.bind(velo_hits=make_RetinaClusters),\
     make_muon_hits.bind(geometry_version=3),\
     make_digits.bind(calo_raw_bank=True),\
     make_velo_full_clusters.bind(make_full_cluster=VPRetinaFullClusterDecoder),\
     get_global_measurement_provider.bind(velo_hits=make_RetinaClusters),\
     hlt2_reconstruction.bind(make_reconstruction=make_light_reco_pr_kf_without_UT):
    config = run_moore(options, make_lines, public_tools=[])
