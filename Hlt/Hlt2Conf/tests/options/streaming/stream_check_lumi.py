###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test if Hlt2LuminosityLine line streaming functionality for Hlt2 or Sprucing works as expected.

   For testing HLT2 streaming this test is  using the output of Hlt/Hlt2Conf/tests/qmtest/streaming.qms/test_hlt2_streaming.qmt
   For testing Sprucing streaming this test is using the output of Hlt/Hlt2Conf/tests/qmtest/streaming.qms/test_sprucing_streaming.qmt
"""
##Usage, inside Moore/

import GaudiPython as GP
from GaudiConf import IOExtension
from Configurables import LHCbApp, IODataManager, HistogramPersistencySvc
LHCb = GP.gbl.LHCb
import argparse

#Argument parser
parser = argparse.ArgumentParser()
parser.add_argument('input', help='Input filename')
parser.add_argument('manifest', help='JSON manifest dump')
parser.add_argument('input_process', help='Hlt2 or Spruce')
parser.add_argument('stream', help='Stream to test as defined in options')

args = parser.parse_args()


def error(msg):
    print("Check ERROR", msg)


# Change the tags if required
LHCbApp(
    DataType="Upgrade",
    Simulation=True,
    #DDDBtag="master",
    #CondDBtag="2022_12_HLT2Processing",
    DDDBtag="dddb-20171126",
    CondDBtag="sim-20171127-vc-md100",
)

IOExtension().inputFiles([args.input], clear=True)
# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)
# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)

#Raw event rawevent location
if args.input_process == "Hlt2":
    raweventloc = '/Event/DAQ/RawEvent'
else:
    raweventloc = f'/Event/{args.stream}/RawEvent'

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)
for i in range(0, 1):
    #loop over 1 event

    saved_banks = {15, 16, 53}  # DAQ == 15?! ; ODIN == 16 ; RoutingBits == 53
    for i in range(0, 99):
        if i in saved_banks:
            if TES[raweventloc].banks(i).size() == 0:
                error(
                    f"Bank {GP.gbl.LHCb.RawBank.typeName(i)} is expected to be non-empty but has a size of {TES[raweventloc].banks(i).size()}"
                )
        else:
            if TES[raweventloc].banks(i).size() != 0:
                error(
                    f"Bank {GP.gbl.LHCb.RawBank.typeName(i)} is expected to be empty but has a size of {TES[raweventloc].banks(i).size()}"
                )

# MonkeyPatch for the fact that RegistryEntry.__bool__
# changed in newer cppyy. Proper fix should go into Gaudi
import cppyy
cppyy.gbl.DataSvcHelpers.RegistryEntry.__bool__ = lambda x: True

TES.dump()

appMgr.run(1)
