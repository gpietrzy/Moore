###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Imports for current HLT2 config running in the pit
from RecoConf.hlt2_tracking import (
    make_PrKalmanFilter_noUT_tracks, make_PrKalmanFilter_Seed_tracks,
    make_PrKalmanFilter_Velo_tracks, make_TrackBestTrackCreator_tracks,
    get_UpgradeGhostId_tool_no_UT)
from RecoConf.hlt1_tracking import (
    make_VeloClusterTrackingSIMD, make_RetinaClusters,
    get_global_measurement_provider, make_reco_pvs, make_PatPV3DFuture_pvs)
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD
from RecoConf.hlt1_muonid import make_muon_hits
from RecoConf.calorimeter_reconstruction import make_digits

make_TrackBestTrackCreator_tracks.global_bind(max_chi2ndof=4.2)
make_PrKalmanFilter_Velo_tracks.global_bind(max_chi2ndof=4.2)
make_PrKalmanFilter_noUT_tracks.global_bind(max_chi2ndof=4.2)
make_PrKalmanFilter_Seed_tracks.global_bind(max_chi2ndof=4.2)
make_VeloClusterTrackingSIMD.global_bind(
    algorithm=VeloRetinaClusterTrackingSIMD, SkipForward=4)
get_UpgradeGhostId_tool_no_UT.global_bind(velo_hits=make_RetinaClusters)
make_muon_hits.global_bind(geometry_version=3)
make_reco_pvs.global_bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs)
get_global_measurement_provider.global_bind(velo_hits=make_RetinaClusters)
make_digits.global_bind(calo_raw_bank=True)
