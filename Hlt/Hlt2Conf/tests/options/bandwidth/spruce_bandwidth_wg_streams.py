###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Test option for the Sprucing bandwidth test in LHCbPR

The streaming configuration in this test is `wg-stream`,
which means one stream per WG.

To launch it in Moore, run with
    ./run gaudirun.py spruce_bandwidth_input.py spruce_bandwidth_wg_streams.py
'''

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
import Moore
import Moore.streams_spruce
from PyConf.application import metainfo_repos
import logging
import json
from PRConfig.bandwidth_helpers import FileNameHelper
log = logging.getLogger()

from Moore.persistence.hlt2_tistos import list_of_full_stream_lines
from Hlt2Conf.sprucing_settings.fixed_line_configs import lines_for_TISTOS_BW_Aug2023 as lines_for_TISTOS

from Hlt2Conf.lines import (
    b_to_open_charm,
    rd,
    bandq,
    qee,
    charm,
    b_to_charmonia,
    semileptonic,
    charmonium_to_dimuon,
    bnoc,
)

MODULES = {
    "b_to_open_charm": b_to_open_charm,
    'rd': rd,
    'bandq': bandq,
    'qee': qee,
    'charm': charm,
    'b_to_charmonia': b_to_charmonia,
    'slepton': semileptonic,
    'c_to_dimuon': charmonium_to_dimuon,
    'bnoc': bnoc
}

metainfo_repos.global_bind(extra_central_tags=['key-ac5f2a28'])
options.input_raw_format = 0.3
options.input_process = 'Hlt2'

fname_helper = FileNameHelper(process="spruce")
fname_helper.make_tmp_dirs()
options.output_file = fname_helper.mdf_fname_for_Moore(stream_config="wg")
options.output_type = 'MDF'
options.output_manifest_file = fname_helper.tck(stream_config="wg")


def make_module_lines(mod):

    builders = []
    try:
        lines = mod.sprucing_lines
        if 'SpruceB2OC_BdToDsmK_DsmToHHH_FEST' in lines.keys():
            print("Removing SpruceB2OC_BdToDsmK_DsmToHHH_FEST")
            lines.pop('SpruceB2OC_BdToDsmK_DsmToHHH_FEST')
        for builder in lines.values():
            builders.append(builder())
    except AttributeError:
        log.info(
            f'line module {mod.__name__} does not define `sprucing_lines`')

    return builders


def make_module_streams():

    linedict = {}
    for mod_name, module in MODULES.items():
        builders = make_module_lines(module)
        if builders:
            linedict[mod_name] = builders

    # Write out stream configuration to JSON file for use later in the test
    with open(fname_helper.stream_config_json_path(stream_config="wg"),
              'w') as f:
        json.dump({k: [line.name for line in v]
                   for k, v in linedict.items()}, f)

    return linedict


Moore.streams_spruce.stream_banks = {
    module_name: []
    for module_name in MODULES.keys()
}

# Only running thread-safe lines
public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(
        from_file=True,
        spruce=True), list_of_full_stream_lines.bind(lines=lines_for_TISTOS):
    config = run_moore(
        options, make_module_streams, public_tools, exclude_incompatible=True)
