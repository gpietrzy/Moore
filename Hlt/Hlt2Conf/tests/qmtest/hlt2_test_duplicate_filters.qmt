<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
This is for a unit test relating to
[Erroneous behaviour in lines that use same hlt_filters](https://gitlab.cern.ch/lhcb/Moore/-/issues/612)
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="timeout"><integer>1000</integer></argument>
<argument name="args"><set>
  <text>$MOOREROOT/tests/options/mdf_input_and_conds_data_2022.py</text>
  <text>$HLT2CONFROOT/tests/options/hlt2_test_duplicate_filters.py</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="error_reference"><text>../../../RecoConf/tests/refs/empty.ref</text></argument>
<argument name="validator"><text>
from GaudiConf.QMTest.LHCbTest import extract_counters
from Moore.qmtest.exclusions import preprocessor, remove_known_warnings
from PyConf.components import findCounters

countErrorLines({"FATAL": 0, "ERROR": 0, "WARNING": 0},
                stdout=remove_known_warnings(stdout))

# Extract the count for the two instances of Hlt filter
counters = extract_counters(stdout)
filtereff1 = findCounters(counters, "Hlt2test_1__Hlt1Filter")["Cut selection efficiency"]
filtereff2 = findCounters(counters, "Hlt2test_2__Hlt1Filter")["Cut selection efficiency"]

print("Counts for the first Hlt filter is: ", filtereff1)
print("Counts for the second Hlt filter is: ", filtereff2)

# The filters are identical so expect the counts to be the same
if not filtereff1==filtereff2:
    causes.append("The same instance of a Hlt filter is not returning the same result! See https://gitlab.cern.ch/lhcb/Moore/-/issues/612")

</text></argument>
</extension>
