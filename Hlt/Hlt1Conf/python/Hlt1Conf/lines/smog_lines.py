###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import mm, MeV
from PyConf import configurable
from RecoConf.hlt1_tracking import make_VeloClusterTrackingSIMD_tracks
from RecoConf.hlt1_muonid import make_fitted_tracks_with_muon_id
from ..algorithms import (Filter, CombineTracks)

from Moore.config import HltLine

from Functors import (CLOSESTTOBEAM, NHITS, P, PT, ISMUON, CHI2DOF, MAXSDOCA,
                      MASS, REFERENCEPOINT_Z, POSITION_Z, CHARGE)
from Functors.math import in_range


@configurable
def smog_minimumbias_line(
        name='Hlt1_Smog_MinimumBias',
        prescale=1,
        make_input_tracks=make_VeloClusterTrackingSIMD_tracks,
        max_Smog_z=-250 * mm,
        min_Smog_z=-550 * mm,
        min_nhits=17,
):
    selection = in_range(min_Smog_z, POSITION_Z @ CLOSESTTOBEAM,
                         max_Smog_z) & (NHITS > min_nhits)
    tracks = {'PrVeloTracks': make_input_tracks()['Pr']}
    track_filter = Filter(tracks, selection)['PrVeloTracks']

    return HltLine(name=name, algs=[track_filter], prescale=prescale)


@configurable
def smog_dimuon_line(
        name='Hlt1_Smog_OSMuonsHighMass',
        prescale=1,
        make_input_tracks=make_fitted_tracks_with_muon_id,
        min_pt=500 * MeV,
        min_p=3000 * MeV,
        max_tracks_chi2dof=3.0,
        max_doca=0.5 * mm,
        max_vtx_chi2dof=25,
        min_inv_mass=2700 * MeV,
        comb_charge_sum=0,
        min_Smog_z=-550 * mm,
        max_Smog_z=-250 * mm,
):

    child_sel = (ISMUON) & (PT > min_pt) & (P > min_p) & (CHI2DOF <
                                                          max_tracks_chi2dof)
    tracks_with_muon_id = make_input_tracks()
    children = Filter(tracks_with_muon_id, child_sel)

    comb_cut = (MAXSDOCA < max_doca) & (CHARGE == comb_charge_sum)
    moth_cut = (CHI2DOF < max_vtx_chi2dof) & (MASS > min_inv_mass) & in_range(
        min_Smog_z, REFERENCEPOINT_Z, max_Smog_z)

    combination_filter = CombineTracks(
        AssumedMass='mu+',
        NBodies=2,
        CombinationCut=comb_cut,
        VertexCut=moth_cut,
        InputTracks=children['PrFittedForwardWithMuonID'],
    )

    return HltLine(name=name, algs=[combination_filter], prescale=prescale)
