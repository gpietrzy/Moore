<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Run Spruce line with Hlt2 filter and check counters agree with counters of test_hlt1filtering
Check that a SpruceLine with Hlt1 and Hlt2 filter has the same count as SpruceLine with Hlt2 filter
where the Hlt2 line has a Hlt1 filter

This test could live in Hlt2Conf/ when Gaudi issue 202 is resolved
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="prerequisites"><set>
  <tuple><text>persistency.make_allen_tck</text><enumeral>PASS</enumeral></tuple>
  <tuple><text>filters.test_hlt1filtering</text><enumeral>PASS</enumeral></tuple>
</set></argument>
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="args"><set>
  <text>$HLT2CONFROOT/tests/options/hlt_filters_test_sprucepass.py</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>

from GaudiConf.QMTest.LHCbTest import extract_counters
from Moore.qmtest.exclusions import remove_known_warnings
countErrorLines({"FATAL": 0, "WARNING": 0, "ERROR": 0},
                stdout=remove_known_warnings(stdout))

import re
# Extract the decision count from this test's log file
counters = extract_counters(stdout)
_, nselected, _ = [v for c,v in counters.items() if c.endswith("_Hlt2Filter")][0]["Cut selection efficiency"]

# Extract the decision count from the HLT2 job log file
with open('test_hlt1filtering.stdout') as f_ref:
    nselected_ref=re.search('LAZY_AND: Hlt2Test1\s+#=\d+\s+Sum=(\d+)', f_ref.read()).group(1)

# Require a non-zero number of positive reference decisions, as otherwise the
# HLT2 filter may unconditionally reject everything but still appear correct
if nselected_ref == 0:
    causes.append("Reference line Hlt2Test1 selected zero events")

print("Decision count (got vs. expected): {} vs. {}".format(nselected, nselected_ref))

# Require the two decision counts to match
if nselected != nselected_ref:
    causes.append("Decision count mismatch (got vs. expected): {} vs. {}. Issue with Hlt2 filter".format(
        nselected,
        nselected_ref,
    ))

# Require the SpruceLine with Hlt1 and Hlt2 filter has the same count as SpruceLine with Hlt2 filter
# where the Hlt2 line has a Hlt1 filter
n_line1=re.search('LAZY_AND: SpruceTest1\s+#=\d+\s+Sum=(\d+)', stdout).group(1)
n_line2=re.search('LAZY_AND: SpruceTest2\s+#=\d+\s+Sum=(\d+)', stdout).group(1)

if n_line1 != n_line2:
    causes.append("Decision count mismatch (Line 1 vs. Line 2). Issue with Hlt filters.: {} vs. {}".format(
        n_line1,
        n_line2,
    ))

</text></argument>
</extension>
