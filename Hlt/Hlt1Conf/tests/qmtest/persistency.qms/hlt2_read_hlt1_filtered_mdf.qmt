<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Verify the an HLT2-like job can filter on DecReports written by Allen, and
that the number of selected events matches.
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="prerequisites"><set>
  <tuple><text>persistency.allen_mdf_write</text><enumeral>PASS</enumeral></tuple>
</set></argument>
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="args"><set>
  <text>$MOOREROOT/tests/options/tck_database_from_tests.py</text>
  <text>$HLT1CONFROOT/tests/options/hlt1_filtered_mdf_input.py</text>
  <text>$MOOREROOT/options/ft_decoding_v4.py</text>
  <text>$HLT2CONFROOT/tests/options/hlt2_with_hlt1_filtering.py</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>
from GaudiConf.QMTest.LHCbTest import extract_counters
from Moore.qmtest.exclusions import preprocessor
from PyConf.components import findCounters, findCountersForRe, unique_name_ext_re

# We expect one warning, "TCK obtained from rawbank seems to be 0...", as we're
# running on a sample wich was created without a TCK
stdout = preprocessor(stdout)
countErrorLines({"FATAL": 0, "ERROR": 0, "WARNING": 0},
                stdout=stdout)

# Extract the decision count from the reference test's log file
with open('test_hlt1_persistence_allen_mdf_write.stdout') as f_ref:
    counters_ref = extract_counters(f_ref.read())
    _, nselected_ref, _ = findCountersForRe(counters_ref["GaudiAllenCountAndDumpLineDecisi..."],
                                            "Selected by Hlt1LowPtMuonDecision")

# Extract the decision count from this test's log file
counters = extract_counters(stdout)
_, nselected, _ = findCounters(counters, ".*_Hlt1Filter")["Cut selection efficiency"]

# Require a non-zero number of positive reference decisions, as otherwise the
# HLT2 filter may unconditionally reject everything but still appear correct
if nselected_ref == 0:
    causes.append("Reference line Hlt1LowPtMuonDecision selected zero events")

# Require the two decision counts to match
if nselected != nselected_ref:
    causes.append("Decision count mismatch (test vs. ref.): {} vs. {}".format(
        nselected,
        nselected_ref,
    ))

</text></argument>
</extension>
