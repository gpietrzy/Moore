<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Run Allen HLT1 and save an MDF file.
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="args"><set>
  <text>$MOOREROOT/tests/options/digi_input_and_conds_Sim10aU1.py</text>
  <text>$HLT1CONFROOT/tests/options/allen_hlt1_mdf_output.py</text>
  <text>$HLT1CONFROOT/tests/options/allen_hlt1_pp_no_ut_with_dec_logger.py</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>


import re

# Check that:
#   1. We read at least two events
#   2. We make a positive decision with the TrackMVA line on at least one event
#   3. We make a negative decision with the TrackMVA line on at least one event
#   4. We make a positive decision with the TwoTrackMVA line on at least one event
#   5. We make a negative decision with the TwoTrackMVA line on at least one event

pattern = re.compile(r'\s+\|\*"Selected by Hlt1TrackMVADecision"\s+\|\s+(\d+)\s+\|\s+(\d+)')
nread = nselected = -1
for line in stdout.split('\n'):
    m = re.match(pattern, line)
    if m:
        nread, nselected = map(int, m.groups())
        break
else:
    causes.append('could not parse event statistics from stdout')

if nread &lt; 2:
    causes.append('expected at least two events to be processed')

if nselected &lt; 1:
    causes.append('expected at least one event to be selected by TrackMVA')

if nselected == nread:
    causes.append('expected at least one event to be filtered out by TrackMVA')

pattern = re.compile(r'\s+\|\*"Selected by Hlt1TwoTrackMVADecision"\s+\|\s+(\d+)\s+\|\s+(\d+)')
nread = nselected = -1
for line in stdout.split('\n'):
    m = re.match(pattern, line)
    if m:
        nread, nselected = map(int, m.groups())
        break
else:
    causes.append('could not parse event statistics from stdout')

if nread &lt; 2:
    causes.append('expected at least two events to be processed')

if nselected &lt; 1:
    causes.append('expected at least one event to be selected by TwoTrackMVA line')

if nselected == nread:
    causes.append('expected at least one event to be filtered out by TwoTrackMVA line')

# Check that the filtering works, i.e. that selected events are written out
pattern = re.compile(r'\s+\| "LHCb__MDFWriter_[^"]*"\s+\|\s+(\d+)\s+\|.*')
nwritten = -1
for line in stdout.split('\n'):
    m = re.match(pattern, line)
    if m:
        nwritten, = map(int, m.groups())
if nwritten &lt;= 0:
    causes.append('no events written out to MDF')

if nwritten == nread:
    causes.append('expected at least one event to be filtered out')

# Write out the log file so that we can compare the number of
# selected events here with the number of events processed by
# a second HLT1 job that uses the output file as input
with open('test_hlt1_persistence_allen_mdf_write.stdout', 'w') as f:
    f.write(stdout)

</text></argument>
</extension>
