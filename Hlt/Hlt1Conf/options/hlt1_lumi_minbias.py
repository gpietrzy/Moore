###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Run the luminosity and no-bias lines with an ODIN emulator.

The emulator deterministically samples Lumi and NoBias event type bits and
creates a new ODIN bank from them. This allows us to test the accept rate of
the technical lines on simulated data, for which the Lumi and NoBias ODIN bits
are never set.

The luminosity line has been removed
"""
from Moore import options, run_moore
from PyConf.application import default_raw_banks, make_odin
from PyConf.Algorithms import createODIN, ODINEmulator
from Hlt1Conf.settings import minimum_bias_lines

LUMI_FRACTION = 0.5
NOBIAS_FRACTION = 0.25


def make_emulated_odin(make_raw=default_raw_banks):
    """Return an emulated ODIN object, based on the original one."""
    # Decode the original LHCb::ODIN object from the raw bank
    odin = createODIN(RawBanks=make_raw(("ODIN"))).ODIN
    # Emulate a new LHCb::ODIN object based on the original
    emulated = ODINEmulator(
        InputODINLocation=odin,
        # Set our accept fractions for p-p crossings
        AcceptFractions={
            "Lumi": [0, 0, 0, LUMI_FRACTION],
            "NoBias": [0, 0, 0, NOBIAS_FRACTION],
        }).OutputODINLocation
    return emulated


def all_lines():
    return minimum_bias_lines()


# Run Moore, substituting the usual ODIN maker with our emulation
with make_odin.substitute(make_emulated_odin):
    run_moore(options, all_lines)
