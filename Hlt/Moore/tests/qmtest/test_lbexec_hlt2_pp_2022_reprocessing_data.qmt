<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Test that HLT2 can run from LHCbDirac and that it produces the expected outputs
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="prerequisites"><set>
  <tuple><text>test_prepare_lbexec_hlt</text><enumeral>PASS</enumeral></tuple>
</set></argument>
<argument name="program"><text>lbexec</text></argument>
<argument name="args"><set>
  <text>Moore.production:hlt2_pp_2022_reprocessing</text>
  <text>$HLT2CONFROOT/options/hlt2_pp_2022_reprocessing_data_production_options.yaml</text>
</set></argument>
<argument name="environment"><set>
  <text>THOR_DISABLE_JIT=1</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="timeout"><integer>2000</integer></argument>
<argument name="validator"><text>

countErrorLines({"FATAL": 0, "ERROR": 0})

import xml.etree.ElementTree as ET
from pathlib import Path
import re

# Ensure the summary XML is as expected
tree = ET.parse(Path.cwd() / "hlt2_pp_2022_reprocessing_data_summary.xml")
out_files = tree.findall("./output/file")
if len(out_files) != 4:
    causes.append(f"expected exactly 4 output files, got {out_files}")

expected_min_events = {"turcal": 5, "lumi": 5, "full": 6, "turboraw": 9}

for out_file in out_files:
    m = re.match("^PFN:(hlt2_pp_2022_reprocessing_data_output_([a-z]+).mdf)$", out_file.attrib["name"])
    if not m:
        causes.append("wrong output file name: " + out_file.attrib["name"])
    fn = m.group(1)
    stream = m.group(2)

    if not (int(out_file.text) &gt;= expected_min_events[stream]):
        causes.append(f"Expected more than {expected_min_events[stream]} events for {stream}, XML summary gives {out_file.text}")

    if not (int(out_file.text) &lt; 15):
        causes.append(f"Expected less than 15 events for {stream}, XML summary gives {out_file.text}")

    # Ensure the pool XML was updated to contain the new output file
    tree = ET.parse(Path.cwd() / "hlt2_data_pool_xml_catalog.xml")
    catalog_output = tree.findall(f'./File/physical/pfn[@name="{fn}"]')
    if len(catalog_output) != 1:
        causes.append(f"Expected exactly one output with the right name, got {catalog_output}")

</text></argument>
</extension>
