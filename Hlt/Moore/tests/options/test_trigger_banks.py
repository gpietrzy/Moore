###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Check the contents of DstData, DecReports and SelReports rawbanks

Check that the same sourceID does not appear twice.

"""
import argparse
from Configurables import (ApplicationMgr, Gaudi__Histograming__Sink__Root,
                           HistogramPersistencySvc, IODataManager, LHCbApp,
                           HltRoutingBitsFilter, GaudiSequencer)
from GaudiConf import IOHelper
from PyConf.application import configured_ann_svc

parser = argparse.ArgumentParser()
parser.add_argument("--type", help="Input type")
parser.add_argument("--input", help="Input file")
parser.add_argument("--events", default=10000, help="Number of events")

args = parser.parse_args()

# Configure basic application with inputs
LHCbApp(DataType="Upgrade", Simulation=True)
IOHelper(args.type).inputFiles([args.input])
# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)

# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)

# Decode Hlt DecReports
from Configurables import LHCb__UnpackRawEvent, HltDecReportsDecoder, createODIN

raw_odin = "DAQ/RawBanks/ODIN"
unpack_odin = LHCb__UnpackRawEvent(
    "UnpackOdin", RawBankLocations=[raw_odin], BankTypes=["ODIN"])

raw_decreports = "DAQ/RawBanks/HltDecReports"
unpack_decreports = LHCb__UnpackRawEvent(
    "UnpackDecReports",
    RawBankLocations=[raw_decreports],
    BankTypes=["HltDecReports"])

raw_dstdata = "DAQ/RawBanks/DstData"
unpack_dstdata = LHCb__UnpackRawEvent(
    "UnpackDstData", RawBankLocations=[raw_dstdata], BankTypes=["DstData"])

raw_selreports = "DAQ/RawBanks/HltSelReports"
unpack_selreports = LHCb__UnpackRawEvent(
    "UnpackSelReports",
    RawBankLocations=[raw_selreports],
    BankTypes=["HltSelReports"])

raw_dstdata = "DAQ/RawBanks/DstData"
unpack_dstdata = LHCb__UnpackRawEvent(
    "UnpackDstData", RawBankLocations=[raw_dstdata], BankTypes=["DstData"])

raw_routingbits = "DAQ/RawBanks/Hlt2RoutingBits"
unpack_routingbits = LHCb__UnpackRawEvent(
    "UnpackHltRoutingBits",
    RawBankLocations=[raw_routingbits],
    BankTypes=["HltRoutingBits"])

create_odin = createODIN("CreateOdin", RawBanks=raw_odin)

hlt1_decreports = HltDecReportsDecoder(
    name="Hlt1DecReportsDecoder",
    SourceID="Hlt1",
    OutputHltDecReportsLocation="/Event/Hlt1/DecReports",
    RawBanks=unpack_decreports.RawBankLocations[0])

hlt2_decreports = HltDecReportsDecoder(
    name="Hlt2DecReportsDecoder",
    SourceID="Hlt2",
    OutputHltDecReportsLocation="/Event/Hlt2/DecReports",
    RawBanks=unpack_decreports.RawBankLocations[0])

phys_filter = HltRoutingBitsFilter(
    name="PhysFilter",
    RawBanks=raw_routingbits,
    RequireMask=(0, 0, 1 << (95 - 64)))

app = ApplicationMgr(TopAlg=[
    unpack_odin,
    create_odin,
    unpack_routingbits,
    GaudiSequencer(
        "UnpackAfterPhysFilter",
        Members=[
            phys_filter,
            unpack_decreports,
            unpack_selreports,
            unpack_dstdata,
            hlt1_decreports,
            hlt2_decreports,
        ]),
])

# Configure ann svc
app.ExtSvc += [configured_ann_svc()]
Gaudi__Histograming__Sink__Root().FileName = "hlt1_hlt2_decreports.root"
app.ExtSvc += [Gaudi__Histograming__Sink__Root()]

import GaudiPython
LHCb = GaudiPython.gbl.LHCb

gaudi = GaudiPython.AppMgr()
TES = gaudi.evtSvc()

n = 0


def check_unique_sourceids(location):
    source_ids = []
    if TES[location].getData().size() == 0:
        error(f"{location} has 0 banks")
        return False

    for rb in TES[location].getData():
        source_ids.append(rb.sourceID())

    has_unique_sourceids = (0 == (len(source_ids) - len(set(source_ids))))
    return has_unique_sourceids


def routing_bits(rbbanks):
    """Return a list with the routing bits that are set."""
    (rbbank, ) = rbbanks  # stop if we have multiple rb banks
    d = rbbank.data()
    bits = "{:032b}{:032b}{:032b}".format(d[2], d[1], d[0])
    ordered = list(map(int, reversed(bits)))
    return [bit for bit, value in enumerate(ordered) if value]


def error(msg):
    print("CheckOutput ERROR", msg)


while n < int(args.events):
    gaudi.run(1)
    raw_event = TES["/Event/DAQ/RawEvent"]

    if not TES["/Event"]:
        break
    odin = TES["/Event/DAQ/ODIN"]
    if not odin:
        error("No ODIN")

    on_bits = routing_bits(raw_event.banks(LHCb.RawBank.HltRoutingBits))
    if 95 not in on_bits:  # this must be a lumi exclusive event
        if 94 not in on_bits:
            error("Bit 94 must be set when 95 is not set")
        # there are no other banks, so nothing more to check
        continue

    unique_decreports_ids = check_unique_sourceids(raw_decreports)
    if not unique_decreports_ids:
        error("Multiple DecReports banks with same SourceID")

    unique_dstdata_ids = check_unique_sourceids(raw_dstdata)
    if not unique_dstdata_ids:
        error("Multiple DstData banks with same SourceID")

    unique_selreports_ids = check_unique_sourceids(raw_selreports)
    if not unique_selreports_ids:
        error("Multiple SelReports banks with same SourceID")

    unique_odin = check_unique_sourceids(raw_odin)
    if not unique_odin:
        error("Multiple ODIN banks with same SourceID")

    if TES[raw_routingbits].getData().size() != 1:
        error("There should be exactly one routing bits bank")

    n = n + 1
