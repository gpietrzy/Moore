###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import Moore
from Configurables import LHCbApp
from Gaudi.Configuration import FileCatalog

from Moore.compat.tests.params import prod_conf_params


def dressInputFileNames(name):
    """Small helper to decorate the input file names."""
    if name.endswith(".raw"):
        return "DATAFILE='%s' SVC='LHCb::MDFSelector'" % name
    return "DATAFILE='%s' TYP='POOL_ROOTTREE' OPT='READ'" % name


### The common part defined in ProdConf.__apply_configuration__()

# common configuration
app = LHCbApp()

# Propagate options to LHCbApp, if set.
app.EvtMax = prod_conf_params["NOfEvents"]
app.DDDBtag = prod_conf_params["DDDBTag"]
app.CondDBtag = prod_conf_params["CondDBTag"]
app.DQFLAGStag = prod_conf_params["DQTag"]
app.TimeStamp = True
app.XMLSummary = prod_conf_params["XMLSummaryFile"]

FileCatalog(Catalogs=["xmlcatalog_file:" + prod_conf_params["XMLFileCatalog"]])

from Configurables import EventSelector
EventSelector().Input = [
    dressInputFileNames(f) for f in prod_conf_params["InputFiles"]
]

from Configurables import Gaudi__Histograming__Sink__Root
Gaudi__Histograming__Sink__Root(
    "Gaudi__Histograming__Sink__Root", FileName="hist.root")

# self.abortStalledEvents()
# self.stopOnSignal()

### The Moore-specific part, defined in in ProdConf._Moore()

moore = Moore()
moore.DDDBtag = prod_conf_params["DDDBTag"]
moore.CondDBtag = prod_conf_params["CondDBTag"]
moore.outputFile = (prod_conf_params["OutputFilePrefix"] + '.' +
                    prod_conf_params["OutputFileTypes"][0].lower())
