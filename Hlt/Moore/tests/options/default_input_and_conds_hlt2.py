###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD, VPRetinaFullClusterDecoder
from RecoConf.hlt1_tracking import make_VeloClusterTrackingSIMD, make_velo_full_clusters

# Use velo retina decoding:
make_VeloClusterTrackingSIMD.global_bind(
    algorithm=VeloRetinaClusterTrackingSIMD)
make_velo_full_clusters.global_bind(
    make_full_cluster=VPRetinaFullClusterDecoder)

options.set_input_and_conds_from_testfiledb(
    'upgrade_Sept2022_minbias_0fb_md_xdigi')

# Override settings used by TestFileDB to set geometry version correctly
# To be removed once
#    https://gitlab.cern.ch/lhcb-datapkg/PRConfig/-/merge_requests/334
# is merged and in a released tag.
from DDDB.CheckDD4Hep import UseDD4Hep
if UseDD4Hep:
    from Configurables import DDDBConf
    DDDBConf().GeometryVersion = 'run3/before-rich1-geom-update-26052022'

options.evt_max = 100
