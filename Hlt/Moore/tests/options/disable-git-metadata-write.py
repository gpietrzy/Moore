###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options
import random, string

# Turn off writes to meta-data git repo
options.write_decoding_keys_to_git = False

# Use a randomly generated manifest file name, to avoid conflicits when
# mulitple tasks are running and use the same area, as it appears there are
# scenarios (such as the throughput tests) where the file locking that is
# designed to handle this seemgingly does not work. As in these cases the
# data meta is never kept it is fine to do this. See
# https://gitlab.cern.ch/lhcb/LHCb/-/issues/260 for more details.
options.output_manifest_file = 'manifest-' + ''.join(
    random.choices(string.ascii_uppercase + string.digits, k=12)) + '.json'
