###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Moore options to run LHCbIntegration test for Tupling_FT
from Moore import Options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from Hlt2Conf.lines.b_to_open_charm.hlt2_b2oc import make_flavor_tagging_hlt2_lines
from RecoConf.decoders import default_ft_decoding_version
from RecoConf.hlt1_muonid import make_muon_hits


def hlt2_ft(options: Options):
    all_lines = {}
    default_lines = [
        'BdToDmPi_DmToPimPimKp'  #Bd->DPi
    ]

    make_flavor_tagging_hlt2_lines(
        line_dict=all_lines, flavor_tagging_lines=default_lines)

    def make_lines():
        lines = [builder() for builder in all_lines.values()]
        return lines

    public_tools = [stateProvider_with_simplified_geom()]
    make_muon_hits.global_bind(geometry_version=2)
    default_ft_decoding_version.global_bind(value=6)

    with reconstruction.bind(from_file=False):
        return run_moore(options, make_lines, public_tools)
