###############################################################################
# (c) Copyright 2020-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Configuration for persisting HLT2 objects in output ROOT/MDF files."""
import collections
import logging, json
from pprint import pformat

from PyConf import configurable
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.components import get_output
from PyConf.location_prefix import prefix
from PyConf.filecontent_metadata import register_encoding_dictionary
from GaudiConf.reading import type_map

from RecoConf.reconstruction_objects import reconstruction
from ..lines import Hlt2LuminosityLine
from .cloning import mc_cloners
from .packing import pack_stream_mc
from .persistreco import persistreco_line_outputs
from .truth_matching import truth_match_lines
from .hlt2_tistos import make_reduced_hlt2_candidates

from PyConf.Algorithms import LHCb__SelectivePacker as SelectivePacker
from PyConf.Algorithms import HltPackedBufferWriter

log = logging.getLogger(__name__)

logging.basicConfig(level=logging.DEBUG)

#: TES prefix under which all persisted objects will be stored
DEFAULT_OUTPUT_PREFIX = "/Event/HLT2"


## TODO: add a parameter for 'how deep' to go...
def _data_deps(handle):
    def __walk(visited, top):
        if top.name in visited: return
        visited.add(top.name)
        for handles in top.inputs.values():
            handles = handles if isinstance(handles, list) else [handles]
            for handle in handles:
                yield handle
                for p in __walk(visited, handle.producer):
                    yield p

    visited = set()
    return __walk(visited, handle.producer)


def _referenced_inputs(lines):
    """Return the set of all locations referenced by each line.

    To serialise the data we must know the all location strings referenced by
    the line output objects.

    Args:
        lines (list of Hlt2Line)

    Returns:
        all_locs (dict of str to list of str)
    """

    inputs = collections.defaultdict(list)
    # Include the locations of the line outputs themselves
    # Gather locations referenced from higher up the data flow tree
    data_handles = sorted(
        set(dh for l in lines for dh in l.objects_to_persist), key=hash)
    for h in data_handles:
        inputs[get_type(h)].append(h)
    return inputs


def get_type(dh):
    #For this to work, one needs to add new types to object_types.py in GaudiConf
    types = type_map()
    if dh.type in types.keys():
        return types[dh.type]

    # DVCommonBase algorithms set the output type to unknown_t
    # So set them by hand
    # Also RawEvent and RawBanks have unknown types
    # but we never need to pack them anyway
    if dh.type == "unknown_t":
        loc = dh.location.split("/")[-1]
        if loc == "Particles":
            return loc
        elif loc == "decayVertices":
            return "Vertices"
        elif loc == "_RefitPVs":
            return "PVs"
        elif loc == "Particle2VertexRelations":
            return "P2VRelations"

    return None


def get_packed_locations(lines, inputs, stream):

    line_locs = set(dh for l in lines for dh in l.objects_to_persist)

    if "PP2MCPRelations" in inputs.keys():
        for loc in inputs["PP2MCPRelations"]:
            line_locs.add(loc)
    packed_dhs = []

    types = type_map()
    k = list(types.keys())
    v = list(types.values())

    reco_objs = reconstruction()
    prdict = persistreco_line_outputs(reco_objs)

    for key, locs in inputs.items():
        for i in locs:
            if i in line_locs or i in prdict.values():
                if isinstance(i, str):
                    t = k[v.index(key)]
                    packed_dhs += [(prefix(i, stream), t)]
                else:
                    t = i.type
                    if i.type == "unknown_t":
                        t = k[v.index(key)]
                    packed_dhs += [(prefix(i.location, stream), t)]

    packed_dhs = list(dict.fromkeys(packed_dhs))
    return {'PackedLocations': packed_dhs}


@configurable
def persist_line_outputs(
        streams,
        lines,
        data_type,
        dec_reports,
        associate_mc,
        source_id,
        output_manifest_file,
        stream=DEFAULT_OUTPUT_PREFIX,  #this is where everything goes
        reco_stream=DEFAULT_OUTPUT_PREFIX,  #this is where reco objects come from
        clone_mc=False,
        enable_packing_checks=False,
        enable_checksum=False,
        allow_missing_containers=False):
    """Return CF node and output locations of the HLT2 line persistence.

    Returns:
        control_flow_node (CompositeNode): CF node with matching,
            cloning, packing and serialisation.
        output_packed_locations (list of str): Locations that should be
            persisted when writing to a ROOT file.

    """
    reco_objs = reconstruction()

    cf = []
    protoparticle_relations = []
    if associate_mc:
        truth_matching_cf, proto_rels = truth_match_lines(lines)
        protoparticle_relations += proto_rels
        cf.append(truth_matching_cf)
    dhs_for_hlt2_tistos = []
    algs_for_hlt2_tistos = []
    locations_for_hlt2_tistos = []
    if "Spruce" in stream and clone_mc:  #Sprucing case
        assert associate_mc is False, 'Sprucing does not support MC association. This is done at the HLT2 step.'
        # request PP2MCP relations from HLT2 input
        protoparticle_relations += [
            reco_objs['ChargedPP2MC'], reco_objs['NeutralPP2MC']
        ]
    if "Spruce" in stream:  #Sprucing case
        algs_for_hlt2_tistos, dhs_for_hlt2_tistos = make_reduced_hlt2_candidates(
        )
        locations_for_hlt2_tistos = [
            prefix(t.location, stream) for t in dhs_for_hlt2_tistos
        ]
        cf += algs_for_hlt2_tistos

    #add  line outputs to fill the dictionary
    inputs = _referenced_inputs(lines)

    #add the locations from reco objects to the dictionary
    for val in persistreco_line_outputs(reco_objs).values():
        name = get_type(val)  #find type of object for this DH
        if name:
            inputs[name] += [get_output(val)]
        else:
            log.warning(
                '*** WARNING: get_type failed for {} -- {} not supported for persistence, skipping!'
                .format(val, val.type))

    # add proto particle relations if they exist
    for p in protoparticle_relations:
        inputs["PP2MCPRelations"] += [p]

    if output_manifest_file:
        with open(output_manifest_file, 'w') as f:
            json.dump(
                get_packed_locations(lines, inputs, stream),
                f,
                indent=4,
                sort_keys=True)

    locify = lambda i: i.location if hasattr(i, 'location') else i
    inputs = {t: [locify(i) for i in dhs] for t, dhs in inputs.items()}

    #for each key remove duplicates in the list
    #and add stream to locations to match post cloning locations
    for key, value in inputs.items():
        inputs[key] = [prefix(l, stream) for l in list(dict.fromkeys(value))]

    locations = set(dh for t, dhs in inputs.items()
                    for dh in dhs if t) | set(locations_for_hlt2_tistos)

    packer_mc_locations = []
    mc_locations_mapping = None
    if clone_mc:
        mc_stream = stream
        if reco_stream not in stream: mc_stream = prefix(reco_stream, stream)
        mc_cloner, mc_locations_mapping = mc_cloners(stream,
                                                     protoparticle_relations)
        mc_packer_cf, packer_mc_locations = pack_stream_mc(prefix(mc_stream))
        locations.update(mc_locations_mapping.values())
        mc_packer_cf.children = mc_cloner + mc_packer_cf.children
        cf.append(mc_packer_cf)

    ##TODO: replace "locations" with "requested" determined below...
    encoding_key = int(
        register_encoding_dictionary("PackedObjectLocations",
                                     sorted(locations)), 16)

    # collect all datahandles
    dhs = set(dh for l in lines for dh in l.objects_to_persist) | set(
        protoparticle_relations) | set(dhs_for_hlt2_tistos)
    locations_for = lambda t :  list(set( dh.location for dh in dhs if dh.type == t ))
    available = set(i for t in type_map().keys() for i in locations_for(t))
    # Rest should be per stream
    bank_writers = {}
    for stream_name, stream_lines in streams.items():
        # Hlt2LuminosityLine is not an Hlt2Line so we must exclude it
        stream_lines_without_lumi = [
            line for line in stream_lines
            if not isinstance(line, Hlt2LuminosityLine)
        ]

        line_to_locations = dict((l.decision_name,
                                  [dh.location for dh in l.objects_to_persist])
                                 for l in stream_lines_without_lumi)

        # finally, verify that everything mentioned in line_to_locations is present in the lists of inputs
        requested = set(j for i in line_to_locations.values() for j in i)

        assert requested.issubset(
            available
        ), 'oops -- persistency request for the following can never be satisfied: {}\n\navailabe: {}\n\nrequested{}\n'.format(
            requested - available, available, requested)

        #  note: only 'top level' containers (not (implicit) dependencies!) should be added here
        #  note: should have one 'selective packing' instance per stream, and only select the lines in that given stream...
        not_supported = set(dh for l in stream_lines_without_lumi
                            for dh in l.objects_to_persist
                            if dh.type not in type_map().keys())
        if not_supported:
            log.error('encountered unsupported types -- {}'.format(
                {dh.location: dh.type
                 for dh in not_supported}))

        # selective packing uses 'optional inputs' (as it uses the DecReport as a mask for its inputs) and
        # must thus be explicitly scheduled in the control flow and classified as a barrier to
        # explicitly _not_ trigger the creation of any data dependencies.
        #
        # in case of MC relations, we always persist them _if_ configured to create them.
        # Note that the relations will be striped of any keys which will not occur in the output
        #
        # Also note that MCParticle and MCVertex are 'special': since they are invariant 'after'
        # the event generation, it is sufficient to just know their key to identify them, regardless
        # of the container in the TES in which they happen to sit.  Note that taking advantage of
        # this is a bit dangerous, as of course not all objects way be 'transported' to the end of the
        # processing, i.e. one may end up with dangling references...
        packer = SelectivePacker(
            # no explicit name to allow sharing of instances between TURBO and TURBORAW
            # name='{}_{}_Packer'.format(
            #     stream_name,
            #     stream.removeprefix('/Event/').replace('/', '_')),
            is_barrier=True,
            AnonymizeDependencies=True,
            AbsentRequestedContainerAllowed=allow_missing_containers,
            AlwaysPack=protoparticle_relations + dhs_for_hlt2_tistos,
            ExternalLocations=mc_locations_mapping if mc_locations_mapping else
            {},  # external locations may be packed under different names from their current one -- and the packer must be informed of this information
            AddTagParticles=[
                l.decision_name for l in stream_lines_without_lumi
                if l.tagging_particles
            ],
            AddCaloDigits=[
                l.decision_name for l in stream_lines_without_lumi
                if l.calo_digits
            ],
            AddCaloClusters=[
                l.decision_name for l in stream_lines_without_lumi
                if l.calo_clusters
            ],
            AddPVTracks=[
                l.decision_name for l in stream_lines_without_lumi
                if l.pv_tracks
            ],
            AddTrackAncestors=[
                l.decision_name for l in stream_lines_without_lumi
                if l.track_ancestors
            ],
            OutputPrefix=stream,
            EncodingKey=encoding_key,
            LineToLocations=line_to_locations,
            DecReports=dec_reports,
            EnableCheck=enable_packing_checks,
            EnableChecksum=enable_checksum,
            **{n: locations_for(t)
               for t, n in type_map().items()})

        bank_writers[stream_name] = HltPackedBufferWriter(
            # no explicit name to allow sharing of instances between TURBO and TURBORAW
            # name='{}_{}_PackedBufferWriter'.format(
            #     stream_name,
            #     stream.removeprefix('/Event/').replace('/', '_')),
            PackedContainers=[packer.outputs['outputLocation']],
            SourceID=source_id)

    if log.isEnabledFor(logging.DEBUG):
        log.debug('packer_locations: ' + pformat(inputs.values()))
        log.debug('packer_mc_locations: ' + pformat(packer_mc_locations))

    control_flow_nodes = {}
    for stream_name in streams.keys():
        control_flow_nodes[stream_name] = CompositeNode(
            "{}_line_output_persistence".format(stream_name),
            combine_logic=NodeLogic.NONLAZY_OR,
            children=cf + [bank_writers[stream_name]],
            force_order=True)
        # special locations (if clone_mc=True)
        # /Event/(Spruce/)HLT2/pSim/MCParticles and /Event/(Spruce/)HLT2/pSim/MCVertices
        # are not serialised to avoid complication and to save space in the clone_mc=False case

    return control_flow_nodes, packer_mc_locations, bank_writers


sim_veto_list = [
    '/Event/pSim', '/Event/Calo', '/Event/Unstripped', '/Event/HC',
    '/Event/Tracker', '/Event/PersistReco', '/Event/Velo', '/Event/Muon',
    '/Event/Rich', '/Event/Trigger', '/Event/pRec', '/Event/Rec', '/Event/DAQ'
]
