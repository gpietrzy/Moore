###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Define hlt2 streams and routing bits

Routing bits are used to decide where to send events, to Hlt2 only, or as well to calibration and monitoring tasks.
Each bit is set according to trigger line decisions.

The routing bits are defined in https://gitlab.cern.ch/lhcb/runII-routingbits.
The meaning of some routing bits (eg. 40, 94, 95 ) has been agreed on and should not be changed.

"""
from PyConf.utilities import ConfigurationError

DETECTOR_RAW_BANK_TYPES = [
    'VPRetinaCluster',
    'VP',  # compatibility with data without VP clusters
    'UT',
    'FTCluster',
    'Rich',
    'Muon',
    'Plume',
    'Calo',
    'EcalPacked',  # compatibility with old data, now under "Calo"
    'HcalPacked',  # compatibility with old data, now under "Calo"
]

#Banks that shoud ALWAYS appear in the output of all processes
NECESSARY_RAW_BANKS = ['ODIN']

# HltRoutingBits bank is not in the list as it is rewritten by HLT2.
HLT1_REPORT_RAW_BANK_TYPES = [
    'HltDecReports', 'HltSelReports', 'HltLumiSummary'
]

# DstData is not in the list as it is discarded during Sprucing.
HLT2_REPORT_RAW_BANK_TYPES = [
    'HltDecReports', 'HltLumiSummary', 'HltRoutingBits'
]

# For now passing all RawBanks to all streams, to be modified based on the streaming development
# list ODIN explicitly just here
# NOTE: the raw bank request of Hlt2LuminosityLine does not consider the values here.
DETECTOR_RAW_BANK_TYPES_PER_STREAM = {
    "default": DETECTOR_RAW_BANK_TYPES,
    "turboraw": DETECTOR_RAW_BANK_TYPES,
    "full": DETECTOR_RAW_BANK_TYPES,
    "turbo": [],
    "turcal": DETECTOR_RAW_BANK_TYPES,
    "no_bias": DETECTOR_RAW_BANK_TYPES,
    "passthrough": DETECTOR_RAW_BANK_TYPES,
    "lumi": DETECTOR_RAW_BANK_TYPES,
}

stream_bits = dict(
    turboraw=85,
    full=87,
    turbo=88,
    turcal=90,
    no_bias=91,
    passthrough=92,
    lumi=93,
    default=64,
)


def stream_bank_types(stream):
    """Return list of detector raw banks to save for given stream"""
    if stream not in DETECTOR_RAW_BANK_TYPES_PER_STREAM:
        raise ConfigurationError(
            "stream %r is not configured in streams.py" % stream)
    return list(DETECTOR_RAW_BANK_TYPES_PER_STREAM[stream])


def get_default_routing_bits(streams):
    """ Define default routing bits via regular expressions. This is currently a placeholder to test the functionality."""
    from Moore.lines import Hlt2LuminosityLine
    # Temporarily hard-code a default mapping of streams to bits, see #268
    routingBits = {}
    for stream, lines_in_stream in streams.items():
        # Bit for stream
        if (stream_bits[stream] == 94 or stream_bits[stream] == 95):
            raise ConfigurationError(
                f"Bits 94 and 95 are reserved. Do not use for stream {stream}."
            )

        routingBits[stream_bits[stream]] = [
            l.decision_name for l in lines_in_stream
        ]
        # Lumi bit, always 94
        routingBits[94] = [
            l.decision_name for l in lines_in_stream
            if isinstance(l, Hlt2LuminosityLine)
        ]
        # Physics bit, always 95
        routingBits[95] = [
            l.decision_name for l in lines_in_stream
            if not isinstance(l, Hlt2LuminosityLine)
        ]
        # NOTE: we must have at most one line that does not save the HltDecReports banks
        # (the lumi line in physics streams that nanofies). Events exclusively
        # selected by this line must be excluded from bit 95. Events selected by
        # this line must be assigned to bit 94.
        # The idea is that if Hlt2 HltDecReports are present, one can always
        # select based on them and expect raw banks according to the line.

    return routingBits
