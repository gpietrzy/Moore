###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from shutil import copy
import re, logging, inspect, itertools
from collections import namedtuple
from Allen.config import allen_detectors, run_allen_reconstruction  # noqa: F401
from PyConf import configurable
from PyConf.Algorithms import (
    ExecutionReportsWriter, HltDecReportsWriter, HltSelReportsWriter,
    Hlt__RoutingBitsWriter, Hlt__RoutingBitsCombiner,
    Gaudi__Hive__FetchLeavesFromFile, SelectiveCombineRawBankViewsToRawEvent,
    CombineRawBankViewsToRawEvent, RawEventSimpleCombiner, AddressKillerAlg,
    VoidFilter, HltDecReportsMonitor, DeterministicPrescaler,
    HltRoutingBitsFilter, LHCb__Tests__RunEventCountAlg,
    LHCb__SelectViewForHltSourceID)

import Functors as F
from PyConf.components import force_location
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.application import (MDF_KEY, ROOT_KEY, mdf_writer, online_writer,
                                root_copy_input_writer, make_odin,
                                default_raw_banks, root_writer)

from PyConf.filecontent_metadata import generate_encoding_dictionary, register_encoding_dictionary
from PyConf.utilities import ConfigurationError
from PyConf.application import all_nodes_and_algs

from PyConf.Algorithms import bankKiller

#: Regular expression (compiled) defining the valid selection line names
# Meaning: line names should start with either of Hlt1, Hlt2, Spruce, Pass
#          the following allowed characters are a to z, A to Z, 0 to 9 and _
#          the line name should not end with Line (change of convention https://gitlab.cern.ch/lhcb/Rec/-/issues/375)
SELECTION_LINE_NAME_PATTERN = re.compile(
    r'^(Hlt[12]|Spruce|Pass)[A-Za-z0-9_]+(?<!Line)$')

ONLINE = 'Online'

# "forward" some useful functions from PyConf.application
from PyConf.application import configure_input, configure

#from .lines import Hlt2LuminosityLine
from .lines import HltLine, Hlt2Line, Hlt2LuminosityLine, SpruceLine  # noqa: forward import
from .persistence import persist_line_outputs, sim_veto_list
from .selreports import make_selreports

log = logging.getLogger(__name__)

ROOT_RAW_EVENT_LOCATION = '/Event/DAQ/RawEvent'

from .streams_hlt2 import (DETECTOR_RAW_BANK_TYPES, HLT1_REPORT_RAW_BANK_TYPES,
                           HLT2_REPORT_RAW_BANK_TYPES, NECESSARY_RAW_BANKS,
                           get_default_routing_bits)


def _unique(x):
    """Return a list of the unique elements in x while preserving order."""
    return list(dict.fromkeys(x).keys())


class Reconstruction(namedtuple('Reconstruction', ['node'])):  # noqa
    """Immutable object fully qualifiying the output of a reconstruction data flow with prefilters.

    Attributes:
        node (CompositeNode): the control flow of the reconstruction.

    """
    __slots__ = ()  # do not add __dict__ (preserve immutability)

    def __new__(cls, name, data_producers, filters=None):
        """Initialize a Reconstruction from name, data_producers and filters.

        Creates two control flow `CompositeNode` with the given `data`
        combined with `NONLAZY_OR` to execute  and a `CompositeNode`.

        Args:
            name (str): name of the reconstruction
            data_producers (list): iterable list of algorithms to produce data
            filters (list): list of filters to apply before running reconstruction
        """
        data_producers_node = CompositeNode(
            name + "_data",
            data_producers,
            combine_logic=NodeLogic.NONLAZY_OR,
            force_order=True)
        if filters is None:
            filters = []
        control_flow = filters + [data_producers_node]
        cf_node = CompositeNode(
            name + "_decision",
            control_flow,
            combine_logic=NodeLogic.LAZY_AND,
            force_order=True)

        return super(Reconstruction, cls).__new__(cls, cf_node)

    @property
    def name(self):
        """Reconstruction name"""
        return self.node.name


@configurable
def report_writers_nodes(options,
                         streams,
                         data_type,
                         process,
                         output_manifest_file,
                         associate_mc=False,
                         clone_mc=False):
    """Return the control flow node and locations to persist of the default reports writers.

    Args:
        streams (dict of stream : DecisionLines): control flow nodes of lines
        data_type (str)
        process (str): "hlt1", "hlt2", "spruce" or "pass"
        associate_mc (bool): If True, run MC association algorithms and
            persist the result.
        clone_mc (bool): If True and process=="spruce", clone MC objects.

    Returns:
        node (CompositeNode): Output creation control flow.
        new_hlt_banks (dict): Raw data to write out per bank type.
        extra_locations_to_persist (list of str): Locations the application
            should write out.
        erw_outputLocation (str): Location of DecReports from ExecutionReportsWriter.
    """
    new_hlt_banks = {}
    # Hack to support writing of objects created in HLT2 jobs
    extra_locations_to_persist = []

    algs = []
    lines = streams_dict_to_lines_list(streams)
    lines_with_output = [l for l in lines if l.produces_output]
    lumi_lines = []
    physics_lines = [l for l in lines_with_output if l not in lumi_lines]

    # We will write the reports to raw banks at these locations
    source_id = {
        "hlt1": "Hlt1",
        "hlt2": "Hlt2",
        "spruce": "Spruce",
        "pass": "Spruce"
    }[process]
    major_name = {
        "hlt1": "Hlt1SelectionID",
        "hlt2": "Hlt2SelectionID",
        "spruce": "SpruceSelectionID",
        "pass": "SpruceSelectionID"
    }[process]

    dec_key = int(
        register_encoding_dictionary(
            major_name,
            generate_encoding_dictionary(major_name,
                                         [l.decision_name for l in lines])),
        16)  # TODO unsigned? Stick to hex string?

    ##spruce and passthrough jobs will write a Spruce report
    erw = ExecutionReportsWriter(
        Persist=[line.name for line in lines],
        ANNSvcKey=major_name,
        TCK=dec_key  # TODO unsigned? Stick to hex string?
    )
    drw = HltDecReportsWriter(
        SourceID=source_id,
        InputHltDecReportsLocation=erw.DecReportsLocation,
        EncodingKey=dec_key,
    )

    algs.extend([erw, drw])
    new_hlt_banks[source_id + 'DecReports'] = drw.OutputView
    packed_data = {}
    line_output_cf = {}
    if process == "pass":
        # For pass, copy the manifest file for `PackedLocations` if requested, nothing else to be done
        if options.input_manifest_file and options.output_manifest_file:
            copy(options.input_manifest_file, options.output_manifest_file)

    elif process == "hlt1":
        # For hlt1, make sel reports
        srm = make_selreports(process, physics_lines, erw)
        srw = HltSelReportsWriter(
            SourceID=source_id,
            DecReports=erw.DecReportsLocation,
            SelReports=srm.SelReports,
            ObjectSummaries=srm.ObjectSummaries,
            EncodingKey=srm.properties['EncodingKey'])
        algs.append(srw)
        new_hlt_banks['HltSelReports'] = srw.OutputView

    else:
        # For hlt2 and spruce, persist_line_outputs
        event_stream = "/Event/Spruce" if process == "spruce" else "/Event/HLT2"
        reco_stream = "/Event/HLT2"
        (line_output_cf, line_output_locations,
         packed_data) = persist_line_outputs(
             streams,
             physics_lines,
             data_type,
             erw.DecReportsLocation,
             associate_mc,
             process.capitalize(),
             output_manifest_file,
             stream=event_stream,
             reco_stream=reco_stream,
             clone_mc=clone_mc)

        extra_locations_to_persist.extend(line_output_locations)

    node = CompositeNode(
        'report_writers',
        combine_logic=NodeLogic.NONLAZY_OR,
        children=algs,
        force_order=True)

    # Transform to a list of unique str
    extra_locations_to_persist = _unique(extra_locations_to_persist)
    return [
        node
    ], line_output_cf, new_hlt_banks, packed_data, extra_locations_to_persist, erw


@configurable
def stream_writer(stream,
                  stream_lines,
                  path,
                  output_type,
                  process,
                  propagate_mc,
                  old_hlt_raw_banks=[],
                  detector_raw_banks=[],
                  new_raw_banks={},
                  dec_reports=None,
                  extra_locations=[],
                  write_all_input_leaves=True):
    """Return node for writing output for a stream.

    These algorithms are not functional and must be explicitly scheduled,
    typically at the very end of the control flow.

    Args:
        stream (str): The name of the output stream. Will prefix `path` in the name of output file.
        path (str): Path the output file should be written to.
        output_type (str): One of `MDF_KEY` or `ROOT_KEY`.
        process (str): "hlt1", "hlt2", "spruce" or "pass".
        propagate_mc (bool): If True and process is "spruce" or "pass", enable propagation of MC objects.
        new_raw_banks (dict of DataHandle: bank type): Newly created raw event parts to write.
        old_hlt_raw_banks (list of DataHandle): Banks to persist from previous hlt steps.
        detector_raw_banks (list of DataHandle): Detector raw banks requested by the stream.
        extra_locations (list of str): Extra locations for ROOT file
            persistence. Ignored for MDF files. Defaults to [].

    Returns:
        (pre_algs, post_algs): Lists of configured algorithms that
            should be scheduled at the very beginning and the very end
            of the control flow, respectively.

    """
    writer_setup = []
    writers = []

    # TODO do not use the magic "default" but perhaps just "" instead.
    stream_part = stream if stream != "default" else ""
    full_fname = path.format(stream=stream_part)
    if stream != "default" and stream_part.lower() not in full_fname.lower(
    ) and output_type != ONLINE:
        raise ConfigurationError("{stream} must be part of output_file")

    # Raw banks that will be saved by every line except the Hlt2LuminosityLine
    stream_raw_banks = list(
        new_raw_banks.keys()) + old_hlt_raw_banks + detector_raw_banks
    ignore_raw_banks = []
    # for hlt1,2 when writing dst, only consolidate new_locations
    # other banks are already in the raw event
    # TODO: This actually means that the raw event will always be saved in MC, something we did not do in Run 2.
    if output_type == ROOT_KEY and process in ["hlt1", "hlt2"]:
        stream_raw_banks = _unique(list(new_raw_banks.keys()))
        ignore_raw_banks = _unique(old_hlt_raw_banks + detector_raw_banks)
    # list of detector raw banks requested by hlt2 and sprucing lines
    # only hlt2 or sprucing lines can request additional detector raw banks
    map_lines_det_raw_banks = {}
    line_raw_banks = []

    if process == 'hlt2' or process == 'spruce':
        for line in stream_lines:
            rb_to_persist = []
            if isinstance(line, Hlt2LuminosityLine):
                # Lumi lines define which raw banks to save by default
                # Unlike physics lines these don't have to be only detector raw banks
                # Check if the requested bank type is in new banks created.
                # If yes, take the new bank.
                # If not, take whatever was saved in previous step.
                for rb in line.raw_banks:
                    if rb in new_raw_banks.values():
                        for k, v in new_raw_banks.items():
                            if v == rb:
                                rb_to_persist += [k]
                    else:
                        rb_to_persist += [default_raw_banks(rb)]
            else:
                rb_to_persist = [
                    default_raw_banks(rb) for rb in line.raw_banks
                ]
                rb_to_persist += stream_raw_banks

            rb_to_persist = list(
                filter(lambda rb: rb not in ignore_raw_banks, rb_to_persist))
            rb_to_persist = _unique(rb_to_persist)
            map_lines_det_raw_banks[line.decision_name] = tuple(
                rb.location for rb in rb_to_persist)

            line_raw_banks += rb_to_persist

        line_raw_banks = _unique(line_raw_banks)
    # In case of HLT1 all collected Raw Banks are combined into new RawEvent
    if process == 'hlt1':
        consolidate_views = CombineRawBankViewsToRawEvent(
            name="CombineRawBanks_for_" + stream + "_HLT1",
            RawBankViews=stream_raw_banks,
            outputs={'RawEvent': force_location("/Event/" + stream)})

    # passthrough at the moment is not line dependent
    # fix this if statement once the passthrough streaming implemented properly
    elif process == "pass":
        consolidate_views = SelectiveCombineRawBankViewsToRawEvent(
            name="SelectiveCombineRawBanks_for_" + stream,
            DecReports=dec_reports,
            MapLinesRawBanks={},
            RequiredRawBanks=stream_raw_banks,
            SelectiveRawBanks=[],
            outputs={'RawEvent': force_location(f"/Event/{stream}/RawEvent")})

    # HLT2 and sprucing allows selective persistency based on request per line
    else:
        consolidate_views = SelectiveCombineRawBankViewsToRawEvent(
            name="SelectiveCombineRawBanks_for_" + stream,
            DecReports=dec_reports,
            MapLinesRawBanks=map_lines_det_raw_banks,
            RequiredRawBanks=[],
            SelectiveRawBanks=line_raw_banks,
            outputs={'RawEvent': force_location(f"/Event/{stream}/RawEvent")})

    writers.append(consolidate_views)

    # TODO: Remove one case once lbexec and gaudirun.py agree on a convention.
    if output_type in [MDF_KEY, "RAW"]:
        writers.append(mdf_writer(full_fname, consolidate_views.RawEvent))
    elif output_type == ONLINE:
        writers.append(online_writer(consolidate_views.RawEvent))
    elif output_type == ROOT_KEY:
        writers.append(AddressKillerAlg())

        if process == "spruce":
            sim_veto_list.append('/Event/HLT2')

        input_raw_event_locations = []
        output_location = ROOT_RAW_EVENT_LOCATION
        if process in ["spruce", "pass"]:
            input_raw_event_locations = sim_veto_list
            output_location = consolidate_views.RawEvent
        else:
            # Get all the raw event locations containing requested raw banks
            # Find the he ones that are obtained from the raw event by unpacking
            all_locations = _unique(stream_raw_banks + line_raw_banks +
                                    detector_raw_banks)
            for l in all_locations:
                if "UnpackRawEvent" in l.producer.name:
                    input_raw_event_locations += [
                        l.producer.inputs["RawEventLocation"].location
                    ]

            input_raw_event_locations = _unique(input_raw_event_locations)

            # Check DAQ/RawEvent is also the input event
            overwrite = ROOT_RAW_EVENT_LOCATION in input_raw_event_locations
            if overwrite:
                # Do not copy raw_out_location onto itself
                input_raw_event_locations.remove(ROOT_RAW_EVENT_LOCATION)

            raw_event_combiner = RawEventSimpleCombiner(
                InputRawEventLocations=[consolidate_views.RawEvent.location] +
                input_raw_event_locations,
                EnableIncrementalMode=overwrite,
                outputs={
                    'OutputRawEventLocation':
                    force_location(ROOT_RAW_EVENT_LOCATION),
                })
            writers.append(raw_event_combiner)

        # This part is only meant for simulation
        # always false for data and by default true for simulation
        if write_all_input_leaves and propagate_mc:
            # Always copy the locations from the input when the output is ROOT
            input_leaves = Gaudi__Hive__FetchLeavesFromFile()
            writer_setup.append(input_leaves)

            # Collecting the locations to copy must be the first thing in the
            # control flow.
            writers.append(
                root_copy_input_writer(
                    full_fname,
                    input_leaves,
                    [output_location] + extra_locations,
                    # don't copy the raw event
                    tes_veto_locations=input_raw_event_locations))
        else:
            writers.append(root_writer(full_fname, [output_location]))

    else:
        raise NotImplementedError()

    return writer_setup, writers


def moore_control_flow(options, streams, process, analytics=False):
    """Return the Moore application control flow node.

    Combines the lines with `NONLAZY_OR` logic in a global decision
    control flow node. This is `LAZY_AND`-ed with the output control
    flow, which consists of Moore-specific report makers/writers and
    generic persistency.

    Args:
        options (ApplicationOptions): holder of application options
        streams (dict of stream : DecisionLines): control flow nodes of lines
        process (str): "hlt1", "hlt2", "spruce" or "pass".
        analytics (bool, optional): For use only in rate/event size analysis. Defaults to False.

    Returns:
        node (CompositeNode): Application control flow.
    """
    options.finalize()

    if process == "spruce" or process == "pass":
        from .streams_spruce import stream_bank_types
    else:
        from .streams_hlt2 import stream_bank_types

    # Sort the lines by name for consistency between runs
    for stream, stream_lines in streams.items():
        streams[stream] = sorted(stream_lines, key=lambda line: line.name)

    lines = [line.node for line in streams_dict_to_lines_list(streams)]

    rw_nodes, line_output_cf, new_report_raw_banks, packed_data, extra_outputs, dec_reports = report_writers_nodes(
        options,
        streams,
        options.data_type,
        process,
        options.output_manifest_file,
        # Can only run association when we have access to the linker tables. Only want association for hlt step
        associate_mc=process == "hlt2" and options.simulation
        and options.input_type == ROOT_KEY and options.output_type == ROOT_KEY,
        clone_mc=options.simulation and options.input_type == 'ROOT')

    decisions_node = CompositeNode(
        'hlt_decision',
        combine_logic=NodeLogic.NONLAZY_OR,
        children=lines,
        force_order=False)

    # precale monitoring to use less resources
    prescaler_decreports_monitor = DeterministicPrescaler(
        name=f"{process.upper()}PrescaleDecReportsMonitor",
        AcceptFraction=0.1,
        SeedName=f"{process.upper()}PrescaleDecReportsMonitor",
        ODINLocation=make_odin())
    postcaler_decreports_monitor = DeterministicPrescaler(
        name=f"{process.upper()}PostscaleDecReportsMonitor",
        AcceptFraction=0.0,
        SeedName=f"{process.upper()}PostscaleDecReportsMonitor",
        ODINLocation=make_odin())
    decreports_monitor = HltDecReportsMonitor(
        name=f"{process.upper()}DecReportsMonitor", Input=dec_reports)

    decisions_monitor_node = CompositeNode(
        'monitor_decisions',
        combine_logic=NodeLogic.LAZY_AND,
        children=[
            prescaler_decreports_monitor, decreports_monitor,
            postcaler_decreports_monitor
        ],
        force_order=True)
    # We want to run the monitoring of line decisions on every event to have the proper normalization.
    # Therefore, add the DecReportsMonitor after the control flow node containing all selection lines
    # and use NONLAZY_OR as type of the node containing both. To not run the monitoring on every event
    # a prescale is used. To not change the decision of the combined node a postscale is used.

    lines_node = CompositeNode(
        "lines",
        combine_logic=NodeLogic.NONLAZY_OR,
        children=[
            decisions_node,
            decisions_monitor_node,
        ],
        force_order=True)

    stream_rbw = {}
    stream_writers = []
    stream_writers_setup = []

    if (process == "hlt1" or process == "hlt2") and not analytics:
        for stream, stream_lines in streams.items():
            rbwriter = Hlt__RoutingBitsWriter(
                RoutingBits=get_default_routing_bits({
                    stream: stream_lines
                }),
                DecReports=dec_reports,
                ODIN=make_odin())
            if (process == "hlt2"):
                rbw_merger = Hlt__RoutingBitsCombiner(
                    Hlt1Bits=default_raw_banks("HltRoutingBits"),
                    Hlt2Bits=rbwriter.OutputView)
                routing_bits = rbw_merger.OutputView
            else:
                routing_bits = rbwriter.OutputView
            if options.output_type == "ROOT":
                # TODO remove once we use a functional raw event combiner
                # for ROOT and not RawEventSimpleCombiner.
                stream_writers.append(routing_bits)

            stream_rbw[stream] = routing_bits

    if options.output_file or options.output_type == ONLINE or analytics:
        #NECESSARY_RAW_BANKS are always propagated
        banks_from_previous_step = []
        for banktype in NECESSARY_RAW_BANKS:
            banks_from_previous_step.append(default_raw_banks(banktype))
        # Collect DataHandles to raw banks which should be propagated from input to output.
        if process == 'hlt1':
            # There is nothing to propagate except ODIN here.
            pass
        elif process == 'hlt2':
            for banktype in HLT1_REPORT_RAW_BANK_TYPES:
                view_hlt1 = LHCb__SelectViewForHltSourceID(
                    name=f"SelectHlt1{banktype}",
                    Input=default_raw_banks(banktype),
                    SourceID="Hlt1").Output
                banks_from_previous_step.append(view_hlt1)
        elif process == 'spruce' or process == "pass":
            for banktype in HLT1_REPORT_RAW_BANK_TYPES:
                view_hlt1 = LHCb__SelectViewForHltSourceID(
                    name=f"SelectHlt1{banktype}",
                    Input=default_raw_banks(banktype),
                    SourceID="Hlt1").Output
                banks_from_previous_step.append(view_hlt1)
            for banktype in HLT2_REPORT_RAW_BANK_TYPES:
                view_hlt2 = LHCb__SelectViewForHltSourceID(
                    name=f"SelectHlt2{banktype}",
                    Input=default_raw_banks(banktype),
                    SourceID="Hlt2").Output
                banks_from_previous_step.append(view_hlt2)

        if process == "pass":
            banks_from_previous_step.append(default_raw_banks("DstData"))

        banks_from_previous_step = _unique(banks_from_previous_step)

        # Assert condition to filter out HLT2 streaming when using DST output as writing multiple DST files is not supported for now
        assert not (len(streams.items()) > 1 and process == "hlt2"
                    and options.output_type == "ROOT"
                    ), "This configuration is not supported at this time"

        for stream, stream_lines in streams.items():
            new_raw_banks = {v: k for k, v in new_report_raw_banks.items()}

            if stream in packed_data.keys():
                new_raw_banks[packed_data[stream].OutputView] = "DstData"

            if (process == "hlt1" or process == "hlt2") and not analytics:
                new_raw_banks[stream_rbw[stream]] = "HltRoutingBits"

            if analytics:
                post_algs = RawEventSize_analysis(process, stream,
                                                  stream_lines, dec_reports,
                                                  banks_from_previous_step,
                                                  list(new_raw_banks.keys()))
            else:
                pre_algs, post_algs = stream_writer(
                    stream=stream,
                    stream_lines=stream_lines,
                    path=options.output_file,
                    output_type=options.output_type,
                    process=process,
                    propagate_mc=options.simulation
                    and options.input_type == 'ROOT',
                    new_raw_banks=new_raw_banks,
                    old_hlt_raw_banks=banks_from_previous_step,
                    detector_raw_banks=[
                        default_raw_banks(rb)
                        for rb in _unique(stream_bank_types(stream))
                    ],
                    dec_reports=dec_reports,
                    extra_locations=extra_outputs)
                for alg in pre_algs:
                    if alg not in stream_writers_setup:
                        stream_writers_setup.append(alg)

            streamFilter = VoidFilter(
                # no explicit name to allow sharing of instances between TURBO and TURBORAW
                # name=f'Stream{stream}_filter',
                Cut=F.DECREPORTS_FILTER(
                    Lines=list(line.decision_name for line in stream_lines),
                    DecReports=dec_reports.DecReportsLocation))
            stream_node_children = [streamFilter]

            # only hlt2 and sprucing do save line outputs
            if process in ["hlt2", "spruce"]:
                stream_node_children += [line_output_cf[stream]]

            stream_node_children += post_algs

            stream_node = CompositeNode(
                stream + "_" + 'writer',
                combine_logic=NodeLogic.LAZY_AND,
                children=stream_node_children,
                force_order=True)
            stream_writers.append(stream_node)

    # If input_process='Hlt2' and process='Hlt2', HLT2 is being re-run so remove the banks from the first HLT2 pass
    bankKillers = []
    if options.input_process == 'Hlt2' and process == 'hlt2':
        log.info(
            "NOTE: Running with Hlt2 over data already processed with Hlt2, Hlt2DecReports and DstData from previous step will be discarded and only the new ones will be kept."
        )
        # If writing DST, the raw banks need to be removed from the original raw event as this is kept.
        # When writing MDF, a new raw event is assembled from the parts which are needed.
        if options.output_type == ROOT_KEY:
            bankKillers = [
                bankKiller(
                    name="KillRoutingBits", BankTypes=["HltRoutingBits"]),
                bankKiller(name="KillDstData", BankTypes=["DstData"]),
                bankKiller(
                    name="KillHlt2DecReports",
                    KillSourceIDMask=0x0700,
                    KillSourceID=0x0200,
                    BankTypes=["HltDecReports"])
            ]

    if stream_writers:
        stream_writers_nodes = [
            CompositeNode(
                'stream_writers',
                combine_logic=NodeLogic.NONLAZY_OR,
                children=stream_writers,
                force_order=False)
        ]
    else:
        stream_writers_nodes = []

    moore_children = (bankKillers + stream_writers_setup + [lines_node] +
                      rw_nodes + stream_writers_nodes)
    moore = CompositeNode(
        'moore',
        combine_logic=NodeLogic.LAZY_AND,
        children=moore_children,
        force_order=True)

    return moore


def is_DVCommonBase_alg(alg):
    # a Gaudi::Property registers it's owner and appends it to the doc string
    # e.g. the doc of ModifyLocations in DVCommonBase is:
    # ' if set to false, does not append /Particles to outputParticles location [DVCommonBase<GaudiAlgorithm>] '
    # so as a proxy if something inherits from DVCommonBase we check if we can find this property
    return '[DVCommonBase<' in alg._propertyDocDct.get("ModifyLocations", "")


def is_GaudiHistoAlg(alg):
    return '[GaudiHistos<' in alg._propertyDocDct.get(
        "UseSequencialNumericAutoIDs", "")


def check_for_known_issues(line):
    all_algs = all_nodes_and_algs(line.node, True)[1]
    # filter out lines which will crash in multi threaded mode
    # this check is likely incomplete....
    # what else is not thread safe?
    # For now we just look for anything that inherits from DVCommonBase or GaudiHistos
    return [
        a for a in all_algs
        if is_DVCommonBase_alg(a.type) or is_GaudiHistoAlg(a.type)
    ]


def run_moore(options,
              make_streams=None,
              public_tools=[],
              analytics=False,
              exclude_incompatible=True):
    """Configure Moore's entire control and data flow.

    Convenience function that configures all services, creates the
    standard Moore control flow and builds the the data flow (by
    calling the global lines maker).

    If ``make_streams`` returns a list a default stream definition is created
    from it as ``{"default": lines}``.

    Args:
        options (ApplicationOptions): holder of application options
        make_streams: function returning dict of {stream : `DecisionLine` objects}) OR a list of `DecisionLine` objects
        public_tools (list): list of public `Tool` instances to configure
        analytics (bool, optional): For use only in rate/event size analysis. Defaults to False.
        exclude_incompatible (bool, optional): Exclude the lines that are incompatible with multithreaded mode. Defaults to True.
    """
    # First call configure_input for its important side effect of
    # changing the default values of default_raw_event's arguments.
    # The latter is the deepest part of the call stack of make_lines.
    config = configure_input(options)
    # Then create the data (and control) flow for all streams.
    streams = (make_streams or options.lines_maker)()
    # Create default streams definition if make_streams returned a list
    if not isinstance(streams, dict):
        streams = dict(default=streams)

    # Exclude the lines with known issues (with non-thread safe algos etc.)
    if exclude_incompatible:
        filtered_streams = {}
        excluded_lines = []
        for stream, lines in streams.items():
            filtered_lines = []
            for l in lines:
                reason = check_for_known_issues(l)
                if not reason: filtered_lines += [l]
                else: excluded_lines += [(l.name, reason)]
            if len(filtered_lines) > 0:
                filtered_streams[stream] = filtered_lines
        streams = filtered_streams
        if len(excluded_lines) > 0:
            log.info(
                f"Following {len(excluded_lines)} lines were automatically excluded:"
            )
            log.info(
                "Name of Line ---- list of found algos that are known to be not thread safe"
            )
            for line_name, reason in excluded_lines:
                log.info(f"{line_name} ---- {reason}")

    lines = streams_dict_to_lines_list(streams)
    # Determine whether Hlt1, Hlt2 or Spruce is being processed
    hlt1 = all(l.name.startswith('Hlt1') for l in lines)
    hlt2 = all(l.name.startswith('Hlt2') for l in lines)
    spruce = all(l.name.startswith('Spruce') for l in lines)
    passthrough = all(l.name.startswith('Pass') for l in lines)

    assert hlt1 ^ hlt2 ^ spruce ^ passthrough, 'Expected exclusively all Hlt1, all Hlt2, all Spruce or all Pass lines'

    if hlt1:
        process = "hlt1"
    elif hlt2:
        process = "hlt2"
    elif spruce:
        process = "spruce"
    elif passthrough:
        process = "pass"

    # Combine all lines and output in a global control flow.
    moore_control_node = moore_control_flow(options, streams, process,
                                            analytics)

    #Filter to return true if physics bit 95 is "on" for this event
    rb_bank = default_raw_banks('HltRoutingBits')
    physFilterRequireMask = (0x0, 0x0, 0x80000000)
    lumiFilterRequireMask = (0x0, 0x0, 0x40000000)

    rb_filter = [
        HltRoutingBitsFilter(
            name="PhysFilter",
            RawBanks=rb_bank,
            RequireMask=physFilterRequireMask)
    ]

    lumi_filter = [
        HltRoutingBitsFilter(
            name="LumiFilter",
            RawBanks=rb_bank,
            RequireMask=lumiFilterRequireMask)
    ]

    physics_sprucing_node = CompositeNode(
        'physics_sprucing_node',
        combine_logic=NodeLogic.LAZY_AND,
        children=rb_filter + [moore_control_node],
        force_order=True)

    lumi_sprucing_node = CompositeNode(
        'lumi_sprucing_node',
        combine_logic=NodeLogic.LAZY_AND,
        children=lumi_filter +
        [LHCb__Tests__RunEventCountAlg(name="LumiCounter", ODIN=make_odin())],
        force_order=True)

    spruce_control_node = CompositeNode(
        'spruce_control_node',
        combine_logic=NodeLogic.NONLAZY_OR,
        children=[lumi_sprucing_node] + [physics_sprucing_node],
        force_order=False)

    top_node = spruce_control_node if (
        process == "spruce" or process == "pass") else moore_control_node

    config.update(configure(options, top_node, public_tools=public_tools))
    # TODO pass config to gaudi explicitly when that is supported
    return config


@configurable
def get_allen_hlt1_decision_ids():
    """
    Read Allen HLT1 decision IDs from the Allen control node
    """
    from RecoConf.hlt1_allen import get_allen_line_names

    from AllenConf.persistency import build_decision_ids
    return build_decision_ids(get_allen_line_names())


@configurable
def allen_control_flow(options, write_all_input_leaves=True):
    from RecoConf.hlt1_allen import (
        allen_gaudi_config, call_allen_raw_reports, call_allen_decision_logger,
        make_allen_decision)

    from Allen.config import setup_allen_non_event_data_service
    from AllenConf.persistency import make_dec_reporter, register_decision_ids

    options.finalize()

    ids = get_allen_hlt1_decision_ids()
    encoding_key = register_decision_ids(ids)

    # TODO: remove when full configuration of Allen from TCK is implemented
    make_dec_reporter.global_bind(TCK=encoding_key)

    # Write DecReports raw banks
    hlt1_config = allen_gaudi_config()
    allen_cf = hlt1_config['control_flow_node']
    detectors = allen_detectors(allen_cf)

    non_event_data_node = setup_allen_non_event_data_service(
        bank_types=detectors)

    algs = []
    srw = call_allen_raw_reports()
    algs.extend([srw])
    new_raw_banks = {
        srw.OutputSelView: "HltSelReports",
        srw.OutputDecView: "HltDecReports"
    }

    # register the decision_ids... and get the oid of their mapping
    # hlt1_decision_ids=decision_ids, hlt2_decision_ids={}, spruce_decision_ids={})

    report_writers_node = CompositeNode(
        'report_writers_allen',
        combine_logic=NodeLogic.NONLAZY_OR,
        children=algs,
        force_order=True)

    writer_setup = []
    writers = [report_writers_node]

    if options.output_file:
        # Give stream name 'default', needed for 'RawEventSimpleCombiner'.
        # Will not change output file name
        # stream_writer requires DecReports but for HLT1 all banks are saved
        stream = "default"

        #NECESSARY_RAW_BANKS are always propagated
        detector_raw_banks = []
        for banktype in NECESSARY_RAW_BANKS:
            detector_raw_banks.append(default_raw_banks(banktype))
        for banktype in DETECTOR_RAW_BANK_TYPES:
            detector_raw_banks.append(default_raw_banks(banktype))
        detector_raw_banks = _unique(detector_raw_banks)

        pre_algs, post_algs = stream_writer(
            stream=stream,
            stream_lines=[],
            path=options.output_file,
            output_type=options.output_type,
            process="hlt1",
            propagate_mc=options.simulation and options.input_type == 'ROOT',
            old_hlt_raw_banks=[],
            new_raw_banks=new_raw_banks,
            detector_raw_banks=detector_raw_banks,
            dec_reports=None,
            write_all_input_leaves=write_all_input_leaves)
        writer_setup += pre_algs
        writers += post_algs

    allen_algs = [non_event_data_node, allen_cf]

    # Check if hlt1_config contains gather selections and if so add a decision logger
    gather_selections = hlt1_config.get('gather_selections', None)
    if gather_selections is not None:
        allen_algs.append(call_allen_decision_logger(gather_selections))

    allen_node = CompositeNode(
        'allen_algorithms',
        combine_logic=NodeLogic.NONLAZY_AND,
        children=allen_algs,
        force_order=True)

    allen = CompositeNode(
        'MooreAllen',
        combine_logic=NodeLogic.LAZY_AND,
        children=writer_setup + [allen_node, make_allen_decision()] + writers,
        force_order=True)

    return allen


def run_allen(options):
    """Configure Allen within Mooore.

    Convenience function that sets up an Allen node and sets up services

    Args:
        options (ApplicationOptions): holder of application options
    """
    config = configure_input(options)
    top_cf_node = allen_control_flow(options)

    config.update(configure(options, top_cf_node))
    # TODO pass config to gaudi explicitly when that is supported
    return config


def run_reconstruction(options, make_reconstruction, public_tools=[]):
    """Configure the reconstruction data flow with a simple control flow.

    Convenience function that configures all services and creates a data flow.

    Args:
        options (ApplicationOptions): holder of application options
        make_reconstruction: function returning a single CompositeNode object
        public_tools (list): list of public `Tool` instances to configure

    """

    config = configure_input(options)
    reconstruction = make_reconstruction()
    config.update(
        configure(options, reconstruction.node, public_tools=public_tools))
    # TODO pass config to gaudi explicitly when that is supported
    return config


def RawEventSize_analysis(process, stream, stream_lines, dec_reports,
                          hlt_raw_banks, new_raw_banks):
    """Function to analyse the size of DstData RawBank and full RawEvent.

    Exploits `SelectiveCombineRawBankViewsToRawEvent` for HLT2
    and Sprucing flows. Note that this alg is special, it is used only to
    report the uncompressed sizes of RawBanks and should not write the output,
    hence `SelectiveCombineRawBankViewsToRawEvent` can be used for both HLT2 and Sprucing

    Args:
        process (str): "hlt2" or "spruce".
        stream (str): The name of the output stream.
        persist_types (list): RawBanks to persist from previous pass.
        new_locations (list of DataHandles): Datahandles of new RawBanks to persist created in this pass.

    """
    assert process == 'hlt2' or process == 'spruce', 'Can only run RawEvent size analytics with Hlt2 or Spruce passes.'

    locations = new_raw_banks + hlt_raw_banks

    # list of detector raw banks requested by hlt2 and sprucing lines
    # only hlt2 or sprucing lines can save detector raw banks
    map_lines_det_raw_banks = {}
    line_raw_banks = []
    for line in stream_lines:
        rb_to_persist = []
        if len(line.raw_banks) > 0:
            rb_to_persist = [
                rb for rb in line.raw_banks
                if default_raw_banks(rb) not in locations
            ]

        if len(rb_to_persist) > 0:
            map_lines_det_raw_banks[line.decision_name] = tuple(
                default_raw_banks(rb).location for rb in rb_to_persist)

            line_raw_banks += [default_raw_banks(rb) for rb in rb_to_persist]

    line_raw_banks = _unique(line_raw_banks)

    consolidate_views = SelectiveCombineRawBankViewsToRawEvent(
        name="CombineRawBanks_for_" + stream,
        DecReports=dec_reports,
        MapLinesRawBanks=map_lines_det_raw_banks,
        RequiredRawBanks=locations,
        SelectiveRawBanks=line_raw_banks,
        outputs={'RawEvent': force_location(f"/Event/{stream}/RawEvent")})

    algs = [consolidate_views]

    return algs


def valid_name(name):
    """Return True if name follows the selection line (HLT, Sprucing) name conventions."""
    try:
        return SELECTION_LINE_NAME_PATTERN.match(name) is not None
    except TypeError:
        return False


def _get_param_default(function, name):
    """Return the default value of a function parameter.

    Raises TypeError if ``function`` has no default keyword parameter
    called ``name``.
    """
    try:
        try:
            sig = inspect.signature(function)
            p = sig.parameters[name]
            if p.default == p.empty:
                raise ValueError()  # just forward to raise below
            return p.default
        except AttributeError:  # Python 2 compatibility
            spec = inspect.getargspec(function)
            i = spec.args.index(name)  # ValueError if not found
            return spec.defaults[i - len(
                spec.args)]  # IndexError if not keyword
    except (KeyError, ValueError, IndexError):
        raise TypeError('{!r} has no keyword parameter {!r}'.format(
            function, name))


def add_line_to_registry(registry, name, maker):
    """Add a line maker to a registry, ensuring no name collisions."""
    if name in registry:
        raise ValueError('{} already names an HLT line maker: '
                         '{}'.format(name, registry[name]))
    registry[name] = maker


def register_line_builder(registry):
    """Decorator to register a named HLT line.

    The decorated function must have keyword parameter `name`. Its
    default value is used as the key in `registry`, under which the
    line builder (maker) is registered.

    Usage:

        >>> from PyConf.tonic import configurable
        ...
        >>> all_lines = {}
        >>> @register_line_builder(all_lines)
        ... @configurable
        ... def the_line_definition(name='Hlt2LineName'):
        ...     # ...
        ...     return DecisionLine(name=name, algs=[])  # filled with control flow
        ...
        >>> 'Hlt2LineName' in all_lines
        True

    """

    def wrapper(wrapped):
        name = _get_param_default(wrapped, 'name')
        if not valid_name(name):
            raise ValueError(
                '{!r} is not a valid selection line name!'.format(name))
        add_line_to_registry(registry, name, wrapped)
        # TODO return a wrapped function that checks the return type is DecisionLine
        return wrapped

    return wrapper


def streams_dict_to_lines_list(streams_dict):
    """Function to return flat list of DecisionLines from streams dict"""
    return list(itertools.chain(*streams_dict.values()))
