###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
MooreCache
----------
#]=======================================================================]

# Import the cache creation module
include(LoKiFunctorsCache)

set(cache_deps
    LHCb::LoKiCoreLib
    LHCb::PhysEvent
    Rec::FunctorCoreLib  # this should bring most dependencies indirectly
    Rec::ParticleCombinersLib
    # TODO can we get this list automatically?
    # TODO can we depend only on the headers and not the .so files?
)

if(TARGET ${PROJECT_NAME}_MergeConfDB2)
    list(APPEND cache_deps ${PROJECT_NAME}_MergeConfDB2)
elseif(TARGET MergeConfDB2)
    # backward compatibility
    list(APPEND cache_deps MergeConfDB2)
endif()

# inter-project dependencies for super project build
# FIXME Allen is not strictly needed, however, it seems that
# it's being written and read at the same time and that causes problems.
foreach(project Gaudi LHCb Lbcom Rec Allen)
    foreach(merge_target MergeComponents MergeRootmaps MergeConfdb MergeConfDB2)
        if (TARGET ${project}_${merge_target})
            list(APPEND cache_deps ${project}_${merge_target})
        endif()
    endforeach()
endforeach()

# Disable LoKi-specific hacks in LoKiFunctorsCachePostActionOpts.py
set(LOKI_FUNCTORS_CACHE_POST_ACTION_OPTS)

list(APPEND hlt1_settings hlt1_pp_default)
list(APPEND hlt2_settings
        options/hlt2_pp_thor
        options/hlt2_pp_commissioning
        options/hlt2_pp_thor_without_UT
        options/hlt2_pp_2022_reprocessing
        options/sprucing/spruce_production_2023_1
        options/sprucing/spruce_production_2023_2
        options/sprucing/spruce_production_2022
        )

foreach(name IN LISTS hlt1_settings)
    # note that we don't use DisableLoKiCacheFunctors.py from Rec since it is not installed (with
    # the python package) and is thus hard to get reliably.
    loki_functors_cache(Moore_FunctorCache_Hlt1_${name}
            ${CMAKE_CURRENT_SOURCE_DIR}/options/DisableLoKiCacheFunctors.py
            ${PROJECT_SOURCE_DIR}/Hlt/Moore/tests/options/mdf_input_and_conds.py
            ${CMAKE_CURRENT_SOURCE_DIR}/options/process_zero_events.py
            ${CMAKE_CURRENT_SOURCE_DIR}/options/silence_application_manager.py
            ${CMAKE_CURRENT_SOURCE_DIR}/options/ThOr_create_cache_opts.py
            ${PROJECT_SOURCE_DIR}/Hlt/Hlt1Conf/options/${name}.py
        FACTORIES FunctorFactory
        LINK_LIBRARIES
            Gaudi::GaudiKernel
            Rec::FunctorCoreLib
        DEPENDS ${cache_deps}
        # no reason to split much becasue hlt1_pp_default currently only has O(20) functors
        SPLIT 1
    )
endforeach()

# Limit number of functor cache compilations so that we use at most 3/4 of physical RAM.
# Note that the number of jobs is determined at configuration time so if you configure
# and build on different machines, the setting might be suboptimal.
cmake_host_system_information(RESULT total_ram QUERY TOTAL_PHYSICAL_MEMORY)
set(average_ram_usage 4000)  # approximate RAM usage of one compilation job in MB
math(EXPR max_cache_compile_jobs "${total_ram}*1/2/${average_ram_usage}" OUTPUT_FORMAT DECIMAL)
if(NOT "${max_cache_compile_jobs}" GREATER_EQUAL "1")
    set(max_cache_compile_jobs 1)
endif()
set(MAX_CACHE_COMPILE_JOBS ${max_cache_compile_jobs} CACHE STRING "Size of the functor cache compilation pool")
set_property(GLOBAL APPEND PROPERTY JOB_POOLS functor_cache_compile_pool=${MAX_CACHE_COMPILE_JOBS})

foreach(options_path IN LISTS hlt2_settings)
    # Replace forward slashes with underscores to form a valid CMake identifier
    string(REPLACE "/" "_" name ${options_path})
    set(cache_name Moore_FunctorCache_Hlt2_${name})
    loki_functors_cache(${cache_name}
            ${CMAKE_CURRENT_SOURCE_DIR}/options/DisableLoKiCacheFunctors.py
            ${CMAKE_CURRENT_SOURCE_DIR}/options/ThOr_create_cache_opts.py
            ${PROJECT_SOURCE_DIR}/Hlt/Moore/tests/options/mdf_input_and_conds_hlt2.py
            ${CMAKE_CURRENT_SOURCE_DIR}/options/output_mdf_streams.py
            ${CMAKE_CURRENT_SOURCE_DIR}/options/process_zero_events.py
            ${CMAKE_CURRENT_SOURCE_DIR}/options/silence_application_manager.py
            ${PROJECT_SOURCE_DIR}/Hlt/Hlt2Conf/${options_path}.py
        FACTORIES
            FunctorFactory
        LINK_LIBRARIES
            Gaudi::GaudiKernel
            Rec::DaVinciKernelLib
            Rec::FunctorCoreLib
            Rec::ParticleCombinersLib
        DEPENDS ${cache_deps}
        SPLIT 25
    )
  if(LOKI_BUILD_FUNCTOR_CACHE)
    set_property(TARGET ${cache_name} PROPERTY JOB_POOL_COMPILE functor_cache_compile_pool)
  endif()
endforeach()
