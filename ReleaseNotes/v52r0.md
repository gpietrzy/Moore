2021-03-12 Moore v52r0
===

This version uses
Phys [v32r0](../../../../Phys/-/tags/v32r0),
Allen [v1r2](../../../../Allen/-/tags/v1r2),
Rec [v32r0](../../../../Rec/-/tags/v32r0),
Lbcom [v32r0](../../../../Lbcom/-/tags/v32r0),
LHCb [v52r0](../../../../LHCb/-/tags/v52r0),
Gaudi [v35r2](../../../../Gaudi/-/tags/v35r2) and
LCG [97a](http://lcginfo.cern.ch/release/97a/) with ROOT 6.20.06.

This version is released on `master` branch.
Built relative to Moore [v51r2](/../../tags/v51r2), with the following changes:

### New features ~"new feature"

- ~selection ~hlt2 | Semileptonics triggers, !659 (@masmith)
- ~hlt2 ~Calo | Calo reco config with selective track-cluster match algos from Rec!2216, !632 (@mveghel)
- ~Configuration | Run HLT2 on HLT1 filtered sample, !664 (@dovombru) [#169]
- ~Jets | Update hltjetsrun3, !589 (@rangel)
- ~Composites | Use new functionals for neutrals, !593 (@aalfonso)
- ~Persistency | PersistReco serialisation functionality, !685 (@axu) [#204,#234,#242,gaudi/Gaudi#162,lhcb-dpa/project#15] :star:
- ~Persistency | Packing and persistence of containers for persist reco, !646 (@nskidmor)
- Upstream project highlights :star:
  - ~Configuration | Move PyConf from Moore to LHCb, LHCb!2906 (@rmatev) [Moore#116]
  - ~Tracking | Simple Kalman Filter to fit PrTracks, Rec!2213 (@chasse)
  - ~Composites | Add Functionals: PhotonMaker and MergedPi0Maker, Phys!756 (@aalfonso)
  - ~"MC checking" | Velo cluster monitoring and performance checks, Rec!2337 (@gbassi)


### Fixes ~"bug fix" ~workaround

- ~Configuration | Change imports of `make_pvs` to `RecoConf.reconstruction_objects`, !641 (@sstahl) [#216]
- Upstream project highlights :star:
  - ~RICH ~PID | RICH - Photon pre-selection reoptimisation, Rec!2246 (@jonrob)


### Enhancements ~enhancement

- ~selection ~hlt2 | Onia DiMuonDetached lines with persistreco enabled, !737 (@gcavalle)
- ~selection ~hlt2 | B2CC: Update Bs2JpsieePhi and Bd2JpsieeKst Hlt2 lines, !642 (@vbatozsk)
- ~selection ~hlt2 ~Composites | Add HLT2 particles makers for upstream tracks, !734 (@bkhanji)
- ~hlt2 ~Tracking | Setup analogous sequence to `light_reco` that uses the SimpleFitter, !656 (@chasse)
- ~hlt2 ~Tracking | Use TracksDownstreamConverter, !635 (@ahennequ)
- ~hlt2 ~Tracking | Updates for `light_reco` configuration, !565 (@ausachov)
- ~hlt2 ~RICH ~PID | Update RICH HLT2 configuration to adapt to CK theta resolution changes, !668 (@jonrob)
- ~Configuration | Reference update for Moore!746, !757 (@cgobelbu)
- ~Configuration | Add track type context to tracking instance names, !746 (@jonrob) [Rec#187]
- ~Configuration | PyConf: match IO DataHandle types, !686 (@rmatev)
- ~Configuration | Add option to  ApplicationOptions to skip first N events, !653 (@chasse)
- ~Configuration | Fix Allen test with new lines, !652 (@raaij)
- ~Configuration | Stop PyConf testing truthiness of properties, !643 (@olupton)
- ~Tracking | Enable combined usage of `fast_reco`  and `PrKalmanFilter`, !691 (@chasse)
- ~Calo ~Monitoring | Update calo reco monitoring histograms, !756 (@cmarinbe)
- ~RICH | Updates to RICH reco. configuration needed for n-1 scale factor calibration task, !741 (@jonrob)
- ~Persistency | Changes to accomodate PersistRecoConf and reading move to LHCb, !735 (@nskidmor)
- ~Persistency | Adapted to semi-functional move of (Un)PackCaloHypo, !666 (@sponce)
- ~"MC checking" | Add 2D tracking histograms, !717 (@sklaver)
- ~"MC checking" | IdealStateCreator should use best extrapolation possible for MC-Checking, !699 (@chasse)
- ~"MC checking" | Split Boole links, add VPFullCluster<->MCHit linkers., !687 (@ldufour)
- ~"MC checking" | Added setup to run TrackResChecker on xdigi files with the HLT2 reco, !673 (@ldufour)
- ~Monitoring | Add raw bank sizes to default data monitoring, !760 (@jonrob)
- ~Monitoring | Option file for Allen ouput in DC tests, !743 (@msaur)
- ~Build | Update git ignore file, to match other projects, to ignore centrally managed CMake files, !667 (@jonrob)
- ~Build | Fix building of documentation, !662 (@rmatev)
- Upstream project highlights :star:
  - ~Tracking | LongLivedTracking cleanup, optimisation and adaptation to Pr::Tracks, Rec!2222 (@ahennequ)
  - ~Tracking ~"PV finding" | Flexible size of Velo tracks and hits containers and PV container, Rec!2303 (@peilian)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~selection ~hlt2 | Moving `inclusive_radiative_b` lines into rd/ directory, !727 (@yafan)
- ~hlt2 | Add test for PbPb and pp test without gec, !744 (@sstahl)
- ~hlt2 | Fix semileptonics python 3 (follow up !659), !709 (@masmith) [#235]
- ~hlt2 ~Configuration ~RICH ~Monitoring | Resolve RICH-55, !547 (@asolomin) [RICH-55]
- ~Configuration | Adapt PyConf API to `snake_case` convention, !755 (@erodrigu)
- ~Configuration | Move PyConf from Moore to LHCb, !718 (@rmatev) [#116]
- ~Configuration | Fix Python 3 build error, !681 (@nskidmor)
- ~Configuration | Remove sequence from `make_charged_protos` from protoparticles.py, !678 (@sstahl)
- ~Configuration | Add test for CompositeNode equality, !651 (@apearce) [#208]
- ~Configuration ~Functors | Pass DataHandle objects to functors directly, !738 (@apearce)
- ~Configuration ~Persistency | Refactor line-specific persistence control flow, !716 (@apearce) [#226]
- ~Decoding | Add example for HLT1 decoding test, !658 (@raaij)
- ~Tracking | Update Moore references for Rec!2303, !740 (@lmeyerga)
- ~Tracking | New reference files for Moore!656 based on lhcb-master-mr 1667, !680 (@acasaisv)
- ~Tracking | New reference files for Moore!565 based on lhcb-head 2787, !677 (@acasaisv)
- ~Tracking | Moore ref update for Rec!2213 based on mr-1622, !672 (@msaur)
- ~Tracking | Updating Moore Refs for Rec!2222, !639 (@rcurrie)
- ~Tracking ~UT | Update refs (follow LHCb!2914), !725 (@xuyuan)
- ~Tracking ~"MC checking" | Update refs (follow Rec!2300), !719 (@rmatev)
- ~UT | Add tests for new UT TELL40 data format, !695 (@xuyuan)
- ~Calo | New references for Rec!2287 based on lhcb-head 2808, !696 (@acasaisv)
- ~Calo | New references for Rec!2275 from lhcb-head 2791, !679 (@acasaisv)
- ~Calo | Update and add refs from lhcb-head 2757, !655 (@peilian)
- ~Calo | Update refs from lhcb-master-mr 1501, !654 (@peilian)
- ~RICH | Updating Moore Refs to lhcb-master-mr 1475, !644 (@rcurrie)
- ~RICH ~PID | New references for Moore!668 based on lhcb-master-mr 1679, !688 (@acasaisv)
- ~RICH ~Monitoring | New refs for https://gitlab.cern.ch/lhcb/Moore/-/merge_requests/547 based on lhcb head 2812, !706 (@lpica)
- ~PID | Follow changes to ChargedPIDANN in Rec!2247, !647 (@graven) [#179]
- ~Composites | Update refs according to lhcb-head-2, !649 (@peilian)
- ~Composites | Follow changes in LHCb/LHCb!2797, !638 (@graven)
- ~Monitoring | Update references for Moore!760, !764 (@cgobelbu)
- ~Monitoring | Moore ref update for Moore!597 based on lhcb-master-mr-1633, !674 (@msaur)
- ~Monitoring | Add track and PV monitors when `do_data_monitoring=True`, !597 (@sklaver)
- ~Monitoring ~Accelerators | Velo cluster monitoring - minimal w/ QMTest, !733 (@gbassi)
- ~Build | Fix race condition in `hlt2_reco_baseline_with_mcchecking_MagUp`, !766 (@rmatev)
- ~Build | Make `hlt2_protoparticles_baseline` test more robust, !745 (@rmatev)
- ~Build | Fixes for LCG 99, !739 (@chasse) [LBRTAPLAN-276]
- ~Build | Expand flake8 coverage and fix fallout, !712 (@apearce) [#181]
- ~Build | Add HLT2 MagUp MC Test & remove `default_conds` files to ensure right conditions are set, !660 (@chasse)
- ~Build | Remove empty modules in Hlt2Conf, !648 (@apearce) [#210,#211]
- Update refs for Moore!660 based on lhcb-master-mr-1578, !663 (@msaur)
- Upstream project highlights :star:
  - ~Tracking ~"PV finding" ~VP ~UT ~FT ~PID ~Functors ~"MC checking" ~Monitoring | Remove Velo, TT, IT, OT, SPD, PRS, Rec!2236 (@pkoppenb) [lhcb-dpa/project#18,lhcb-dpa/project#37]
  - ~VP ~UT ~FT ~"Event model" ~Persistency | Removal of Velo, ST, OT, IT, PRS, SPD from master, LHCb!2804 (@pkoppenb)


### Documentation ~Documentation

- Fix small typo, !722 (@gligorov)
- More troubleshooting in documentation, !661 (@sstahl)
- Add guidelines for writing HLT2 lines, !650 (@apearce) [#142,#217]
- Add info box for `reconstruction_objects` module in line tutorial., !640 (@sstahl)
