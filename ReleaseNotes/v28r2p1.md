2018-05-22 Moore v28r2p1
========================================

Production version for 2018
----------------------------------------
Based on Gaudi v29r4, LHCb v44r2p1, Lbcom v22r0p3, Rec v23r2p1, Phys v25r4, Hlt v28r2p1
This version is released on the 2018-patches branch.

- Moore_RateTest rewrite, !80 (@rmatev, @sstahl)
