2020-10-22 Moore v51r2
===

This version uses
Allen [v1r1](../../../../Allen/-/tags/v1r1),
Phys [v31r2](../../../../Phys/-/tags/v31r2),
Rec [v31r2](../../../../Rec/-/tags/v31r2),
Lbcom [v31r2](../../../../Lbcom/-/tags/v31r2),
LHCb [v51r2](../../../../LHCb/-/tags/v51r2),
Gaudi [v34r1](../../../../Gaudi/-/tags/v34r1) and
LCG [97a](http://lcginfo.cern.ch/release/97a/) with ROOT 6.20.06.

This version is released on `master` branch.
Built relative to Moore [v51r1](../-/tags/v51r1), with the following changes:

### New features ~"new feature"

- ~selection ~hlt2 | Inclusive dilepton trigger update, !621 (@mmaterok)
- ~selection ~hlt2 | B2CC: Bs->JpsiPhi/Kstar HLT2 Lines, !620 (@peilian)
- ~selection ~hlt2 | Add D0 to KS KS HLT2 line, !585 (@lpica)
- ~selection ~Composites ~Functors | Use the new ThOr::Combiner<T>, !583 (@nnolte)
- ~Persistency | Persistreco v1, !619 (@nskidmor)
- ~Persistency | Propagate TES locations from input file, !591 (@axu) [#143]
- ~"MC checking" | Adding electron categories to seeding mc checking, !605 (@lohenry)
- ~"MC checking" | Create and persist MC association tables, !601 (@apearce) [#197] :star:
- Upstream project highlights :star:
  - ~selection ~Composites ~Functors | Add a new ThOr::Combiner<T>, Phys!749 (@nnolte)
  - ~Decoding ~RICH | Add support for realistic RICH PMT encoding and decoding, LHCb!2664 (@jonrob)
  - ~Decoding ~Monitoring | Add decoding for SciFi NZS dataformat, LHCb!2648 (@legreeve)
  - ~Tracking | Added specialized similarity transform, LHCb!2763 (@ldufour)
  - ~Composites ~Functors ~"Event model" ~Utilities | Changes supporting the new ThOr::Combiner<T>, LHCb!2668 (@nnolte) [#96,#97]
  - ~"MC checking" | Add ProtoParticle association algorithms, Rec!2172 (@apearce) [Moore#197]


### Fixes ~"bug fix" ~workaround

- ~Calo | Update refs for Rec!2187, !607 (@mzdybal)
- ~"MC checking" | Fix bugs in fromBorD MC checking categories, !622 (@peilian)
- ~Build | Use Allen nightly build in CI, !637 (@apearce)
- Upstream project highlights :star:
  - ~Muon | Cleanup of the MuonMatching code, Rec!2212 (@cprouve)
  - ~Conditions | FTDet: Fix platform diffs due to floating point bug, LHCb!2679 (@sesen)


### Enhancements ~enhancement

- ~hlt2 ~Configuration ~RICH | Adapt to removal of ODIN as input to RICH raw decoder, !586 (@jonrob)
- ~Configuration ~Conditions | Change TES path for IOVLock of DD4hep, !494 (@sponce)
- ~Calo | Follow changes in Rec!2080, !608 (@graven)
- ~Calo ~"MC checking" | Extend calo efficiency test to check calo hypo efficiency, !612 (@cmarinbe)
- Upstream project highlights :star:
  - ~Tracking | Adapt to tracks containers with flexible sizes, Rec!2208 (@peilian)
  - ~Tracking ~"Event model" | Make the sizes of Long/Velo/Upstream track container flexible, LHCb!2733 (@peilian)
  - ~Tracking ~"Event model" | Make the size of Pr::Seeding::Tracks container flexible, LHCb!2702 (@peilian)
  - ~Core ~Conditions ~Utilities ~Build | ARM support, LHCb!2756 (@ahennequ)
  - Add copyright statement to all Allen files (without the license statement)., Allen!433 (@raaij)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~selection ~hlt2 | Move charm lines to Hlt2Conf/lines/charm and fix d0_to_ksks.py line, !596 (@tpajero)
- ~Configuration | Fix python 3 support, !630 (@rmatev)
- ~Configuration | Refactor persistence configuration, !628 (@apearce)
- ~Configuration | Add FetchDataFromFile  now required by LHCb!2750, !610 (@graven)
- ~Configuration | Prepare tests for python 3 compatibility changes, !600 (@rmatev)
- ~Configuration | Python 3 support, !592 (@rmatev) [LHCBPS-1858]
- ~Tracking ~Calo ~"MC checking" | Update references for  Rec!2215, Rec!2210, Rec!2214 and  !622, !624 (@gunther)
- ~Tracking ~Calo ~"MC checking" | Create new ref for LHCb!2672 and Rec!2200, !613 (@vrenaudi)
- ~Tracking ~"Event model" | New ref for LHCb!2702, !611 (@vrenaudi)
- ~Muon ~"MC checking" | Reference Update following Rec!2212, !625 (@gunther)
- ~Calo | Reference updates for Lbcom!505 and !612, !627 (@ascarabo)
- ~Calo | Remove duplicated make_acceptance in calo configuration, !606 (@cmarinbe)
- ~Calo | Increase multi-threaded tolerances for FutureNeutralProtoPAlg, !584 (@rmatev) [#189]
- ~Composites | Update refs for Moore!583, !603 (@mzdybal)
- ~"MC checking" | Reference Update follwing Moore!605, !615 (@vrenaudi)
- ~Conditions | Update tests to latest physics constants in Gaudi, !614 (@clemenci)
- Add skylake symlink, !631 (@ascarabo)
- Adapt to new Gaudi Monitoring, !618 (@sponce)
- Fix race condition between hlt1_reco_baseline_with_mcchecking tests, !616 (@rmatev)
- Upstream project highlights :star:
  - ~Calo ~Functors ~"Event model" | Remove L0 related code, LHCb!2735 (@pkoppenb)
  - ~Monitoring | Remove HCMonitors, Lbcom!503 (@pkoppenb) [LHCBPS-1880]


### Documentation ~Documentation

- Fix rST hyperlink syntax., !636 (@apearce)
- Add documentation how to enable or disable real-time reconstruction, !634 (@sstahl)
- Add advice on HLT2 line rate tuning., !623 (@apearce)
- Fix typos, !617 (@misarpis)
