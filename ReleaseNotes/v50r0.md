2019-10-08 Moore v50r0
========================================

Moore development release: HLT1 demonstrator
----------------------------------------

This version uses Phys v30r6, Rec v30r6, Lbcom v30r6, LHCb v50r6, Gaudi v32r2 and LCG_96b with ROOT 6.18.04

This version is released on branch [master](../tree/master). The previous release on that branch  was [v30r1](../tags/v30r1).

### HLT1

- Add zipping of SOA tracks + PV relations demonstrator, !229 (@olupton)
- Add TrackMVA and TwoTrackMVA implementations, !223 (@olupton)
- Add HLT1 single muon lines, !219 (@sstahl, @mramospe, @rvazquez)
- Add a 2-track line using new combiner/functors, !204 (@olupton)

### HLT2

- Change default track selectors, !212 (@lohenry)
- Add HLT2 test for all lines and particle makers, tidy up D02HH line module, !216 (@apearce)
- Bs->Jpsi Phi Hlt2 trigger line and qmtests, !177 (@egovorko)
- Configure DVAlgorithm locations, !194, !172 (@nnolte, @sstahl)

### Reconstruction and MC checking

- More reconstruction algorithms (tracking), !214 (@mstahl)
- Specify all required data dependencies to PrTrackAssociator, !211 (@apearce)
- Configurations for Hlt1 and Hlt2 track reconstruction, !199, !180 (@sstahl, @mstahl)
- Merge MC checking and vectorized HLT1, !196, !179, !192 (@nnolte, @sstahl, @ahennequ)
- Remove unused PV maker argument from base particle maker, !186 (@nnolte)

### Framework

- Implement global (unscoped) bind, !250 (@rmatev)
- Tonic improvements, !239 (@apearce)
- Refactor application configuration, !233 (@rmatev)
- Add diffopts.py script that diffs Gaudi options, !230 (@rmatev)
- Generate data and control flow graphs by default, !225, !227 (@apearce)
- Support filtered writing of  MDF and DST files, !221 (@apearce)
- Fix ignoreChecksum, !215 (@ahennequ)
- Fix hash dependence on dict order, !208 (@nnolte)
- Make tools stateless, !206 (@nnolte)
- Fix reference to old graph functions, !202 (@nnolte)
- Fix list of inputs in case of datahandles and list of inputs for tools, !201 (@nnolte)
- Make algorithm/tool immutable after instantiation, !200 (@nnolte)
- Fix _is_configurable_alg and _is_configurable_tool, !198 (@nnolte)
- Refactor and document PyConf, !197 (@apearce)
- Use getGaudiType() to get Configurable type, !195 (@nnolte)
- Add dynamic modules for importing wrapped Tools and Algorithms, !187 (@nnolte)
- Allow positional arguments and arguments without defaults in @configurable functions, !185 (@nnolte)
- Add register_public_tool function and use simplified material in HLT2 example, !175 (@nnolte)
- Fix with_lines in hlt1conf, !174 (@nnolte)
- Migrate protoconf to new Moore, !170 (@apearce)

### Infrastructure

- more source files for the functor cache, !241 (@olupton)
- Add Sphinx-based documentation generation, !235, !243, !252 (@apearce)
- Take input files from the TestFileDB, !236 (@apearce)
- Add test profiling the python config, !231 (@rmatev)
- Update test validator now warning source is fixed, !222 (@nnolte)
- Create README and CONTRIBUTING files, !190 (@apearce)
- Check Python and C++ formatting in the CI, !188 (@apearce)
- Remove gaudi_install_scripts, !173 (@nnolte)
- Remove everything to prepare for Upgrade Moore, !171 (@apearce)
