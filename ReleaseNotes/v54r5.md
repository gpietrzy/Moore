2023-03-03 Moore v54r5
===

This version uses
Allen [v3r5](../../../../Allen/-/tags/v3r5),
Rec [v35r5](../../../../Rec/-/tags/v35r5),
Lbcom [v34r5](../../../../Lbcom/-/tags/v34r5),
LHCb [v54r5](../../../../LHCb/-/tags/v54r5),
Detector [v1r9](../../../../Detector/-/tags/v1r9),
Gaudi [v36r11](../../../../Gaudi/-/tags/v36r11) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Moore [v54r4](/../../tags/v54r4), with the following changes:

### New features ~"new feature"

- ~selection ~hlt2 | New QEE HLT2 and Sprucing lines and improvements to existing lines, !1989 (@rjhunter)
- ~selection ~hlt2 | BnoC: new HLT2 and Sprucing lines, !1746 (@pbaladro)
- ~"Event model" | Add relation tables to neutral reconstruction, !2060 (@tnanut)
- Upstream project highlights :star:


### Fixes ~"bug fix" ~workaround

- ~selection | BandQ: fix PID_P cut for detached_protons_tights, !2106 (@ipolyako)
- ~selection | Fix some lines that would trigger fatal errors, !1998 (@mstahl)
- Fix analytics in HLT2, !2022 (@nskidmor)
- Upstream project highlights :star:


### Enhancements ~enhancement

- ~Decoding | Raw bank filter, !2140 (@sesen)
- ~Luminosity | Add routing bits filter to control flow, !2052 (@nskidmor)
- ~Build | Fix documentation jobs in pipeline, !2076 (@rmatev)
- Upstream project highlights :star:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~hlt2 | Remove V0 monitoring lines and adjust prescale of updated Ks lines, !1770 (@gtuci)
- ~Configuration | Only configure direct dependencies, not indirect ones, !2115 (@graven)
- ~Monitoring | Add Monitoring of physics quantities for sprucing, !1991 (@eleckste)
- ~Build | Increase tolerance for some CALO counters, !2096 (@rmatev)
- ~Build | Fix test race conditions, !2089 (@rmatev)
- Remove removal of no longer occuring WARNINGs, !2114 (@graven)
- Add detdesc specific reference log files, !2062 (@jonrob)
- Remove redundant without RICH/CALO/MUON tests, !2043 (@jonrob)
- Suppress dd4hep tests via cmake, !2040 (@jonrob)
- Upstream project highlights :star:


### Documentation ~Documentation

- ~selection ~hlt2 | Automatically generated hlt2 line reference documentation pages, !2097 (@roneil)
- ~Functors | ThOr functor references update, !1996 (@tfulghes)
- Streaming documentation, !2078 (@mstahl)
- Fix doc for running HLT1, !2079 (@alobosal)
- Hlt1EfficiencyChecker Documentation bugfix, !2075 (@mamartin)
- Update docs for profiling with callgrind, !1985 (@rmatev)
### Other

- ~selection | NOPID configuration flags for selection, !2026 (@ipolyako)
- ~selection | Neutral Charm lines (photons and pi0s), !1293 (@shtaneja)
- ~selection | Fix combiner names in hyperon lines, !2032 (@mstahl)
- ~selection ~hlt2 | HLT2 and sprucing lines for B2Xpbarmumu decays, !2028 (@jagoodin) [gitlab.cern.ch/lhcb/Moore/-/merge_requests/1036/diffs#4]
- ~selection ~hlt2 | Add 2 b2cc lines with photons in the final states, !2018 (@yihou)
- ~selection ~hlt2 | Add exclusive hadronic doubly charmed baryon lines, !1508 (@axu)
- ~selection ~hlt2 | Add D0 -> h l nu  HLT2 lines, !1289 (@adavis)
- ~selection ~hlt2 | Add missing radiative inclusive lines, !2012 (@alobosal)
- ~Calo | Add test to compare Allen and Moore calo digits, !2111 (@lmeyerga) [#393]
- ~Functors ~Tuples | Add more functors to access neutral CALO information, !2094 (@jzhuo)
- ~Persistency | Line output control flows per stream, !2046 (@sesen)
- ~Persistency | Line options for persistence of lower level objects associated to candidates, !2023 (@sesen) [#154]
- Update References for: Moore!2026 based on lhcb-master-mr/7152, !2142 (@lhcbsoft)
- Update References for: Allen!1096 based on lhcb-master-mr/7164, !2138 (@lhcbsoft)
- Update References for: LHCb!3963 based on lhcb-master-mr/7187, !2137 (@lhcbsoft)
- Update References for: Moore!2120 based on lhcb-master-mr/7157, !2134 (@lhcbsoft)
- Update References for: LHCb!3894, Rec!3233, Moore!2127 based on lhcb-master-mr/7136, !2131 (@lhcbsoft)
- Removed direct usage of CondDB in tests, !2125 (@sponce)
- Remove qee lines using UT from pp_commissioning_2022, !2120 (@sstahl)
- Rework creation of Upstream protoparticles and add fallback for data without UT, !2081 (@sstahl)
- Brem functors tests, !2127 (@mveghel)
- Dropped unused option files, !2126 (@sponce)
- Update References for: LHCb!3970 based on lhcb-master-mr/7100, !2121 (@lhcbsoft)
- Update References for: Moore!2046 based on lhcb-master-mr/7073, !2118 (@lhcbsoft)
- Fix of hlt2_light_reco_pr_kf_without_UT_with_mcchecking output file name, !2103 (@msaur)
- Add possibility to require deployed key in production, !2085 (@sstahl)
- Update refs for !2060, !2105 (@rmatev)
- Fix issue with !2060, !2100 (@rmatev)
- Update References for: LHCb!3956, Rec!3302, Moore!2060 based on lhcb-master-mr/7044, !2098 (@lhcbsoft)
- Update References for: LHCb!3891, Rec!3192 based on lhcb-master-mr/7020, !2092 (@lhcbsoft)
- Follow LHCb!3954, !2091 (@rmatev)
- Update References for: Rec!3234 based on lhcb-master-mr/7004, !2088 (@lhcbsoft)
- Update References for: Allen!1107 based on lhcb-master-mr/6994, !2083 (@rmatev)
- Make default refs symlinks to detdesc refs, !2082 (@rmatev)
- Update References for: LHCb!3950, MooreOnline!201 based on lhcb-master-mr/7016, !2056 (@lhcbsoft)
- Update References for: LHCb!3943, Moore!2023 based on lhcb-master-mr/7035, !2038 (@lhcbsoft)
- Update v3 detdesc refs from lhcb-master/1924 (follow up !2067), !2077 (@rmatev)
- Update References for: LHCb!3953, Moore!2053, DaVinci!809 based on lhcb-master-mr/6955, !2069 (@lhcbsoft)
- Update References for: Allen!1118, Moore!2055 based on lhcb-master-mr/6950, !2067 (@lhcbsoft)
- Revert "Merge branch 'sponce_detdescRefFiles' into 'master'", !2063 (@sponce)
- Set correctly geometry and conditions versions in some tests, !2059 (@clemenci)
- Use views in Velo and Velo-UT Allen-Gaudi converters, !2055 (@thboettc)
- Make sure there is no collision of name between hito and ntuple root files, !2053 (@sponce)
- Added detdesc specific ref files where dd4hep is failing, !2061 (@sponce)
- Update References for: Rec!3241 based on lhcb-master-mr/6876, !2051 (@lhcbsoft)
- Use views in Allen-Gaudi long track converter, !2042 (@thboettc)
- Update References for: Allen!1106 based on lhcb-master-mr/6836, !2041 (@lhcbsoft)
- Update References for: LHCb!3926, Rec!3248 based on lhcb-master-mr/6820, !2037 (@lhcbsoft)
- Attempt to reconciliate opt and dbg ref files, !2036 (@sponce)
- Add flexibility to functions Moore.production, !2025 (@cburr)
